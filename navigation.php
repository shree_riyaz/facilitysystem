<?php
include_once("lib/language.php");
include_once("lib/class.authorise.php");
include_once("lib/dbfilter.php");
$language = new Language();
$lang = $language->english();
$auth = new Authorise();
$DB = new DBFilter();
$user_id = $_SESSION['user_id'];
$module_name = $auth->getOneAccessibleModule($user_id);
$user_name = $DB->SelectRecord('users', "user_id='$user_id'");
$get_language_list= $DBFilter->RunSelectQuery("select * from language_support where is_file_uploaded='Y' AND is_deleted='N' and is_active='Y'");
$company_id = $_SESSION['company_id'];

$fault_based_result = $DBFilter->RunSelectQuery("select flt.fault_name,flt.fault_id, count(rating) as feedback_count from feedback as fd INNER JOIN faults flt on fd.fault_id = flt.fault_id where fd.is_deleted = 'N' and fd.is_active= 'Y' AND fd.company_id='" .$company_id . "' GROUP BY flt.fault_name,flt.fault_id");
$count_faults = count(current($fault_based_result));
//$count_faults = 12;

$get_company_logo= $DBFilter->RunSelectQuery("select company_logo from company_detail where company_id=" .$company_id );
$company_logo = current(current($get_company_logo))->company_logo;
//        echo '<pre>'; print_r($count_faults); exit;

?>


<?php
if (isset($user_id)) {
    ?>

    <div class="container-fluid page-wrapper">
    <div class="row nomargin">
    <div class="flex-container">
<!--        <div class="col-sm-3 nopadding list-tabs" style="display: none">-->
        <div class="col-sm-3 col-xs-6 nopadding list-tabs">
        <div class="side-nav-close">
            <i class="fa fa-angle-left" aria-hidden="true"></i>
            <div class="logo-padding">
                <?php if(file_exists( "images/company_logo/".$company_logo)){ ?>

                <a href="<?php print CreateURL('index.php'); ?>"><img src="<?php echo IMAGEURL.'company_logo/'.$company_logo; ?>" alt="" class="img-responsive center-block"></a>
            <?php }else{ ?>
                    <a href="<?php print CreateURL('index.php'); ?>"><img src="<?php echo IMAGEURL.'company_logo/no-picture.png'; ?>" alt="" class="img-responsive center-block"></a>

            <?php } ?>
            <?php if($_SESSION['usertype'] == 'super_admin'){ ?>
                <a href="<?php print CreateURL('index.php'); ?>"><img src="<?php echo IMAGEURL.'logo.png'; ?>" alt="" class="img-responsive center-block"></a>

            <?php } ?>


            </div>

            <nav class="navbar navbar-default">
                <div class="navbar-header">
                    <!--<button type="button" class="navbar-toggle" data-toggle="collapse"
                            data-target="#myNavbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>-->
                </div>
                <div class="" id="myNavbar">
                    <ul class="nav tabbing">
                        <?php
                        for ($i = 0; $i < count($module_name[0]); $i++) {
                            $module = $module_name[0][$i]->module_name;

                            if ($module == 'device') {
                                ?>
                                <li <?php if ($_REQUEST['mod'] == $module) { ?> class="" <?php } ?>>
                                    <div class="side-navigation panel-group" id="accordion1" role="tablist"
                                         aria-multiselectable="true">
                                        <div class="panel panel-default">
                                            <div class="panel-heading" role="tab" id="headingTwo">
                                                <h4 class="panel-title">
                                                    <a class="device-link <?php if ($_REQUEST['mod'] == 'device') { ?> <?php } elseif($_REQUEST['mod'] == 'device_locations') { ?> <?php }else{ ?>collapsed<?php } ?>" data-toggle="collapse" data-parent="#accordion1"
                                                       href="#collapseTwo" aria-expanded="false"
                                                       aria-controls="collapseTwo">
                                                        <i class="fa fa-laptop" aria-hidden="true"></i>
                                                        <?php echo $lang[ucfirst($module) . "s"]; ?>
                                                        <span class="nav-span-plus">
                                                            <i class="fa fa-minus" aria-hidden="true"></i>
                                                            <i class="fa fa-plus" aria-hidden="true"></i>

                                                           <!-- <i class="fa fa-<?php if ($_REQUEST['mod'] == 'device' || $_REQUEST['mod'] == 'device_locations' ) { ?>minus <?php } else { ?>plus <?php } ?>"
                                                                                       aria-hidden="true"></i>-->
                                                        </span>
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapseTwo"
                                                 class="panel-collapse <?php if ($_REQUEST['mod'] == 'device' || $_REQUEST['mod'] == 'device_locations') { ?> collapse in <?php } else { ?> collapse <?php } ?>"
                                                 role="tabpanel" aria-labelledby="headingTwo">
                                                <div class="panel-body device-link-body">
                                                    <ul class="list-unstyled nav-list-padding-left">

                                                        <li <?php if ($_GET['mod'] == 'device_locations') { ?> class="active-tab" <?php } ?>
                                                                style="color: #3969ab; border-bottom: none;">
                                                            <a href="<?php print CreateURL('index.php', 'mod=device_locations'); ?>">
                                                                <?php echo $lang['Device Locations'] ?></a>
                                                        </li>

                                                        <li <?php if ($_GET['mod'] == 'device') { ?> class="active-tab" <?php } ?>
                                                                style="color: #3969ab; border-bottom: none;">
                                                            <a href="<?php print CreateURL('index.php', 'mod=device'); ?>">
                                                                <?php echo $lang['Devices'] ?></a>
                                                        </li>


                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>

                            <?php }
                            else if ($module == 'faults') {
                                ?>
                                <li style="display: <?php if ($module == 'faults'){?>none<?php } ?>" <?php if ($_REQUEST['mod'] == $module) { ?> class="active-tab" <?php } ?>>
                                    <a class=""
                                       href="<?php print CreateURL('index.php', 'mod=faults&do=view&id=' . $service->service_id); ?>">
                                        <i class="fa fa-exclamation-triangle"
                                           aria-hidden="true"></i><?php echo $lang['Facility & Faults'] ?></a>
                                </li>
                                <?php
                            }
                            else if ($module == 'reports') { ?>
                                <li>
                                    <div class="side-navigation panel-group" id="accordion2" role="tablist"
                                         aria-multiselectable="true">
                                        <div class="panel panel-default">
                                            <div class="panel-heading" role="tab" id="headingThree">
                                                <h4 class="panel-title">
                                                    <a class="<?php if ($_REQUEST['mod'] == 'reports') { ?> <?php } else { ?>collapsed <?php } ?> report-link" data-toggle="collapse"
                                                       data-parent="#accordion2"
                                                       href="#collapseThree" aria-expanded="false"
                                                       aria-controls="collapseThree">
                                                        <i class="fa fa-pie-chart" aria-hidden="true"></i> <?php echo $lang['Reports']?>
                                                        <span class="nav-span-plus">
                                                            <i class="fa fa-minus " aria-hidden="true"></i>
                                                            <i class="fa fa-plus" aria-hidden="true"></i>

                                                            <!--<i class="fa fa-<?php if ($_REQUEST['mod'] == 'reports') { ?>minus <?php } else { ?>plus <?php } ?>"
                                                                                       aria-hidden="true"></i>-->
                                                        </span>
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapseThree" class="panel-collapse <?php if ($_REQUEST['mod'] == 'reports') { ?> collapse in <?php } else { ?> collapse <?php } ?> "
                                                 role="tabpanel"
                                                 aria-labelledby="headingThree">
                                                <div class="panel-body report-link-body">
                                                    <ul class="list-unstyled nav-list-padding-left">
                                            <li <?php if ($_GET['do'] == 'interval') { ?> class="active-tab" <?php } ?>
                                                                style="color: #3969ab; border-bottom: none;">
                                    <a href="<?php print CreateURL('index.php', 'mod=reports&do=interval'); ?>"><?php echo $lang['Interval Wise Report']?></a>
                                                        </li>
                                                        <li <?php if ($_GET['do'] == 'weekly_interval') { ?> class="active-tab" <?php } ?>
                                                                style="color: #3969ab; border-bottom: none;">
                                                            <a href="<?php print CreateURL('index.php', 'mod=reports&do=weekly_interval'); ?>"><?php echo $lang['Weekly Wise Report']?></a>
                                                        </li>

                        <li <?php if ($_GET['do'] == 'location_wise_report') { ?> class="active-tab" <?php } ?>
                                style="color: #3969ab; border-bottom: none;">
            <a href="<?php print CreateURL('index.php', 'mod=reports&do=location_wise_report'); ?>"><?php echo $lang['Location Wise Report']?></a></li>
                    <?php if($count_faults > 0) { ?>

                        <li <?php if ($_GET['do'] == 'fault_wise_report') { ?> class="active-tab" <?php } ?>
                                style="color: #3969ab; border-bottom: none;">
                            <a href="<?php print CreateURL('index.php', 'mod=reports&do=fault_wise_report'); ?>"><?php echo $lang['Fault Wise Report']?></a>
                        </li>
                        <?php } ?>
                        <?php if (is_assign_users() == 'Y'){ ?>
                        <li style="color: #3969ab; border-bottom: none;" <?php if ($_GET['do'] == 'staff_wise_report') { ?> class="active-tab" <?php } ?> >
                            <a href="<?php print CreateURL('index.php', 'mod=reports&do=staff_wise_report'); ?>"><?php echo $lang['Staff Wise Report']?></a>
                        </li>
                        <?php } ?>
                        <li <?php if ($_GET['do'] == 'month_wise_report') { ?> class="active-tab" <?php } ?>
                                style="color: #3969ab; border-bottom: none;">
                            <a href="<?php print CreateURL('index.php', 'mod=reports&do=month_wise_report'); ?>"><?php echo $lang['Month on Month Report']?></a>
                        </li>
                        <li <?php if ($_GET['do'] == 'genuine_wise_report') { ?> class="active-tab" <?php } ?>
                                style="color: #3969ab; border-bottom: none;">
                <a href="<?php print CreateURL('index.php', 'mod=reports&do=genuine_wise_report'); ?>"><?php echo $lang['Genuine Feedback Report']?> </a>
                        </li>

                        <li <?php if ($_GET['do'] == 'daily_wise_report') { ?> class="active-tab" <?php } ?>
                                                                style="color: #3969ab; border-bottom: none;">
                                <a href="<?php print CreateURL('index.php', 'mod=reports&do=daily_wise_report'); ?>"><?php echo $lang['Daily Feedback Report']?> </a>
                                                        </li>
                                                        <li <?php if ($_GET['do'] == 'rating_wise_report') { ?> class="active-tab" <?php } ?>
                                                                style="color: #3969ab; border-bottom: none;">
                                                    <a href="<?php print CreateURL('index.php', 'mod=reports&do=rating_wise_report'); ?>"> <?php echo $lang['Rating Wise Report']?></a>
                                                        </li>
                                                        <!--<li <?php /*if ($_GET['do'] == 'rating_nature_wise_report') { */?> class="active-tab" <?php /*} */?>
                                                                style="color: #3969ab; border-bottom: none;">
                                                            <a href="<?php /*print CreateURL('index.php', 'mod=reports&do=rating_nature_wise_report'); */?>"> <?php /*echo $lang['Feedback Nature Wise Report']*/?></a>
                                                        </li>-->
                                                        <li <?php if ($_GET['do'] == 'question_wise_report') { ?> class="active-tab" <?php } ?>
                                                                style="color: #3969ab; border-bottom: none;">
                                                            <a href="<?php print CreateURL('index.php', 'mod=reports&do=question_wise_report'); ?>"> Question Wise report</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            <?php }
                            else if ($module == 'role')//Added By : Neha Pareek, Dated : 30 Nov 2015
                            {
                                if (($_SESSION['usertype']) == 'super_admin') {
                                    ?>
                                    <li <?php if ($_REQUEST['mod'] == $module) { ?> class="active-tab" <?php } ?>>
                                        <a href="<?php print CreateURL('index.php', 'mod=role'); ?>">
                                            <i class="fa fa-user-plus" aria-hidden="true"></i>
                                            <?php echo $lang['Roles'] ?>
                                        </a>
                                    </li>
                                    <?php
                                } else {
                                    continue;
                                }
                            }
                            else if ($module != 'device_locations') {
                                ?>
                                <li style="display: <?php if (($module == 'user' && $_SESSION['usertype'] == 'company_admin' )||  $module == 'rating' ){ ?>none <?php } ?> " <?php if ($_REQUEST['mod'] == $module) { ?> class="active-tab" <?php } ?>>
                                    <a class="li-opton-nav"
                                       href="<?php print CreateURL('index.php', 'mod=' . $module); ?>">
                                        <i class="<?php if ($module == 'user'){ ?>fa fa-user-circle-o<?php }elseif($module == 'company'){ ?> fa fa-th-large  <?php } elseif($module == 'feedback'){ ?> fa fa-comments-o  <?php } elseif($module == 'plan'){ ?>fa fa-tasks<?php }elseif($module == 'language'){ ?>fa fa-language<?php }elseif($module == 'setting'){ ?>fa fa-gear<?php }elseif($module == 'question'){ ?>fa fa-question-circle-o<?php }elseif($module == 'rating'){?> fa fa-star-o <?php } ?>" aria-hidden="true"></i>
                                        <?php
                                        if (($_SESSION['usertype']) == 'super_admin') {
                                            ###### convert singular to plural : For Module Names in Navigation (added by : Neha Pareek) ####
                                            if (substr($module, -1) != 'y') {
                                                echo $lang[ucfirst($module) . "s"];
                                            } else {
                                                echo $lang[ucfirst(str_replace(substr($module, -1), 'ies', $module))];
                                            }
                                        } else {

                                            if ($module != 'company') {
                                                echo $lang[ucfirst($module) . "s"];
                                            } else {
                                                echo $lang[ucfirst('Dashboard')];
                                                //echo str_replace(substr($module, -1), 'ies', $module);
                                                //ucfirst($module)."ies";
                                            }
                                        }
                                        ?>
                                    </a>
                                </li>
                            <?php }

                        } ?>
                    </ul>
                </div>
            </nav>
        </div>
    </div>


    <?php
} else {
    Redirect(CreateURL('index.php'));

}

?>