<?php
class passEncDec
{
        //var $this->pswKey;
		function passEncDecs($passwordKey)
		{
		    $this->pswKey = $passwordKey;
		}
		
		// function to get the encrypted user password-
		function encrypt_password($paswd)
		{
		  $mykey=$this->getEncryptKey();
		  $encryptedPassword=$this->encryptPaswd($paswd,$mykey);
		  return $encryptedPassword;
		}
		 
		// function to get the decrypted user password
		function decrypt_password($paswd)
		{ //echo $paswd;
		  $mykey=$this->getEncryptKey();
		  $decryptedPassword=$this->decryptPaswd($paswd,$mykey);
		  return $decryptedPassword;
		}
		 
		function getEncryptKey()
		{
		  //return base64_encode('mykeyhere');
		  if(trim($this->pswKey) != '')
			return base64_encode($this->pswKey);
		  else
			return base64_encode('PasswordSaltSessionORPdocMan'); 		  
		}
		
		function encryptPaswd($string, $key)
		{
		  $result = '';
		  for($i=0; $i<strlen ($string); $i++)
		  {
			$char = substr($string, $i, 1);
			$keychar = substr($key, ($i % strlen($key))-1, 1);
			$char = chr(ord($char)+ord($keychar));
			$result.=$char;
		  }
			return base64_encode($result);
		}
		 
		function decryptPaswd($string, $key)
		{
		  $result = '';
		  $string = base64_decode($string);
		  for($i=0; $i<strlen($string); $i++)
		  {
			$char = substr($string, $i, 1);
			$keychar = substr($key, ($i % strlen($key))-1, 1);
			$char = chr(ord($char)-ord($keychar));
			$result.=$char;
		  }
		 
			return $result;
		}

 }
/*$paswd = '456eventadmin000'; 
$obj = new passEncDec;
$ENCpaswd = $obj->encrypt_password($paswd);
echo '<br>ENC: ';print_r($ENCpaswd);
$DECpaswd = $obj->decrypt_password($ENCpaswd);
echo '<br>DEC: ';print_r($DECpaswd); */
?>





