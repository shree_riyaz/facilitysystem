<?php
/*Translation file for project
File Created by Jaishree Sahal*/
defined("ACCESS") or die("Access Restricted");

class Language
{
    function english()
    {

        $DBFilter = new DBFilter();
        $locale = '';
        if (isset($_POST['select_locale'])) {

            $locale = $_POST['select_locale'];
            $_SESSION['selected_language'] = $locale;

        } else {
            $locale = 'en';
        }
//        echo '<pre>'; print_r(LANGUAGEURL."$locale.json"); exit;


//	        $locale = $_POST['select_locale'];
        $locale_session = $_SESSION['selected_language'];


        if (isset($locale_session) && !empty($locale_session)) {
            $data = file_get_contents(LANGUAGEURL . "$locale_session.json");
            $lang_arr = json_decode($data, true);
            return $lang_arr;

        } else {

            $data = file_get_contents(LANGUAGEURL . "en.json");
            $lang_arr = json_decode($data, true);
            return $lang_arr;
        }
        $get_language = $DBFilter->RunSelectQuery("select short_code from language_support");
        $get_lang_all_array = get_language_short_code($get_language[0]);
        $get_key = array_search($locale, $get_lang_all_array);

//            echo $_SESSION['selected_language'];
//            exit;
//	        $language_array =

//        }

//			$lang = 'eng';
//			if($lang=='eng')  /*IF selected language is English language*/
//			{
//				$nl_array = array(
//
//				'Cleaner Feedback System'		=> 	'Cleaner Feedback System',
//				'Profile'						=> 	'Profile',
//				'Settings'						=> 	'Settings',
//				'Select All'					=>	'Select All',
//				'Unselect All'					=>	'Unselect All',
//				'CFS'							=>	'CFS',
//				'Login'							=>	'Login',
//				'Name'							=>  'Name',
//				'User Email'					=>  'User Email',
//				'Password'						=>	'Password',
//				'Reset'							=>	'Reset',
//				'Company'						=>	'Company',
//				'Logout'						=>	'Logout',
//				'Facility & Services' 			=>	'Facility & Services',
//				'Facility & Faults' 			=>	'Facility & Faults',
//				'Language' 			            =>	'Language',
//				'Device Locations'				=>	'Device Locations',
//				'Role'				    		=>	'Role',
//				'Roles'				    		=>	'Roles',
//				'User'							=>	'User',
//				'Devices'						=>	'Devices',
//				'Faults'						=>	'Faults',
//				'Feedback'						=>	'Feedback',
//				'S.No.'							=>  'S.No.',
//				'Search'						=>	'Search',
//				'Edit / Delete'					=>  'Edit / Delete',
//				'Edit'							=>  'Edit',
//				'All fields are mandatory'		=> 	'All fields are mandatory',
//				'Please Select'					=>	'Please Select',
//				'Add'							=>  'Add',
//				'Back'							=>  'Back',
//				'Update'						=>  'Update',
//				'Assigned By'					=> 'Assigned By',
//				'Contact Person'					=> 'Contact Person',
//				/*Facility & Services module*/
//				'Search Facility & Services'	=>	'Search Facility & Services',
//				'Add New Facility/Services'		=>	'Add New Facility/Services',
//				'Facility/Services Name'		=>	'Facility/Services Name' ,
//				'Description'					=>	'Description',
//				'Related To'					=>	'Related To',
//				'Image'							=>	'Image',
//				'Faults'						=>	'Faults',
//				'Edit Facility/Services'		=>	'Edit Facility/Services',
//				'Fault'							=>  'Fault',
//				'View Faults'					=>	'View Faults',
//				'Add & View Faults'				=>	'Add & View Faults',
//				'Add New Faults'				=>	'Add New Faults',
//				'Remove'						=>	'Remove',
//				'Fault Name'					=>	'Fault Name',
//				'Fault Image'					=>	'Fault Image',
//				'Remove / Add'					=>	'Remove / Add',
//
//                /*Language module*/
//                'Add New Language'				=>	'Add New Language',
//                'Add Language'				    =>	'Add Language',
//                'Languages'				        =>	'Languages',
//                'Search By Language Name'		=>	'Search By Language Name',
//                'Language Name'				    =>	'Language Name',
//                'Short Code'				    =>	'Short Code',
//                'Select Language'			    =>	'Select Language',
//
//				/* Device Module */
//				'Search Device'					=>	'Search Device',
//				'Add New Device'				=>	'Add New Device',
//				'Edit Device'				    =>	'Edit Device',
//				'Device Name'					=>	'Device Name',
//				'Device Location'				=>	'Device Location',
//				'Device Owner'					=>	'Device Owner',
//				'Device Description'			=>	'Device Description',
//				/* Company Module */
//				'Company Logo'					=> 	'Company Logo',
//				'Search Company'             	=>	'Search Company',
//				'Add New Company'               =>	'Add New Company',
//				'Company Name'                  =>	'Company Name',
//				'Company Status'                =>	'Company Status',
//				'Subscription Plan'             =>	'Subscription Plan',
//				'Administrator'					=>	'Administrator',
//				'Email Id'                    	=>	'Email Id',
//				'User'                 			=>	'User',
//				'Expiry Date'                 	=>	'Expiry Date',
//				'Users'							=>	'Users',
//				'Creation Date'					=>  'Creation Date',
//				'Subscription Plan'             =>	'Subscription Plan',
//				'User Name'               		=>	'User Name',
//				'Edit Company'                  =>	'Edit Company',
//				'*Enter password if you want to change password.' =>	'*Enter password if you want to change password.',
//				'Password'                 		=>	'Password',
//				'Confirm Password'              =>	'Confirm Password',
//				'Name'							=>	'Name',
//				'Designation'					=>	'Designation',
//				'Cleaner'						=>	'Cleaner',
//				'Superviser'					=>	'Superviser',
//				'Company Admin'					=>	'Company Admin',
//				'Devices'						=>	'Devices',
//				/* User Module */
//				'Search User'					=>	'Search User',
//				'Phone Number'					=>	'Phone Number',
//				'Role'							=>	'Role',
//				'Last Name'						=>	'Last Name',
//				'First Name'					=>	'First Name',
//				'Add New User'					=>	'Add New User',
//				'Add User'						=>	'Add User',
//				'Upload Profile Picture'		=>	'Upload Profile Picture',
//				'Update User'					=>	'Update User',
//				'Active'						=>	'Active',
//				'In-Active'						=>	'In-Active',
//				/* Feedback module */
//
//				'Search Feedback'				=>	'Search Feedback',
//				'Assigned To'					=>	'Assigned To',
//				'Status'						=>	'Status',
//				'Company Name'			    	=>	'Company Name',
//				'Device Status'					=>	'Device Status',
//				'Rating'						=>	'Rating',
//				'Feedback'						=>	'Feedback',
//				'Device'						=>	'Device',
//				'Add New Feedback'				=>	'Add New Feedback',
//				'Date'							=>	'Date',
//				'Update Feedback'				=>	'Update Feedback',
//				'Modified Date'					=>	'Modified Date',
//				'Add Feedback'					=> 'Add Feedback',
//				/*Device_Location Module */
//				'Search Device Location'		=>	'Search Device Location',
//				'Reset'							=>	'Reset',
//				'Search'						=>	'Search',
//				'Add New Device Location'		=>	'Add New Device Location',
//				'S.No.'							=>	'S.No.',
//				'Location Name'					=>	'Location Name',
//				'Add New Location'				=>	'Add New Location',
//				'Location Name'					=>	'Location Name',
//				'Add'							=>	'Add',
//				'Back'							=>	'Back',
//				'Edit Device Location'			=>	'Edit Device Location',
//				'Location Name'					=>	'Location Name',
//				'Update'						=>	'Update',
//				'Edit Device Location'			=>	'Edit Device Location',
//
//				'Location Description'			=> 'Location Description',
//				/* Fault Module */
//				'Add Faults'					=>	'Add Faults',
//				'Service Name'					=> 	'Service Name',
//				'Service Description'			=>	'Service Description',
//				'Search Fault'					=>	'Search Fault',
//				'Add New Fault'					=>	'Add New Fault',
//				'Fault Name'					=>	'Fault Name',
//
//				/* Role Module  */
//
//				'Role Name'						=>	'Role Name',
//				'Search Role'					=>	'Search Role',
//				'Manage Role'					=>	'Manage Role',
//				'Add Role'						=>	'Add Role',
//				'Assign Role'					=>	'Assign Role',
//				'Admin Users'					=>	'Admin Users',
//				'Module Access'					=>	'Module Access',
//				'Select All'					=>	'Select All',
//				'Unselect All'					=>	'Unselect All',
//				'User Name'						=>	'User Name',
//				'From'							=>	'From',
//				'To'							=>	'To',
//				/* Plan Module */
//				'Plan Name'						=>	'Plan Name',
//				'Plan Description'				=>	'Plan Description',
//				'No Of Managers' 				=>  'No Of Managers',
//				'No Of Supervisors' 			=>  'No Of Supervisors',
//				'No Of Officers' 				=>  'No Of Officers',
//				'No Of Cleaners' 				=>  'No Of Cleaners',
//				'Payment Option'				=>  'Payment Option',
//				'Search Plan'					=>	'Search Plan',
//				'Add New Plan' 					=>  'Add New Plan',
//				'Price'							=>  'Price',
//				'Stickernr'	=>'Sticker Number',
//				'Name Employee' => 'Name Of Employee',
//				'Type Of Work'	=>	'Type Of Work',
//				'Plan Image' => 'Plan Image',
//				'Recording Image' =>'Recording Image',
//				'Used In'=> 'Ticket','Detail Of Work Order'=>'Detail Of Work Order',
//				'Detail Of Used Material In Project'=>'Detail Of Used Material In Project',
//				'Daily Entry Details'=>'Daily Entry Details',
//                'Working Hours'=>'Working Hours',
//                'Reason'=>'Reason',
//                'Waiting Hours'=>'Waiting Hours',
//                'Waiting Hours Reason'=>'Waiting Hours Reason',
//                'Parking Fees'=>'Parking Fees',
//                'Created By'=>'Created By',
//                'Date'=>'Date',
//                'Details of Ticket'=>'Details Of Ticket',
//				);
//				$lang_arr = $nl_array;
//				return $lang_arr;
//			}

    }
}



?>
