<?php

/*=======================================================================
	@Author		Rahul Gahlot
	@Date   	16 Aug, 2014
	@Company	Sunarc Technologies
//======================================================================= */
defined("ACCESS") or die("Access Restricted");

include_once("dbclass.php");
class DBFilter
{
/*
	@para
	@$table : String( Name of table)
	@$data  : Array of data that needed to be enter in database
	@Description : this function is used to take data as formof array and genrates a insert query.

*/
	
	function __construct()
	{	$host=HOST;
		$user=USER;
		$password=PASSWORD;
		$database=DATABASE;
		$this->conn= new PDO('mysql:host='.$host.';dbname='.$database.'', ''.$user.'', ''.$password.'');
		//$qr = 'select*from compnay';
		//   'connected to db'."<br/>";
		//$result = $this->conn->query($qr);
		//$data=$result->fetch(PDO::FETCH_ASSOC);
	}	
	
function InsertRecord($table,$data)
{ 
	$DB= new DBConnect();
	 if($table=='')
		{
			return false;
		}	
	else
	{	//print_r($data);
		if($data=='')
		{
			return false;
		}
		else
		{
		$host=HOST;
		$user=USER;
		$password=PASSWORD;
		$database=DATABASE;
		$conn =  new PDO('mysql:host='.$host.';dbname='.$database.'', ''.$user.'', ''.$password.'');
			//$conn = new PDO("mysql:host=$dbhost;dbname=$dbname",$dbuser,$dbpass);
			//$sql = "SELECT title FROM books ORDER BY title";
			//$query = $conn->query($sql);
						
			//$this->stmt = $this->conn->query($query);
			//echo  $table;
		 $sql = "SHOW COLUMNS FROM ".$table;
		   	$resource = $conn->query($sql);
			 if($resource)
			{ 	
				$fields="";
				$values="";
				
			 $resource->setFetchMode(PDO::FETCH_BOTH);
			//while($result = $query->fetch()){
				//$data= $resource->fetch(PDO::FETCH_BOTH);
				while($row = $resource->fetch())
				{	 
					if(array_key_exists($row['Field'],$data))
					{  
						 $fields.=$row['Field'].",";
						if(is_array($data[$row['Field']]))
						{
							
						   //$data[$row['Field']]=array_values(array_filter($data[$row['Field']]));
							// ($data[$row['Field']]);
							$data[$row['Field']]=implode(",",$data[$row['Field']]);
						}
						$values.="'".$data[$row['Field']]."',";
					}
				}

				if($fields)
				{
					$fields=substr($fields,0,strlen($fields)-1);
					$values=substr($values,0,strlen($values)-1);					
				  	$query="Insert into ".$table."(".$fields.") values(".$values.")";
//				echo $query;
//				exit;
					if($id =$DB->SelectId($query))
					{
						/*if($id=mysql_insert_id())
						return $id;
						else*/
						return $id;
					} 
					return false;
				}
				
			}
			
			
		}
	}
	return false;			
}

function InsertedRelatedRecord($table, $data)
{
	$DB= new DBConnect();
	if($table == '')
	{
		return false;
	}
	else
	{
		if($data == '')
		{
			return false;
		}
		else
		{
			//$resource=mysql_query("SHOW COLUMNS FROM".$table);
			$sql = "SHOW COLUMNS FROM".$table;
				$resource = $conn->query($sql);
			 if($resource)
			{
				
				
				$fields="";
				$values="";
				
				$resource->setFetchMode(PDO::FETCH_ASSOC);    
				while($row = $resource->fetch())
				{	 
					
					if(array_key_exists($row['Field'],$data))
					{  
						 $fields.=$row['Field'].",";
						
						 
						
						$values.="'".$data[$row['Field']]."',";
					}
				}
				
				if($fields)
				{	
					$fields=substr($fields,0,strlen($fields)-1);
					$values=substr($values,0,strlen($values)-1);					
				  	$query="Insert into ".PREFIX.$table."(".$fields.") values(".$values.")";
			
			
					if($DB->Select($query))
					{
						if($id=$this->conn->lastInsertId())
						return $id;
						else
						return true;
					}
					return false;
				}
				
			}
			
		}
		
	}
	return false;
}

/*
	@para
	@$table : String( Name of table)
	@$data  : Array of data that needed tobe enter in database
	@$cond  : it will be an array. 'or' or 'and' will also be part of array as
			  element like array{ 'name'=> Neha, 'or' => 'or', 'country' => 'India'  }
	@Description : this function is used to take data as form of array and generates a update query.

*/



function UpdateRecord($table,$data,$cond='')
{
//    print_r($data);
	$DB= new DBConnect();
	if($table=='')
	{  
		return false;
	}	
	else
	{
		
		// if($data=='')
		// {  
			// echo $table.$cond;
// print_r($data);
// exit;
			// return false;
		// }
		// else
		// {
		
			$resource= $DB->Select("SHOW COLUMNS FROM ".$table);
			
			if($resource)
			{

				$sql="";
				$resource->setFetchMode(PDO::FETCH_ASSOC);    
				
				while($row = $resource->fetch())
				{
						
					if(array_key_exists($row['Field'],$data))
					{
							
						if(is_array($data[$row['Field']]))
						 {
						     $data[$row['Field']]=array_values(array_filter($data[$row['Field']]));
							 $data[$row['Field']]=implode(",",$data[$row['Field']]);
						  } 
					 $sql.=$row['Field']."='".$data[$row['Field']]."',";
					
					}
				}
				
				if($sql)
				{
					
					$sql=substr($sql,0,strlen($sql)-1);
										
					$query="update ".PREFIX.$table." set ".$sql;
					
					if($cond)
					{
						if(!is_array($cond))
						{
							$query.=" where ".$cond;
						}
						else
						{
								$count=count($cond);
								if($count)
								{
									$fields=@array_keys($cond);
									$query.=" where ";
									for($counter=0;$counter<$count;$counter++)
									{
										
										if($fields[$counter]=='or' || $fields[$counter]=='and')
										{
												 $query.=" ".$fields[$counter]." "; 
										}
										else
										{
										
												 $query.=" ".$fields[$counter]."='".$cond[$fields[$counter]]."'"; 
										}		
									}
								}
							}		
					}

//				echo  $query.'<br/>'; exit;
			
					if($DB->Update($query))
					{
						return true;
					}
					
					return false;
				}
				
			}
			
		//}
	}
	return false;			
}


/*
	@para
	@$table : String( Name of table)
	@$cond  : condtion to delte reocrd
	@Description : this function is used to delte record

*/
function DeleteRecord($table,$cond='')
{
 
 

	$DB= new DBConnect();
	if($table)
	{
		$query="delete from ".$table;
		if($cond)
		{
			 $query.=" where ".$cond;
		}
		/* echo $query.'<br/>'; 
		 exit;*/
		if($DB->Delete($query))
		{
			return true;
		}
	}
	return false;
}


/*
	@para
	@$table : String( Name of table)
	@$cond  : condtion to select recorde reocrd
	@$fields : name of fields data are needed from query
	@backqury : It wil be include in end of query like order by statement
	
	@Description : this function is used to select multiple records

*/
function SelectRecords($table,$cond='',$fields='*',$backquery='')
{
	global $frmdata;
	$DB=new DBConnect();
	
	if($table)
	{
		if($fields)
		{		
				if(is_array($fields))
				{
					$fields=implode(",",$fields);
				}
				
				$query="select ".$fields." from ".$table." ";

            if($cond)
				{
					 $query.="where ".$cond." ";
				}
				
				if($backquery)
				{
					    $query.=$backquery;
				}
				//echo  $query.'<br/>'; //exit;
//            echo '<pre>'; print_r($query); exit;

            //$resource = $conn->query($sql);
				
				if($result=$DB->Select($query))
				{
					if($result->rowCount() > 0)
					{
						$arr=array();
				  		while($row= $result->fetchAll(PDO::FETCH_OBJ)) 
				  		{	
				         $arr[]=$row;
					    }
						return $arr;
					}		
				 	 
				}
				
		}
	}
	return false;
}


/*
	@para
	@$table : String( Name of table)
	@$cond  : condtion to select recorde reocrd
	@$fields : name of fields data are needed from query
	@backqury : It wil be include in end of query like order by statement
	@totalcount : to count of records
	@Description : this function is used to select multiple records with pagination work
			
*/
function  SelectRecordsWithPagination($table,$cond='',$fields='*',$backquery='',&$totalCount)
{
	global $frmdata;
	$DB=new DBConnect();
	if($table)
	{
		if($fields)
		{		
				if(is_array($fields))
				{
					$fields=implode(",",$fields);
				}
				
				$query="select ".$fields." from ".$table." ";
				
				if($cond)
				{
					 $query.="where ".$cond." ";
				}
				
				if($backquery)
				{
					    $query.=$backquery;
				}
		
		
				  //echo $query;exit;
				if($result=$DB->Select($query))
				{
					$totalCount= $result->rowCount();
					if( $totalCount > 0)
					{
						
						if($frmdata['from'] && $frmdata['from']<$totalCount)
						//mysql_data_seek($result,$frmdata['from']);
						$result->fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_ABS,$frmdata['from']);
						$countRows=1;
						
						$arr=array();
						$result->setFetchMode(PDO::FETCH_OBJ);     
				  		while($row=$result->fetch()) 
				  		{	
				         	
							if($countRows <= $frmdata['to'])
						 		$arr[]=$row;
							$countRows++;
							
					    }
						return $arr[0];
					}		
				 	 
				}
				
		}
	}
	return false;
}
/*
	@para
	@$table : String( Name of table)
	@$cond  : condtion to select recorde reocrd
	@$fields : name of fields data are needed from query
	
	
	@Description : this function is used to select single record

*/
function SelectRecord($table,$cond='',$fields='*')
{
	global $frmdata;
	$DB=new DBConnect();
	if($table)
	{ 
		if($fields)
		{		
				if(is_array($fields))
				{
					 $fields=implode(",",$fields);
				}
				
				 $query="select ".$fields." from ".$table." ";
				if($cond)
				{
					 $query.=" where ".$cond;
				}
				
				$result=$DB->Select($query);
				
				if($result=$DB->Select($query))
				{
					if($result->rowCount() > 0)
					{
						$result->setFetchMode(PDO::FETCH_OBJ)  ;
				  		$row= $result->fetchAll(); 
				  		
						return $row[0];
						
					}		
				 	 
				}
			//echo $query.'<br/>';//exit;
		}
	}
	return false;
}

/*
	@para
	query : query statement
	@Description : this function is used to run join or other complex quries

*/
function RunSelectQuery($query)
{
	//echo $query; //exit;
	$DB=new DBConnect;
	if($result=$DB->Select($query))
	{  
		//print_r($result);exit;
		if($result->rowCount() > 0)
		{
			$arr=array();
			while($row= $result->fetchAll(PDO::FETCH_OBJ)) 
			{	
			 $arr[]=$row;
			}
			//echo "<pre>";print_r($arr); exit;
			return $arr;
		}		
		
	}
	return false;
}



 
 

function RunSelectQueryWithPagination($query,&$totalCount)
{
	global $frmdata;
	$DB=new DBConnect;
	//echo $query;
//exit;
	if($result=$DB->Select($query))
	{	
			
		 $totalCount = $result->rowCount();
		if($totalCount > 0)
		{
			
				
				$res = $DB->conn->prepare($query);
				$res->execute();
				
				$countRows=1;
				
				$arr=array();
				
				$row=$res->fetchAll(PDO::FETCH_OBJ);
				
				$totalCount = count($row);
				foreach($row as $key => $value)
				{
					if($key >= $frmdata['from'])
					{
						$arry[]=$value;
					}
				}
				
			
				for($i=0; $i<count($arry) ;$i++)
				{
					if(!isset($frmdata['record']) || (isset($frmdata['record']) && $frmdata['record']!='All') )
					{
							if($countRows <= $frmdata['to'])
							{
									$arr[]=$arry[$i];
							} 
					}
					else
					{
							$arr[]=$arry[$i];
					}	
					$countRows++;
				
				}
			//echo count($arr); exit;
			//echo "<pre>"; print_r($arr); exit;
			//echo $countRows;
			return $arr;
		}		
				 	 
	}
	
	return false;
}
function RunSelectQuerySigle($query)
{
	
	$DB=new DBConnect;
	if($result=$DB->Select($query))
	{
			$result-> setFetchMode(PDO::FETCH_OBJ);
		$row=$result->fetch();
		if($row)
			return $row;
	}
	return false;
} 


/*
	@para
	@$table : String( Name of table)
	@$value  : single value
	@$cond  : condition for updation
	@Description : this function is used to take data as formof array and gerates a update status query.

*/
function UpdateStatus($table,$value,$cond,$field='statusval')
{
	$DB= new DBConnect();
	if($table=='')
	{
		return false;
	}	
	else
	{
		if($value=='')
		{
			return false;
		}
		else
		{
				$query=" update ".$table." set ".$field."='".$value."'";
				if($cond)
				{
					 $query.=" where  ".$cond;
				}
//		   echo  $query; exit;
				if($DB->Update($query))
				{
					return true;
				}
		}
	}
	
 return false;
} 	
/*
	Added by Neha Kaushik
	@para
	@$table : String(Name of table)
	@$data : Array of data that needed to be enter in database
	@Descriptiion : this function is used to take data as form of array and generates a insert query.
*/

function insert_array($table, $data)
 {
$DB= new DBConnect();
	
foreach ($data as $field=>$value) 
  {
		$fields[] = '`' . $field . '`';
		
		if ($field) 
		{
			$values[] = "'" . mysql_real_escape_string($value) . "'";
		}
	}
	$field_list = join(',', $fields);
	$value_list = join(', ', $values);
	
	$query ="INSERT INTO `".$table ."`(" . $field_list .")VALUES(".$value_list.")";
	//  echo $query;exit;
	return $query;

			
}
/*
	@added by Neha Kaushik
	@para
	@$table : String( Name of table)
	@$data  : Array of data that needed to be enter in database
	@Description : this function is used to take data as formof array and genrates a insert query.

*/		
function InsertarrayRecords($table,$data)
{

	$DB= new DBConnect();
	
	if($table=='')
	{
		return false;
	}	
	else
	{
		if($data=='')
		{
			return false;
		}
		else
		{
			
			$resource= mysql_query("SHOW COLUMNS FROM ".$table);
			
			if($resource)
			{
				$fields="";
				$values="";
				
				while($row = mysql_fetch_array($resource))
				{
					
					if(array_key_exists($row['Field'],$data))
					{
						 $fields.=$row['Field'].",";
						if(is_array($data[$row['Field']]))
						{
						   // $data[$row['Field']]=array_values(array_filter($data[$row['Field']]));
							// ($data[$row['Field']]);
							$data[$row['Field']]=implode(",",$data[$row['Field']]);
						}	
						$values.="'".$data[$row['Field']]."',";
					}
				}
				
				if($fields)
				{
					$fields=substr($fields,0,strlen($fields)-1);	
					$values=substr($values,0,strlen($values)-1);				
					$query="Insert into ".PREFIX.$table."(".$fields.") values(".$values.")";
				
		
					if($DB->Select($query))
					{
						return mysql_insert_id();
					}
					return false;
				}
				
			}
			
		}
	}
	return false;			
}

/*
	@para
	@$table : String( Name of table)
	@$cond  : condtion to select recorde reocrd
	@$fields : name of fields data are needed from query
	@backqury : It wil be include in end of query like order by statement
	
	@Description : this function is used to select multiple records

*/
function SelectRecords1($table,$cond='',$fields='*',$backquery='')
{
	global $frmdata;
	$DB=new DBConnect();
	
	if($table)
	{
		if($fields)
		{		
				if(is_array($fields))
				{
					$fields=implode(",",$fields);
				}
				
				$query="select distinct ".$fields." from ".$table." ";
				
				if($cond)
				{
					 $query.="where ".$cond." ";
				}
				
				if($backquery)
				{
					    $query.=$backquery;
				}
		
				if($result=$DB->Select($query))
				{
					if(mysql_num_rows($result) > 0)
					{
						$arr=array();
				  		while($row=mysql_fetch_object($result)) 
				  		{	
				         $arr[]=$row;
					    }
						return $arr;
					}		
				 	 
				}
				
		}
	}
	return false;
}

/*
	@added by Neha Kaushik
	@para
	@$table : String( Name of table)
	@$data  : Array of data that needed to be enter in database
	@Description : this function is used to take data as formof array and genrates a insert query.

*/		
function InsertarrayRecords1($table,$data,$cond='')
{
	$DB= new DBConnect();
	if($table=='')
	{  
		return false;
	}	
	else
	{
		if($data=='')
		{  
			return false;
		}
		else
		{
			$resource= $DB->Select("SHOW COLUMNS FROM ".$table);
			if($resource)
			{
				$sql="";
				
				while($row = mysql_fetch_array($resource))
				{ 
				
					if(array_key_exists($row['Field'],$data))
					{
					     if(is_array($data[$row['Field']]))
						 {
						     $data[$row['Field']]=array_values(array_filter($data[$row['Field']]));
							 $data[$row['Field']]=implode(",",$data[$row['Field']]);
						  } 
					 $sql.=$row['Field']."='".$data[$row['Field']]."',";
					
					}
				}
			
				
				if($sql)
				{
					$sql=substr($sql,0,strlen($sql)-1);
										
					$query="update ".PREFIX.$table." set ".$sql;
					if($cond)
					{
						if(!is_array($cond))
						{
							$query.=" where ".$cond;
						}
						else
						{
								$count=count($cond);
								if($count)
								{
									$fields=@array_keys($cond);
									$query.=" where ";
									for($counter=0;$counter<$count;$counter++)
									{
										
										if($fields[$counter]=='or' || $fields[$counter]=='and')
										{
												 $query.=" ".$fields[$counter]." "; 
										}
										else
										{
										
												 $query.=" ".$fields[$counter]."='".$cond[$fields[$counter]]."'"; 
										}		
									}
								}
							}		
					}
		
					if($DB->Update($query))
					{
						return true;
					}
					return false;
				}
				
			}
			
		}
	}
	return false;			
}


function DrawFromDB($table,$field,$type,$data,$sort="yes",$title="")
{
//connect to DB;
	$DB= new DBConnect();
$query= $DB->Select("SHOW COLUMNS FROM ".$table) or die (mysql_error()); 
if(mysql_fetch_array($query)>0)
{
$row= mysql_fetch_array($query);
$count=count($row);
//$options=explode("','",preg_replace("/(enum|set)\('(.+?)'\)/","\\2",$row[1]));

if ($sort =="yes")
sort ($options);
$ARay = explode(",",$data);
}
switch ($type)
{ 
case "select":
$text="\n\n<select name='". $field ."'>\n";
if ($title > "")
$text.="<option value=\"\">" . $title . "</option>\n";
for ($i=0;$i<count($options);$i++)
{ 
$selected = NULL;
if ($data == $options[$i])
$selected ="SELECTED ";
$text.="<option " . $selected .
"value=\"".$options[$i]."\">".ucfirst($options[$i])."</option>\n";
}
$text.="</select>\n\n";
break;

default:
$text="\n";
for ($i=0;$i<count($options);$i++)
{
$checked=NULL;
for ($j=0;$j<count($ARay);$j++)
{ 
if ($ARay[$j] == $options[$i])
$checked=" CHECKED ";
}
$text.="<INPUT TYPE='checkbox'" . $checked . " NAME='" . $field."[]'
VALUE='". $options[$i] . "'>".ucfirst($options[$i])." \n";
}
$text.="\n";
break;
}
return $text; 
}
/****************Insert and update image ADDED BY ANU R*********************************/
function InsertImage($table,$files)
{
			$p_id=0;
			$DB= new DBConnect();
			$_FILES=$files;
			$idQry="Select id from project_data order by id desc";
			$result=$DB->Select($idQry);
				if($result=$DB->Select($idQry))
				{
					if($result->rowCount() > 0)
					{
						$result->setFetchMode(PDO::FETCH_OBJ)  ;
				  		$row= $result->fetchAll(); 
						$row[0];
						$p_id=$row[0]->id;
					}		
				}
			$query = "INSERT into $table(`image_name`,`p_id`)VALUES(:image_name,:p_id)";
			$stmt  = $this->conn->prepare($query);
			$errors= array();
			foreach($_FILES['files']['tmp_name'] as $key => $error )
			{
				
				$image_name = $_FILES['files']['name'][$key];
				$image_tmp  = $_FILES['files']['tmp_name'][$key];
					if($image_name!='' /*&& $file_size!=0 */)
					{
						$image_name= time().'_'.$image_name;
						$stmt->bindParam( ':image_name', $image_name , PDO::PARAM_STR);
						$stmt->bindValue( ':p_id', $p_id , PDO::PARAM_INT );
						$stmt->execute();
						
						$desired_dir=ROOT."/panel/uploadfiles";
						if(is_dir($desired_dir)==false)
						{
								mkdir($desired_dir, 0700);// Create directory if it does not exist
						}
						if(is_file($desired_dir.'/'.$image_name)==false)
						{
								move_uploaded_file($image_tmp,$desired_dir.'/'.$image_name);
						}
					}
			}
			Redirect(CreateURL('index.php','mod=projects&do=add'));
			
}

function UpdateImage($table,$files,$pid)
{
			$p_id=0;
			$DB= new DBConnect();
			$_FILES=$files;
			$p_id=$pid;
			
			$query = "INSERT into $table(`image_name`,`p_id`)VALUES(:image_name,:p_id)";
			$stmt  = $this->conn->prepare($query);
			$errors= array();
			foreach($_FILES['files']['tmp_name'] as $key => $error )
			{
				
				$image_name = $_FILES['files']['name'][$key];
				$image_tmp  = $_FILES['files']['tmp_name'][$key];
					if($image_name!='' /*&& $file_size!=0 */)
					{
						$image_name= time().'_'.$image_name;
						$stmt->bindParam( ':image_name', $image_name , PDO::PARAM_STR);
						$stmt->bindValue( ':p_id', $p_id , PDO::PARAM_INT );
						$stmt->execute();
						
						$desired_dir=ROOT."/panel/uploadfiles";
						if(is_dir($desired_dir)==false)
						{
								mkdir($desired_dir, 0700);// Create directory if it does not exist
						}
						if(is_file($desired_dir.'/'.$image_name)==false)
						{
							    //unlink("/uploadfiles".$file_name);
								move_uploaded_file($image_tmp,$desired_dir.'/'.$image_name);
						}
					}
			}
			Redirect(CreateURL('index.php','mod=projects&do=list'));
			
}


}
?>