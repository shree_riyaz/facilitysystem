<?php
   /**********************************************************************************************************************************************
   		File Name : commonfunction.php
		Use : Used to define comman functions for project
			
   /**********************************************************************************************************************************************/

use PHPMailer\PHPMailer\PHPMailer;
//use PHPMailer\PHPMailer\Exception;
use Statickidz\GoogleTranslate;

//require ADMINROOT.'/vendor/phpmailer/phpmailer/src/Exception.php';
require ADMINROOT.'/vendor/phpmailer/phpmailer/src/PHPMailer.php';
require ADMINROOT.'/vendor/phpmailer/phpmailer/src/SMTP.php';
require ADMINROOT.'/vendor/autoload.php';


include_once("dbfilter.php");

	function is_company_logo_available(){
        $DBFilter = new DBFilter();
        $company_logo= $DBFilter->RunSelectQuery("select company_logo from company_detail where company_id=".$_SESSION['company_id']);
        $company_logo_edit = current(current($company_logo))->company_logo;
        return $company_logo_edit;
	}
	function number_of_feedback()
	{
		$DBFilter = new DBFilter();
		$total_feedback_overall = $DBFilter->RunSelectQuery("select COUNT(*) as count from feedback where is_deleted ='N' AND is_active ='Y' AND feedback_type='rating' AND  created_at BETWEEN '" . $_SESSION['start_date'] . "' AND '" . $_SESSION['end_date'] . "' AND company_id=".$_SESSION['company_id']);
//	        echo '<pre>'; print_r($total_feedback_overall); exit;
		$count = current(current($total_feedback_overall))->count;
		return $count;
	}
	function like_dislike($nature_type)
	{
		$DBFilter = new DBFilter();
		$total_feedback_overall_by_nature = $DBFilter->RunSelectQuery("select COUNT(*) as count from feedback where is_deleted ='N' AND is_active ='Y' AND feedback_type='rating' AND feedback_nature= '$nature_type' AND  created_at BETWEEN '" . $_SESSION['start_date'] . "' AND '" . $_SESSION['end_date'] . "' AND company_id=".$_SESSION['company_id']);
		$count = current(current($total_feedback_overall_by_nature))->count;
//        echo '<pre>'; print_r($count); exit;

        return $count;
	}
	function ShowMessage($table = true)
    {
        global $frmdata;
        
        if ($table)
        {

            if (isset($frmdata['message']) && $frmdata['message'] != ''){

                echo '<table cellspacing="0" cellpadding="0" border="0" align="center" width="40%" ><tbody><tr><td colspan="6"  align="center">
				<div class="alert alert-danger alert-dismissable">
				   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
                echo $frmdata['message'];
                echo '</div></td></tr></tbody></table><br>';
                unset($frmdata['message']);
            }
        }
        else
        {
            echo $frmdata['message'];
        }
        $frmdata['message'] = '';
    }

    function ShowMessageForgot($table = true)
    {
        global $frmdata;

        if ($table)
        {

            if (isset($_SESSION['message_forgot']) && $_SESSION['message_forgot'] != ''){

                echo '<table cellspacing="0" cellpadding="0" border="0" align="center" width="40%" ><tbody><tr><td colspan="6"  align="center">
				<div class="alert alert-danger alert-dismissable">
				   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
                echo $_SESSION['message_forgot'];
                echo '</div></td></tr></tbody></table><br>';
                unset($_SESSION['message_forgot']);
            }
        }
        else
        {
            echo $_SESSION['message_forgot'];
        }
        $_SESSION['message_forgot'] = '';
    }

    function ShowMessageForgotSuccess($table = true)
    {
        global $frmdata;

        if ($table)
        {

            if (isset($_SESSION['message_forgot_success']) && $_SESSION['message_forgot_success'] != ''){

                echo '<table cellspacing="0" cellpadding="0" border="0" align="center" width="40%" ><tbody><tr><td colspan="6"  align="center">
				<div class="alert alert-success alert-dismissable">
				   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
                echo $_SESSION['message_forgot_success'];
                echo '</div></td></tr></tbody></table><br>';
                unset($_SESSION['message_forgot_success']);
            }
        }
        else
        {
            echo $_SESSION['message_forgot_success'];
        }
        $_SESSION['message_forgot_success'] = '';
    }

    function ShowMessageResetPassword($table = true)
    {
        global $frmdata;

        if ($table)
        {

            if (isset($_SESSION['message_reset_psd']) && $_SESSION['message_reset_psd'] != ''){

                echo '<table cellspacing="0" cellpadding="0" border="0" align="center" width="40%" ><tbody><tr><td colspan="6"  align="center">
				<div class="alert alert-danger alert-dismissable">
				   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
                echo $_SESSION['message_reset_psd'];
                echo '</div></td></tr></tbody></table><br>';
                unset($_SESSION['message_reset_psd']);
            }
        }
        else
        {
            echo $_SESSION['message_reset_psd'];
        }
        $_SESSION['message_reset_psd'] = '';
    }

    function validateUserLogin()
    {
        global $frmdata;
        $frmdata['message'] = '';
        if ($frmdata['user_email'] == '')
		{
            $frmdata['message'] .= '<span style=" color:red">Please enter your user email.</span><br>';
        }
		if ($frmdata['password'] == '')
        {
            $frmdata['message'] .= '<span style=" color:red">Please enter your password.</span>';
        }
        if ($frmdata['message'] == '')
 			return true;
     	else
            return false;
    }

    function validateUserLoginForForgotPage()
    {
        global $frmdata;

        $_SESSION['message_forgot'] = '';
        if ($frmdata['email_forgot'] == '')
		{
            $_SESSION['message_forgot'] .= '<span style=" color:red">Please enter your registered email.</span><br>';
        }

        if ($_SESSION['message_forgot'] == '')
 			return true;
     	else
            return false;
    }

    function validateUserLoginForResetPassword()
    {
        global $frmdata;
//        echo '<pre>'; print_r($frmdata); exit;

        $_SESSION['message_reset_psd'] = '';
//        if ($frmdata['password'] == '')
//		{
//            $_SESSION['message_reset_psd'] .= '<span style=" color:red">Please enter password.</span><br>';
//        }
//        if ($frmdata['confirm_password'] == '')
//        {
//            $_SESSION['message_reset_psd'] .= '<span style=" color:red">Please enter confirm password.</span>';
//        }

        if (trim($frmdata['password']) != '' && trim($frmdata['confirm_password']) == '')
        {
            $_SESSION['message_reset_psd'] .= "Please enter confirm password.<br>";
            $frmdata['confirm_password']='';
        }
        if (trim($frmdata['password']) == '' && trim($frmdata['confirm_password']) == '')
        {
            $_SESSION['message_reset_psd'] .= "Please enter password.<br>";
            $frmdata['confirm_password']='';
        }
        if (trim($frmdata['password']) == '' && trim($frmdata['confirm_password']) != '')
        {
            $_SESSION['message_reset_psd'] .= "Please enter password.<br>";
            $frmdata['confirm_password']='';
        }

        if (($frmdata['confirm_password']!='') && (trim($frmdata['confirm_password']) != trim($frmdata['password'])))
        {
            $_SESSION['message_reset_psd'] .= "Confirm password doesn't match.<br>";
            $frmdata['confirm_password']='';
        }


        if ($_SESSION['message_reset_psd'] == '')
 			return true;
     	else
            return false;
    }

	function randomString($length) {
		$str = "";
		$characters = array_merge(range('A','Z'), range('a','z'), range('0','9'));
		$max = count($characters) - 1;
		for ($i = 0; $i < $length; $i++) {
			$rand = mt_rand(0, $max);
			$str .= $characters[$rand];
		}
		return $str;
	}

	function validateMemberLogin()
    {
        global $frmdata;
		$DBFilter = new DBFilter();
		$LoginCredentials = getLoginCredentials();
		$obj = new passEncDec;

		if (isset($_COOKIE['password'])){
            $password = $_COOKIE['password'];
        }else{
			$password = $obj->encrypt_password($frmdata['password']);
        }
//        echo '<pre>'; print_r($accessKey); exit;

        $cmpy_status = $DBFilter->SelectRecords('company_detail',"is_deleted='N' and company_id=".$LoginCredentials['company_id']);

		if($LoginCredentials['password'] == $password && $LoginCredentials['user_email'] == $frmdata['user_email'] && $LoginCredentials['is_active'] == 'Y' and $cmpy_status[0][0]->is_active == 'Y' )
		{

            $cookie_expiration_time= time() + (86400 * 10);
            $email_cookie = $frmdata['user_email'];
            $password_cookie = $frmdata['password'];

            if (isset($_POST['remember_me']) && $_POST['remember_me'] == 'on'){
                setcookie("password",$password, $cookie_expiration_time);
                setcookie("user_email", $email_cookie,$cookie_expiration_time);
                setcookie("is_remember", 'on',$cookie_expiration_time);
			}else{
                setcookie("password",NULL, $cookie_expiration_time);
                setcookie("user_email", NULL,$cookie_expiration_time);
                setcookie("is_remember", '',$cookie_expiration_time);
            }

            $randomString =  substr("abcdefghijklmnopqrstuvwxyz" ,mt_rand( 0 ,10 ) ,1 ) .substr( md5( time( ) ) ,1 );
			$data= array('user_id'=>$LoginCredentials['user_id'],'access_key'=>$randomString);
			$key = insertAccessKey('login_detail',$data);
			$accessKey = getAccessKey($LoginCredentials['user_id']);
//            echo '<pre>'; print_r($accessKey); exit;

            /*
            if($_REQUEST['call'] == 'API')
            {
                $user_data = array();

                if($data!='')
                {
                    $user_data['user_id'] = $LoginCredentials['user_id'];
                    $user_data['first_name'] = $LoginCredentials['first_name'];
                    $user_data['last_name'] = $LoginCredentials['last_name'];
                    $user_data['user_email'] = $LoginCredentials['user_email'];
                    $user_data['ACCESS_TOKEN']=$accessKey['access_key'];
                    $user_data['status'] = 1;

                    echo json_encode($user_data);
                    exit;
                }
            }
            */
			//$cmpy_status = $DBFilter->SelectRecords('company_detail',"is_deleted='N' and company_id=".$LoginCredentials['company_id']);
			//echo $LoginCredentials['company_id']; 
			//print_r($cmpy_status);exit;
			//if($LoginCredentials!='' and $cmpy_status[0][0]->is_active == 'Y')//AddedBy Neha Pareek, Dated : 30 Nov 2015, to check requested user company is active or not.
			// if($LoginCredentials!='')
			// {
				if($LoginCredentials['role_id']==1)
				{ 
					$_SESSION['usertype']='super_admin';
					 $_SESSION['user_id']=$LoginCredentials['user_id'];
					$_SESSION['role_id']=$LoginCredentials['role_id'];
					$_SESSION['ACCESS_TOKEN']=$accessKey['access_key'];
				}
				 else if($LoginCredentials['role_id']==2)
				{
                    $_SESSION['usertype']='company_admin';
					 $_SESSION['user_id']=$LoginCredentials['user_id'];
					 $_SESSION['first_name']=$LoginCredentials['first_name'];
					 $_SESSION['role_id']=$LoginCredentials['role_id'];
					 $_SESSION['company_id']=$LoginCredentials['company_id'];
					 $_SESSION['ACCESS_TOKEN']=$accessKey['access_key'];
				}
				else 
				{
					$_SESSION['usertype']='company_users';
					 $_SESSION['user_id']=$LoginCredentials['user_id'];
					 $_SESSION['role_id']=$LoginCredentials['role_id'];
					 $_SESSION['company_id']=$LoginCredentials['company_id'];
					 $_SESSION['ACCESS_TOKEN']=$accessKey['access_key'];
				}
			ShowMessage();
				return true;
			/*}
			else//Added by Neha Pareek. Dated : 30 Nov 2015
			{
			$frmdata['user_email'] = $frmdata['user_email']; 
			$frmdata['password'] = '';
			$frmdata['message'] = "<span style=' color:red'>Inactive company user. Contact to administrator.</span>";
			}*/
		}
		elseif ($LoginCredentials['password'] != $password && $LoginCredentials['user_email'] == $frmdata['user_email'] && $LoginCredentials['is_active'] == 'Y' and $cmpy_status[0][0]->is_active == 'Y' ) 
		{
			$frmdata['message'] = "<span style=' color:red'>Invalid password.</span>";
			ShowMessage();
		}
		elseif ($LoginCredentials['password'] == $password && $LoginCredentials['user_email'] == $frmdata['user_email'] && $LoginCredentials['is_active'] == 'N' and $cmpy_status[0][0]->is_active == 'Y')
		{
			$frmdata['message'] = "<span style=' color:red'>Inactive user login.</span>";
		}
		elseif ($LoginCredentials['password'] == $password && $LoginCredentials['user_email'] == $frmdata['user_email'] && $LoginCredentials['is_active'] == 'N' and $cmpy_status[0][0]->is_active == 'N')
		{
			$frmdata['message'] = "<span style=' color:red'>Inactive company user. Contact to administrator.</span>";
		}
		else
		{
			$frmdata['message'] = "<span style=' color:red'>Invalid user email and password.</span>";
			ShowMessage();
		}

//		if ($frmdata['message'] == '')
//			return true;
//		else
//			return false;
    }
	function translate_via_google_api($from_lan, $to_lan, $text){
//        $source = 'es';
//        $target = 'en';
//        $text = 'verdadero';

        $trans = new GoogleTranslate();
        $result = $trans->translate($from_lan, $to_lan, $text);
//                echo '<pre>'; print_r($result); exit;

        return $result;
	}
    /*
    @para	: string name of file
    @return : --
    @Des 	: redirect to given file
    */
    function Redirect($file)
    {
        echo '<script language="javascript1.2">';
        echo 'document.location="' . $file . '"';
        echo '</script>';
        
    }
	
	
	/*###################################################################################
	
	  Function is used to check email is exist or not when edit user or company
	  CREATED BY : NEHA PAREEK
	  Dated : 25-01-2016
	//###################################################################################*/
	
	function checkMail($email)
	{	
		$DBFilter = new DBFilter();
		if($_SESSION['action_name'] == 'edit' && $_SESSION['module_name'] == 'user') // when edit template
		{	
			$id = $_GET['id']; //Get current record's id, which we going to edit
			$users = $DBFilter->SelectRecords("users","user_id NOT LIKE '%{$id}%' and company_id='".$_SESSION['company_id']."' and (is_active='Y' or is_active='N')");
//			$users = $DBFilter->SelectRecords(" select user_email from users WHERE user_email=".$email);
//		echo '<pre>'; print_r($users); exit;
//            echo '<pre>'; print_r($users); exit;


            $count_user = count($users[0]);
			$flag = 0;
			for($i = 0; $i < $count_user ; $i++)
			{
				if($users[0][$i]->user_email == trim($email))
				{
					$flag = 1;
				}
			}
		}
		elseif($_SESSION['action_name'] == 'edit' && $_SESSION['module_name'] == 'company')
		{
			$id = $_GET['id']; //Get current record's id, which we going to edit
			$company = $DBFilter->SelectRecord("users","company_id  LIKE '%{$id}%' and role_id ='2' and is_deleted='N' and (is_active='Y' or is_active='N')");
			//print_r($company);exit;
			$user_id = $company->user_id;
			$users = $DBFilter->SelectRecords("users","user_id NOT LIKE '%{$user_id}%' and is_deleted='N' and (is_active='Y' or is_active='N')");
			$count_user = count($users[0]);
			$flag = 0;
			for($i = 0; $i < $count_user ; $i++)
			{
				if($users[0][$i]->user_email == trim($email))
				{
					$flag = 1;
				}
			}
		}
		elseif($_SESSION['action_name'] == 'add') //when add template
		{
			$users = $DBFilter->SelectRecords("users","is_deleted='N' and user_email= '".$email."' and company_id='".$_SESSION['company_id']."' and (is_active='Y' or is_active='N')");

            $count_user = count($users[0]);
			$flag = 0;
			for($i = 0; $i < $count_user ; $i++)
			{
				if($users[0][$i]->user_email == trim($email))
				{
					$flag = 1;
				}
			}
		}
		// echo $_SESSION['action_name'];
		// echo "<pre>";print_r($users[0]);
		// echo $flag; exit;
		if($flag == 1)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
	/*###################################################################################
	
	  Function is set focus on error element
	  CREATED BY : NEHA PAREEK
	//###################################################################################*/
	/*  function SetFocus($id)
    {
       echo "Setting focus to: '". $id ."'";
        echo "<script type='text/javascript'>";
        echo "var elem = document.getElementById('".$id."');";
        echo "elem.focus();";
        //echo "elem.select()";
        echo "</script>";
    }*/
	/*###################################################################################
	
	  Function is used to check role name is exist or not
	  CREATED BY : NEHA PAREEK
	//###################################################################################*/
	
	function chkRolename($name)
	{
	 	 $DBFilter = new DBFilter();
		 $flag = 0;
		 //$sql="SELECT * FROM `posts` WHERE `content` LIKE '%{$searchstring}%'";
		/*$roles = $DBFilter->SelectRecords("roles","role_name LIKE '%{$name}%' and is_deleted='N' and is_active='Y'");
		//print_r($roles);exit;
		if($roles)
		{
			return 1;
		}
		else 
		return 0;*/
		$roles = $DBFilter->SelectRecords("roles","is_deleted='N'");
		
		if($roles[0])
		{
			for($i =0; $i < count($roles[0]); $i++)
			{
				if(strtoupper($roles[0][$i]->role_name) == strtoupper(trim($name)))
				{
					$flag =1;
				}
			}
		}
		
		if($flag == 1)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
    
	/*###################################################################################
	
	  Function is used to check company name is exist or not
	  CREATED BY : NEHA PAREEK
	  Dated : 25-01-2015
	//###################################################################################*/
	
	function chkCompanyName($name)
	{
		
	 	$DBFilter = new DBFilter();
		if($_SESSION['action_name'] == 'add')
		{	$flag = 0;
			$company = $DBFilter->SelectRecords("company_detail","is_deleted='N' and (is_active = 'Y' or is_active = 'N')");
			//print_r($company);exit;
			if($company[0])
			{
				for($i = 0; $i < count($company[0]);$i++)
				{	//echo "<br/>".strtoupper($company[0][$i]->company_name) ." ==  ".strtoupper(trim($name));
					if(strtoupper($company[0][$i]->company_name) == strtoupper(trim($name)))
					{
						$flag = 1;
					}
					
				}
			}
		}
		else if($_SESSION['action_name'] == 'edit')
		{
			$id = $_GET['id'];
			$flag = 0;
			$company = $DBFilter->SelectRecords("company_detail","company_id NOT LIKE '%{$id}%' and is_deleted='N'");
			//print_r($roles);exit;
			if($company[0])
			{
				for($i = 0; $i < count($company[0]);$i++)
				{	//echo "<br/>".strtoupper($company[0][$i]->company_name) ." ==  ".strtoupper(trim($name));
					if(strtoupper($company[0][$i]->company_name) == strtoupper(trim($name)))
					{
						$flag = 1;
					}
				}
			}
		}
		//echo "<pre>";print_r($_SESSION);
		//print_r($company[0]);
		// echo $flag; exit;
		if($flag == 1)
		{
			return 1;
		}
		else
		{ 
			return 0;
		}
	}/*###################################################################################
	
	  Function is used to check role name is exist or not when edit role_name
	  CREATED BY : NEHA PAREEK
	  Dated : 25-01-2016
	//###################################################################################*/
	
	function changeRoleName($name)
	{	
		$id = $_GET['id']; //Get current record's id, which we going to edit
		$DBFilter = new DBFilter();
		$roles = $DBFilter->SelectRecords("roles","role_id NOT LIKE '%{$id}%' and is_deleted='N' and (is_active='Y' or is_active='N')");
		
		$count_role = count($roles[0]);
		$flag = 0;
		if($count_role > 0)
		{
			for($i = 0; $i < $count_role ; $i++)
			{
				//echo ucwords($roles[0][$i]->role_name)." ". ucwords($name)."<br/>";
				if(strtoupper($roles[0][$i]->role_name) == strtoupper(trim($name)))
				{
					$flag = 1;
				}
			}
		}
		if($flag == 1)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
	/*###################################################################################
	
	  Function is used to check plan name is exist or not
	  CREATED BY : NEHA PAREEK
	  Dated : 22-01-2016
	//###################################################################################*/
	
	function chkPlanName($name)
	{	
	 	 $DBFilter = new DBFilter();
		 //$sql="SELECT * FROM `posts` WHERE `content` LIKE '%{$searchstring}%'";
		/*$plans = $DBFilter->SelectRecords("plan","plan_name LIKE '%{$name}%' and is_deleted='N'");
		//print_r($plans);exit;
		if($plans)
		{
			return 1;
		}
		else 
		return 0; */
		
		$plans = $DBFilter->SelectRecords("plan","is_deleted='N'");
		
		if($plans[0])
		{
			for($i =0; $i < count($plans[0]); $i++)
			{
				if(strtoupper($plans[0][$i]->plan_name) == strtoupper(trim($name)))
				{
					$flag =1;
				}
			}
		}
		
		if($flag == 1)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
    
	/*###################################################################################
	
	  Function is used to check plan name is exist or not when edit plan_name
	  CREATED BY : NEHA PAREEK
	  Dated : 25-01-2016
	//###################################################################################*/
	
	function changePlanName($name)
	{	
		$id = $_GET['id']; //Get current record's id, which we going to edit
		$DBFilter = new DBFilter();
		$plans = $DBFilter->SelectRecords("plan","plan_id NOT LIKE '%{$id}%' and is_deleted='N' and (is_active='Y' or is_active='N')");
		
		$count_plan = count($plans[0]);
		$flag = 0;
		if($count_plan > 0)
		{
			for($i = 0; $i < $count_plan ; $i++)
			{
				//echo strtoupper($plans[0][$i]->plan_name)." ". strtoupper($name)."<br/>";
				if(strtoupper($plans[0][$i]->plan_name) == strtoupper(trim($name)))
				{
					$flag = 1;
				}
			}
		}
		if($flag == 1)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
    
	/*###################################################################################
	
	  Function is used to delete company's related feedback , devices, services*/
	
	//###################################################################################*/
	function DeleteRelatedData($cid,$role,$table)
	{
//	echo $cid; exit;
		 $DBFilter = new DBFilter();
		 if($role =='super_admin')
		 { 

			 switch($table)
			 {
				case 'company_detail':
				 $related_user = $DBFilter->SelectRecords('users',"company_id='$cid' and is_deleted ='N'");
				 $related_feedback  = $DBFilter->SelectRecords('feedback',"company_id='$cid' and is_deleted ='N'");
				 $related_device  = $DBFilter->SelectRecords('device',"company_id='$cid' and is_deleted ='N'");
				 $related_services  = $DBFilter->SelectRecords('services',"company_id='$cid' and is_deleted ='N'");
				 
				 if($related_user)
				 {
					$data = array('is_deleted'=>'Y','is_active'=>'N');
					$DBFilter->UpdateRecord('users',$data,"company_id=".$cid);
				 }
			
				if($related_feedback)
				 {
					$data = array('is_deleted'=>'Y','is_active'=>'N');
					$DBFilter->UpdateRecord('feedback',$data,"company_id=".$cid);
				 }
				
				if($related_device)
				 {
					$data = array('is_deleted'=>'Y','is_active'=>'N');
					$DBFilter->UpdateRecord('device',$data,"company_id=".$cid);
					$DBFilter->UpdateRecord('device_locations',$data,"company_id=".$cid);
				 }
				 
				if($related_services)
				 {
					$data = array('is_deleted'=>'Y','is_active'=>'N');
					for($i=0;$i<count($related_services[0]);$i++)
					{
						$service_id = $related_services[0][$i]->service_id;
						$DBFilter->UpdateRecord('faults',$data,"service_id=".$service_id);		
					}
					$DBFilter->UpdateRecord('services',$data,"company_id=".$cid);
				 }
					$data = array('is_deleted'=>'Y','is_active'=>'N');
					$DBFilter->UpdateRecord('company_detail',$data,"company_id=".$cid);
					$_SESSION['success'] = 'Company has been deleted successfully.';
			    	Redirect(CreateURL('index.php', 'mod=company'));
					die();

				case 'users':
				echo 'in users'.$cid. $role .$table;
				
				  $user_detail = $DBFilter->SelectRecord('users',"role_id = '2' and user_id='$cid'");
				// print_r($user_detail); exit;
				  $c_id = $user_detail->company_id;
				  $company = $DBFilter->SelectRecord('company_detail',"company_id='$c_id'  and is_deleted = 'N' or is_active='Y'");
				  if($company)
				  {
					$_SESSION['error'] = 'Please delete company first.';
					Redirect(CreateURL('index.php', 'mod=company'));
					die();
				  }
					
			 }
		return true; 
		}
		if($role =='company_admin')
		{
			if($table == 'users')
			{
				  $data = array('is_deleted'=>'Y','is_active'=>'N');
				  $user = $DBFilter->SelectRecord('users','user_id='.$cid);
				//  print_r($user);exit;
				if($user->role_id == '3')// to check cleaner record deletion
				{
					$related_feedback  = $DBFilter->SelectRecords('feedback',"assigned_to='$cid' and is_deleted ='N'");
					 if($related_feedback)
					 {
						$DBFilter->UpdateRecord('feedback',$data,"assigned_to=".$cid);
					 }
					//$_SESSION['success'] = 'User has been deleted successfully.';
					//Redirect(CreateURL('index.php', 'mod=user'));
					//die();
					return true;
				}
				else//added by : neha at 7:00 pm Dated 9 dec
				{	
					
					 //to check cleaners under supervisor
					 $related_cleaners  = $DBFilter->SelectRecords('users',"assigned_to='$cid' and is_deleted ='N'");
					 if($related_cleaners)
					 {
						$_SESSION['error'] = 'Many cleaners are assigned to this supervisor. Please assigned cleaner to other supervisor.';
						Redirect(CreateURL('index.php', 'mod=user'));
						die();
						return false;
					 }
					 else
					 {
					 //to check device owner
					  $related_device  = $DBFilter->SelectRecords('device',"user_id='$cid' and is_deleted ='N'");
					 if($related_device)
					 {
						$DBFilter->UpdateRecord('device',$data,"user_id=".$cid);
					 }
					 // to check feedback assigned by
					 $related_feedback  = $DBFilter->SelectRecords('feedback',"user_id='$cid' and is_deleted ='N'");
					 if($related_feedback)
					 {
						$DBFilter->UpdateRecord('feedback',$data,"user_id=".$cid);
					 }
					 $result = $DBFilter->UpdateRecord('users', $data, "user_id ='$cid'");
					// $result = $DBFilter->UpdateRecord('users', $data, "user_id ='$cid'");
					//exit;
					$_SESSION['success'] = 'User has been deleted successfully.';
					Redirect(CreateURL('index.php', 'mod=user'));
					die();
					return true;	
					}
				}
			}
		}
		else 
		{
			$_SESSION['error'] = 'You are not allowed to perform this action';
			Redirect(CreateURL('index.php', 'mod=user'));
			die();	
			
		} 
	}
	
	/*###################################################################################
	
	  Function is used to delete /active /inactive all data of diffrent module
	
	//###################################################################################*/
	
	 function multipleAction($id,$table,$action,$cond)
	 {
	 		//print_r($id);
	     	//exit;
		$DBFilter = new DBFilter();
		$role = $_SESSION['usertype'];
		if($action=='Active')
		{
			$count = count($id);
			for($i=0;$i<$count;$i++)
			{
				$data = array('is_active'=>'Y');
				$update_id = $id[$i]; 
				$DBFilter->UpdateRecord($table,$data,$cond.$update_id);				
			}
			//exit;
		}
		if($action=='Inactive')
		{
			
			$count = count($id);
			$data = array('is_active'=>'N');
			for($i=0;$i<$count;$i++)
			{
				
				$update_id = $id[$i]; 
				if($table == 'users' && $_SESSION['usertype']=='company_admin')
				{
					 $related_cleaners  = $DBFilter->SelectRecords('users',"is_deleted='N' and assigned_to='$update_id'");
					// echo '<pre>'; print_r($related_cleaners);exit;
					 if($related_cleaners)
					{
						$_SESSION['error'] = 'Many cleaners are assigned to this supervisor. Please assigned cleaner to other supervisior.';
						Redirect(CreateURL('index.php', 'mod=user'));
						die();
					}
					else 
					{	
						$DBFilter->UpdateRecord($table,$data,$cond.$update_id);	
					}
				}
				else
				$DBFilter->UpdateRecord($table,$data,$cond.$update_id);	
				
			}
			
			//Added By : Neha Pareek, Dated : 09 Dec 2015
			/* for($i=0;$i<$count;$i++)
			{
				$update_id = $id[$i]; 
				
				if($table == 'users' && $_SESSION['usertype']=='company_admin')
				{
					 $related_cleaners  = $DBFilter->SelectRecords('users',"assigned_to='$update_id' and is_deleted='N'");
					 $related_device = $DBFilter->SelectRecords('device',"user_id='$update_id' and is_deleted='N'");
					 $related_feedback = $DBFilter->SelectRecords('feedback',"assigned_to='$update_id' and is_deleted='N'");
					// echo '<pre>'; print_r($related_cleaners);exit;
					if($related_cleaners && $related_device)
					{
						$_SESSION['error'] = 'Many cleaners are assigned to this supervisor. Please assigned cleaner to other supervisor.<br/> And also change user related device owner.';
						Redirect(CreateURL('index.php', 'mod=user'));
						die();
					}
					else if($related_cleaners)
					{
						$_SESSION['error'] = 'Many cleaners are assigned to this supervisor. Please assigned cleaner to other supervisor.';
						Redirect(CreateURL('index.php', 'mod=user'));
						die();
					}
					else if($related_device)
					{
						$_SESSION['error'] = 'Change user related device owner first.';
						Redirect(CreateURL('index.php', 'mod=device'));
						die();
					}
					else if($related_feedback)
					{
						$_SESSION['error'] = 'Delete or in-active user related feedback first.';
						Redirect(CreateURL('index.php', 'mod=user'));
						die();
					}
					else 
					{	
						$DBFilter->UpdateRecord($table,$data,$cond.$update_id);	
					}
				}
				else if($table == 'users' && $_SESSION['usertype']=='super_admin')
				{
					$DBFilter->UpdateRecord($table,$data,$cond.$update_id);	
				}
				else
				{
					$DBFilter->UpdateRecord($table,$data,$cond.$update_id);	
				}
			}*/
			//exit;
		}
		if($action=='Delete')
		{	
			// print_r($id);
			 //exit;	
			$count = count($id);
			for($i=0;$i<$count;$i++)
			{	//echo $i;
				$update_id = $id[$i]; 
				
				if($table == 'company_detail')
				{
					$data = array('is_deleted'=>'Y','is_active'=>'N');
					$DBFilter->UpdateRecord($table,$data,$cond.$update_id);	
					$DBFilter->UpdateRecord('users',$data,$cond.$update_id);	
					$DBFilter->UpdateRecord('feedback',$data,$cond.$update_id);	
				}
				if($table == 'users' && $_SESSION['usertype']=='super_admin')
				{
					$user_company_data = $DBFilter->SelectRecord('users',"user_id='".$update_id."'");
					$user_company = $DBFilter->SelectRecord('company_detail',"is_deleted='N' and is_active='Y' and company_id='".$user_company_data->company_id."'");
					//	echo '<pre>'; print_r($user_company);exit;
					if($user_company)
					{
						$_SESSION['error'] = 'Please delete company first.';
						Redirect(CreateURL('index.php', 'mod=company'));
						die();
					}
					else 
					{
					$data = array('is_deleted'=>'Y','is_active'=>'N');
					$DBFilter->UpdateRecord($table,$data,$cond.$update_id);	
					$DBFilter->UpdateRecord('users',$data,$cond.$update_id);	
					$DBFilter->UpdateRecord('feedback',$data,$cond.$update_id);	
					}
				} 
				if($table == 'users' && $_SESSION['usertype']=='company_admin')
				{	//Modified By : Neha Pareek . Dated : 09/12/2015
					 $related_cleaners  = $DBFilter->SelectRecords('users',"assigned_to='$update_id' and is_deleted ='N'");
					 $related_device = $DBFilter->SelectRecords('device',"user_id='$update_id' and is_deleted ='N'");
					 $related_feedback = $DBFilter->SelectRecords('feedback',"is_deleted ='N' and assigned_to='$update_id'");
					// echo '<pre>'; print_r($related_cleaners);exit;
					 if($related_cleaners && $related_device)
					{
						$_SESSION['error'] = 'Many cleaners are assigned to this supervisor. Please assigned cleaner to other supervisor.<br />And delete user related device first.';
						Redirect(CreateURL('index.php', 'mod=user'));
						die();
					}
					else if($related_cleaners)
					{
						$_SESSION['error'] = 'Many cleaners are assigned to this supervisor. Please assigned cleaner to other supervisor.';
						Redirect(CreateURL('index.php', 'mod=user'));
						die();
					}		
					else if($related_device)
					{
						$_SESSION['error'] = 'Delete user related device first.';
						Redirect(CreateURL('index.php', 'mod=device'));
						die();
					}					
					else if($related_feedback)
					{	
						$_SESSION['error'] = 'Delete user related feedback first.';
						Redirect(CreateURL('index.php', 'mod=feedback'));
						die();
					}
					else
					{
						$data = array('is_deleted'=>'Y','is_active'=>'N');
						$DBFilter->UpdateRecord($table,$data,$cond.$update_id);	
					}
				}
				/*if($table == 'device')
				{	//Added By : Neha Pareek. Dated : 09/12/2015
					 $related_feedback = $DBFilter->SelectRecords('feedback',"is_deleted ='N' and device_id ='$update_id'");
					// echo '<pre>'; print_r($related_cleaners);exit;				
					if($related_feedback)
					{	
						$_SESSION['error'] = 'Delete device related feedback first.';
						Redirect(CreateURL('index.php', 'mod=feedback'));
						die();
					}
					else
					{
						$data = array('is_deleted'=>'Y','is_active'=>'N');
						$DBFilter->UpdateRecord($table,$data,$cond.$update_id);	
					}
				}
				if($table == 'device_locations')
				{	//Added By : Neha Pareek. Dated : 09/12/2015
					 $related_device = $DBFilter->SelectRecords('device',"is_deleted ='N' and location_id ='$update_id'");
					print_r($related_device);
					exit;
					 $related_feedback = $DBFilter->SelectRecords('feedback',"is_deleted ='N' and device_id ='$update_id'");
					 
					// echo '<pre>'; print_r($related_cleaners);exit;				
					if($related_device)
					{	
						$_SESSION['error'] = 'Delete location related device first.';
						Redirect(CreateURL('index.php', 'mod=device'));
						die();
					}
					else
					{
						$data = array('is_deleted'=>'Y','is_active'=>'N');
						$DBFilter->UpdateRecord($table,$data,$cond.$update_id);	
					}
				}*/
				else 
				{	
					$data = array('is_deleted'=>'Y','is_active'=>'N');
					$DBFilter->UpdateRecord($table,$data,$cond.$update_id);	
				}
				
			}
		//	exit;
		}	
	 }


	/*###################################################################################
	
	  Function is used to check number of supervisior and cleaner
	
	//###################################################################################*/
	
	function checkUsers($plan,$role_id) //Edited on : 11-12-2015 By : Neha Pareek
	{
		$DBFilter = new DBFilter();
		
			if($plan =='Basic')
			{
				if($role_id==4)
				{
					//$no_of_supervisor =  $DBFilter->SelectRecords("users","role_id = '4' and is_deleted = 'N' and is_active='Y' and  company_id=".$_SESSION['company_id']);
					$no_of_supervisor =  $DBFilter->SelectRecords("users","role_id = '4' and is_deleted = 'N'  and  company_id=".$_SESSION['company_id']);
					if(count($no_of_supervisor[0]) >= 1)
					{
						$_SESSION['error'] = 'You are not allowed to add more then 1 supervisor. Please contact to administrator for upgrading plan.';
						  Redirect(CreateURL('index.php', 'mod=user'));
						  die();
					}
				}
				if($role_id==3)
				{
					//$no_of_cleaner =  $DBFilter->SelectRecords("users","role_id = '3' and is_deleted ='N' and is_active='Y' and  company_id=".$_SESSION['company_id']);
					$no_of_cleaner =  $DBFilter->SelectRecords("users","role_id = '3' and is_deleted ='N' and  company_id=".$_SESSION['company_id']);
					if(count($no_of_cleaner[0]) >= 5)
					{
						$_SESSION['error'] = 'You are not allowed to add more then 5 cleaners. Please contact to administrator for upgrading plan.';
						 Redirect(CreateURL('index.php', 'mod=user'));
						 die();
					}
				}
	
			}
		
			if($plan =='Optimum')
			{
				if($role_id==4)
				{
					//$no_of_supervisor =  $DBFilter->SelectRecords("users","role_id = '4' and is_deleted = 'N' and is_active='Y' and  company_id=".$_SESSION['company_id']);
					$no_of_supervisor =  $DBFilter->SelectRecords("users","role_id = '4' and is_deleted = 'N' and  company_id=".$_SESSION['company_id']);
					if(count($no_of_supervisor[0]) >= 5)
					{
						$_SESSION['error'] = 'You are not allowed to add more then 5 supervisers. Please contact to administrator for upgrading plan.';
						  Redirect(CreateURL('index.php', 'mod=user'));
						  die();
					}
				}
				if($role_id==3)
				{
					//$no_of_cleaner =  $DBFilter->SelectRecords("users","role_id = '3' and is_deleted ='N' and is_active='Y' and  company_id=".$_SESSION['company_id']);
					$no_of_cleaner =  $DBFilter->SelectRecords("users","role_id = '3' and is_deleted ='N' and  company_id=".$_SESSION['company_id']);
					if(count($no_of_cleaner[0]) >= 20)
					{
						$_SESSION['error'] = 'You are not allowed to add more then 20 cleaners. Please contact to administrator for upgrading plan.';
						  Redirect(CreateURL('index.php', 'mod=user'));
						  die();
					}
				}
	
			}
				
			if($plan =='Premium')
			{
				if($role_id==4)
				{
					//$no_of_supervisor =  $DBFilter->SelectRecords("users","role_id = '4' and is_deleted = 'N' and is_active='Y' and  company_id=".$_SESSION['company_id']);
					$no_of_supervisor =  $DBFilter->SelectRecords("users","role_id = '4' and is_deleted = 'N' and  company_id=".$_SESSION['company_id']);
					if(count($no_of_supervisor[0]) >= 20)
					{
						$_SESSION['error'] = 'You are not allowed to add more then 20 supervisors. Please contact to administrator for upgrading plan.';
						  Redirect(CreateURL('index.php', 'mod=user'));
						  die();
					}
				}
				if($role_id==3)
				{
					//$no_of_cleaner =  $DBFilter->SelectRecords("users","role_id = '3' and is_deleted ='N' and is_active='Y' and  company_id=".$_SESSION['company_id']);
					$no_of_cleaner =  $DBFilter->SelectRecords("users","role_id = '3' and is_deleted ='N' and  company_id=".$_SESSION['company_id']);
					if(count($no_of_cleaner[0]) >= 200)
					{
						$_SESSION['error'] = 'You are not allowed to add more then 200 cleaners. Please contact to administrator for upgrading plan.';
						  Redirect(CreateURL('index.php', 'mod=user'));
						  die();
					}
				}
			}
		//	exit;
		//$_SESSION['error'] = $result;
		//return $result;	
	}
	
    function roleList()
    {
        $result = SelectRecords('role','','*','order by roleName');
        return $result;
    }
    
    function getComponentKeyEntries($component_id,$specification = '')
    {
        $where  = " status='A' ";
        if(trim($component_id) > 0)
        	$where.= " and component_id='$component_id'";

        if(trim($specification) != '')
            $where.= " and specification='$specification'";
        
        $result = SelectRecords('component_key_factor', $where);
        return $result;
    }
    
    function genrateDropdown($key_factor_title, $name, $preselected = '', &$icOptionArr)
    {
        $result   = RunSelectQuery("SELECT *  FROM  `kf_values` WHERE kf_type =  '$key_factor_title' and status='A'");
        //print_r($result[1]->kf_title); 
        //echo count($result); return;
        $dropdown = "<select name='$name' id='$name' style='width:200px'> 
	              <option value=''>Please select</option>";
        for ($i = 0; $i < count($result) AND $result; $i++)
        {
            if (trim($key_factor_title) == Commonvar_additionalchips_label)
                $icOptionArr[$result[$i]->id] = $result[$i]->kf_title;
            
            if (trim($result[$i]->kf_title) != "")
            {
                $sel = '';
                if ($result[$i]->id == $preselected)
                    $sel = ' selected ';
                $dropdown .= "<option " . $sel . " value='" . $result[$i]->id . "'>" . ucfirst($result[$i]->kf_title) . "</option>";
            } //trim($result[$i]->kf_title) != ""
            
        } //$i = 0; $i < count($result) AND $result; $i++
        $dropdown .= "</select>";
        //if($result)
        {
            echo $dropdown;
            return true;
        }
        
    }
    
    function RecordPerPage($no)
    {
        if ($no)
        {
            return $no;
        } //$no
        else
        {
            return 10;
        }
    }
    //=============================================================================================
    // function pagination 
    //======================================================================================       
    function PaginationWork($option = '')
    {
        global $frmdata;
        //print_r($frmdata);
        //echo $frmdata['record'];
        $recordPerPage = RecordPerPage($frmdata['record']);
    // echo $recordPerPage;
        
        //print_r($frmdata[0]->recordPerPage);
        //if page number not set or search done
        if (!isset($frmdata['pageNumber']))
        {
            $frmdata['pageNumber'] = 1;
        } //!isset($frmdata['pageNumber'])
        if ($recordPerPage != 'All')
        {
            //at first page 
            if ($frmdata['pageNumber'] == 1)
            {
                $frmdata['from'] = 0;
                $frmdata['to']   = $recordPerPage;
            } //$frmdata['pageNumber'] == 1
            //for next pages
            else
            {
                if ($frmdata['pageNumber'] <= 0)
                {
                    $frmdata['pageNumber'] = 1;
                    $frmdata['from']       = 0;
                    $frmdata['to']         = $recordPerPage;
                } //$frmdata['pageNumber'] <= 0
                else
                {
                    $frmdata['from'] = $recordPerPage * (((int) $frmdata['pageNumber']) - 1);
                    $frmdata['to']   = $recordPerPage;
                }
                
                
                //echo $frmdata['pageNumber']."<br>";
                
            }
            
        } //$recordPerPage != 'All'
        else
        {
            //$frmdata['from']=0;
            //$frmdata['to']=9999999999999999999999999999999;
        }
    }
    /*###########################################################################################
    @Auther : Niteen Acharya
    @para	: int (total number of record)
    @return : ----
    @Des 	: to show pagination at the bottom of the page
    //###########################################################################################*/
    
    function PaginationDisplay123($totalCount, $option = '')
    {
        //echo $totalCount;
        // echo $option='';
        global $frmdata;
        //	print_r($frmdata);
        //	echo $totalCount;
        $recordPerPage = RecordPerPage($frmdata['record']);
        // echo $recordPerPage;
        if ($recordPerPage != 'All')
        {
            if ($totalCount > $recordPerPage)
            {
                echo '<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="fontstyle" >
				  <tr valign="bottom" > 
					<td width="70" height="5" align="left">';
                //===============================================	============================
                //LINK FOR PREVIOUS PAGE.....
                //===========================================================================
                //===========================================================================
                if ($frmdata['from'] > 0)
                {
                    echo '<a href="javascript:PrePage();" ><img border="0" src="' . IMAGEURL . '/previous.gif"></a>';
                    //Added by richa verma on 12 may 2011 to show first data
                    
                    /*echo'</td><td width="70" height="5" align="right" valign="bottom"><a href="javascript:setvalue(document.frmlist,document.frmlist.pageNumber,1);" ><img border="0" src="'.IMAGEURL.'/first.gif"></a>';	*/
                    
                } //$frmdata['from'] > 0
                
                echo '</td>
							<td>&nbsp;</td>';
                
                //==============================================================================
                //INDIVIDUAL PAGE LINKS.....
                //==============================================================================
                //==============================================================================
                
                $i = 1;
                $j = $frmdata['pageNumber'];
                if ($j >= 10)
                    $i = $j - 9;
                if ($totalCount > 2 * $recordPerPage)
                {
                    //=========================================================================================================================
                    //=========================================================================================================================
                    for (; $i <= 10 + $frmdata['pageNumber'] && ($totalCount > ($i - 1) * $recordPerPage); $i++)
                    {
                        if ($i == $frmdata['pageNumber'])
                        {
                            echo '<a onmouseover="this.style.cursor=\'hand\'" >
							<td height="20" width="23" align="center" valign="middle" onmouseover="this.style.cursor=\'hand\'" style="color:#50BFC9" class="fontstyle">';
                            
                            echo ($i);
                            echo '</td> </a> <td  width="1" valign="middle" >|</td>';
                            
                        } //$i == $frmdata['pageNumber']
                        else
                        {
                            echo '<td height="10" width="23"  valign="middle" align="center"class="fontstyle">';
                            echo '<a href="javascript:setvalue(document.frmlist,document.frmlist.pageNumber,' . $i . ');">'. $i ;
                            
                          
                            
                            echo '</a></td><td   width="1" valign="middle">|</td>';
                            
                        }
                    } //; $i <= 10 + $frmdata['pageNumber'] && ($totalCount > ($i - 1) * $recordPerPage); $i++
                    //=========================================================================================================================
                    //=========================================================================================================================
                } //$totalCount > 2 * $recordPerPage
                $frmdata['pageNumber'] = $j;
                
                echo '<td>&nbsp;</td>';
                echo '<td  width="50" height="5" align="right" valign="top">&nbsp; ';
                
                //==============================================================================
                //NEXT PAGE LINK.....
                //==============================================================================
                //==============================================================================
                //print_r($frmdata);	
                if ($totalCount > ($frmdata['from'] + $frmdata['to']))
                {
                    //Added by Richa Verma on 12 may 2011 to show last data
                    
                    /*
                    $count=$totalCount/$recordPerPage;
                    if($pos = strpos($count, '.'))// if return value in float
                    {
                    $pagelast=explode('.',$count);
                    
                    if ($pagelast[0]<$count)
                    {
                    $plast=$pagelast[0]+1;
                    }
                    }	
                    else 
                    {	
                    $plast=$count;
                    }	
                    echo '<a href="javascript:setvalue(document.frmlist,document.frmlist.pageNumber,'.$plast.')" ><img border="0" src="'.IMAGEURL.'/last.gif"></a></td><td width="50" height="5" align="right" >';
                    */
                    
                    echo '<a href="javascript:NextPage();" ><img border="0" src="' . IMAGEURL . '/next.gif"></a>';
                    
                    
                } //$totalCount > ($frmdata['from'] + $frmdata['to'])
                
                echo '</td>
				  </tr>
				</table>';
                
            } //$totalCount > $recordPerPage
        } //$recordPerPage != 'All'
        
    }
	function PaginationDisplay($totalCount, $option = '')
	{
    //echo $totalCount;
    // echo $option='';
    global $frmdata;
    //	print_r($frmdata);
    //	echo $totalCount;
    $recordPerPage = RecordPerPage($frmdata['record']);
//     echo $frmdata['pageNumber'];

        if ($recordPerPage != 'All')
    	{

        if ($totalCount > $recordPerPage)
        {

            echo '<ul class="pagination">';

            $i = 1;
            $j = $frmdata['pageNumber'];
            if ($j >= 10)
                $i = $j - 9;
            if ($totalCount > 2 * $recordPerPage)
            {
//                echo 99888; exit;
                //=========================================================================================================================
                //=========================================================================================================================
                for (; $i <= 10 + $frmdata['pageNumber'] && ($totalCount > ($i - 1) * $recordPerPage); $i++)
                {
                    if ($i == $frmdata['pageNumber'])
                    {
                        echo '<li class="active"><a onmouseover="this.style.cursor=\'hand\'" >';
                        echo ($i);
						echo '</li>';
                    }
                    else
                    {
                        echo '<li>';
                        echo '<a href="javascript:setvalue(document.frmlist,document.frmlist.pageNumber,' . $i . ');">'. $i ;



                        echo '</a></li>';

                    }
                } //; $i <= 10 + $frmdata['pageNumber'] && ($totalCount > ($i - 1) * $recordPerPage); $i++
                //=========================================================================================================================
                //=========================================================================================================================
            } //$totalCount > 2 * $recordPerPage
            $frmdata['pageNumber'] = $j;


        } //$totalCount > $recordPerPage
    } //$recordPerPage != 'All'

}
    
    function getoptions($tablename)
    {
        //echo 'SELECT * from '.$tablename;
        $result = RunSelectQuery('SELECT * from ' . $tablename);
        return $result;
    }
    function CreateLog($log_msg)
    {
        $log_time = date('Y-m-d h:i:s A');
        log_data($log_msg.'[ '. $log_time .' ].');
    }
	function log_data($log_msg)
	{
		$log_filename = "log";
		$desired_dir= ROOT."/log";

		if (!is_dir($log_filename))
		{
			mkdir($desired_dir);
			shell_exec('chmod -R 777 '.$desired_dir);
//            chmod("$desired_dir", 0777);
		}

		$log_file_data = $desired_dir.'/log_' . date('d-M-Y') . '.log';

		file_put_contents($log_file_data, $log_msg. "\n\n", FILE_APPEND);
	}
    function getDeviceInfo($device_id)
    {
        $result = RunSelectQuery('SELECT * from device left join manufacturers as make on  make.id=device.make left join models on models.id=device.model where device.id="' . $device_id . '"');
        return $result;
    }
    
    function deleteDevice($device_id)
    {
        $components = RunSelectQuery('SELECT * from device_components where id="' . $device_id . '"');
        
        for ($i = 0; $i < count($components); $i++)
        {
            $keyFactors = RunSelectQuery('delete from device_component_key_factors where device_id="' . $device_id . '" and component_id=' . $components[$i]->component_id);
        } //$i = 0; $i < count($components); $i++
        
        RunSelectQuery('delete  from device_components where device_id="' . $device_id . '"');
        return true;
    }
      
    function selfURL()
    {
        $s        = empty($_SERVER["HTTPS"]) ? '' : ($_SERVER["HTTPS"] == "on") ? "s" : "";
        $protocol = strleft(strtolower($_SERVER["SERVER_PROTOCOL"]), "/") . $s;
        $port     = ($_SERVER["SERVER_PORT"] == "80") ? "" : (":" . $_SERVER["SERVER_PORT"]);
        return $protocol . "://" . $_SERVER['SERVER_NAME'] . $port . $_SERVER['REQUEST_URI'];
    }
    
    function strleft($s1, $s2)
    {
        return substr($s1, 0, strpos($s1, $s2));
    }
    
    function getBackurl()
    {
        if (selfURL() != $_SERVER['HTTP_REFERER'])
        {
            $_SESSION['gotoback']        = $_SERVER['HTTP_REFERER'];
            $_SESSION['gotobackCounter'] = 0;
        } //selfURL() != $_SERVER['HTTP_REFERER']
        
        return $_SESSION['gotoback'];
    }
    /****************************************************************************************************************************
    function: getPageRecords()
    paran :
    Return : Combo box of  Page Record
    Purpose: to add feature, Number of row per page 
    Date: 17-02-2009
    Author :Shakti Singh Bhati
    *****************************************************************************************************************************/
    function getPageRecords()
    {
        global $frmdata;
        //$output = 'Show:';
        $output = '<select class="form-control show-result" name="record" id="record" onchange="return FormRecord()" class="rounded">';

        $output .= '<option selected="selected" value="">Show</option>';

       /* $output .= '<option value="10"';
        if (!isset($frmdata['record']) || $frmdata['record'] == "10")
            $output .= 'selected';
        $output .= '>10</option>';*/

        $output .= '<option value="10"';
        $output .= '>10</option>';


        $output .= '<option value="20"';
        if ($frmdata['record'] == '20')
            $output .= 'selected';
        $output .= '>20</option>';
        $output .= '<option value="50"';
        if ($frmdata['record'] == '50')
            $output .= ' selected ';
        $output .= '>50</option>';
        $output .= '<option value="100"';
        if ($frmdata['record'] == '100')
            $output .= ' selected ';
        $output .= '>100</option>';
        $output .= '<option value="All"';
        if ($frmdata['record'] == 'All')
            $output .= ' selected ';
        $output .= '>All</option>';
        $output .= '</select>';
        return $output;
    }
    
    /****************************************************************************************
     * 			Added By :		Ashwini Agarwal
     * 			Date :			March 05, 2012
     * 			Description :	To highlight searched string. (style class 'highlightSubString' is defined in 'header.php')
     */
    function highlightSubString($sub_str, $str)
    {
        $result = '';
        $len    = strlen($sub_str);
        if ($len > 0)
        {
            $found = stripos($str, $sub_str);
            $last  = 0;
            
            while ($found !== false)
            {
                $result .= substr($str, $last, ($found - $last));
                $result .= "<label class = 'highlightSubString'>";
                $result .= substr($str, $found, $len);
                $result .= "</label>";
                
                $last  = $found + $len;
                $found = stripos($str, $sub_str, $found + 1);
            } //$found !== false
            $result .= substr($str, $last);
        } //$len > 0
        else
        {
            $result = $str;
        }
        return ($result);
    }
    
    
    function deleteComponent($id)
    {
        $error      = 0;
        $select     = "SELECT * from device_components where id ='$id' ";
        $components = RunSelectQuery($select);
        //print_r($components);
        for ($i = 0; $i < count($components); $i++)
        {
            $cond_device_components = " id = '$id' ";
            $condValues             = "device_id='" . $components[$i]->device_id . "' and component_id='" . $components[$i]->component_id . "'";
            
            if (!DeleteRecord("device_components", $cond_device_components))
                $error = 1;
            if (!DeleteRecord("device_component_key_factors", $condValues))
                $error = 1;
        } //$i = 0; $i < count($components); $i++
        if ($error == 0)
            return true;
        else
            return false;
    }
    
   
    //================== password generator ==================
    
    function MakeNewpassword($lenth = 8)
    {
        $s = rand(time() % 357, time());
        srand($s);
        $san  = rand((time() - $s), time());
        $san1 = rand((time() - 4322), time());
        $san2 = encrypt($san . $san1, time());
        $pass = encrypt(time(), $san2);
        return substr($pass, 0, $lenth);
    }
    
    //=====================FUNCTION END============\\
    //====FUNCTION TO ENCRPT THE PASSED STRING==================	
    function encrypt($password, $strtoencrypt)
    {
        $ralphabet    = "qKxy67RmUnMoJpSLrZ9sEdDe3fPgYhHw2G8iIt5BuFvQ1VaTb4NcACjkXOlzW";
        $alphabet     = $ralphabet . $ralphabet;
        $strtoencrypt = str_replace("\t", "[tab]", $strtoencrypt);
        $strtoencrypt = str_replace("\n", "[new]", $strtoencrypt);
        $strtoencrypt = str_replace("\r", "[ret]", $strtoencrypt);
        
        for ($i = 2; $i < strlen($password); $i++)
        {
            $cur_pswd_ltr    = substr($password, $i, 1);
            $pos_alpha_ary[] = substr(strstr($alphabet, $cur_pswd_ltr), 0, strlen($ralphabet));
        } //$i = 2; $i < strlen($password); $i++
        
        $i                = 0;
        $n                = 0;
        $nn               = strlen($password) - 2;
        $c                = strlen($strtoencrypt);
        $encrypted_string = '';
        
        while ($i < 40)
        {
            $encrypted_string .= substr($pos_alpha_ary[$n], strpos($ralphabet, substr($strtoencrypt, $i, 1)), 1);
            // echo  $encrypted_string."<br>";
            $n++;
            if ($n == $nn)
                $n = 0;
            $i++;
            if (strlen($encrypted_string) == 8)
                break;
            if ($i == 40)
                $i = rand(13, 37);
        } //$i < 40
        return $encrypted_string;
    }
	function validateimage($img)
	{
			$allowedExts = array("jpg", "jpeg", "gif", "png");
			$extension = end(explode(".",$img));
			return in_array($extension, $allowedExts);
	}
	function validate_lang_file($img)
	{
        $allowedExts = array("json");
        $extension = end(explode(".",$img));
        return in_array($extension, $allowedExts);
    }
	function downloadFile($file)
	{
//		echo '<pre>'; print_r($file);exit;

        header("Pragma: public");
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Content-Type: application/json");
		header("Content-Disposition: attachment; filename=sample.json;" );
		header("Content-Transfer-Encoding: binary");
		readfile($file);
		exit;

	}
	function validateEmail($email)
    {
        return preg_match("/^([a-zA-Z0-9])+([.a-zA-Z0-9-])*@([a-zA-Z0-9])+(\.[A-Za-z0-9-]+)+([a-zA-Z0-9-])$/", $email);
    }
	function validatename($name)
    {
		//  return preg_match("/^[a-zA-Z ]*$/",$name);
		return preg_match("/^[A-Za-z0-9-_,\s]+$/ ",$name);
  
    }
	/********Added by Shivender***********/
	function validatecompanyname($name)
    {
		//  return preg_match("/^[a-zA-Z ]*$/",$name);
		return preg_match("/^[A-Za-z-().,\s]+$/ ",$name);
  
    }
     function validatenum($num)
    {
		// return preg_match('/([0-9])/',$num);
		//preg_match("/^([1]-)?[0-9]{3}-[0-9]{3}-[0-9]{4}$/i", $num);
		// return preg_match('/^[0-9]+$/',$num);
		return preg_match('/^(?:\d+|\d*\.\d+)$/',$num);
		//return is_numeric($num);
    }

    function validatePhNum($num)
    {	
    	if(preg_match('/^(?:\d{10})$/',$num))
		{
			return preg_match('/^(?:\d{10})$/',$num);
		}
		else//added by : Neha Pareek, Dated:7/12/15 (for Landline no. validation)
		{ if(preg_match(' /^(\()?\d{3}(\))?(-|\s)?\d{3}(-|\s)\d{4}$/',$num))
			return preg_match(' /^(\()?\d{3}(\))?(-|\s)?\d{3}(-|\s)\d{4}$/',$num);
		}		
    		
    }
		
	function validatedate($date)
    
	{
	   return preg_match("/^(\d{4})-(\d{2})-(\d{2})$/",$date);
	  
	} 
	function validatespace($name)
	{
		return preg_match('\s*', $name);
	}
	// check if URL address syntax is valid (this regular expression also 	allows dashes in the URL)
	function validateurl($url)
	{
		$website = $url;
		return preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i",$website);
	}

    function checkConectedEntity($parametersArr)
    {
        //print_r($parametersArr);
        $connected_flag          = false;
        $connected_items         = array();
        $connected_items_massage = array();
        $connected_All_Arr       = array();
        
        
        //Check to device is connected to component.
		if($parametersArr['entity'] == 'device')
		{
		        $Components =''; 
		        $device              = SelectRecords("device", "id='" . $parametersArr['device_id'] . "'");
		        $device_name = $device[0]->device_id;
		        $device_id   = $device[0]->id;

		        $componentList = getComponentList($device_id);
		        for($i=0;$i<count($componentList) and $componentList;$i++)
		        {
		        	$connected_items[$device_name][$i]=$componentList[$i]->component_name ;
		        	$connected_flag = true;
		        }

		}
		else //else is for $parametersArr['entityToCheck'] == 'manufacturer' to check.
		{
		
		
		        $make              = SelectRecords("manufacturers", "id='" . $parametersArr['manufecturer_id'] . "'");
		        $manufacturer_name = $make[0]->company_name;
		        $manufacturer_id   = $make[0]->id;
		        $connectedOld      = $make[0]->manufacturer_for;
		        $connectto         = $parametersArr['connectto'];
		        $connected_items_massage[0] .= "The following entities are still connected to <b>$manufacturer_name</b>.Please remove them first.";
		        
		        if ($connectedOld != $connectto and $connectto != 'all')
		        {
		            
		            switch ($connectedOld)
		            {
		                case $connectedOld:
		                    $runAll = 1;
		                case 'device':
		                    if (trim($connectto) != 'device')
		                    {
		                        $connected_items = array();
		                        
		                        $query      = "select * from device d where d.make = '$manufacturer_id' ";
		                        $devicelist = RunSelectQuery($query);
		                        if ($devicelist and count($devicelist) > 0)
		                        {
		                            for ($i = 0; $i < count($devicelist) and $devicelist; $i++)
		                            {
		                                $connected_items[] = $devicelist[$i]->device_id;
		                                $connected_flag    = true;
		                            }
		                            
		                        } 
		                        
		                        
		                        $devices = implode(", ", $connected_items);
		                        if (trim($devices) != '')
		                            $connected_items_massage[0] .= "<br>Devices : <b>$devices </b>";
		                        
		                        if ($runAll == 0)
		                            break;
		                    } //trim($connectto) != 'device'
		                case 'component':
		                    if (trim($connectto) != 'component')
		                    {
		                        $connected_items = array();
		                        
		                        $component_names          = '';
		                        $query                    = "SELECT cl.component_name,cl.component_id,d.device_id 
															FROM `component_key_factor` ckf
																left join component_list cl on cl.component_id = ckf.component_id
																left join device_component_key_factors dckf on dckf.key_factor_id = ckf.id and dckf.component_id = ckf.component_id
																left join device d on d.id = dckf.device_id
															where trim(ckf.key_factor_title) like 'Manufacturer' and 
															dckf.key_factor_value='$manufacturer_id'
															group by dckf.component_id,dckf.device_id"; 
		                        $connected_component_list = RunSelectQuery($query);
		                        for ($i = 0; $i < count($connected_component_list) and $connected_component_list; $i++)
		                        {
		                            $connected_items[] = "<b>".$connected_component_list[$i]->component_name . "</b> Under device <b>" . $connected_component_list[$i]->device_id."</b>";
		                            $connected_flag    = true;
		                        } 
		                        $component_names = trim($component_names, ",");
		                        
		                        $Components .= implode(",<br>", $connected_items);
		                        if (trim($Components) != '')
		                            $connected_items_massage[0] .= "<br><table class=plaintable><tr><td valign=top>Components :</td><td>$Components </td></tr></table>";
		                        break;
		                    } 
		            } //$connectedOld
		            
		        } //$connectedOld != $connectto and $connectto != 'all'
		        
		}
        

        
        
        
        $connected_All_Arr['items']   = $connected_items;
        $connected_All_Arr['massage'] = $connected_items_massage;
        if ($connected_flag)
            return $connected_All_Arr;
        else
            return false;
        
    }
		
	function getPermission($user_id)
	{
		 $query = 'SELECT * FROM module m LEFT JOIN rolepermission rp 
					  ON m.moduleID = rp.moduleID
					  LEFT JOIN role r ON rp.roleID = r.roleID
					  LEFT JOIN permission p ON rp.permissionID = p.permissionID
					  LEFT JOIN users c ON rp.roleID = c.role_id
					  WHERE m.moduleID in (6,7) and p.permissionName="A"
					  AND c.user_id="'.$user_id.'" ';
					 $result = RunSelectQuery($query);	
					 return $result;
	}
	function DeletePermission($user_id,$module)
	{
		$query = 'SELECT * FROM module m LEFT JOIN rolepermission rp 
					  ON m.moduleID = rp.moduleID
					  LEFT JOIN role r ON rp.roleID = r.roleID
					  LEFT JOIN permission p ON rp.permissionID = p.permissionID
					  LEFT JOIN users c ON rp.roleID = c.role_id
					  WHERE m.moduleName ="'.$module.'" and p.permissionName="D"
					  AND c.user_id="'.$user_id.'" ';
	   $result = RunSelectQuery($query);	
		if(empty($result))
		return true;
		else
		return false;
	}


	function getConnectedMakeAndModel($paramArr)
    {
        $return_flag     = false;
        $connetctedItems = array();
        
        if ($paramArr['entity'] == 'manufacturer')
        {
            $selectDevice    = "select d.id,d.device_id from device d where d.make = '" . $paramArr['manufecturer_id'] . "'";
            $selectComponent = "select  dc.component_id,
                            cl.component_name,
                            d.device_id
                         from device_components dc
                              left join component_list cl on dc.component_id = cl.component_id
                              left join device d on d.id = dc.device_id
                         where dc.manufacturer_id =  '" . $paramArr['manufecturer_id'] . "'";
            
            $selectModel         = "SELECT id,model_name FROM models WHERE manufacturer_id = '" . $paramArr['manufecturer_id'] . "'";
            $connectedDevice     = RunSelectQuery($selectDevice);
            $connectedComponents = RunSelectQuery($selectComponent);
            $connectedModel      = RunSelectQuery($selectModel);
            
            
            for ($i = 0; $i < count($connectedDevice) and $connectedDevice; $i++)
            {
                $connetctedItems['device'][] = $connectedDevice[$i]->device_id;
                $return_flag                 = true;
            }
            
            for ($i = 0; $i < count($connectedComponents) and $connectedComponents; $i++)
            {
                $connetctedItems['component'][] = $connectedComponents[$i]->component_name . " Under device " . $connectedComponents[$i]->device_id;
                $return_flag                    = true;
            }
            
            for ($i = 0; $i < count($connectedModel) and $connectedModel; $i++)
            {
                $connetctedItems['model'][] = $connectedModel[$i]->model_name;
                $return_flag                = true;
            }
            
        }
        else if ($paramArr['entity'] == 'model')
        {
            $selectDevice    = "select d.id,d.device_id from device d where d.model = '" . $paramArr['model_id'] . "'";
            $selectComponent = "select  dc.component_id,
                            cl.component_name,
                            d.device_id
                         from device_components dc
                              left join component_list cl on dc.component_id = cl.component_id
                              left join device d on d.id = dc.device_id
                         where dc.model_id =  '" . $paramArr['model_id'] . "'";
            
            $connectedDevice     = RunSelectQuery($selectDevice);
            $connectedComponents = RunSelectQuery($selectComponent);
            
            
            for ($i = 0; $i < count($connectedDevice) and $connectedDevice; $i++)
            {
                $connetctedItems['device'][] = $connectedDevice[$i]->device_id;
                $return_flag                 = true;
            }
            
            for ($i = 0; $i < count($connectedComponents) and $connectedComponents; $i++)
            {
                $connetctedItems['component'][] = $connectedComponents[$i]->component_name . " Under device " . $connectedComponents[$i]->device_id;
                $return_flag                    = true;
            }
            
        }
        
        
        if ($return_flag)
            return $connetctedItems;
        else
            return false;
        
    }


    
    function getComponentUnderDevice()
    {
       $select = "select d.id as devid,
				        cl.component_name,
				        count(dc.component_id) as totalComponent
				 from device d
				      left join device_components dc on dc.device_id = d.id
				      left join component_list cl on cl.component_id = dc.component_id
				 group by devid ";
       $list = RunSelectQuery($select);
       $componentCountArr = array();
       for($i=0; $i < count($list) and $list;$i++)
       {
         $componentCountArr[$list[$i]->devid] = $list[$i]->totalComponent;
       }
       return $componentCountArr;
    }

	function validate_alphanumeric_underscore($str) 
	{
		return preg_match('/^[A-Za-z][A-Za-z0-9 ]*(?:_ [A-Za-z0-9 ]+)*$/',$str);
	}
	function validate_alpha($str) 
	{
		return preg_match('/^[A-Za-z ][A-Za-z ]*(?:_ [A-Za-z]+)*$/',$str);
	}
	function validate_minStrLen($str,$field)
	{
		if(strlen($str)<3)
		$msg="Enter ".$field." atleast 3 characters long.";
	}
	function CheckconnectedDevice($id)
	{
		$select="select d.device_id from device_component_key_factors as df
			 left join device d on d.id =df.device_id where component_id='".$id."' group by df.device_id";
			$result = RunSelectQuery($select);
			return $result;
	}


	
	function upload_file($file,$Params)
	{
			$imgname    = $file['name'];
			$imgsize    = $file['size'];
			$imgtmpname = $file['tmp_name'];
			$extention  =  substr($file['name'],strpos($file['name'],".")+1,strlen($file['name']));
			$allowed_extentionArr = explode(",",$Params['allowed_extention']);
	
			if($Params['use_name'] != '')
				$target_path = $Params['target_path']."/" . $Params['use_name'] ;
			elseif($Params['use_same_name'] == true)
				$target_path = $Params['target_path']."/".$imgname;
			else
				$target_path = $Params['target_path']."/".time()."_".$imgname;    	
			
			/*echo $imgtmpname;
			echo "<br>";
			echo $target_path;*/
			
			if(! in_array($extention,$allowed_extentionArr))
				return "Please select (.".$Params['allowed_extention'].") files only.";
				
			if(move_uploaded_file($imgtmpname, $target_path))
				return true;
			else
				return false;
	}
	
	//Function added by bajrang
	function listFolderFiles($dir,$exclude)
	{
		$fileArr = array();
		$ffs = scandir($dir);
		//echo '<ul class="ulli">';
		foreach($ffs as $ff)
		{
			if(is_array($exclude) and !in_array($ff,$exclude)){
				if($ff != '.' && $ff != '..'){
				if(!is_dir($dir.'/'.$ff)){
					//echo '<li><a href="edit_page.php?path='.ltrim($dir.'/'.$ff,'./').'">'.$ff.'</a>';
					$fileArr[]=$ff;
				} else {
				//echo '<li>'.$ff;
				$fileArr[]=$ff;   
				}
				if(is_dir($dir.'/'.$ff)) listFolderFiles($dir.'/'.$ff,$exclude);
				//echo '</li>';
				}
			}
		}
		//echo '</ul>';
		
		return $fileArr;
	}
	function get_language_short_code($arr)
	{

		$get_lang_all = [];
		foreach($arr as $arr_key => $arr_value)
		{
			$get_lang_all[] = $arr_value->short_code;
		}

		return $get_lang_all;
	}

	function is_assign_users()
	{
        $DBFilter = new DBFilter();
        $check_assign_users = $DBFilter->RunSelectQuery("select assign_users from feedback_form_settings where company_id=".$_SESSION['company_id']);
        $is_assign_user_or_not = current(current($check_assign_users))->assign_users;
        return $is_assign_user_or_not;
	}
	function number_of_questions()
	{
        $DBFilter = new DBFilter();
        $number_question = $DBFilter->RunSelectQuery("select * from feedback_questions where company_id=".$_SESSION['company_id']);
//        echo '<pre>'; print_r($number_question); exit;
        $count = current(current($number_question))->id;
        return $count;
	}
	function is_fault_available_for_company()
	{
        $DBFilter = new DBFilter();
        $company_id = $_SESSION['company_id'];
        $between_date = check_date_today_yesterday_or_other();


        $fault_based_result = $DBFilter->RunSelectQuery("select flt.fault_name,flt.fault_id, count(rating) as feedback_count from feedback as fd INNER JOIN faults flt on fd.fault_id = flt.fault_id where fd.is_deleted = 'N' and fd.is_active= 'Y' AND fd.company_id='" .$company_id . "' and $between_date GROUP BY flt.fault_name,flt.fault_id");

		$count_faults = count(current($fault_based_result));
//        echo '<pre>'; print_r($count_faults); exit;

        return $count_faults;
	}
	function weekOfMonth($date) {
		//Get the first day of the month.
		$firstOfMonth = strtotime(date("Y-m-01", $date));
		//Apply above formula.
		return intval(date("W", $date)) - intval(date("W", $firstOfMonth)) + 1;
	}
	function get_weekly_interval_format($date) {
		$number_of_week = weekOfMonth(strtotime($date));
		$number_of_week = '0'.$number_of_week;
		$explode_created_at = explode('-',$date);
		return $finalweek_column_data = $explode_created_at[0].'-'.$explode_created_at[1].'-'.$number_of_week;
	}
	function is_rating_option_available()
	{
		$DBFilter = new DBFilter();
		$company_id = $_SESSION['company_id'];
        $get_option_details = $DBFilter->RunSelectQuery("select ro.title as title,ro.id as rtoid,ro.option_sequence  from feedback_questions as fq 
INNER JOIN feedback_question_options as fqo ON fqo.question_id =  fq.id 
INNER JOIN rating_options as ro ON fqo.option_id = ro.id WHERE ro.feedback_type ='rating' AND fq.feedback_type='rating' AND  fq.company_id=" . $company_id. " GROUP BY title,rtoid ORDER BY ro.option_sequence DESC");

		return $get_option_details;
	}
	function get_feedback_setting_detail($field_name, $feedback_company_id)
	{
        $DBFilter = new DBFilter();
        $check_assign_users = $DBFilter->RunSelectQuery("select $field_name from feedback_form_settings where company_id=".$feedback_company_id);
        $feedback_details = current(current($check_assign_users))->$field_name;
        return $feedback_details;
	}
	function is_single_question()
	{
        $DBFilter = new DBFilter();
        $is_single_question = $DBFilter->RunSelectQuery("select count(*) as total_que from feedback_questions where company_id=".$_SESSION['company_id']);
        $number_of_que = current(current($is_single_question))->total_que;
//        echo '<pre>'; print_r($number_of_que); exit;
        return $is_single_question;
	}

	//###########################################################################
	function easy_crypt($string, $key)
	{
	  $iv_size = mcrypt_get_iv_size(MCRYPT_BLOWFISH, MCRYPT_MODE_CBC); 
	   $iv = mcrypt_create_iv($iv_size, MCRYPT_DEV_URANDOM);
	  $string = mcrypt_encrypt(MCRYPT_BLOWFISH, $key,
								$string, MCRYPT_MODE_CBC, $iv);
	
	   return array(base64_encode($string), @base64_encode($iv));
	   //exit;
	}
	//###########################################################################
	//.....function to decrypt any string.................................
	//###########################################################################
	function easy_decrypt($cyph_arr, $key)
	{
	   $out = @mcrypt_decrypt(MCRYPT_BLOWFISH, $key, @base64_decode($cyph_arr[0]),
							 MCRYPT_MODE_CBC, @base64_decode($cyph_arr[1]));
	
	   return trim($out);
	}
	
	function Encode($value)
	{
		return base64_encode($value);
	}

function 	send_mail_to_company_admin_cleaner_supervisor($user_email,$passwd,$name){

    $image_email = IMAGEURL.'logo.png';
    $mail = new PHPMailer(true);

    try {
        //Server settings
        $mail->SMTPDebug = 0;                                 // Enable verbose debug output
        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
//			$mail->Username = 'johnarc5@gmail.com';               // SMTP username
        $mail->Username = 'sunarcuser@gmail.com';               // SMTP username
        $mail->Password = 'sunarc123';                           // SMTP password
        $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 587;                                    // TCP port to connect to

        //Recipients
        $mail->setFrom('from@example.com', 'Mailer');
        $mail->addAddress($user_email);     // Add a recipient
        //            $mail->addAddress('riyaz.ahamad@sunarctechnologies.com');     // Add a recipient
        //            $mail->addAddress('ellen@example.com');               // Name is optional
        //            $mail->addReplyTo('info@example.com', 'Information');
        //            $mail->addCC('cc@example.com');
        //            $mail->addBCC('bcc@example.com');

        //Attachments
        //            $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
        //            $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

        //Content
        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = 'Facility System Password';
        $mail->Body    = "<div>
									<table style='background-color:#F6F6F6;padding:40px 0px;width:100%;'>
									<thead>
						
						 </thead>
						 <tbody>
						 <tr>
						 <td>
						 <table style='max-width:660px; width: 100%; display: table; margin: 0 auto;'>
						  <tr>
						 <td>
						<img src='$image_email' alt='' style=''>
						 
						 </td>
						 </tr>
						 <br>
						 <br>
							 <tr>
							 <td style='background-color:#fff;padding:20px;'>
							 <p style='font-size:15px; margin:0px 0px 10px;'>Dear <strong>$name</strong>,</p>
							 <p style='font-size:15px; margin:0px 0px 10px;'>Your Email Id is <strong>$user_email</strong></p>
							 <p style='font-size:15px; margin:0px 0px 10px;'>Your Password is <strong>$passwd</strong></p>                         
							 <p style='font-size:15px; margin:0px 0px 10px;'>Thank you.</p>
							 </td>
							 </tr>
							 <br>
							 <br>
	</table>
	</td>
						 </tr>
						
						
						 </tbody>
							</table></div>";
        $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

        //            $mail->send();
        if(!$mail->Send()) {
            return 0;
        } else {
            return 1;
        }
        //			echo 'Message has been sent';
    } catch (Exception $e) {
        //            echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
    }
}
function send_mail($user_email,$random_key,$name){

//    echo '<pre>'; print_r($user_email); exit;

    $image_email = IMAGEURL.'logo.png';
    $mail = new PHPMailer(true);
//    $image_email = IMAGEURL.'logo.png';
//echo IMAGEURL."faults_images/no_image.gif";
    try {
        //Server settings
        $mail->SMTPDebug = 0;                                 // Enable verbose debug output
        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = 'johnarc5@gmail.com';               // SMTP username
//                    $mail->Username = 'sunarcuser@gmail.com';               // SMTP username
        $mail->Password = 'sunarc123';                           // SMTP password
        $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 587;                                    // TCP port to connect to

        //Recipients
        $mail->setFrom('from@example.com', 'Mailer');
        $mail->addAddress($user_email);     // Add a recipient
//                    $mail->addAddress('riyaz.ahamad@sunarctechnologies.com');     // Add a recipient
        //            $mail->addAddress('ellen@example.com');               // Name is optional
        //            $mail->addReplyTo('info@example.com', 'Information');
        //            $mail->addCC('cc@example.com');
        //            $mail->addBCC('bcc@example.com');

        //Attachments
        //            $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
        //            $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

        //Content
        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = 'Facility System Password';
        $mail->Body    = "	<div>
	                            <table style='background-color:#F6F6F6;padding:40px 0px;width:100%;'>
	                            <thead>
					
					 </thead>
					 <tbody>
					 <tr>
					 <td>
					 <table style='max-width:660px; width: 100%; display: table; margin: 0 auto;'>
					  <tr>
					 <td>
					 <img src='$image_email' alt='' style=''>
					 
					 </td>
					 </tr>
					 <br>
					 <br>
					 	 <tr>
						 <td style='background-color:#fff;padding:20px;'>
						 <p style='font-size:15px; margin:0px 0px 10px;'>Dear $name,</p>
						 <p style='font-size:15px; margin:0px 0px 10px;'>There was recently a request to change the password for your account.</p>
						 <p style='font-size:15px; margin:0px 0px 10px;'>If you requested this change, set a new password here:</p>
						 <p style='text-align: center;  cursor: pointer;width: 250px; margin-left: 150px;'>
						    <a href='" . SITEURL . "/index.php?mod=login&do=reset_password&key=" . $random_key . "' style='font-size:16px;
						    outline: 0 !important;box-shadow:none !important; padding: 10px 20px;
						    border-radius: 90px;display: block;margin-right: auto;margin-left: auto;
						    color: #fff;background-color: #337ab7;text-decoration: none;border-color: #337ab7;border-style: none;'>Set a New Password</a>
                         </p>
                         <p style='font-size:15px; margin:0px 0px 10px;'>If you did not make this request, you can ignore this email and your password will remain the same.</p>
                         <br>
                         <br>
                         <p style='font-size:15px; margin:0px 0px 10px;'>Thank you.</p>
						 </td>
						 </tr>
						 <br>
						 <br>
</table>
</td>
					 </tr>
					
					
				     </tbody>
						 <!--<tr >
						 <td style='padding:10px'>
							Your Email Id = $user_email
						</td>
						</tr>
						 <tr >
						 <td style='padding:10px'>
							Your Password = $passwd
						</td>
						</tr>
						<tr >
						<td style='padding:10px'>
							Thanks,
						</td>
						</tr>
						<tr >
						<td style='padding:10px'>
							The Facility System Team
						</td>
						</tr>-->
						</table></div>";

        $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

        //            $mail->send();
        if(!$mail->Send()) {
            return 0;
        } else {
            return 1;
        }
//        echo 'Message has been sent';
    } catch (Exception $e) {
        //            echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
    }
}
//function check_date_today_yesterday_or_other()
//{
//    $yesterday = date('Y-m-d', strtotime("-1 days"));
//    $today = date('Y-m-d');
//    if ($_SESSION['start_date'] == $today) {
//
//        $between_date = "(DATE(fd.created_at) ='" . $_SESSION['start_date'] . "')";
//        return $between_date;
//    } elseif ($_SESSION['start_date'] == $yesterday) {
//        $between_date = "(DATE(fd.created_at) ='" . $_SESSION['start_date'] . "')";
//        return $between_date;
//    } else {
//        $between_date = "(fd.created_at BETWEEN '" . $_SESSION['start_date'] . "' AND '" . $_SESSION['end_date'] . "')";
//        return $between_date;
//    }
//}
//function check_date_today_yesterday_or_other_without_alias()
//{
//    $yesterday = date('Y-m-d', strtotime("-1 days"));
//    $today = date('Y-m-d');
//    if ($_SESSION['start_date'] == $today) {
//
//        $between_date = "(DATE(feedback.created_at) ='" . $_SESSION['start_date'] . "')";
//        return $between_date;
//    } elseif ($_SESSION['start_date'] == $yesterday) {
//        $between_date = "(DATE(feedback.created_at) ='" . $_SESSION['start_date'] . "')";
//        return $between_date;
//    } else {
//        $between_date = "(feedback.created_at BETWEEN '" . $_SESSION['start_date'] . "' AND '" . $_SESSION['end_date'] . "')";
//        return $between_date;
//    }
//}

/*###########################################################################################
The decode function is defined in libfunc.php
//###########################################################################################*/
/*function Decode($value)
{
	return base64_decode($value);
}*/

?>





