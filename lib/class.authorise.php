<?php
/**************************************************************************
					Developer: Shakti Singh
					Date: 26-03-2012
					A class for authorization
**************************************************************************/
include_once('dbfilter.php');

class Authorise
{
	 var $read ;
	 var $add ;
	 var $edit ;	
	 var $permission;
	 
	/* Construct: map the actions with their synonyms terms used throughout the system*/
	function __construct()
	{
		$this->read = array('view');
		
		$this->list = array('list');
							 
		$this->add = array('add');
							
		$this->edit = array('edit');
		
		$this->export = array('candidate_detail', 'download','export_question');
		
		$this->delete = array('del');
	
		$this->showMessage = true;							 
	}
	
	//check if the requested action is permitted for this user to perform
	//return true if yes otherwise return false
	function isAuthorisedAction($user_id, $module, $action)
	{ 
		$DB = new DBFilter();
		//echo $user_id . $module .$action;exit;
		if (in_array($action, $this->read))
		{
			$this->permission = 'R';
		}
		else if (in_array($action, $this->list))
		{
			$this->permission = 'L';
		}
		else if (in_array($action, $this->add))
		{
			$this->permission = 'A';
		}
		else if (in_array($action, $this->edit))
		{
			$this->permission = 'E';
		}
		else if (in_array($action, $this->export))
		{
			$this->permission = 'ED';
		}
		else if (in_array($action, $this->delete))
		{
			$this->permission = 'D';
			return false;
		}
		if ($this->permission == '') //if unknown action found don't allow to access
		{
			return false;
		}
		$query = 'SELECT * FROM module m LEFT JOIN rolepermission rp 
				  ON m.module_id = rp.module_id
				  LEFT JOIN roles r ON rp.role_id = r.role_id
				  LEFT JOIN permission p ON rp.permission_id = p.permission_id ';

		if(in_array($_SESSION['usertype'], array('Super Admin', 'Company Admin')))
		{
			$roleName = ($_SESSION['usertype'] == 'Super Admin') ? 'Super Admin' : 'Company Admin'; 
			$query .= " WHERE r.role_name = '$roleName' ";
		}
		else
		{
			$query .= " LEFT JOIN users u ON rp.role_id = u.role_id WHERE u.user_id='$user_id' ";
		}
				  
		  $query .= " AND m.module_name='$module' AND p.permission_name='$this->permission' ";

		$result = $DB->RunSelectQuery($query);	
				// echo  $result[0][0]->permission_id;
		//echo '<pre>';		print_r($result);  exit;
		if (!empty($result))
		{
			//Permitted action! authorised 
			//$module_name = $this->getOneAccessibleModule($user_id);
			//return $module_name;
			$user_permission = array();
			for($i=0;$i<count($result[0]);$i++)
			{
				$user_permission[$i]['permission_name'] = $result[0][$i]->permission_name;
				$user_permission[$i]['module_name'] =  $result[0][$i]->module_name;
				if($user_permission[$i]['permission_name']=='R')
				{
					$user_permission[$i]['do'] = 'view';	
				}
				if($user_permission[$i]['permission_name']=='L')
				{
					$user_permission[$i]['do'] = 'list';	
				}
				if($user_permission[$i]['permission_name']=='E')
				{
					$user_permission[$i]['do'] = 'edit';	
				}
				if($user_permission[$i]['permission_name']=='A')
				{
					$user_permission[$i]['do'] = 'add';	
				}
				if($user_permission[$i]['permission_name']=='D')
				{
					$user_permission[$i]['do'] = 'del';	
				}
								
			}
			//print_r($user_permission);
			return $user_permission;
		}
		return false;
	}
	
	function determineRedirection($user_id, $mod, $do)
	{		
		$DB = new DBFilter();
		//$url = ''; echo $user_id. $mod. $do; exit;
		//if the Read permission is restricted for a module redirect them to the module for which the user has access		
		if ($this->permission == 'R' || $this->permission =='') 
		{
			
			/*find out at least one child or sibling module to redirect them
			if ($module_name = $this->getOneAccesibleChildORSiblingModule($user_id, $mod))
			{				
				$url = CreateURL('index.php','mod='.$module_name);
				if ($_GET['master_nav'] == 1) //if the link clicked from the master navigation 
				{
					$this->showMessage = false;
				}	
			}
			else if ($this->isModuleAccessible('dashboard',$user_id)) //if the dashboard is accessible redirect them 
			{
				 $url = CreateURL('index.php','mod=dashboard&do=showinfo');
			}*/
				$module_name = $this->getOneAccessibleModule($user_id);
				if($module_name != '')
				{
					$url = CreateURL('index.php','mod='.$module_name);
				}
				else
				{
					echo "<script>alert('You are no longer authorized to access admin section.');</script>";
					$url = CreateURL('index.php','pid=103');
				}
							
		}
		//if the Add or Edit or Export Data permission is restricted for this module redirect them to the list page of the module
		if ($this->permission == 'A' || $this->permission == 'E' || $this->permission == 'ED' || $this->permission == 'D')
		{
			$url = CreateURL('index.php','mod='.$mod); 		
		}
		return $url;
	}
	//Return the one accessible module 
	function getOneAccessibleModule($user_id)
	{
        $DB = new DBFilter();
		 $query = "select DISTINCT rp.module_id,m.module_name from rolepermission as rp 
					left join module as m on rp.module_id = m.module_id
					left join users as u on rp.role_id = u.role_id where  m.module_name in('company','user','feedback','faults','device','device_locations','role','plan','reports','language','setting','question','rating') and u.user_id=".$user_id;
		
		$result = $DB->RunSelectQuery($query);
		if($result)
		{
			return $result;
		}				  
	}
	
	
	
	//check if the module is accessible return TRUE if yes, FALSE otherwise
	function isModuleAccessible($module,$user_id)
	{
		$DB = new DBFilter();
		if($module == 'homework_history') $module = 'report';
		
		$query = 'SELECT * FROM module m LEFT JOIN rolepermission rp 
				  	ON m.module_id = rp.module_id
				  	LEFT JOIN roles r ON rp.role_id = r.role_id
				  	LEFT JOIN permission p ON rp.permission_id = p.permission_id ';
		
		if(in_array($_SESSION['usertype'], array('Super Admin', 'Company Admin')))
		{
			$roleName = ($_SESSION['usertype'] == 'Super Admin') ? 'Super Admin' : 'Company Admin'; 
			$query .= " WHERE r.role_name = '$roleName' ";
		}
		else
		{
			$query .= " LEFT JOIN users au ON rp.role_id = u.role_id WHERE u.id='$user_id' ";
		}
		
		$query .= " AND m.module_name='$module' AND p.permission_name='R' ";
		echo $query;
		$result = $DB->RunSelectQuery($query);			  
		if($result)
		{
			return true;
		}
		else 
		{
			return false;
		}		
	}
	/**/
	//Find out at least one child or sibling module which is accesible, to redirect the users
	function getOneAccesibleChildORSiblingModule($user_id, $mod)
	{
		$DB = new DBFilter();
		foreach ($this->siblingorchild as $module_group)
		{
			if (in_array($mod, $module_group))
			{
				$query = 'SELECT * FROM module m LEFT JOIN rolepermission rp 
				  			ON m.module_id = rp.module_id
				  			LEFT JOIN roles r ON rp.role_id = r.role_id
				  			LEFT JOIN permission p ON rp.permission_id = p.permission_id ';
				
				if(in_array($_SESSION['admin_user_type'], array('T', 'P')))
				{
					$roleName = ($_SESSION['admin_user_type'] == 'T') ? 'Teacher' : 'Parent'; 
					$query .= " WHERE r.roleName = '$roleName' ";
				}
				else
				{
					$query .= " LEFT JOIN admin_users au ON rp.roleID = au.role_id WHERE au.id='$user_id' ";
				}
				
				$query .= " AND m.moduleName IN ('".implode("','",$module_group)."') AND p.permissionName='R' LIMIT 1 ";
				
				$result = $DB->RunSelectQuery($query);			  
				if($result)
				{
					return $result[0]->moduleName;
				}					
			}
		}
		//nothing found accessible from child or sibling
		return false;		
	}
} //end of the class
?>