<?php
/*======================================
Developer	-	Jaishree Sahal
Module      -   Feedback
SunArc Tech. Pvt. Ltd.
======================================
******************************************************/
?>
<script>
    function Clear() {
        document.getElementById('user_id').value = '';
        document.getElementById('fault_id').value = '';
        document.getElementById('service_id').value = '';
        document.getElementById('device_id').value = '';
        document.getElementById('rating').value = '';
        document.getElementById('rating').value = '';
        document.getElementById('device_status').value = '';
        document.getElementById('status').value = '';
        //location.href="index.php?mod=company&do=add";
        return false;

    }
</script>

<section>


    <div class="col-sm-9 drop-shadow nopadding">
        <form method="post" name="add_feedback" class="form-horizontal" id="add_feedback">

            <?php
            if (isset($_SESSION['error'])) {
                echo '<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" ><tbody><tr><td colspan="6"  align="center">
                    <div class="alert alert-danger alert-dismissable">
                       <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
                echo $_SESSION['error'];
                echo '</div></td></tr></tbody></table>';
                unset($_SESSION['error']);
            }
            if (isset($_SESSION['success'])) {
                echo '<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" ><tbody><tr><td colspan="6"  align="center">
				<div class="alert alert-success alert-dismissable">
				   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
                echo $_SESSION['success'];
                echo '</div></td></tr></tbody></table>';
                unset($_SESSION['success']);
            }
            ?>
            <div class="user-heading">
                <span><?php echo $lang['Add Feedback'] ?></span>
                <?php
                include_once 'user_profile.php';
                ?>
            </div>
            <div class="userbg">
                <div id="users" class="">
                    <h4 class="update-user"><?php echo $lang['Add Feedback'] ?></h4>
                </div>
                <div class="plan-category user-page-form">

                    <?php if ($_SESSION['usertype'] == 'admin') { ?>
                        <div class="form-group">
                            <label for="RelatedTo" class="col-sm-3"><?php echo $lang['Related To'] ?>
                                <sup>*</sup></label>
                            <div class="col-sm-9">
                                <select class="form-control" name="company_id" style="width:170px;">
                                    <option value="">Please Select</option>
                                    <?php
                                    if ($_SESSION['company_id']) {
                                        $selected = 'selected';
                                    }
                                    for ($i = 0; $i < count($company[0]); $i++) { ?>
                                        <option value="<?php echo $company[0][$i]->company_id; ?>" <?php if ($_POST['company_id'] == $company[0][$i]->company_id) {
                                            echo "selected";
                                        } ?>><?php echo $company[0][$i]->company_name; ?></option>
                                    <?php }
                                    ?>
                                </select>
                            </div>
                        </div>
                    <?php } else { ?>
                        <input type="hidden" class="form-control" name="company_id"
                               value="<?php echo $company[0][0]->company_id; ?>">
                    <?php } ?>
                    <div class="form-group">
                        <label for="Role" class="col-sm-3"><?php echo $lang['Feedback'] ?>
                            <sup>*</sup></label>
                        <div class="col-sm-9">
                            <input type="text" name="service_id" class="form-control" id="service_id"
                                   readonly="readonly" value="<?php echo ucfirst($services[0][0]->service_name) ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="FirstName" class="col-sm-3"><?php echo $lang['Fault'] ?>
                            <sup>*</sup></label>
                        <div class="col-sm-9">
                            <select class="form-control" name="fault_id" id="fault_id" style="width:170px;">
                                <option value="" selected="selected">Please Select</option>
                                <?php
                                //print_r($faults); exit;

                                /*for($i=0;$i<count($faults[0]);$i++)
                                { ?>
                                <option value="<?php echo $faults[0][$i]->fault_id ;?>" <?php if($_POST['fault_id']==$faults[0][$i]->fault_id) { ?> selected="selected" <?php  } ?>><?php echo ucfirst($faults[0][$i]->fault_name) ?></option>
                                <?php
                                }*/

                                //Added By Neha Pareek. Dated : 04 Nov 2015. for current company faults
                                for ($i = 0; $i < count($faults_res[0]); $i++) { ?>
                                    <option value="<?php echo $faults_res[0][$i]->fault_id; ?>" <?php if ($_POST['fault_id'] == $faults_res[0][$i]->fault_id) {
                                        echo "selected";
                                    } ?>><?php echo ucfirst($faults_res[0][$i]->fault_name) ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="LastName" class="col-sm-3"><?php echo $lang['Device'] ?>
                            <sup>*</sup></label>
                        <div class="col-sm-4">
                            <select class="form-control" name="device_id" id="device_id" style="width:170px;">
                                <option value="" selected="selected">Please Select</option>
                                <?php
                                for ($i = 0; $i < count($location[0]); $i++) {
                                    $device_status = $location[0][$i]->device_status;
                                    ?>
                                    <option value="<?php echo $location[0][$i]->device_id; ?>" <?php if ($_POST['device_id'] == $location[0][$i]->device_id) {
                                        echo "selected";
                                    } ?>><?php echo ucfirst($location[0][$i]->device_name) ?></option>

                                <?php }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="EmailId" class="col-sm-3"><?php echo $lang['Rating'] ?>
                            <sup>*</sup></label>
                        <div class="col-sm-9">
                            <select class="form-control" name="rating" id="rating" style="width:170px;">
                                <option value="" selected="selected">Please Select</option>
                                <option value="Excellent" <?php if ($_POST['rating'] == 'Excellent') {
                                    echo "selected";
                                } ?>>Excellent
                                </option>
                                <option value="Good" <?php if ($_POST['rating'] == 'Good') {
                                    echo "selected";
                                } ?>>Good
                                </option>
                                <option value="Average" <?php if ($_POST['rating'] == 'Average') {
                                    echo "selected";
                                } ?>>Average
                                </option>
                                <option value="Poor" <?php if ($_POST['rating'] == 'Poor') {
                                    echo "selected";
                                } ?>>Poor
                                </option>
                                <option value="Very Poor" <?php if ($_POST['rating'] == 'Very Poor') {
                                    echo "selected";
                                } ?>>Very Poor
                                </option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="Password" class="col-sm-3"><?php echo $lang['Status'] ?>
                            <sup>*</sup></label>
                        <div class="col-sm-9">
                            <select class="form-control" name="status" id="status" style="width:170px;">
                                <option value="" selected="selected">Please Select</option>
                                <option value="1" <?php if ($_POST['status'] == '1') {
                                    echo "selected";
                                } ?>>Completed
                                </option>
                                <option value="0" <?php if ($_POST['status'] == '0') {
                                    echo "selected";
                                } ?>>Pending
                                </option>

                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="Password" class="col-sm-3"><?php echo $lang['Assigned To'] ?>
                            <sup>*</sup></label>
                        <div class="col-sm-9">
                            <select class="form-control" name="assigned_to" id="assigned_to" style="width:170px;">
                                <option value="" selected="selected">Please Select</option>
                                <?php
                                for ($i = 0; $i < count($users[0]); $i++) //loop for supervisors
                                {
                                    $usr = $users[0][$i]->user_id;
                                    ?>
                                    <!-- create optgroup of supervisers -->
                                    <optgroup
                                            label="<?php echo ucfirst($users[0][$i]->first_name . ' ' . $users[0][$i]->last_name); ?>">
                                        <?php
                                        for ($j = 0; $j < count($cleaner[0]); $j++)//loop for cleaners
                                        {
                                            if ($cleaner[0][$j]->assigned_to == $usr) { //create options list of cleaners
                                                ?>
                                                <option value="<?php echo $cleaner[0][$j]->user_id; ?>" <?php if ($_POST['user_id'] == $cleaner[0][$j]->user_id) {
                                                    echo "selected";
                                                } ?>><?php echo ucfirst($cleaner[0][$j]->first_name . ' ' . $cleaner[0][$j]->last_name); ?></option>
                                            <?php }
                                        }
                                        ?>
                                    </optgroup>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-12">
                            <button type="submit" id="add_feedback" name="add_feedback"
                                    class="btn btn-danger add-company pull-right"><?php echo $lang['Add Feedback'] ?>
                            </button>

                        </div>
                    </div>

                </div>
            </div>
        </form>
    </div>


</section>