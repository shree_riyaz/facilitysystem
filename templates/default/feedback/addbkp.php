<?php
	/*======================================
	Developer	-	Jaishree Sahal
	Module      -   Feedback
	SunArc Tech. Pvt. Ltd.
	======================================		
	******************************************************/
?>
<script>
function Clear()
{
	document.getElementById('user_id').value='';
	document.getElementById('fault_id').value='';
	document.getElementById('service_id').value='';
	document.getElementById('device_id').value='';
	document.getElementById('rating').value='';
	document.getElementById('rating').value='';
	document.getElementById('device_status').value='';
	document.getElementById('status').value='';
	//location.href="index.php?mod=company&do=add";
	return false;	
	
}
</script>
<form method="post" name="add_feedback" id="add_feedback" enctype="multipart/form-data">
<center>
	<?php 
			
			if(isset($_SESSION['error']))
			{
				echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" ><tbody><tr><td colspan="6"  align="center">
				<div class="alert alert-danger alert-dismissable">
				   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
				  echo $_SESSION['error'];
				echo '</div></td></tr></tbody></table><br>';
				unset($_SESSION['error']);
			}
			if(isset($_SESSION['success']))
			{
				echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" ><tbody><tr><td colspan="6"  align="center">
				<div class="alert alert-success alert-dismissable">
				   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
				echo $_SESSION['success'];
				echo '</div></td></tr></tbody></table><br>';
				unset($_SESSION['success']);
			}
			?>

 <table width="60%"  border="0" align="center" cellpadding="0" cellspacing="0" class="table table-bordered">
    <tbody><tr valign="middle" align="center"> 
      <th height="30" class="thColor" colspan="2" style="padding-left: 5px;"><font color="#FFFFFF"><?php echo $lang['Add Feedback']?></font></th>
    </tr>
	<tr>
		<td   colspan="2"  style="font-size:9px"align="right"  class="fontstyle"><?php echo $lang['All fields are mandatory']?></td>
	</tr>
	<?php if ($_SESSION['usertype'] == 'admin') { ?>
	<tr> 
		<td align="right" class="fontstyle" width="30%"> <label for="subscription_plan" class="control-label col-xs-10"><?php echo $lang['Related To'].MANDATORYMARK?></label></td>
		<td align="left">
		<div class="col-xs-4">
		
		<select class="form-control" name="company_id">
		<option value="">Please Select</option>
		<?php 
		if($_SESSION['company_id'])	
		{
			$selected = 'selected';
		}
		for($i=0;$i<count($company[0]);$i++)
		{ ?>
			<option value="<?php echo $company[0][$i]->company_id;?>" <?php if($_POST['company_id']==$company[0][$i]->company_id) { ?> selected="selected" <?php  } ?>><?php echo $company[0][$i]->company_name; ?></option>
		<?php }
		?>
		</select>
		</div></td>
	</tr>
		<?php } else {?>
		<input type="hidden" class="form-control" name="company_id" value="<?php echo $company[0][0]->company_id;?>">
		<?php } ?>
		
	
    <tr>
		<td class="fontstyle" align="right"><label for="feedback" class="control-label col-xs-10"><?php echo $lang['Feedback'].MANDATORYMARK?></label></td>
		<td align="left">
		<div class="col-xs-4">
        <select class="form-control" name="service_id" id="service_id">
			<option value="" selected="selected">Please Select</option>
			<?php 
			for($i=0;$i<count($services[0]);$i++)
			{ ?>
				<option value="<?php echo $services[0][$i]->service_id ;?>" <?php if($_POST['service_id']==$services[0][$i]->service_id) { ?> selected="selected" <?php  } ?>><?php echo ucfirst($services[0][$i]->service_name) ?></option>
			<?php }
			?>
			</select> </div>
		</td>
	</tr>
	 <tr>
		<td class="fontstyle" align="right"><label for="feedback" class="control-label col-xs-10"><?php echo $lang['Fault'].MANDATORYMARK?></label></td>
		<td align="left">
		<div class="col-xs-4">
        <select class="form-control" name="fault_id" id="fault_id">
			<option value="" selected="selected">Please Select</option>
			<?php 
			//print_r($faults); exit;
			for($i=0;$i<count($faults[0]);$i++)
			{ ?>
			<option value="<?php echo $faults[0][$i]->fault_id ;?>" <?php if($_POST['fault_id']==$faults[0][$i]->fault_id) { ?> selected="selected" <?php  } ?>><?php echo ucfirst($faults[0][$i]->fault_name) ?></option>
			<?php 
			}
			?>
			</select> </div>
		</td>
	</tr>
	 <tr>
		<td class="fontstyle" align="right"><label for="location" class="control-label col-xs-10"><?php echo $lang['Location'].MANDATORYMARK?></label></td>
		<td align="left"><div class="col-xs-4">
        <select class="form-control" name="device_id" id="device_id">
			<option value="" selected="selected">Please Select</option>
			<?php 
			for($i=0;$i<count($location[0]);$i++)
			{?>
				<option value="<?php echo $location[0][$i]->device_id ;?>" <?php if($_POST['device_id']==$location[0][$i]->device_id) { ?> selected="selected" <?php  } ?>><?php echo ucfirst($location[0][$i]->device_name) ?></option>
			<?php }
			?>
		</select> </div></td>
	</tr>
	 <tr>
		<td class="fontstyle" align="right"><label for="rating" class="control-label col-xs-10"><?php echo $lang['Rating'].MANDATORYMARK?> </label></td>
		<td align="left">
		<div class="col-xs-4">
        <select class="form-control" name="rating" id="rating">
			<option value="" selected="selected">Please Select</option>
			<option value="Excellent" <?php if($_POST['rating']=='Excellent') { ?> selected="selected" <?php } ?>>Excellent</option>
			<option value="Good" <?php if($_POST['rating']=='Good') { ?> selected="selected" <?php } ?>>Good</option>
			<option value="Average" <?php if($_POST['rating']=='Average') { ?> selected="selected" <?php } ?>>Average</option>
			<option value="Poor" <?php if($_POST['rating']=='Poor') { ?> selected="selected" <?php } ?>>Poor</option>
			<option value="Very Poor" <?php if($_POST['rating']=='Very Poor') { ?> selected="selected" <?php } ?>>Very Poor</option>
		</select> </div></td>
	</tr>
	 <tr>
		<td class="fontstyle" align="right"><label for="rating" class="control-label col-xs-10"><?php echo $lang['Status'].MANDATORYMARK?> </label></td>
		<td align="left">
		<div class="col-xs-4">
        <select class="form-control" name="status" id="status">
			<option value="" selected="selected">Please Select</option>
			<option value="1" <?php if($_POST['status']=='1') { ?> selected="selected" <?php } ?>>Completed</option>
			<option value="0"  <?php if($_POST['status']=='0') { ?> selected="selected" <?php } ?>>Pending</option>
				
		</select> </div></td>
	</tr>
	 <tr>
		<td class="fontstyle" align="right"><label for="location" class="control-label col-xs-10"><?php echo $lang['Device Status'].MANDATORYMARK?></label></td>
		<td align="left">
		<div class="col-xs-4">
        <select class="form-control" name="device_status" id="device_status">
			<option value="" selected="selected">Please Select</option>
			<option value="Active" <?php if($_POST['device_status']=='Active') { ?> selected="selected" <?php } ?>>Active</option>
			<option value="In-Active" <?php if($_POST['device_status']=='In-Active') { ?> selected="selected" <?php } ?>>In-Active</option>
		</select> </div>
		</td>
	</tr>
	 <tr>
		<td class="fontstyle" align="right"><label for="users" class="control-label col-xs-10"><?php echo $lang['Assigned To'].MANDATORYMARK?></label></td>
		<td align="left">
		<div class="col-xs-4">
        <select class="form-control" name="assigned_to" id="assigned_to">
			<option value="" selected="selected">Please Select</option>
			<?php 
			for($i=0;$i<count($users[0]);$i++)
			{ ?>
				<option value="<?php echo $users[0][$i]->user_id ;?>" <?php if($_POST['user_id']==$users[0][$i]->user_id) { ?> selected="selected" <?php  } ?>><?php echo ucfirst($users[0][$i]->first_name.' '.$users[0][$i]->last_name); ?></option>
			<?php }
			?>
		</select> </div>
   
		</td>
	</tr>
	 <tr class="alt">
	 <td colspan=2 style="text-align: center;" align="left">
		<div class="col-xs-offset-2 col-xs-10" style="width:50% !important; margin-left: 24.6667%;">
		
		<button type="submit" class="btn btn-primary" name="add_feedback" id="add_feedback"><?php echo $lang['Add']?></button>
	<button type="submit" class="btn btn-primary" name="clearsearch" onclick="return Clear()"><?php echo $lang['Reset']?></button>
		<button type="reset" class="btn btn-primary" name="Back" onClick="window.location.href='<?php print CreateURL('index.php','mod=feedback');?>'"><?php echo $lang['Back']?></button>
     </div>
		</td>
		
	</tr>
</table>
<b class="xbottom"><b class="xb4"></b><b class="xb3"></b><b class="xb2"></b><b
	class="xb1"></b></b></div>

	
</form>

</center>
</body>
</html>
