<?php
	/*======================================
	Developer	-	Jaishree Sahal
	Module      -   Feedback
	SunArc Tech. Pvt. Ltd.
	======================================
	******************************************************/
?>

<script>

function checkAll(ele) {
     var checkboxes = document.getElementsByTagName('input');

	if (ele.checked) {
         for (var i = 0; i < checkboxes.length; i++) {
             if (checkboxes[i].type == 'checkbox') {
                 checkboxes[i].checked = true;
             }
         }
     } else {
         for (var i = 0; i < checkboxes.length; i++) {
             console.log(i);
             if (checkboxes[i].type == 'checkbox') {
                 checkboxes[i].checked = false;
             }
         }
     }
 }

function showpassdiv(id){
$("#password_"+id).css("display", "block");
}
function Check()
{
	if(document.getElementById('keyword').value=='')
	{
		alert('Please enter any value for search.');
		return false;
	}
	else
	{
		return true;
	}
}
function Clear()
{
	document.getElementById('keyword').value='';
	location.href="index.php?mod=feedback";
	return false;
}
</script>


<?php
$lang = $language->english($lang);
?>

      <section>
          <div class="col-sm-12 drop-shadow nopadding">
              <form method="post" name="frmlist" id="frmlist" >
                  <?php
                  if (isset($_SESSION['ActionAccess']))
                  {
                      echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="90%" ><tbody><tr><td colspan="6"  align="center"><div class="errormsg">';
                      echo $_SESSION['ActionAccess'];
                      echo '</div></td></tr></tbody></table>';
                      unset($_SESSION['ActionAccess']);

                  }
                  if(isset($_SESSION['error']))
                  {
                      echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" ><tbody><tr><td colspan="6"  align="center">
				<div class="alert alert-danger alert-dismissable">
				   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
                      echo $_SESSION['error'];
                      echo '</div></td></tr></tbody></table>';
                      unset($_SESSION['error']);
                  }
                  if(isset($_SESSION['success']))
                  {
                      echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" ><tbody><tr><td colspan="6"  align="center">
        <div class="alert alert-success alert-dismissable">
           <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
                      echo $_SESSION['success'];
                      echo '</div></td></tr></tbody></table>';
                      unset($_SESSION['success']);
                  }
                  ?>

                  <div class="user-heading fixedHeader">
                      <div class="row">

                          <div class="col-sm-3 col-xs-6">
                              <span class="side-nav-open">
                                <i class="fa fa-bars" aria-hidden="true"></i>
                            </span>
                              <span style="vertical-align: text-bottom"><?php  echo $lang['Feedbacks'];?></span>
                          </div>
                          <div class="col-md-5 col-sm-3 hidden-xs"></div>
                          <div style="margin-top:0px !important;" class="col-md-2 col-sm-3 hidden-xs">

                          </div>
                          <div class="col-md-2 col-sm-3 col-xs-6">
                              <?php
                              include_once 'user_profile.php';
                              ?>
                          </div>

                      </div>



              </div>
              <div class="userbg">

                  <div class="row">
                      <div class="col-sm-12 ">
                          <div style="padding:0px 0px 30px 0px; overflow: hidden;" class="icon-group">
                              <input type="text" id="keyword" value="<?php echo (isset($frmdata['keyword'])? $frmdata['keyword']:'');?>"
                                     onblur="return  KeywordSearch()" name="keyword" class="form-control search"
                                     placeholder="Search Feedback">
                              <i class="fa fa-search search-icon" aria-hidden="true"></i>
                              <p style="margin:12px 0px; text-align: center;">
                                        <span style="font-weight:bold; font-size:12px">
                                    <?php if(($_SESSION['usertype'])=='super_admin')
                                    { ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*Search By Name ,Role,Phone No,Email Id,Company Name <?php }
                                    else
                                    { ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*Search By Name , Role, Phone No, Email Id, Assigned To<?php } ?>
                                   </span></p>
                          </div>
                      </div>
                      <!--<div class="col-xs-offset-2 col-xs-10" style="width:100% !important; margin-left: 0px; padding-bottom : 10px;">

                                <button type="submit" class="btn btn-primary" name="search"  onclick="return Check()"><?php /*echo $lang['Search']*/?></button>
                                <button type="reset" class="btn btn-primary" name="clearsearch" id="clear" onclick="return Clear()"><?php /*echo $lang['Reset']*/?></button>
                            </div>-->
                  </div>

                  <div class="row">
                      <div class="col-sm-3">

                      </div>
                      <!--<div class="col-sm-9">
                          <div class="pull-right">
                              <?php /* if(($_SESSION['usertype'])=='super_admin' ||($_SESSION['usertype'])=='company_admin' ) { */?>
                                  <a title="Add New Feedback" href="index.php?mod=feedback&do=add" class="btn btn-danger add-company margin_30"><?php /*echo $lang['Add New Feedback']*/?></a>
                              <?php /*} echo '<br>';  */?>

                          </div>
                      </div>-->
                  </div>
                  <?php
                  $srNo = $frmdata['from'];
                  //                        echo "<pre>"; print_r($frmdata['from']); exit;

                  $count = count($Row);
                  ?>
                  <div class="user-heading">
                      <div class="row">
                          <div class="col-md-2 col-sm-3 col-xs-4 select-caret">
                              <?php if (($_SESSION['usertype']) == 'super_admin' ||($_SESSION['usertype'])=='company_admin' ) { ?>
                                  <select name="actions" id="action_list_id" class="form-control show-result">
                                      <option value=""><?php echo $lang['Select Action']?></option>
                                      <option value="Delete"><?php echo $lang['Delete']?></option>
<!--                                      <option value="Active">--><?php //echo $lang['Active']?><!--</option>-->
<!--                                      <option value="Inactive">--><?php //echo $lang['In-Active']?><!--</option>-->
                                  </select>
                              <?php } ?>
                          </div>
                          <div class="col-md-8 col-sm-6 col-xs-4">
                              <p class="showing-results margin_4">
                                  <?php
                                  if($totalCount > 0)
                                      print $lang['Showing Results'].' '.($frmdata['from']+1).'-'.($frmdata['from']+$count)." of ".$totalCount;
                                  else
                                      print $lang['Showing Results'];
                                  ?>
                              </p>
                          </div>
                          <div class="col-md-2 col-sm-3 col-xs-4 select-caret">
                              <?php echo getPageRecords(); ?>
                          </div>
                      </div>
                  </div>
                  <div class="row">
                      <div class="col-sm-12">
                          <div class="table-responsive">
                              <table class="table dashboard-table table-bordered">
                                  <thead>
                                  <tr>
                                      <th style="width: 70px;">
                                          <?php
                                          if(($_SESSION['usertype'])=='super_admin' ||($_SESSION['usertype'])=='company_admin' )
                                          {
                                              ?>
                                              <input type="checkbox" name="chkAll[]" id="chkAll"  onchange="checkAll(this)"/>&nbsp;
                                              <?php
                                          }
                                          echo $lang['S.No.'];
                                          ?>
                                      </th>
                                      <th>
                                          <a  style="text-decoration:none;color:#006699;cursor:pointer;" onClick="OrderPage('f.rating');"><?php echo $lang['Rating']?>
                                              <?php if($frmdata['orderby']=='f.rating') {print '<img src="'.IMAGEURL.'/asc.gif">';} elseif($frmdata['orderby']=='f.rating desc'){print '<img src="'.IMAGEURL.'/desc.gif">';}?>
                                          </a>
                                      </th>
                                      <th>
                                          <a  style="text-decoration:none;color:#006699;cursor:pointer;" onClick="OrderPage('faults.fault_name');">Issue details
                                              <?php if($frmdata['orderby']=='faults.fault_name') {print '<img src="'.IMAGEURL.'/asc.gif">';} elseif($frmdata['orderby']=='faults.fault_name desc'){print '<img src="'.IMAGEURL.'/desc.gif">';}?>
                                          </a>
                                      </th>
                                      <?php if ($number_of_que == 1) { ?>
                                      <th>
                                          <a  style="text-decoration:none;cursor:pointer;color:#006699;" onClick="OrderPage('f.is_active');"><?php echo $lang['Status']?>
                                              <?php if($frmdata['orderby']=='f.is_active') {print '<img src="'.IMAGEURL.'/asc.gif">';} elseif($frmdata['orderby']=='f.is_active desc'){print '<img src="'.IMAGEURL.'/desc.gif">';}?>
                                          </a>
                                      </th>
                                      <?php } ?>

                                      <th>
                                          <a  style="text-decoration:none;color:#006699;cursor:pointer;" onClick="OrderPage('d.device_name');"><?php echo $lang['Device']?>
                                              <?php if($frmdata['orderby']=='d.device_name') {print '<img src="'.IMAGEURL.'/asc.gif">';} elseif($frmdata['orderby']=='d.device_name desc'){print '<img src="'.IMAGEURL.'/desc.gif">';}?>
                                          </a>
                                      </th>
                                      <?php if ($number_of_que == 1) { ?>
                                      <th>
                                          <a  style="text-decoration:none;color:#006699;cursor:pointer;" onClick="OrderPage('d.device_status');"><?php echo $lang['Device Status']?>
                                              <?php if($frmdata['orderby']=='d.device_status') {print '<img src="'.IMAGEURL.'/asc.gif">';} elseif($frmdata['orderby']=='d.device_status desc'){print '<img src="'.IMAGEURL.'/desc.gif">';}?>
                                          </a>
                                      </th>
                                      <th>
                                          <a  style="text-decoration:none;color:#006699;cursor:pointer;" onClick="OrderPage('f.feedback_status');"><?php echo $lang['Status']?>
                                              <?php if($frmdata['orderby']=='f.feedback_status') {print '<img src="'.IMAGEURL.'/desc.gif">';} elseif($frmdata['orderby']=='f.feedback_status desc'){print '<img src="'.IMAGEURL.'/asc.gif">';}?>
                                          </a>
                                      </th>
                                      <th>
                                          <a  style="text-decoration:none;color:#006699;cursor:pointer;" onClick="OrderPage('u.first_name');"><?php echo $lang['Assigned By']?>
                                              <?php if($frmdata['orderby']=='u.first_name') {print '<img src="'.IMAGEURL.'/asc.gif">';} elseif($frmdata['orderby']=='u.first_name desc'){print '<img src="'.IMAGEURL.'/desc.gif">';}?>
                                          </a>
                                      </th>
                                      <?php } ?>
                                      <th>
                                          <a  style="text-decoration:none;color:#006699;cursor:pointer;" onClick="OrderPage('u1.first_name');"><?php echo $lang['Assigned To']?>
                                              <?php if($frmdata['orderby']=='u1.first_name') {print '<img src="'.IMAGEURL.'/asc.gif">';} elseif($frmdata['orderby']=='u1.first_name desc'){print '<img src="'.IMAGEURL.'/desc.gif">';}?>
                                          </a>
                                      </th>
                                      <th>
                                          <a  style="text-decoration:none;color:#006699;cursor:pointer;" onClick="OrderPage('f.creation_date');"><?php echo $lang['Creation Date']?>
                                              <?php if($frmdata['orderby']=='f.creation_date') {print '<img src="'.IMAGEURL.'/asc.gif">';} elseif($frmdata['orderby']=='f.creation_date desc'){print '<img src="'.IMAGEURL.'/desc.gif">';}?>
                                          </a>
                                      </th>
                                      <th>
                                          <a  style="text-decoration:none;color:#006699;cursor:pointer;" onClick="OrderPage('f.modified_date');"><?php echo $lang['Modified Date']?>
                                              <?php if($frmdata['orderby']=='f.modified_date') {print '<img src="'.IMAGEURL.'/asc.gif">';} elseif($frmdata['orderby']=='f.modified_date desc'){print '<img src="'.IMAGEURL.'/desc.gif">';}?>
                                          </a>
                                      </th>


                                      <th class="anth"><?php if(($_SESSION['usertype'])=='super_admin' ||($_SESSION['usertype'])=='company_admin') echo 'Action' ;?></th>
                                  </tr>
                                  </thead>
                                  <tbody>
                                  <?php
                                  if($Row)
                                  {
                                  $objpassEncDec = new passEncDec;
                                  for($counter=0;$counter<$count;$counter++)
                                  {
                                      $srNo++;
                                      if(($counter%2)==0) { $trClass = "tdbggrey"; } else { $trClass = "tdbgwhite"; }
                                      $confirmDelete = 'Do you really want to delete this Feedback ?';
                                      $obj = new passEncDec;
                                      ?>
                                      <tr>
                                          <td>
                                              <?php
                                              if(($_SESSION['usertype'])=='super_admin' ||($_SESSION['usertype'])=='company_admin' ) { ?>
                                                  <input type="checkbox" name="chkbox[]" id="chkbox" value="<?php echo $Row[$counter]->feedback_id;?>"/>&nbsp;&nbsp;
                                              <?php } echo $srNo; ?>
                                          </td>
<!--                                          <td>--><?php //echo $lang[ucfirst($Row[$counter]->rating)] ; ?><!--&nbsp;</td>-->
                                          <td><?php echo ucfirst($Row[$counter]->rating) ; ?>&nbsp;</td>

                                          <?php if(number_of_questions() == 1){ ?>
                                          <td><?php echo isset($Row[$counter]->fault_name) ? ucwords($Row[$counter]->fault_name) : 'NA' ; ?>&nbsp;</td>
                                          <?php }else{ ?>
                                              <td><?php echo isset($Row[$counter]->title) ? ucwords($Row[$counter]->title) : 'NA' ; ?>&nbsp;</td>

                                          <?php } ?>
                                          <?php if ($number_of_que == 1) { ?>
                                            <td><?php if($Row[$counter]->is_active == 'Y') echo $lang["Active"]; else echo $lang["In-Active"];?>&nbsp;</td>
                                          <?php } ?>
                                          <td><?php echo ucfirst($Row[$counter]->device_name); ?>&nbsp;</td>
                                          <?php if ($number_of_que == 1) { ?>
                                          <td><?php echo $lang[ucfirst($Row[$counter]->device_status)]; ?>&nbsp;</td>
                                          <td><?php if($Row[$counter]->feedback_status =='1') { echo 'Completed'; } else { echo 'Pending';} ?></td>
                                          <td><?php echo ucwords($Row[$counter]->first_name.' '.$Row[$counter]->last_name); ?></td>
                                          <?php } ?>
                                          <td><?php echo ucwords($Row[$counter]->assignF.' '.$Row[$counter]->assignL); ?></td>
                                          <td><?php if($Row[$counter]->creation_date) {echo date('m/d/Y h:i:s:A',strtotime($Row[$counter]->creation_date)); }
                                              else echo '-' ; ?></td>
                                          <td><?php if($Row[$counter]->modified_date) {echo date('m/d/Y',strtotime($Row[$counter]->modified_date)); }
                                              else echo '-' ; ?></td>

                                          <td>
                                              <?php
                                              if(($_SESSION['usertype'])=='super_admin' ||($_SESSION['usertype'])=='company_admin')
                                              { ?>
<!--                                                  <a class="fontstyle" href='--><?php //print CreateURL('index.php','mod=feedback&do=edit&id='.$Row[$counter]->feedback_id);?><!--' title="Edit" ><img src="--><?php //echo IMAGEURL."/b_edit.png" ?><!--" border=0 /></a>-->

                                                  <a href="#" data-toggle="modal" data-message="<?php echo $confirmDelete;  ?>" delete-link="<?php print CreateURL('index.php','mod=feedback&do=del&id='.$Row[$counter]->feedback_id) ?>" data-target="#myModal" class="delete-modal" >
                                                      <img src="<?php print IMAGEURL ?>/b_drop.png"></a>

                                              <?php }
                                              else
                                              {
                                                  ?>
                                                  <a class="fontstyle" href='<?php print CreateURL('index.php','mod=feedback&do=edit&id='.$Row[$counter]->feedback_id);?>' title="Edit" ><img src="<?php echo IMAGEURL."/b_edit.png" ?>" border=0 /></a>
                                              <?php } ?>
                                          </td>
                                      </tr>
                                      <?php $sno++; } ?>
                                  </tbody>
                              </table>
                          </div>
                      </div>
                  </div>
                  <div class="row">
                      <div class="col-sm-12">
                          <div class="text-center">
                              <?php
                              PaginationDisplay($totalCount);
                              ?>
                          </div>
                      </div>
                  </div>
              <?php
              }
              elseif($_SESSION['keywords'] == 'Y') {
                  $frmdata['message']="Sorry ! Feedback not found for the selected criteria";
                  unset ($_SESSION['keywords']);
                  ShowMessage(); }
              else {
                  $frmdata['message']="Sorry ! No feedback found.";
                  ShowMessage(); }
              ?>
              </div>
                  <input name="pageNumber" type="hidden" value="<?php print $frmdata['pageNumber']?>" >
                  <input name="orderby" type="hidden" value="<?php if($frmdata['orderby']!='')  echo $frmdata['orderby']; else echo 'f.feedback_id desc';?>" >
                  <input name="order" type="hidden" value="<?php print $frmdata['order']?>" >
                  <input name="actUserID" type="hidden" value="" /></form>
          </div>
      </section>


