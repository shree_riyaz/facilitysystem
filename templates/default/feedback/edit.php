<?php
 //echo "<pre>";
// print_r($_SESSION);
// print_r($Row);
 // print_r($users);
 // print_r($cleaner);exit;

?>
<script>
function deleteConfirm()
{
	if(confirm("Are you really want to delete ? "))
		return true;
	else
		return false;
}
</script>


    <section>

                <div class="col-sm-12 drop-shadow nopadding">
                    <form method="post" class="form-horizontal" name="feedback_edit" id="feedback_edit" enctype="multipart/form-data">
                        <?php
                        if(isset($_SESSION['error']))
                        {
                            echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" ><tbody><tr><td colspan="6"  align="center">
                    <div class="alert alert-danger alert-dismissable">
                       <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
                            echo $_SESSION['error'];
                            echo '</div></td></tr></tbody></table>';
                            unset($_SESSION['error']);
                        }
                        if(isset($_SESSION['success']))
                        {
                            echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" ><tbody><tr><td colspan="6"  align="center">
				<div class="alert alert-success alert-dismissable">
				   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
                            echo $_SESSION['success'];
                            echo '</div></td></tr></tbody></table>';
                            unset($_SESSION['success']);
                        }
                        ?>

                    <div class="user-heading text-left fixedHeader">
                        <div class="row">
                            <div class="col-xs-3">
                        <span class="side-nav-open">
                                <i class="fa fa-bars" aria-hidden="true"></i>
                            </span>
                        <span style="vertical-align: text-bottom"><?php echo $lang['Update Feedback'] ?></span>
                                </div>
                            <div class="col-md-5 col-sm-3 col-xs-2"></div>
                        <div style="margin-top:0px !important;" class="col-md-2 col-sm-3 col-xs-4 select-caret">
                            <select name="select_locale" onchange="this.form.submit()" class="form-control show-result select_locale">
                                <option style="color: white;" value="">Select Language</option>
                                <?php foreach ($get_language_list[0] as $get_language_list_list_value) { ?>
                                    <option <?php if ($get_language_list_list_value->short_code == $_SESSION['selected_language']){?> selected="selected" <?php } ?> style="color: white;" value="<?php echo $get_language_list_list_value->short_code ?>"> <?php echo trim($get_language_list_list_value->language_name); ?> </option>
                                <?php } ?>
                            </select>
                        </div>
                            <div class="col-md-2 col-xs-3">
                        <?php
                        include_once 'user_profile.php';
                        ?>
                                </div>
                        </div>
                    </div>
                    <div class="userbg">
                        <!--                            <div class="form-group icon-group">-->
                        <!--                                <input type="text" class="form-control search margin_bot_30" placeholder="Search User">-->
                        <!--                                <i class="fa fa-search search-icon" aria-hidden="true"></i>-->
                        <!--                            </div>-->
                        <div id="users">
                            <h4 class="update-user"><?php echo $lang['Update Feedback'] ?></h4>
                        </div>
                        <div class="plan-category user-page-form">

                            <form class="form-horizontal">
                                <?php if ($_SESSION['usertype'] == 'admin') { ?>
                                    <div class="form-group">
                                        <label for="RelatedTo" class="col-sm-3"><?php echo $lang['Related To']?>
                                            <sup>*</sup></label>
                                        <div class="col-sm-9">
                                            <select class="form-control" name="company_id" style="width:170px;">
                                                <option value="">Please Select</option>
                                                <?php
                                                /*if($_SESSION['company_id'])
                                                {
                                                    $selected = 'selected';
                                                }*/

                                                for($i=0;$i<count($company[0]);$i++)
                                                {
                                                    // if($Row[0][0]->company_id == $company[0][$i]->company_id)
                                                    // {
                                                    // $selected = 'selected';
                                                    // }
                                                    ?>
                                                    <option value="<?php echo $company[0][$i]->company_id;?>" <?php if($Row[0][0]->company_id == $company[0][$i]->company_id) { echo "selected"; } ?>><?php echo $company[0][$i]->company_name;?>
                                                    </option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <?php// } else {?>
                                    <input type="hidden" class="form-control" name="company_id" value="<?php echo $Row[0][0]->company_id;?>">
                                <?php } ?>


                                <div class="form-group">
                                    <label for="Role" class="col-sm-3"><?php echo $lang['Feedback']?>
                                        <sup>*</sup></label>
                                    <div class="col-sm-9">
                                        <!--                <input type="text" name="service_id" class="form-control" id="service_id" readonly="readonly" value="--><?php //echo ucfirst($services[0][0]->service_name) ?><!--">-->
                                        <input type="text" name="service_id" class="form-control" id="service_id" readonly="readonly" value="<?php echo $services[0][0]->service_name ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="FirstName" class="col-sm-3"><?php echo $lang['Fault']?>
                                        <sup>*</sup></label>
                                    <div class="col-sm-9">
                                        <select class="form-control" name="fault_id" id="fault_id" style="width:170px;">
                                            <option value="">Please Select</option>
                                            <?php
                                            //replace $faults to $faults_res By : Neha Pareek. Dated : 04 Nov 2015
                                            for($i=0;$i<count($faults_res[0]);$i++)
                                            {?>
                                                <option value="<?php echo $faults_res[0][$i]->fault_id;?>" <?php if($Row[0][0]->fault_id == $faults_res[0][$i]->fault_id) { echo "selected"; } ?>><?php echo $faults_res[0][$i]->fault_name;?>
                                                </option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-3"><?php echo $lang['Device'] ?>
                                        <sup>*</sup></label>
                                    <div class="col-sm-4">
                                        <select class="form-control" name="device_id" id="device_id" style="width:170px;">
                                            <option value="">Please Select</option>
                                            <?php
                                            for($i=0;$i<count($location[0]);$i++)
                                            { ?>
                                                <option value="<?php echo $location[0][$i]->device_id;?>" <?php if($Row[0][0]->device_id == $location[0][$i]->device_id) { echo "selected"; } ?>><?php echo $location[0][$i]->device_name;?>
                                                </option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="EmailId" class="col-sm-3"><?php echo $lang['Rating']?>
                                        <sup>*</sup></label>
                                    <div class="col-sm-9">
                                        <select class="form-control" name="rating" id="rating" style="width:170px;">
                                            <option value="" <?php if ($Row[0][0]->rating == '' || $Row->rating == '0') {  echo "selected"; } ?>>Please Select</option>
                                            <!--	<option value="">Please Select</option>-->
                                            <option value="Excellent" <?php if($Row[0][0]->rating=='Excellent'){ echo "selected";}?>>Excellent</option>
                                            <option value="Good" <?php if($Row[0][0]->rating=='Good'){ echo "selected"; }?>>Good</option>
                                            <option value="Average" <?php if($Row[0][0]->rating=='Average'){ echo "selected"; }?>>Average</option>
                                            <option value="Poor" <?php if($Row[0][0]->rating=='Poor'){ echo "selected"; }?>>Poor</option>
                                            <option value="Very Poor" <?php if($Row[0][0]->rating=='Very Poor'){ echo "selected"; }?>>Very Poor</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="" class="col-sm-3"><?php echo $lang['Device Status']?>
                                        <sup>*</sup></label>
                                    <div class="col-sm-9">
                                        <select class="form-control" name="device_status" id="device_status" style="width:170px;">
                                            <option value="" <?php if ($Row[0][0]->device_status == '' || $Row->device_status == '0') {echo "selected";} ?>>Please Select</option>
                                            <option value="Active" <?php if($Row[0][0]->device_status=='Active'){ echo "selected"; }?>>Active</option>
                                            <option value="Inactive" <?php if($Row[0][0]->device_status=='Inactive'){echo "selected"; }?>>In-Active</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="" class="col-sm-3"><?php echo $lang['Status'] ?>
                                        <sup>*</sup></label>
                                    <div class="col-sm-9">
                                        <select class="form-control" name="status" id="status" style="width:170px;">
                                            <option value="" <?php if ($Row[0][0]->feedback_status == '' || $Row[0][0]->feedback_status == '0') { echo "selected"; } ?>>Please Select</option>
                                            <option value="1" <?php if($Row[0][0]->feedback_status=='1'){ echo "selected";}?>>Completed</option>
                                            <option value="0" <?php if($Row[0][0]->feedback_status=='0'){ echo "selected";}?>>Pending</option>
                                        </select>
                                    </div>
                                </div>

                                <?php if($_SESSION['role_id']!='3')  { ?>

                                    <div class="form-group">
                                        <label for="" class="col-sm-3"><?php echo $lang['Assigned To'] ?>
                                            <sup>*</sup></label>
                                        <div class="col-sm-9">
                                            <select class="form-control" name="assigned_to" id="assigned_to" style="width:170px;">
                                                <option value="" >Please Select</option>
                                                <?php
                                                for($i=0; $i<count($users[0]); $i++) //loop for supervisors
                                                {
                                                    $usr = $users[0][$i]->user_id;
                                                    ?>
                                                    <!-- create optgroup of supervisers -->
                                                    <optgroup label="<?php echo ucfirst($users[0][$i]->first_name.' '.$users[0][$i]->last_name);?>">
                                                        <?php
                                                        for($j = 0; $j<count($cleaner[0]); $j++)//loop for cleaners
                                                        {
                                                            if($cleaner[0][$j]->assigned_to == $usr)
                                                            { //create options list of cleaners
                                                                ?>
                                                                <option value="<?php echo $cleaner[0][$j]->user_id ;?>" <?php if($Row[0][0]->assigned_to == $cleaner[0][$j]->user_id) { echo "selected"; } ?>><?php echo ucfirst($cleaner[0][$j]->first_name.' '.$cleaner[0][$j]->last_name); ?></option>
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                                    </optgroup>
                                                    <?php
                                                }
                                                ?>
                                            </select>

                                        </div>
                                    </div>
                                <?php } else { ?> <input type="hidden" name="assigned_to" value="<?php echo $Row[0][0]->assigned_to;?>" /><?php } ?>


                                <div class="form-group">
                                    <label for="" class="col-sm-3"><?php echo $lang['Active']?>
                                        <sup>*</sup></label>
                                    <div class="col-sm-3">
                                        <input type="radio" name="is_active" value="Y" <?php if($Row[0][0]->is_active=='Y') {echo "checked";} ?> /><?php echo $lang['Active']?>  &nbsp;&nbsp;&nbsp;&nbsp;
                                        <input type="radio" name="is_active" value="N" <?php if($Row[0][0]->is_active=='N') {echo "checked";} ?>/><?php echo $lang['In-Active']?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <button type="submit" id="update" name="update" class="btn btn-danger add-company pull-right"><?php echo $lang['Update']?></button>

                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    </form>
                </div>



    </section>