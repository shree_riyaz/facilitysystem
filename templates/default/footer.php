</div>

</div>
</div>
</div>
<!-- end #footer -->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-center">Are you sure ?</h4>
            </div>
            <div class="modal-body text-center">
                <p class="modal-message-p">Do you really want to delete these records?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <!--                <button type="button"  class="btn btn-danger" data-dismiss="modal">Delete</button>-->
                <a href="#" id="confirm_delete_action" class="btn btn-danger delete-href">Delete</a>
            </div>
        </div>

    </div>
</div>

</body>
</html>

<script type="text/javascript">
    $('document').ready(function() {
        $('#action_list_id').on("change",function() {
            var selectedOption = $(this).children("option:selected").val();
//            alert(selectedOption);
            if(selectedOption == 'Delete' && $("#chkbox").prop("checked") == true){
                $('#myModal').modal('show');
            }else{
                this.form.submit();
            }


        });
    });
</script>

<script type="text/javascript">
    $('document').ready(function() {
        $('#confirm_delete_action').on("click",function() {
//            alert(6456);
            document.frmlist.submit();
//            $('#frmlist').form.submit();
            $('#myModal').modal('hide');
        });
    });
</script>

<script>
    $('.delete-modal').click(function () {
        var get_link = $(this).attr("delete-link");
        var get_delete_msg = $(this).attr("data-message");
//        alert(get_delete_msg);
        $('.delete-href').attr("href",get_link);
        $('.modal-message-p').text(get_delete_msg);

    });
</script>
<script type="text/javascript">

    //Make focus to first element of the form.
    for(var i = 0; document.forms[0].elements[i].type == 'hidden' || document.forms[0].elements[i].disabled; i++)
    {continue;}

    document.forms[0].elements[i].focus();



</script>
<script type="text/javascript" src="<?php echo ADMINSCRIPTROOT ?>/js/app.js"></script>
<script type="text/javascript">

    $(document).on('click','applyBtn',function () {
//    var selected_span_text = $('.span_range').text();
//    var selected_text_box = $('#selected_date_range_hidden').val();
//
//    var vall =  $('#reportrange .span_range').text($('#selected_date_range_hidden').val(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))) ;
//    $('#data_of_custom').val(vall);

    });

    $(function() {

//        alert('aa gya');

        var start = <?php echo isset($_SESSION['start_date_by_interval']) ? $_SESSION['start_date_by_interval'] : "moment().subtract(30, 'days')"?>;
        var end = <?php echo  isset($_SESSION['end_date_by_interval']) ? $_SESSION['end_date_by_interval'] : "moment()" ?>;

//        var start = <?php //echo $_SESSION['start_date'] ?>//;
//        var end = <?php //echo  $_SESSION['end_date'] ?>//;

//        console.log(123);
//        console.log(start);
//        console.log(456);
//        console.log(end);

        function cb(start,end,label) {

            $('#reportrange .span_range').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));

//            var is_system = <?php //echo $_SESSION['selected_date_range_type_hidden']; ?>//;
//            alert(is_system);

            $('#selected_date_range_hidden').val(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));

//            $('.ranges ul li').data('data-range-key',label);
            $('#selected_date_range_type_hidden').val(label);

//            $(document).on('click','div.ranges>ul>li',function () {
//                var selected_li = $(this).text();
//                $('#selected_date_range_type_hidden').val(selected_li.trim());
//                $('div.ranges>ul>li').val(label);
//            });
//
            console.log('New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')');
        }

        $('#reportrange').daterangepicker({
            "showDropdowns": true,
            "autoApply": true,

            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(30, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
//                'Custom': [moment(end,'YYYY-MM-DD'), moment(start,'YYYY-MM-DD')]
//                'Custom Range': [end, start]
            },
            startDate: start,
            endDate: end,
            "locale": {
                "format": "YYYY-MM-DD",
                "separator": "-",
                "applyLabel": "Apply",
                "cancelLabel": "Cancel",
                "fromLabel": "From",
                "toLabel": "To",
                "customRangeLabel": "Custom",
                "weekLabel": "W",
                "daysOfWeek": [
                    "Su",
                    "Mo",
                    "Tu",
                    "We",
                    "Th",
                    "Fr",
                    "Sa"
                ],
                "monthNames": [
                    "January",
                    "February",
                    "March",
                    "April",
                    "May",
                    "June",
                    "July",
                    "August",
                    "September",
                    "October",
                    "November",
                    "December"
                ],
                "firstDay": 1
            },
            opens: 'left'
        }, cb);

        cb(start, end);

    });


    $('.navbar-collapse a.collapsed').click(function (e) {
        $('.list-tabs').animate({
            scrollTop: $(this).offset().top - 50
        }, 500);


    });

    $(document).ready(function (e) {
        if( $('li.active-tab').length >= 1 ) {
            var offset = $('li.active-tab').offset().top;
            $('.list-tabs').animate({
                scrollTop: offset - 50
            }, 500);
        }
    });

    $('.device-link').click(function() {
        if ($('#collapseThree').hasClass('in'))
        {
            $('#collapseThree').removeClass('in');
            $('#collapseThree').addClass('collapse');
            $('a.report-link').addClass('collapsed');

        }



    });

    $('.side-nav-open .fa.fa-bars').click(function () {



            $('.list-tabs').animate({'left' : '0%'});
        $('.list-tabs').addClass('zIndex');

    });



    $('.side-nav-close .fa.fa-angle-left').click(function () {
        if ($(window).innerWidth() <= 767) {
            $('.list-tabs').animate({'left': '-50%'});
        } else {
            $('.list-tabs').animate({'left': '-25%'});
        }
    });




    $(document).mouseup(function(e) {
        var container = $(".list-tabs");
        if (!container.is(e.target) && container.has(e.target).length === 0) {
           if ($(window).innerWidth() <= 767) {
                $('.list-tabs').animate({'left': '-50%'});
            } else {
                $('.list-tabs').animate({'left': '-25%'});
            }



        }

    });




    $('.report-link').click(function() {
        if ($('#collapseTwo').hasClass('in'))
        {
            $('#collapseTwo').removeClass('in');
            $('#collapseTwo').addClass('collapse');
            $('a.device-link').addClass('collapsed');
        }


    });


</script>
<script>
    $("#show_chart_submit_id").click(function(){

        var selected_text_box = $('#selected_date_range_hidden').val(); //date string

        var selected_range_type = $('#selected_date_range_type_hidden').val(); //today

        var result = selected_text_box.split(" ");
//                alert(result);
//        console.log(12323); console.log(result);

        var date_details = {date_type:selected_range_type, start_date: result[1], end_date: result[0], complete_date: selected_text_box};
        var data_parse = {};
        localStorage.setItem('filter_details', JSON.stringify(date_details));
        localStorage.getItem('filter_details', JSON.stringify(date_details));
        var data_from_local_storage = JSON.parse(localStorage.getItem('filter_details') || '{}');
        data_parse= data_from_local_storage;
//        console.log(12323); console.log(data_from_local_storage);

        $.ajax ({
//            url: 'http://10.10.10.58/facilitysystem/index.php?mod=reports&do=interval',
            url: 'http://10.10.10.58/facilitysystem/index.php?mod=reports',
            data: { vals_one : data_parse, do : 'xss',vals_two : 'xss' },
            type: "post",
            dataType: 'JSON',
            success: function( result ) {
//                location.reload();
                console.log('Lalalala');
                var array = JSON.parse(result);
                console.log(7788);
                console.log(array);
            }
        });
    });
</script>

