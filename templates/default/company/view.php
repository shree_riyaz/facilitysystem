<?php

$lang = $language->english($lang);
$DBFilter = New $DBFilter();
//print_r($_SESSION);
//echo "Hello";
//print_r($Row);
$id = $_SESSION['user_id'];
$date= date("d-M-Y");
$expiary_date = date("d-M-Y", strtotime($Row->expiary_date));
//echo "<pre>";
// print_r($Row);
//print_r($feedbk);
//print_r( $DBFilter->SelectRecord('faults'));
//echo count($feedbk[0]);
//exit;
//
//echo $_COOKIE[$cookie_name];
//exit;
?>
<style>text.highcharts-credits {
        display: none;
    }</style>

<section>

    <form class="col-sm-12 drop-shadow nopadding" method="post" name="frmlist" id="frmlist">
                <div class="user-heading fixedHeader">

                    <div class="row">
                        <div class="col-sm-3 col-xs-6">
                            <span class="side-nav-open">
                                <i class="fa fa-bars" aria-hidden="true"></i>
                            </span>
                            <span style="vertical-align: text-bottom"> <?php echo $lang['Dashboard']?></span>

                        </div>
                        <div class="col-md-5 col-sm-3 hidden-xs"></div>
                        <div style="margin-top:0px !important;" class="col-md-2 col-sm-3 hidden-xs">

                        </div>

                        <div class="col-md-2 col-sm-3 col-xs-6">
                            <?php
                            include_once 'user_profile.php';
                            ?>
                        </div>

                    </div>



                </div>
                <?php
                //echo '<pre>';
//                print_r($selected_date_interval); //exit;
//                print_r($_SESSION['selected_date_range_session']); //exit;
//                $aa = explode('-',$_SESSION['selected_date_range_session']);
//                print_r($aa);
                ?>
                <div class="userbg">

                    <div class="row">

                        <div>
                            <div style="padding: 0px 0px 30px 0px;overflow: hidden;">
                            <?php
                            include_once(CURRENTTEMP."/"."date_picker.php");
                            ?>
                            </div>
                        </div>
                        <?php if (is_assign_users() == 'Y' && is_fault_available_for_company() > 0 ) { ?>
                            <div class="col-md-4 col-sm-12 dashboard-details">
                                <div class="feedback">
                                    <div class="row">
                                        <div class="col-xs-3">
                                            <i class="fa fa-thumbs-up" aria-hidden="true"></i>
                                        </div>
                                        <div class="col-xs-9">

                                            <p class="pull-right font_60"><?php echo count($cleaner[0]); ?></p>
                                            <p class="pull-right font_22"> <?php echo $lang['Employees']?></p>

                                        </div>
                                    </div>
                                </div>
                                <div class="view-details1">
                                    <a href="<?php print CreateURL('index.php', 'mod=user'); ?>">
                                        <i class="fa fa-arrow-right" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-12 dashboard-details">
                                <div class="new-user">
                                    <div class="row">
                                        <div class="col-xs-3">
                                            <i class="fa fa-user" aria-hidden="true"></i>
                                        </div>
                                        <div class="col-xs-9">

                                            <p class="pull-right font_60"><?php echo count($superviser[0]); ?></p>
                                            <p class="pull-right font_22"><?php echo $lang['Supervisors']?></p>

                                        </div>
                                    </div>

                                </div>
                                <div class="view-details2">
                                    <a href="<?php print CreateURL('index.php', 'mod=user'); ?>">
                                        <i class="fa fa-arrow-right" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-12 dashboard-details">
                            <div class="new-companies">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-suitcase" aria-hidden="true"></i>
                                    </div>
                                    <div class="col-xs-9">

                                        <p class="pull-right font_60"><?php echo count($devices[0]); ?></p>
                                        <p class="pull-right font_22"><?php echo $lang['Devices']?></p>

                                    </div>
                                </div>

                            </div>
                            <div class="view-details3">
                                <a href="<?php print CreateURL('index.php', 'mod=device'); ?>">
                                    <i class="fa fa-arrow-right" aria-hidden="true"></i>
                                </a>
                            </div>
                        </div>
                        <?php }else{ ?>
                            <div class="col-md-3 col-sm-12 dashboard-details">
                                <div class="new-companies">
                                    <div class="row">
                                        <div class="col-xs-3">
                                            <i class="fa fa-suitcase" aria-hidden="true"></i>
                                        </div>
                                        <div class="col-xs-9">

                                            <p class="pull-right font_60"><?php echo count($devices[0]); ?></p>
                                            <p class="pull-right font_22"><?php echo $lang['Devices']?></p>

                                        </div>
                                    </div>

                                </div>
                                <div class="view-details3">
                                    <a href="<?php print CreateURL('index.php', 'mod=device'); ?>">
                                        <i class="fa fa-arrow-right" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-12 dashboard-details">
                                <div class="new-companies">
                                    <div class="row">
                                        <div class="col-xs-3">
                                            <i class="fa fa-thumbs-up" aria-hidden="true"></i>
                                        </div>
                                        <div class="col-xs-9">

                                            <p class="pull-right font_60"><?php echo $total_positive; ?></p>
                                            <p class="pull-right font_22">Positive</p>

                                        </div>
                                    </div>

                                </div>
                                <div class="view-details3">
                                    <a href="<?php print CreateURL('index.php', 'mod=feedback'); ?>">
                                        <i class="fa fa-arrow-right" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-12 dashboard-details">
                                <div class="new-user">
                                    <div class="row">
                                        <div class="col-xs-3">
                                            <i class="fa fa-thumbs-down" aria-hidden="true"></i>
                                        </div>
                                        <div class="col-xs-9">

                                            <p class="pull-right font_60"><?php echo $total_negative ?></p>
                                            <p class="pull-right font_22">Negative</p>

                                        </div>
                                    </div>

                                </div>
                                <div class="view-details2">
                                    <a href="<?php print CreateURL('index.php', 'mod=feedback'); ?>">
                                        <i class="fa fa-arrow-right" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>

                            <div class="col-md-3 col-sm-12 dashboard-details">
                                <div class="new-companies">
                                    <div class="row">
                                        <div class="col-xs-3">
                                            <i class="fa fa-smile-o" aria-hidden="true"></i>
                                        </div>
                                        <div class="col-xs-9">

                                            <p class="pull-right font_60"><?php echo $total_neutral; ?></p>
                                            <p class="pull-right font_22">Neutral</p>

                                        </div>
                                    </div>

                                </div>
                                <div class="view-details3">
                                    <a href="<?php print CreateURL('index.php', 'mod=feedback'); ?>">
                                        <i class="fa fa-arrow-right" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>

                        <?php } ?>

                    </div>
                    <?php if(($_SESSION['usertype']) == 'company_admin' ) { ?>

                    <div class="row">
                        <div class="col-lg-6">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <i class="fa fa-bar-chart-o fa-fw"></i> Breakdown By Feedback Nature
                                </div>
                                <?php if (count($get_data_for_pie_feedback_type_nature)){ ?>
                                    <div class="panel-body">
                                        <div class="poor-feedback-div">
                                            <!--                        https://jsfiddle.net/gh/get/library/pure/highcharts/highcharts/tree/master/samples/highcharts/demo/line-basic/-->
                                            <script type="text/javascript">
                                                $(function () {
                                                    $('#containers_nature').highcharts({

                                                        chart: {
                                                            plotBackgroundColor: null,
                                                            plotBorderWidth: 0,//null,
                                                            plotShadow: false
                                                        },
                                                        title: {
                                                            text: ''
                                                        },
                                                        tooltip: {
                                                            pointFormat: '{series.name}: <b>{point.percentage:.2f}%</b>'
                                                        },
                                                        plotOptions: {
                                                            pie: {
                                                                allowPointSelect: true,
                                                                cursor: 'pointer',

                                                                dataLabels: {
                                                                    enabled: true,
                                                                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                                                    style: {
                                                                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                                                    }
                                                                },
                                                                showInLegend: true
                                                            }
                                                        },
                                                        series: [{
                                                            type: 'pie',
                                                            name: 'Feedback',
                                                            size: '80%',
                                                            data: <?php echo json_encode($get_data_for_pie_feedback_type_nature);?>
                                                        }]
                                                    });
                                                });

                                            </script>

                                            <div id="containers_nature"></div>
                                        </div>
                                    </div>
                                    <!-- /.panel-body -->
                                <?php } else { ?>

                                    <div class="panel-body">
                                        <br>
                                        <div class="text-center">
                                            <span  style="font-size: 15px;">Oops! No data available to show feedback type based graph chart.</span>
                                        </div>
                                        <br>
                                    </div>

                                <?php } ?>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <i class="fa fa-bar-chart-o fa-fw"></i> Breakdown By Feedback Survey
                                </div>
                                <?php if (count($get_data_for_pie_feedback_type_survey)){ ?>
                                    <div class="panel-body">
                                        <div class="poor-feedback-div">
                                            <!--                        https://jsfiddle.net/gh/get/library/pure/highcharts/highcharts/tree/master/samples/highcharts/demo/line-basic/-->
                                            <script type="text/javascript">
                                                $(function () {
                                                    $('#containers_survey').highcharts({

                                                        chart: {
                                                            plotBackgroundColor: null,
                                                            plotBorderWidth: 0,//null,
                                                            plotShadow: false
                                                        },
                                                        title: {
                                                            text: ''
                                                        },
                                                        tooltip: {
                                                            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                                                        },
                                                        plotOptions: {
                                                            pie: {
                                                                allowPointSelect: true,
                                                                cursor: 'pointer',

                                                                dataLabels: {
                                                                    enabled: true,
                                                                    format: '<b>{point.name}</b>: {point.percentage:.2f} %',
                                                                    style: {
                                                                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                                                    }
                                                                },
                                                                showInLegend: true
                                                            }
                                                        },
                                                        series: [{
                                                            type: 'pie',
                                                            name: 'Feedback',
                                                            size: '80%',
                                                            data: <?php echo json_encode($get_data_for_pie_feedback_type_survey);?>
                                                        }]
                                                    });
                                                });

                                            </script>

                                            <div id="containers_survey"></div>
                                        </div>
                                    </div>
                                    <!-- /.panel-body -->
                                <?php } else { ?>

                                    <div class="panel-body">
                                        <br>
                                        <div class="text-center">
                                            <span  style="font-size: 15px;">Oops! No data available to show feedback type based graph chart.</span>
                                        </div>
                                        <br>
                                    </div>

                                <?php } ?>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-<?php if ($count_faults == 0){ ?>12<?php } else { ?>6<?php } ?>">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <i class="fa fa-bar-chart-o fa-fw"></i> <?php echo $lang['Breakdown By Feedback Type']?>
                                </div>
                                <?php if (count($get_data_for_pie_feedback_type)){ ?>
                                <div class="panel-body">
                                    <div class="poor-feedback-div">
                                        <!--                        https://jsfiddle.net/gh/get/library/pure/highcharts/highcharts/tree/master/samples/highcharts/demo/line-basic/-->
                                        <script type="text/javascript">
                                            $(function () {
                                                $('#containers_poor').highcharts({

                                                    chart: {
                                                        plotBackgroundColor: null,
                                                        plotBorderWidth: 0,//null,
                                                        plotShadow: false
                                                    },
                                                    title: {
                                                        text: ''
                                                    },
                                                    tooltip: {
                                                        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                                                    },
                                                    plotOptions: {
                                                        pie: {
                                                            allowPointSelect: true,
                                                            cursor: 'pointer',

                                                            dataLabels: {
                                                                enabled: true,
                                                                format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                                                style: {
                                                                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                                                }
                                                            },
                                                            showInLegend: true
                                                        }
                                                    },
                                                    series: [{
                                                        type: 'pie',
                                                        name: 'Feedback',
                                                        size: '80%',
                                                        data: <?php echo json_encode($get_data_for_pie_feedback_type);?>
                                                    }]
                                                });
                                            });

                                        </script>

                                        <div id="containers_poor"></div>
                                    </div>
                                </div>
                                <!-- /.panel-body -->
                                <?php } else { ?>

                                    <div class="panel-body">
                                        <br>
                                        <div class="text-center">
                                            <span  style="font-size: 15px;">Oops! No data available to show feedback type based graph chart.</span>
                                        </div>
                                        <br>
                                    </div>

                                <?php } ?>
                            </div>
                        </div>
                        <?php if ($count_faults > 0){ ?>
                        <div class="col-lg-<?php if ($count_faults > 0){ ?>6<?php } else { ?>6<?php } ?>">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <i class="fa fa-bar-chart-o fa-fw"></i> <?php echo $lang['Breakdown By Fault Type']?>
                                </div>
                                <!-- /.panel-heading -->
                                <?php if (count($get_data_for_pie_data_list_fault)){ ?>

                                <div class="panel-body">
                                    <div class="overall-feedback-div">

                                        <script type="text/javascript">

                                            $(function () {


                                                $('#container_fault').highcharts({

                                                    chart: {
                                                        renderTo: 'container',
                                                        type: 'pie'
                                                    },
                                                    title: {
                                                        text: ''
                                                    },
                                                    tooltip: {
                                                        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                                                    },
                                                    plotOptions: {
                                                        pie: {
                                                            borderColor: '#000000',
                                                            innerSize: '80%',
                                                        }
                                                    },

                                                    series: [{
                                                        type: 'pie',
                                                        name: 'Feedback',
                                                        size: '100%',
                                                        innerSize: '55%',
                                                        data: <?php echo json_encode($get_data_for_pie_data_list_fault);?>,
                                                        showInLegend: true,
                                                        dataLabels: {
                                                            enabled: false
                                                        }
                                                    }]
                                                });

                                            });

                                        </script>

                                        <div id="container_fault"></div>

                                    </div>
                                </div>
                                <!-- /.panel-body -->
                                <?php } else { ?>

                                    <div class="panel-body">
                                        <br>
                                        <div class="text-center">
                                            <span  style="font-size: 15px;">Oops! No data available to show fault based feedback graph chart.</span>
                                        </div>
                                        <br>
                                    </div>

                                <?php } ?>
                            </div>
                        </div>
                        <?php } ?>

                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <i class="fa fa-bar-chart-o fa-fw"></i> <?php echo $lang['Daily Feedback']?>
                                </div>
                                <?php if (count($data_overall)){ ?>

                                    <!-- /.panel-heading -->
                                    <div class="panel-body">
                                        <div class="overall-feedback-div">

                                            <div id="container_overall"></div>


                                            <script type="text/javascript">


                                                // create the chart
                                                var chart = Highcharts.chart('container_overall', {
                                                    chart: {
                                                        events: {
                                                            addSeries: function () {
                                                                var label = this.renderer.label('A series was added, about to redraw chart', 100, 120)
                                                                    .attr({
                                                                        fill: Highcharts.getOptions().colors[0],
                                                                        padding: 10,
                                                                        r: 5,
                                                                        zIndex: 8
                                                                    })
                                                                    .css({
                                                                        color: '#FFFFFF'
                                                                    })
                                                                    .add();

                                                                setTimeout(function () {
                                                                    label.fadeOut();
                                                                }, 1000);
                                                            }
                                                        }
                                                    },
                                                    title: {
                                                        text: ''
                                                    },
                                                    xAxis: {
                                                        categories: <?php echo $overall_feedback_by_date; ?>
                                                    },
                                                    yAxis: {
                                                        title: {
                                                            text: 'Number of Feedback'
                                                        }
                                                    },

                                                    series: [{
                                                        data: <?php echo $overall_feedback_count; ?>,
                                                        name: "Daily Feedback Record"

                                                    }]
                                                });

                                            </script>


                                        </div>
                                    </div>
                                <?php } else { ?>

                                    <div class="panel-body">
                                        <br>
                                        <div class="text-center">
                                            <span  style="font-size: 15px;">Oops! No data available to show overall daily based feedback graph chart.</span>
                                        </div>
                                        <br>
                                    </div>

                                <?php } ?>
                            </div>
                        </div>

                    </div>
                    <?php } ?>
                </div>

            </div>
    </form>
</section>

