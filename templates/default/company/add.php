<?php
	/*======================================
	Developer	-	JAishree Sahal
	Module      -   Company
	SunArc Tech. Pvt. Ltd.
	======================================
	******************************************************/
	$lang = $language->english($lang);
	//echo "<pre>";
	//print_r($plan[0]);
	//exit;
	//echo "<pre>";print_r($frmdata);
			/*print_r($_SESSION);
			print_r($_POST);
			*/
?>
<script>
 $(function() {
	var dateToday = new Date();
	var yrRange = dateToday.getFullYear() + ":" + (dateToday.getFullYear() + 15); // year drop down with current year to next 15 year Added By : Neha Pareek Dated : 10-11-2015
		$("#creation_date").datepicker({
		dateFormat: 'yy-mm-dd',
			minDate: 0,
			changeMonth: true,changeYear: true, yearRange: yrRange//added by : Neha Pareek. Dated : 10-11-2015
		});
		$("#expiry_date").datepicker({
			dateFormat: 'yy-mm-dd' ,minDate: 0,
			changeMonth: true, changeYear: true, yearRange: yrRange //added by : Neha Pareek. Dated : 10-11-2015
		});
	});
function Clear()
{
	document.getElementById('user_name').value='';
	document.getElementById('company_name').value='';
	document.getElementById('creation_date').value='';
	document.getElementById('expiry_date').value='';
	document.getElementById('subscription_plan').value='';
	document.getElementById('company_email').value='';
	location.href="index.php?mod=company&do=add";
	return false;
}
</script>
<section>

        <div class="col-sm-12 drop-shadow nopadding">
            <form method="post" class="form-horizontal" name="company_add" id="company_add" enctype="multipart/form-data">
                <?php

                if(isset($_SESSION['error']))
                {
                    echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" ><tbody><tr><td colspan="6"  align="center">
				<div class="alert alert-danger alert-dismissable">
				   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
                    echo $_SESSION['error'];
                    echo '</div></td></tr></tbody></table>';
                    unset($_SESSION['error']);
                }
                if(isset($_SESSION['success']))
                {
                    echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" ><tbody><tr><td colspan="6"  align="center">
				<div class="alert alert-success alert-dismissable">
				   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
                    echo $_SESSION['success'];
                    echo '</div></td></tr></tbody></table>';
                    unset($_SESSION['success']);
                }
                ?>
            <div class="user-heading fixedHeader">
                 <span class="side-nav-open">
                                <i class="fa fa-bars" aria-hidden="true"></i>
                            </span>
                <span>Add Company</span>
                <?php
                include_once 'user_profile.php';
                ?>
            </div>
            <div class="userbg">
                <div id="users">
                    <h4 class="update-user"></h4>
                    <div class="backbtn"><button type="button" class="btn btn-danger">Back</button></div>
                </div>
                <div class="plan-category user-page-form">
                    <div class="form-group">
                        <label for="company_name" class="col-sm-3"><?php echo $lang['Company Name']?>
                            <sup>*</sup></label>
                        <div class="col-sm-9">
                            <input type="text" title="Enter Company Name" class="form-control" id="company_name" name="company_name" value="<?php echo $_POST['company_name'];?>" onbuler="return  KeywordSearch()" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="user_name" class="col-sm-3"><?php echo $lang['Contact Person'] ?>
                            <sup>*</sup></label>
                        <div class="col-sm-9">
                            <input type="text" title="Enter Contact Person" class="form-control" id="user_name" name="user_name" value="<?php echo $_POST['user_name'];?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="subscription_plan" class="col-sm-3"><?php echo $lang['Subscription Plan']?>
                            <sup>*</sup></label>
                        <div class="col-sm-9">
                            <select class="form-control" name="plan_id" id="subscription_plan" style="width:170px;">
                                <option value="">Please Select</option>
                                <?php //echo"<pre>"; print_r($plan[0]);
                                for($i=0;$i<count($plan[0]);$i++)
                                { //echo"<pre>"; echo $plan[0][$i];
                                    ?>
                                    <option value="<?php echo $plan[0][$i]->plan_id?>" <?php if($plan[0][$i]->plan_id == $_POST['plan_id']) { echo "selected";} ?>><?php echo $plan[0][$i]->plan_name; ?></option>
                                <?php } ?>
                            </select>

                        </div>
                    </div>
                    <div class="form-group">
                        <label for="creation_date" class="col-sm-3"><?php echo $lang['Creation Date'] ?>
                            <sup>*</sup></label>
                        <div class="col-sm-9">
                            <input type="text" title="Enter Creation Date" class="form-control" id="creation_date"  name="creation_date" value="<?php echo $_POST['creation_date'];?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="expiry_date" class="col-sm-3"><?php echo $lang['Expiry Date'] ?>
                            <sup>*</sup></label>
                        <div class="col-sm-9">
                            <input type="text" title="Enter Expiry Date" class="form-control" id="expiry_date" name="expiary_date" value="<?php echo $_POST['expiary_date'];?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="company_email" class="col-sm-3"><?php echo $lang['Email Id'] ?>
                            <sup>*</sup></label>
                        <div class="col-sm-9">
                            <input type="text" title="Enter Email Id" class="form-control" id="company_email" name="company_email" value="<?php echo $_POST['company_email'];?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="company_logo" class="col-sm-3"><?php echo $lang['Company Logo'] ?><sup>*</sup></label>
                        <div class="col-sm-9">
                            <input type="file" title="Enter Company Logo" class="form-control" id="company_logo" name="company_logo">
                            <p style="margin: 12px 0px;">(upload only : .jpg, .jpeg, .gif and .png image and size should not be more than 2MB.)</p>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="UpdateProfilePicture" class="col-sm-3 "><?php echo $lang['Upload Profile Picture'] ?>
                        </label>
                        <div class="col-sm-9">
                            <input id="fileUpload" class="hidden" type="file" name="file-upload">
                            <div><button type="button" id="uploadButton" class="btn btn-danger add-company">
                                    <i class="fa fa-upload" aria-hidden="true"></i>Choose File</button></div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="box">

                                        <img src="images/user-profile.png" class="img-responsive user-profile" alt="">


                                        <span>(<?php echo $lang['upload only: .jpg, .jpeg, .gif and .png image and size should not be more than 2MB.'] ?>)
                                            </span>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>


                    <div class="form-group">
                        <div class="col-sm-12">
                            <button type="submit" class="btn btn-danger add-company pull-right" name="add_company"><?php echo $lang['Add']?></button>
                        </div>
                    </div>
                </div>
            </div>
            </form>
        </div>



</section>

<script>
    $(document).ready(function() {
        $('#uploadButton').click(function(){

            $('#fileUpload').click();
        });
    });

</script>