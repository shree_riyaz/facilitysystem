<?php
/*======================================
Developer	-	Jaishree Sahal
Module      -   Order
SunArc Tech. Pvt. Ltd.
======================================
******************************************************/
//include_once '../../templates/default/company/modal_popup.php';

$lang = $language->english('eng');

?>

<script>
    function checkAll(ele) {
        var checkboxes = document.getElementsByTagName('input');

        if (ele.checked) {
            for (var i = 0; i < checkboxes.length; i++) {
                if (checkboxes[i].type == 'checkbox') {
                    checkboxes[i].checked = true;
                }
            }
        } else {
            for (var i = 0; i < checkboxes.length; i++) {
                console.log(i)
                if (checkboxes[i].type == 'checkbox') {
                    checkboxes[i].checked = false;
                }
            }
        }
    }

    function Check() {
        if (document.getElementById('keyword').value == '') {
            alert('Please enter any value for search.');
            return false;
        }
        else {
            return true;
        }
    }
    function Clear() {
        document.getElementById('keyword').value = '';
        location.href = "index.php?mod=company&do=list";
        return false;
    }

</script>


    <section>
        <div class="col-sm-12 drop-shadow nopadding">
            <form method="post" name="frmlist" id="frmlist" class="form-horizontal">
                <?php
                if (isset($_SESSION['error'])) {
                    echo '<table cellspacing="0" cellpadding="0" border="0" align="center" width="40%" ><tbody><tr><td>
				<div class="alert alert-danger text-center alert-dismissable">
				   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
                    echo $_SESSION['error'];
                    echo '</div></td></tr></tbody></table>';
                    unset($_SESSION['error']);
                }
                if (isset($_SESSION['success'])) {
                    echo '<table cellspacing="0" cellpadding="0" border="0" align="center" width="40%"><tbody><tr><td>
				<div class="alert alert-success text-center alert-dismissable">
				   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
                    echo $_SESSION['success'];
                    echo '</div></td></tr></tbody></table>';
                    unset($_SESSION['success']);
                }
                ?>

                <div class="user-heading fixedHeader">
                    <div class="row">
                        <div class="col-xs-3">
                             <span class="side-nav-open">
                                <i class="fa fa-bars" aria-hidden="true"></i>
                            </span>
                            <span style="vertical-align: text-bottom;"><?php echo $lang['Companies']?></span>
                        </div>
                        <div class="col-md-5 col-sm-3 col-xs-2"></div>
                        <div style="margin-top:0px !important;" class="col-md-2 col-sm-3 col-xs-4 select-caret">
                            <select name="select_locale" onchange="this.form.submit()" class="form-control show-result select_locale">
                                <option style="color: white;" value="">Select Language</option>
                                <?php foreach ($get_language_list[0] as $get_language_list_list_value) { ?>
                                    <option <?php if ($get_language_list_list_value->short_code == $_SESSION['selected_language']){?> selected="selected" <?php } ?> style="color: white;" value="<?php echo $get_language_list_list_value->short_code ?>"> <?php echo trim($get_language_list_list_value->language_name); ?> </option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="col-md-2 col-xs-3">
                            <?php
                            include_once 'user_profile.php';
                            ?>
                        </div>
                    </div>



            </div>
            <div class="userbg">

                <div class="row">
                    <div class="col-sm-12 ">
                        <div class="icon-group">
                            <input type="text" id="keyword" value="<?php echo(isset($frmdata['keyword']) ? $frmdata['keyword'] : ''); ?>"
                                   onblur="return  KeywordSearch()" name="keyword" class="form-control search"
                                   placeholder="<?php echo $lang['Search Company'] ?>">
                            <i class="fa fa-search search-icon" aria-hidden="true"></i>
                            <p style="text-align: center; margin: 12px 0px;">
                                        <span style="font-size:12px;font-weight: bold;">
                                    <?php if(($_SESSION['usertype'])=='super_admin')
                                    {  ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;* <?php echo $lang['Search By Name ,Role,Phone No,Email Id,Company Name'] ?><?php }
                                    else
                                    { ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*<?php echo $lang['Search By Name , Role, Phone No, Email Id, Assigned To'] ?><?php } ?>
                                    </span>
                            </p>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-3">

                    </div>
                    <div class="col-sm-9">
                        <div class="pull-right">
                            <a title="Add New Company" href="index.php?mod=company&do=add" class="btn btn-danger add-company margin_30"><?php echo $lang['Add New Company']?></a>
                        </div>
                    </div>
                </div>
                <?php
                $srNo = $frmdata['from'];
                $count = count($Row);
                ?>
                <div class="user-heading">
                    <div class="row">
                        <div class="col-md-2 col-sm-3 select-caret">
                            <select id="action_list_id" name="actions"
                                    class="form-control show-result">
                                <option value=""><?php echo $lang['Select Action']?></option>
                                <option value="Delete"><?php echo $lang['Delete']?></option>
                                <option value="Active"><?php echo $lang['Active']?></option>
                                <option value="Inactive"><?php echo $lang['In-Active']?></option>
                            </select>
                        </div>
                        <div class="col-md-8 col-xs-6">
                            <p class="showing-results margin_4">
                                <?php
                                if($totalCount > 0)
                                    print $lang['Showing Results'].' '. ($frmdata['from']+1).'-'.($frmdata['from']+$count)." of ".$totalCount;
                                else
                                    print $lang['Showing Results'];
                                ?>
                            </p>
                        </div>
                        <div class="col-md-2 col-sm-3 col-xs-6 select-caret">
                            <?php echo getPageRecords(); ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="table-responsive">
                            <table class="table dashboard-table table-bordered">
                                <thead>
                                <tr>
                                    <th><input type="checkbox" value="">S.No</th>
                                    <th><a  style="text-decoration:none; cursor:pointer" onClick="OrderPage('o.company_name');"><?php echo $lang['Company Name']?>
                                            <?php if($frmdata['orderby']=='o.company_name') {print '<img src="'.IMAGEURL.'/asc.gif">';} elseif($frmdata['orderby']=='o.company_name desc'){print '<img src="'.IMAGEURL.'/desc.gif">';}?>
                                        </a></th>
                                    <th>
                                        <a style="text-decoration:none; cursor:pointer" onClick=
                                        "OrderPage('o.is_active');"><?php echo $lang['Company Name']?>
                                            <?php if($frmdata['orderby']=='o.is_active') {print '<img src="'.IMAGEURL.'/asc.gif">';} elseif($frmdata['orderby']=='o.is_active desc'){print '<img src="'.IMAGEURL.'/desc.gif">';}?>
                                        </a>
                                    </th>
                                    <th>
                                        <a style="text-decoration:none; cursor:pointer" onClick=
                                        "OrderPage('p.plan_name');"><?php echo $lang['Plan']?>
                                            <?php if($frmdata['orderby']=='p.plan_name') {print '<img src="'.IMAGEURL.'/asc.gif">';} elseif($frmdata['orderby']=='p.plan_name desc'){print '<img src="'.IMAGEURL.'/desc.gif">';}?>
                                        </a>
                                    </th>
                                    <th>
                                        <a style="text-decoration:none; cursor:pointer" onClick=
                                        "OrderPage('o.company_email');"><?php echo $lang['Email Id']?>
                                            <?php if($frmdata['orderby']=='o.company_email') {print '<img src="'.IMAGEURL.'/asc.gif">';} elseif($frmdata['orderby']=='o.company_email desc'){print '<img src="'.IMAGEURL.'/desc.gif">';}?>
                                        </a>
                                    </th>
                                    <th>
                                        <a style="text-decoration:none; cursor:pointer" onClick=
                                        "OrderPage('o.user_name');"><?php echo $lang['Contact Person']?>
                                            <?php if($frmdata['orderby']=='o.user_name') {print '<img src="'.IMAGEURL.'/asc.gif">';} elseif($frmdata['orderby']=='o.user_name desc'){print '<img src="'.IMAGEURL.'/desc.gif">';}?>
                                        </a>
                                    </th>
                                    <th><?php echo $lang['Members']?></th>
                                    <th><?php echo $lang['Action']?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                if ($Row) {
                                for ($counter = 0; $counter < $count; $counter++) {
                                    $srNo++;
                                    if (($counter % 2) == 0) {
                                        $trClass = "tdbggrey";
                                    } else {
                                        $trClass = "tdbgwhite";
                                    }
                                    $confirmDelete = 'All the related data of this company will be deleted. Do you really want to delete this Company ?';
                                    //added By : Neha Pareek. Dated : 10-11-2015, for getting all users of particular company.
                                    $users = $DBFilter->SelectRecords('users', 'is_deleted = "N" and company_id=' . $Row[$counter]->company_id);
                                    $cleaners = $DBFilter->SelectRecords('users', 'is_deleted = "N" and role_id ="3" and company_id=' . $Row[$counter]->company_id);
                                    $company_admin = $DBFilter->SelectRecords('users', 'is_deleted = "N" and role_id ="2" and company_id=' . $Row[$counter]->company_id);
                                    $supervisor = $DBFilter->SelectRecords('users', 'is_deleted = "N" and role_id ="4" and company_id=' . $Row[$counter]->company_id);
                                    //echo count($users[0]);
                                    //echo $Row[$counter]->company_id;
                                    ?>
                                    <tr>
                                        <td><input type="checkbox" name="chkbox[]" id="chkbox"
                                                   value="<?php echo $Row[$counter]->company_id; ?>">
                                        <?php echo $srNo; ?></td>
                                        <td><?php echo ucwords($Row[$counter]->company_name); ?>&nbsp;</td>
                                        <td><span class="status-bg-<?php if($Row[$counter]->is_active != 'Y'){echo 'inactive' ;} else {echo 'active';}?>"><?php if($Row[$counter]->is_active != 'Y'){echo ucfirst('in-Active');} else {echo ucfirst('active');}?></span></td>
                                        <td><?php echo ucfirst($Row[$counter]->plan_name); ?></td>
                                        <td><?php echo $Row[$counter]->company_email; ?></td>
                                        <td><?php echo ucfirst($Row[$counter]->user_name) ; ?></td>
                                        <td title="CompanyAdmin : <?php echo count($company_admin[0]);?>&nbsp;&nbsp; Supervisor : <?php echo count($supervisor[0]);?> &nbsp;&nbsp;Cleaners: <?php echo count($cleaners[0]);?>">
                                            <!--<a style="color : black; cursor:pointer" href="#" title="CompanyAdmin : <?php echo count($company_admin[0]);?>&nbsp;&nbsp; Supervisor : <?php echo count($supervisor[0]);?> &nbsp;&nbsp;Cleaners: <?php echo count($cleaners[0]);?>"><?php echo count($users[0]); ?></a>-->
                                            <?php echo count($users[0]); ?>
                                        </td>
                                        <td>
                                            <a href='<?php print CreateURL('index.php','mod=company&do=edit&id='.$Row[$counter]->company_id);?>' title="Edit">

                                                <i class="fa fa-pencil" aria-hidden="true"></i></a>

                                            <a href="#" data-toggle="modal" data-message="<?php echo $confirmDelete;  ?>" delete-link="<?php print CreateURL('index.php','mod=company&do=del&id='.$Row[$counter]->company_id) ?>" data-target="#myModal" class="delete-modal" ><i class="fa fa-trash" aria-hidden="true"></i></a>
                                        </td>
                                    </tr>
                                    <?php $sno++; } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="text-center">
                            <?php
                            PaginationDisplay($totalCount);
                            ?>
                        </div>
                    </div>
                </div>
            <?php
            }
            elseif($_SESSION['keywords'] == 'Y') {
                $frmdata['message']="Sorry ! Company not found for the selected criteria";
                unset ($_SESSION['keywords']);
                ShowMessage(); }
            else {
                $frmdata['message']="Sorry ! No company found. Please add company first.";
                ShowMessage(); }
            ?>
            </div>
                <input name="pageNumber" type="hidden" value="<?php print $frmdata['pageNumber']?>" >
                <input name="orderby" type="hidden" value="<?php if($frmdata['orderby']!='')  echo $frmdata['orderby']; else echo 'company_name';?>" >
                <input name="order" type="hidden" value="<?php print $frmdata['order']?>" >
                <input name="actUserID" type="hidden" value="" />
            </form>
        </div>


    </section>

