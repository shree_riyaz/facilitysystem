
<script type="text/javascript" language="javascript">
/*$(function() {
		$("#creation_date").datepicker({dateFormat: 'yy-mm-dd',
		minDate: 0
		});
		$("#expiry_date").datepicker({dateFormat: 'yy-mm-dd' ,minDate: 0});
});*/
//added By : Neha Pareek. Dated : 14-12-2015
 $(function() {
	var dateToday = new Date();
	var yrRange = dateToday.getFullYear() + ":" + (dateToday.getFullYear() + 15); // year drop down with current year to next 15 year Added By : Neha Pareek Dated : 10-11-2015
		$("#creation_date").datepicker({
		dateFormat: 'yy-mm-dd',
			minDate: 0,
			changeMonth: true,changeYear: true, yearRange: yrRange//added by : Neha Pareek. Dated : 10-11-2015	
		});
		$("#expiry_date").datepicker({
			dateFormat: 'yy-mm-dd' ,minDate: 0,
			changeMonth: true, changeYear: true, yearRange: yrRange //added by : Neha Pareek. Dated : 10-11-2015
		});
	});
function Reset() {
    document.getElementById("company_edit").reset();
}
function Clear()
{
	var id = <?php echo $_GET['id'];?>;
	document.getElementById('user_name').value='';
	document.getElementById('company_name').value='';
	document.getElementById('creation_date').value='';
	document.getElementById('expiry_date').value='';
	document.getElementById('subscription_plan').value='';
	document.getElementById('company_email').value='';
	location.href="index.php?mod=company&do=edit&id=".id;
	return false;
}
</script>


    <section>

            <?php
            // print_r($Row);exit;
            ?>

            <div class="col-sm-12 drop-shadow nopadding">
                <form method="post" class="form-horizontal" name="company_edit" id="company_edit" enctype="multipart/form-data">

                    <?php

                    //print_r($Row);
                    $lang = $language->english('eng');
                    if(isset($_SESSION['error']))
                    {
                        echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" ><tbody><tr><td colspan="6"  align="center">
				<div class="alert alert-danger alert-dismissable">
				   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
                        echo $_SESSION['error'];
                        echo '</div></td></tr></tbody></table><br>';
                        unset($_SESSION['error']);
                    }
                    if(isset($_SESSION['success']))
                    {
                        echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" ><tbody><tr><td colspan="6"  align="center">
				<div class="alert alert-success alert-dismissable">
				   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
                        echo $_SESSION['success'];
                        echo '</div></td></tr></tbody></table><br>';
                        unset($_SESSION['success']);
                    }
                    ?>
                <div class="user-heading text-left fixedHeader">
                    <span class="side-nav-open">
                                <i class="fa fa-bars" aria-hidden="true"></i>
                            </span>
                    <span><?php echo $lang['Edit Company'] ?></span>
                    <?php
                    include_once 'user_profile.php';
                    ?>
                </div>
                <div class="userbg">
                    <div id="users">
                        <h4 class="update-user"><?php echo $lang['Edit Company'] ?></h4>
                        <div class="backbtn"><button type="button" class="btn btn-danger">Back</button></div>
                    </div>
                    <div class="plan-category user-page-form">

                        <div class="form-group">
                            <label for="company_name" class="col-sm-3"><?php echo $lang['Company Name']?>
                                <sup>*</sup></label>
                            <div class="col-sm-9">
                                <input type="text" title="Enter Company Name" class="form-control" id="company_name" name="company_name"
                                       value="<?php if($_POST['company_name']) echo $_POST['company_name']; else echo $Row->company_name; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="user_name" class="col-sm-3"><?php echo $lang['Contact Person'] ?>
                                <sup>*</sup></label>
                            <div class="col-sm-9">
                                <input type="text" title="Enter User Name" class="form-control" id="user_name" name="user_name" value ="<?php if($_POST['user_name']) echo $_POST['user_name']; else echo $Row->user_name;?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="subscription_plan" class="col-sm-3"><?php echo $lang['Subscription Plan']?>
                                <sup>*</sup></label>
                            <div class="col-sm-9">
                                <select class="form-control" name="plan_id" id="subscription_plan" style="width:170px;">
                                    <option value="" >Please Select</option>
                                    <?php for($i=0;$i<count($plan[0]);$i++)
                                    {
                                        ?>
                                        <option value="<?php echo $plan[0][$i]->plan_id?>" <?php if($Row->plan_id == $plan[0][$i]->plan_id) { echo "selected"; } elseif($Row->plan_id == $_POST['plan_id']) echo "selected"; ?>><?php echo $plan[0][$i]->plan_name; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="creation_date" class="col-sm-3"><?php echo $lang['Creation Date'] ?>
                                <sup>*</sup></label>
                            <div class="col-sm-9">
                                <input type="text" title="Enter Creation Date" class="form-control" id="creation_date"  name="creation_date" value="<?php if($_POST['creation_date']) echo $_POST['creation_date']; else echo $Row->creation_date;?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="expiry_date" class="col-sm-3"><?php echo $lang['Expiry Date'] ?>
                                <sup>*</sup></label>
                            <div class="col-sm-9">
                                <input type="text" title="Enter Expiry Date" class="form-control" id="expiry_date" name="expiary_date" value="<?php if($_POST['expiary_date']) echo $_POST['expiary_date']; else echo $Row->expiary_date;?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="company_email" class="col-sm-3"><?php echo $lang['Email Id'] ?>
                                <sup>*</sup></label>
                            <div class="col-sm-9">
                                <input type="text" readonly title="Enter Email Id" class="form-control" id="company_email" name="company_email" value="<?php if($_POST['company_email']) echo $_POST['company_email']; else echo $Row->company_email;?>">
                            </div>
                        </div>



                        <div class="form-group">
                            <label for="UpdateProfilePicture" class="col-sm-3 "><?php echo $lang['Update Company Logo'] ?>
                            </label>
                            <div class="col-sm-9">
                                <input id="fileUpload" class="hidden" type="file" name="company_logo">
                                <div><button type="button" id="uploadButton" class="btn btn-danger add-company">
                                        <i class="fa fa-upload" aria-hidden="true"></i>Choose File</button></div>
                                <div class="row">
                                    <div class="col-sm-2">
                                        <?php

                                        if(trim($Row->company_logo) == '')
                                        {
                                            $image =  IMAGEURL."company_logo/no-picture.png";
                                        }
                                        else{
                                            $image = IMAGEURL."company_logo/".$Row->company_logo ;
                                        }
                                        ?>
                                        <img src="<?php echo $image; ?>" class="img-responsive user-profile" alt="">

                                    </div>
                                    <div class="col-sm-10">
                                        <p>(upload only: .jpg, .jpeg, .gif and .png image
                                            and size should not be more than 2MB.)</p>
                                    </div>
                                    <input type="hidden" name="company_logo_hidden" value="<?php echo $Row->company_logo; ?>">
                                    <input type="hidden" name="is_active" value="<?php echo $Row->is_active; ?>">

                                </div>

                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-sm-12">
                                <button type="submit" class="btn btn-danger add-company pull-right" name="update"><?php echo $lang['Update']?></button>
                            </div>
                        </div>
                    </div>
                </div>
                </form>
            </div>



    </section>

<script>
    $(document).ready(function() {
        $('#uploadButton').click(function(){

            $('#fileUpload').click();
        });
    });

</script>