
<script>
function deleteConfirm()
{
	if(confirm("Are you really want to delete ? "))
		return true;
	else
		return false;
}

</script>

<?php
$obj = new passEncDec;
$pwd  = $obj->decrypt_password($Row[0][0]->password);
?>
    <section>
        <form method="post" class="form-horizontal" name="lang_edit" id="lang_edit" enctype="multipart/form-data">
            <?php
            $lang = $language->english($lang);
            if(isset($_SESSION['error']))
            {
                echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" ><tbody><tr><td colspan="6"  align="center">
            <div class="alert alert-danger alert-dismissable">
               <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
                echo $_SESSION['error'];
                echo '</div></td></tr></tbody></table><br>';
                unset($_SESSION['error']);
            }
            if(isset($_SESSION['success']))
            {
                echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" ><tbody><tr><td colspan="6"  align="center">
        <div class="alert alert-success alert-dismissable">
           <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
                echo $_SESSION['success'];
                echo '</div></td></tr></tbody></table><br>';
                unset($_SESSION['success']);
            }
            ?>
        <div class="container-fluid page-wrapper">
        <div class="row nomargin">
            <div style="height: 100vh" class="col-sm-12 drop-shadow nopadding">
                <div class="user-heading text-left fixedHeader">
                    <div class="row">
                        <div class="col-xs-3">
                    <span class="side-nav-open">
                                <i class="fa fa-bars" aria-hidden="true"></i>
                            </span>
                    <span style="vertical-align: text-bottom">Question</span>
                            </div>
                    <div class="col-md-5 col-sm-3 col-xs-2"></div>
                    <div style="margin-top:0px !important;" class="col-md-2 col-sm-3 col-xs-4">

                    </div>
                    <div class="col-md-2 col-xs-3">
                    <?php
                    include_once 'user_profile.php';
                    ?>
                        </div>
                    </div>
                </div>
                <div class="userbg">

<!--                    </form>-->
                    <div id="users">
                    <h4 class="update-user">Update Question</h4>
                    </div>
                    <div class="plan-category user-page-form">
                        <div class="form-group">
                            <label for="form_heading" class="col-sm-3">Question Title
                                <sup>*</sup></label>
                            <div class="col-sm-9">
                                <input type="text" name="title" title="Enter Question Title" value="<?php echo $Row[0][0]->title;?>" class="form-control">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="feedback_type" class="col-sm-3">Feedback Type <sup>*</sup></label>
                            <div class="col-sm-9">
                                <select name="feedback_type" class="form-control">
                                    <option value="">Select Option</option>
                                    <option value="rating" <?php if($_POST['feedback_type'] == 'rating'){?> selected <?php }elseif($Row[0][0]->feedback_type == 'rating'){?> selected <?php }?>>Rating</option>
                                    <?php if ($Row[0][0]->feedback_type == 'survey') { ?>
                                    <option value="survey" <?php if($_POST['feedback_type'] == 'survey'){?> selected <?php }elseif($Row[0][0]->feedback_type == 'survey'){?> selected <?php }?>>Survey</option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="capture_issue_detail" class="col-sm-3">Capture Issue Detail
                                <sup>*</sup></label>
                            <div class="col-sm-9">
                                <select name="capture_issue_detail" class="form-control">
                                    <option value="">Select Option</option>
                                    <option value="Y" <?php if($_POST['capture_issue_detail'] == 'Y'){?> selected <?php }elseif($Row[0][0]->capture_issue_detail == 'Y'){?> selected <?php }else{?> <?php } ?>>YES</option>
                                    <option value="N" <?php if($_POST['capture_issue_detail'] == 'N'){?> selected <?php }elseif($Row[0][0]->capture_issue_detail == 'N'){?> selected <?php }else{?> <?php } ?>>NO</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="extra_comment" class="col-sm-3">Extra Comment <sup>*</sup></label>
                            <div class="col-sm-9">
                                <select name="extra_comment" class="form-control">
                                    <option value="">Select Option</option>
                                    <option value="Y" <?php if($_POST['extra_comment'] == 'Y'){?> selected <?php }elseif($Row[0][0]->extra_comment == 'Y'){?> selected <?php }?>>YES</option>
                                    <option value="N" <?php if($_POST['extra_comment'] == 'N'){?> selected <?php }elseif($Row[0][0]->extra_comment == 'N'){?> selected <?php }?>>NO</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="capture_fault_detail" class="col-sm-3">Capture Fault Detail<sup>*</sup></label>
                            <div class="col-sm-9">
                                <select name="capture_fault_detail" class="form-control">
                                    <option value="">Select Option</option>
                                    <option value="Y" <?php if($_POST['capture_fault_detail'] == 'Y'){?> selected <?php }elseif($Row[0][0]->capture_fault_detail == 'Y'){?> selected <?php }?>>YES</option>
                                    <option value="N" <?php if($_POST['capture_fault_detail'] == 'N'){?> selected <?php }elseif($Row[0][0]->capture_fault_detail == 'N'){?> selected <?php }?>>NO</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="sequence" class="col-sm-3">Add Rating Options <sup>*</sup></label>

                            <div class="field_wrapper col-sm-9">

                                <?php
                                if (count(current($option_list_collection))){?>


                            <?php foreach (current($option_list_collection) as $option_list_collection_key => $option_list_collection_val){ ?>

                                <select disabled="disabled" name="question_option_type[][]" class="form-control add_more">
                                    <option value="">Select Option</option>
                                    <?php foreach (current($option_lists) as $option_lists_key => $option_lists_val){  ?>
                                        <option <?php if ($option_list_collection_val->ration_option_id == $option_lists_val->id) echo 'selected' ?> value="<?php echo $option_lists_val->id ?>" ><?php echo $option_lists_val->title.' ('.$option_lists_val->field_type.')' ?></option>
                                    <?php } ?>

                                </select>

                                        <?php if ($option_list_collection_key == (count(current($option_list_collection)))-1){ ?>
                                            <a href="javascript:void(0);" style="display: none " class="add_button" title="Add field"><i class="fa fa-plus-circle123"></i></a>
                                        <?php  } ?>
                                    <br>
                                    <br>

                                <?php } }else{  ?>


                                    <select name="question_option_type[]" class="form-control add_more">
                                        <option value="">Select Option</option>
                                        <?php foreach (current($option_lists) as $option_lists_key => $option_lists_val){  ?>
                                            <option value="<?php echo $option_lists_val->id ?>" ><?php echo $option_lists_val->title.' ('.$option_lists_val->field_type.')' ?></option>
                                        <?php } ?>

                                    </select>
                                    <a href="javascript:void(0);" class="add_button" title="Add field"><i class="fa fa-plus-circle"></i></a>
                                    <br><br><br>
                                <?php }   ?>

                            </div>

                        </div>

                        <div class="form-group">
                                <div class="col-sm-12">
                                    <a class="btn btn-danger cancel_button pull-right add-company" href="<?php echo ROOTADMINURL.'/index.php?mod='.$_GET['mod'] ?>" >Cancel</a>&nbsp;
                                    <button type="submit" name="update_question" class="btn btn-danger add-company pull-right">Update Question</button>
                                </div>
                            </div>


                    </div>
                </div>
            </div>
        </div>
        </div>
        </form>

    </section>
<script>
    $(document).ready(function() {
        $('#uploadButton').click(function(){

            $('#fileUpload').click();
        });
    });

</script>
<script type="text/javascript">
    $(document).ready(function(){
        var maxField =  <?php echo isset($total_option_in_setting) ? $total_option_in_setting : 10; ?>; //Input fields increment limitation
        var addButton = $('.add_button'); //Add button selector
        var wrapper = $('.field_wrapper'); //Input field wrapper
        var option_list = [];
        var fieldHTML = '<div class="form-group"><div class="col-sm-12"><select name="question_option_type[]" class="form-control add_more"> <option value="">Select Option</option><?php foreach (current($option_lists) as $option_lists_key => $option_lists_val){  ?> <option value="<?php echo $option_lists_val->id ?>"><?php echo $option_lists_val->title.' ('.$option_lists_val->field_type.')' ?></option> <?php } ?></select><a href="javascript:void(0);" class="remove_button"><i class="fa fa-minus-circle"></i></a></div></div></div>'; //New input field html
        var x = 1; //Initial field counter is 1

        //Once add button is clicked
        $(addButton).click(function(){
            //Check maximum number of input fields
            if(x < maxField){
                x++; //Increment field counter
                $(wrapper).append(fieldHTML); //Add field html
            }
        });

        //Once remove button is clicked
        $(wrapper).on('click', '.remove_button', function(e){
            e.preventDefault();
            $(this).parent('div').remove(); //Remove field html
            x--; //Decrement field counter
        });
    });
</script>