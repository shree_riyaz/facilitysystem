<?php
/*======================================
Developer	-	Jaishree Sahal
Module      -   USer
SunArc Tech. Pvt. Ltd.
======================================
******************************************************/
?>

<script>

    function checkAll(ele) {
        var checkboxes = document.getElementsByTagName('input');

        if (ele.checked) {
            for (var i = 0; i < checkboxes.length; i++) {
                if (checkboxes[i].type == 'checkbox') {
                    checkboxes[i].checked = true;
                }
            }
        } else {
            for (var i = 0; i < checkboxes.length; i++) {
                console.log(i);
                if (checkboxes[i].type == 'checkbox') {
                    checkboxes[i].checked = false;
                }
            }
        }
    }

    function showpassdiv(id) {
        $("#password_" + id).css("display", "block");
    }
    function Check() {
        if (document.getElementById('keyword').value == '') {
            alert('Please enter any value for search.');
            return false;
        }
        else {
            return true;
        }
    }
    function Clear() {
        document.getElementById('keyword').value = '';
        location.href = "index.php?mod=user&do=list";
        return false;
    }
</script>


    <section>


        <div style="height: 100vh" class="col-sm-12 drop-shadow nopadding">

            <form method="post" name="frmlist" id="frmlist">
                <?php
                if (isset($_SESSION['ActionAccess'])) {
                    echo '<table cellspacing="0" cellpadding="0" border="0" align="center" width="90%" ><tbody><tr><td colspan="6"  align="center"><div class="errormsg">';
                    echo $_SESSION['ActionAccess'];
                    echo '</div></td></tr></tbody></table>';
                    unset($_SESSION['ActionAccess']);

                }
                if (isset($_SESSION['error'])) {
                    echo '<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" ><tbody><tr><td colspan="6"  align="center">
				<div class="alert alert-danger alert-dismissable">
				   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
                    echo $_SESSION['error'];
                    echo '</div></td></tr></tbody></table>';
                    unset($_SESSION['error']);
                }
                if (isset($_SESSION['success'])) {
                    echo '<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" ><tbody><tr><td colspan="6"  align="center">
            <div class="alert alert-success alert-dismissable">
               <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
                    echo $_SESSION['success'];
                    echo '</div></td></tr></tbody></table>';
                    unset($_SESSION['success']);
                }
                ?>
            <div class="user-heading fixedHeader">
                <div class="row">
                    <div class="col-xs-3">
                  <span class="side-nav-open">
                                <i class="fa fa-bars" aria-hidden="true"></i>
                            </span>
                <span style="vertical-align: text-bottom"><?php echo $lang['Question'] ?> </span>
                        </div>
                <div class="col-md-5 col-sm-3 col-xs-2"></div>
                <div style="margin-top:0px !important;" class="col-md-2 col-sm-3 col-xs-4">

                </div>
                <div class="col-md-2 col-xs-3">
                <?php
                include_once 'user_profile.php';
                ?>
                    </div>
                </div>
            </div>
            <div class="userbg">




                <div class="row">


                    <div style="padding:0px 0px 30px 0px; overflow: hidden;" class="col-sm-12">
                        <div class="pull-right">
                                <a style="margin-right: 14px;" title="" href="index.php?mod=question&do=add" class="btn btn-danger add-company "><?php echo $lang['Add Question'] ?></a>
                            <?php echo '<br>';  ?>

                        </div>
                    </div>

                </div>
                <?php
                $srNo = $frmdata['from'];
                $count = count($Row);
                ?>
                <div id="users">
                    <h4 class="update-user">Feedback Question Details</h4>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="table-responsive">
                            <table class="table dashboard-table table-bordered">
                                <thead>
                                <tr>
                                    <th class="text-center"><a  onClick="OrderPage('title');" style="text-decoration:none; cursor:pointer" >Title
                                            <?php if($frmdata['orderby']=='title') {print '<img src="'.IMAGEURL.'/asc.gif">';} elseif($frmdata['orderby']=='title desc'){print '<img src="'.IMAGEURL.'/desc.gif">';}?>
                                        </a></th>
                                        <th class="text-center">
                                            <a onClick="OrderPage('feedback_type');" style="text-decoration:none; cursor:pointer" >Feedback Type
                                                <?php if($frmdata['orderby']=='feedback_type') {print '<img src="'.IMAGEURL.'/asc.gif">';} elseif($frmdata['orderby']=='feedback_type desc'){print '<img src="'.IMAGEURL.'/desc.gif">';}?>
                                            </a>
                                        </th>

                                    <th class="text-center">
                                        <a onClick="OrderPage('capture_issue_detail');" style="text-decoration:none; cursor:pointer" >Capture Issue Detail
                                            <?php if($frmdata['orderby']=='capture_issue_detail') {print '<img src="'.IMAGEURL.'/asc.gif">';} elseif($frmdata['orderby']=='capture_issue_detail desc'){print '<img src="'.IMAGEURL.'/desc.gif">';}?></a>
                                    </th>

                                    <th class="text-center">

                                        <a onClick="OrderPage('extra_comment');" style="text-decoration:none; cursor:pointer" >Extra Comment
                                            <?php if($frmdata['orderby']=='extra_comment') {print '<img src="'.IMAGEURL.'/asc.gif">';} elseif($frmdata['orderby']=='extra_comment desc'){print '<img src="'.IMAGEURL.'/desc.gif">';}?></a>
                                        </th>
                                    <th class="text-center">

                                        <a onClick="OrderPage('capture_fault_detail');" style="text-decoration:none; cursor:pointer" >Capture Fault Detail
                                            <?php if($frmdata['orderby']=='capture_fault_detail') {print '<img src="'.IMAGEURL.'/asc.gif">';} elseif($frmdata['orderby']=='capture_fault_detail desc'){print '<img src="'.IMAGEURL.'/desc.gif">';}?></a>
                                    </th>
                                    <th class="text-center">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                if($Row)
                                {
                                $objpassEncDec = new passEncDec;
                                for($counter=0;$counter<$count;$counter++)
                                {
                                    $srNo++;
                                    if(($counter%2)==0)
                                    {
                                        $trClass="tdbggrey";
                                    }
                                    else
                                    {
                                        $trClass="tdbgwhite";
                                    }

                                    $confirmDelete = 'All the related data of this user will be deleted . Do you really want to delete this User ?';
                                    $obj = new passEncDec;
                                    ?>
                                    <tr>

                                        <td class="text-center"><?php echo isset($Row[$counter]->title) ? ucfirst($Row[$counter]->title) : 'NA'; ?></td>
                                        <td class="text-center"><?php echo isset($Row[$counter]->feedback_type) ? ucfirst($Row[$counter]->feedback_type) : 'NA'; ?></td>
                                        <td class="text-center"> <?php if ($Row[$counter]->capture_issue_detail == 'N'){echo 'NO';} else { echo 'YES' ;}?></td>
                                        <td class="text-center"> <?php if ($Row[$counter]->extra_comment == 'N'){echo 'NO';} else { echo 'YES' ;}?></td>
                                        <td class="text-center"> <?php if ($Row[$counter]->capture_fault_detail == 'N'){echo 'NO';} else { echo 'YES' ;}?></td>
                                        <td class="text-center">
                                            <a class="fontstyle" href='<?php print CreateURL('index.php','mod=question&do=edit&id='.$Row[$counter]->id);?>' title="Edit" ><img src="<?php echo IMAGEURL."/b_edit.png" ?>" border=0 />
                                            </a>&nbsp;
<!--                                            <a href="#" data-toggle="modal" data-message="--><?php //echo $confirmDelete;  ?><!--" delete-link="--><?php //print CreateURL('index.php','mod=question&do=del&id='.$Row[$counter]->id) ?><!--" data-target="#myModal" class="delete-modal" ><img src="--><?php //print IMAGEURL ?><!--/b_drop.png"></a>-->
                                        </td>
                                    </tr>
                                    <?php $sno++; } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="text-center">
                            <?php
                            PaginationDisplay($totalCount);
                            ?>
                        </div>
                    </div>
                </div>
            <?php
            }
            elseif($_SESSION['keywords'] == 'Y') {
                $frmdata['message']="Sorry! No record found for the selected criteria";
                unset ($_SESSION['keywords']);
                ShowMessage(); }
            else {
                $frmdata['message']="Sorry! No feedback setting created yet.";
                ShowMessage(); }
            ?>
            </div>
                <input name="pageNumber" type="hidden" value="<?php print $frmdata['pageNumber'] ?>">
                <input name="orderby" type="hidden"
                       value="<?php if ($frmdata['orderby'] != '') echo $frmdata['orderby']; else echo 'id desc'; ?>">
                <input name="order" type="hidden" value="<?php print $frmdata['order'] ?>">
                <input name="actUserID" type="hidden" value=""/>
            </form>
        </div>

    </section>

<script>
    $parent.find('button[name=download]').click(function () {
        window.location.href = 'download.php';
    });
</script>