<?php
	/*======================================
	Developer	-	Jaishree Sahal
	Module      -   User
	SunArc Tech. Pvt. Ltd.
	======================================
	******************************************************/
	$lang = $language->english($lang);
?>
<script>
function Clear()
{	//alert(document.getElementById('role_id')[0].value);
	try{
	document.getElementById('user_phone').value='';
	//alert(document.getElementById('user_phone').value);
	document.getElementById('first_name').value='';
	document.getElementById('last_name').value='';
	document.getElementById('user_email').value='';
	//document.getElementById('confirm_password').value='';
	//document.getElementById('password').value='';
	document.getElementById('role_id').value='';
	//document.getElementById('superviser_row').style.display = 'none';
	if(document.getElementById('assigned_to'))	// to hide Assingned to selectbox on Reset form Added By : Neha Pareek
	{
		document.getElementById('assigned_to').value='';
		document.getElementById('superviser_row').style.display = 'none';
	}
	document.getElementById('profile_image').value='';
	return false;
	}
	catch(e)
	{
		alert("Error: " + e.description);
	}
}
</script>

<section>
    <form method="post" name="user_add" class="form-horizontal" id="user_add" enctype="multipart/form-data">


        <div class="container-fluid page-wrapper">
            <div class="row nomargin">
                <div style="100vh" class="col-sm-12 drop-shadow nopadding">
                    <div class="user-heading text-left fixedHeader">
                        <div class="row">
                            <div class="col-xs-3">
                         <span class="side-nav-open">
                                <i class="fa fa-bars" aria-hidden="true"></i>
                            </span>
                        <span style="vertical-align: text-bottom">Question</span>
                                </div>
                            <div class="col-md-5 col-sm-3 col-xs-2"></div>
                            <div style="margin-top:0px !important;" class="col-md-2 col-sm-3 col-xs-4">

                            </div>
                            <div class="col-md-2 col-xs-3">
                        <?php
                        include_once 'user_profile.php';
                        ?>
                                </div>
                            </div>
                    </div>
                    <div class="userbg">
                        <?php

                        if(isset($_SESSION['error']))
                        {
                            echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" ><tbody><tr><td colspan="6"  align="center">
        <div class="alert alert-danger alert-dismissable">
           <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
                            echo $_SESSION['error'];
                            echo '</div></td></tr></tbody></table><br>';
                            unset($_SESSION['error']);
                        }
                        if(isset($_SESSION['success']))
                        {
                            echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" ><tbody><tr><td colspan="6"  align="center">
				<div class="alert alert-success alert-dismissable">
				   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
                            echo $_SESSION['success'];
                            echo '</div></td></tr></tbody></table><br>';
                            unset($_SESSION['success']);
                        }
                        ?>
                        <div id="users">
                            <h4 class="update-user">Add New Question</h4>
                        </div>
                        <div class="plan-category user-page-form">
                            <div class="form-group">
                                <label for="form_heading" class="col-md-3 col-sm-6">Question Title
                                    <sup>*</sup></label>
                                <div class="col-md-9 col-sm-6">
                                    <input type="text" name="title" title="Enter Question Title" placeholder="Enter Question Title" value="<?php echo $_POST['title'];?>" class="form-control">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="feedback_type" class="col-md-3 col-sm-6">Feedback Type <sup>*</sup></label>
                                <div class="col-md-9 col-sm-6">
                                    <select name="feedback_type" class="form-control">
                                        <option value="">Select Option</option>
                                        <option value="rating" <?php if($_POST['feedback_type']=='rating') echo 'selected="selected"';?>>Rating</option>
                                        <?php if ($is_attach_survey == 'Y') { ?>
                                            <option value="survey" <?php if($_POST['feedback_type']=='survey') echo 'selected="selected"';?>>Survey</option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="capture_issue_detail" class="col-md-3 col-sm-6">Capture Issue Detail
                                    <sup>*</sup></label>
                                <div class="col-md-9 col-sm-6">
                                    <select name="capture_issue_detail" class="form-control">
                                        <option value="">Select Option</option>
                                        <option value="Y" <?php if($_POST['capture_issue_detail']=='Y') echo 'selected="selected"';?>>YES</option>
                                        <option value="N" <?php if($_POST['capture_issue_detail']=='N') echo 'selected="selected"';?>>NO</option>
                                    </select>                                </div>
                            </div>

                            <div class="form-group">
                                <label for="extra_comment" class="col-md-3 col-sm-6">Extra Comment <sup>*</sup></label>
                                <div class="col-md-9 col-sm-6">
                                    <select name="extra_comment" class="form-control">
                                        <option value="">Select Option</option>
                                        <option value="Y" <?php if($_POST['extra_comment']=='Y') echo 'selected="selected"';?>>YES</option>
                                        <option value="N" <?php if($_POST['extra_comment']=='N') echo 'selected="selected"';?>>NO</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="capture_fault_detail" class="col-md-3 col-sm-6">Capture Fault Detail<sup>*</sup></label>
                                <div class="col-md-9 col-sm-6">
                                    <select name="capture_fault_detail" class="form-control">
                                        <option value="">Select Option</option>
                                        <option value="Y" <?php if($_POST['capture_fault_detail']=='Y') echo 'selected="selected"';?>>YES</option>
                                        <option value="N" <?php if($_POST['capture_fault_detail']=='N') echo 'selected="selected"';?>>NO</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="question_option_type" class="col-md-3 col-sm-6">Add Rating Options <sup>*</sup></label>

                                <div class="field_wrapper col-md-9 col-sm-6">
                                <select name="question_option_type[]" class="form-control add_more">
                                    <option value="">Select Option</option>
                                    <?php foreach (current($option_lists) as $option_lists_key => $option_lists_val){  ?>
                        <option value="<?php echo $option_lists_val->id ?>" ><?php echo $option_lists_val->title.' ('.$option_lists_val->field_type.')' ?></option>
                                    <?php } ?>
                                </select>
                                <a href="javascript:void(0);" class="add_button" title="Add field"><i class="fa fa-plus-circle"></i></a>
                                    <br><br><br>
                                </div>

                            </div>

                        <div class="form-group">
                            <div class="col-sm-12">
                                <a class="btn btn-danger cancel_button pull-right add-company" href="<?php echo ROOTADMINURL.'/index.php?mod='.$_GET['mod'] ?>" >Cancel</a>&nbsp;
                                <button type="submit" name="add_question" class="btn btn-danger add-company pull-right">Save Question</button>
                            </div>
                        </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    </section>

<script>
    $(document).ready(function() {
        $('#uploadButton').click(function(){

            $('#fileUpload').click();
        });
    });

</script>

<script type="text/javascript">
    $(document).ready(function(){
        var maxField = <?php echo isset($total_option_in_setting) ? $total_option_in_setting : 10; ?>; //Input fields increment limitation
        var addButton = $('.add_button'); //Add button selector
        var wrapper = $('.field_wrapper'); //Input field wrapper
        var option_list = [];
        var fieldHTML = '<div class="form-group"><div class="col-sm-12"><select name="question_option_type[]" class="form-control add_more"> <option value="">Select Option</option><?php foreach (current($option_lists) as $option_lists_key => $option_lists_val){  ?> <option value="<?php echo $option_lists_val->id ?>"><?php echo $option_lists_val->title.' ('.$option_lists_val->field_type.')' ?></option> <?php } ?></select><a href="javascript:void(0);" class="remove_button"><i class="fa fa-minus-circle"></i></a></div></div>'; //New input field html
        var x = 1; //Initial field counter is 1

        //Once add button is clicked
        $(addButton).click(function(){
            //Check maximum number of input fields
            if(x < maxField){
                x++; //Increment field counter
                $(wrapper).append(fieldHTML); //Add field html
            }
        });

        //Once remove button is clicked
        $(wrapper).on('click', '.remove_button', function(e){
            e.preventDefault();
            $(this).parent('div').remove(); //Remove field html
            x--; //Decrement field counter
        });
    });
</script>