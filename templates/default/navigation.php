<?php
defined("ACCESS") or die("Access Restricted");
if(isset($_GET['mod']))
{
	$mod= $_GET['mod'];
}
else
{
	$mod='';
}

$admin_info = $DB->SelectRecord('admin_users','user_name = "admin"');
if($_GET['install']!='yes')
{
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>online-exam</title>

<div id="tabs-header">
		<ul id="primary">
			<?php
				$admin_user = $DB->SelectRecord('admin_users');
			 //Call the isModuleAccessible($module,$user_id) and decide whether to show the module in navigation or not
			 if ($_SESSION['admin_user_name'] == $admin_user->user_name || $auth->isModuleAccessible('dashboard',$user_id) || $auth->isModuleAccessible('query',$user_id)):
			 ?>
				<?php if ($mod=='dashboard' || $mod == 'query') {?>
				<li><span>Dashboard</span>
					<ul id="secondary" style="right: 10px;">
						<!--<li style="float:left;"><a href="<?php print CreateURL('index.php','mod=query');?>">Query</a></li>
						<li style="float:left;"><a href="<?php print CreateURL('index.php','mod=help');?>"><img src="images/help.gif" title="help"></a></li>-->
						<li style="float:right;">Welcome, <?php echo ucfirst($_SESSION['admin_user_name']); ?></li>
					</ul>
				</li>
				<? }
				else
				{
				?>
				<li><a href="<?php print CreateURL('index.php','mod=dashboard&do=showinfo');?>">Dashboard</a></li>
				<? }?>
			<?php
			endif;
			?>
			<?php
			 if ($_SESSION['admin_user_name'] == $admin_user->user_name ||  $auth->isModuleAccessible('institute',$user_id) ||  $auth->isModuleAccessible('course',$user_id) ||  $auth->isModuleAccessible('cbt',$user_id) ||  $auth->isModuleAccessible('promotion_cadre',$user_id)):
				if ($mod=='institute' || $mod=='course' || $mod=='cbt' || $mod=='promotion_cadre'|| $mod=='sub_promotion_cadre') {?>
				<li><span>Course</span>
					<ul id="secondary" style="right: 10px;">
						<li><a href="<?php print CreateURL('index.php','mod=institute&do=manage');?>">Manage Institute</a></li>
						<li><a href="<?php print CreateURL('index.php','mod=institute&do=add');?>">Add Institute</a></li>
						<li><a href="<?php print CreateURL('index.php','mod=course&do=manage');?>">Manage Course</a></li>
						<li><a href="<?php print CreateURL('index.php','mod=course&do=add');?>">Add Course</a></li>
						<li><a href="<?php print CreateURL('index.php','mod=cbt&do=manage');?>">Manage CBT</a></li>
						<li><a href="<?php print CreateURL('index.php','mod=cbt&do=add');?>">Add CBT</a></li>
						<li><a href="<?php print CreateURL('index.php','mod=promotion_cadre&do=manage');?>">Manage Syllabus</a></li>
                      <li><a href="<?php print CreateURL('index.php','mod=promotion_cadre&do=add');?>">Add Syllabus</a></li>
                      <li><a href="<?php print CreateURL('index.php','mod=sub_promotion_cadre&do=manage');?>">Manage cadre</a></li>
                      <li><a href="<?php print CreateURL('index.php','mod=sub_promotion_cadre&do=add');?>">Add cadre</a></li>
						<!--<li style="float:right;"><a href="<?php print CreateURL('index.php','mod=help');?>"><img src="images/help.gif" title="help"></a></li>
					--></ul>
				</li>
				<? }
				else
				{
				?>
				<li><a href="<?php print CreateURL('index.php','mod=institute&do=manage&master_nav=1');?>">Course</a></li>
				<?
				}
			endif;
?>
			<?php
			 if ($_SESSION['admin_user_name'] == $admin_user->user_name ||  $auth->isModuleAccessible('test_master',$user_id) || $auth->isModuleAccessible('examination',$user_id)):
				if ($mod=='test_master' || $mod=='examination' || $mod=='paper') {?>
				<li><span>Exam</span>
					<ul id="secondary">
						<li><a href="<?php print CreateURL('index.php','mod=examination&do=manage');?>">Manage Exams</a></li>
						<li><a href="<?php print CreateURL('index.php','mod=examination&do=add');?>">Add Exam</a></li>
						<li><a href="<?php print CreateURL('index.php','mod=test_master&do=manage');?>">Manage Tests</a></li>
						<li><a href="<?php print CreateURL('index.php','mod=test_master&do=add');?>">Add Test</a></li>
						<li><a href="<?php print CreateURL('index.php','mod=test_master&do=archive');?>">Archive Tests</a></li>
						<li><a href="<?php print CreateURL('index.php','mod=paper&do=manage');?>">Manage Papers</a></li>
						<li><a href="<?php print CreateURL('index.php','mod=paper&do=add');?>">Add Paper</a></li>
						<li><a href="<?php print CreateURL('index.php','mod=paper&do=archive');?>">Archive Paper</a></li>
					</ul>
				</li>
				<? }
				else
				{
				?>
				<li><a href="<?php print CreateURL('index.php','mod=examination&do=manage&master_nav=1');?>">Exam</a></li>
				<?
				}
			endif;
			 if ($_SESSION['admin_user_name'] == $admin_user->user_name ||  $auth->isModuleAccessible('question_master',$user_id)):
				if ($mod=='question_master') {?>
				<li><span>Question</span>
					<ul id="secondary">
						<li><a href="<?php print CreateURL('index.php','mod=question_master&do=manage');?>">Manage Questions</a></li>
						<li><a href="<?php print CreateURL('index.php','mod=question_master&do=add');?>">Add Question</a></li>
						<li><a href="<?php print CreateURL('index.php','mod=question_master&do=upload_question');?>">Upload Question details</a></li>
						<li><a href="<?php print CreateURL('index.php','mod=question_master&do=export_question');?>">Export Questions</a></li>
					</ul>
				</li>
				<? }
				else
				{
				?>
				<li><a href="<?php print CreateURL('index.php','mod=question_master&do=manage');?>">Question</a></li>
				<!--<li><a href="#">Question Master</a></li>-->
				<?
				}
			endif;
			if ($_SESSION['admin_user_name'] == $admin_user->user_name ||  $auth->isModuleAccessible('stream_master',$user_id) || $auth->isModuleAccessible('grade_master',$user_id) || $auth->isModuleAccessible('subject_master',$user_id)):
				if ($mod=='stream_master' || $mod=='grade_master' || $mod=='subject_master') {?>
				<li ><span>Trade</span>
					<ul id="secondary">
						<li><a href="<?php print CreateURL('index.php','mod=stream_master&do=manage');?>">Manage Trade</a></li>
						<li><a href="<?php print CreateURL('index.php','mod=stream_master&do=add');?>">Add Trade</a></li>
						<li><a href="<?php print CreateURL('index.php','mod=grade_master&do=manage');?>">Manage Grade</a></li>
						<li><a href="<?php print CreateURL('index.php','mod=grade_master&do=add');?>">Add Grade</a></li>
						<li><a href="<?php print CreateURL('index.php','mod=subject_master&do=manage');?>">Manage Subject</a></li>
						<li><a href="<?php print CreateURL('index.php','mod=subject_master&do=add');?>">Add Subject</a></li>
					<!--<li><a href="#">Add Question Bank</a></li>-->
					</ul>
				</li>
				<? }
				else
				{
				?>
				<li><a href="<?php print CreateURL('index.php','mod=stream_master&do=manage&master_nav=1');?>">Trade</a></li>
				<!--<li><a href="#">Question Master</a></li>-->
				<? }
			endif;
			if ($_SESSION['admin_user_name'] == $admin_user->user_name ||  $auth->isModuleAccessible('equipment',$user_id) || $auth->isModuleAccessible('subequipment',$user_id)):
				if ($mod == 'equipment' || $mod == 'subequipment')
				{
				?>
					<li><span>Equipment</span>
						<ul id="secondary">
							<li><a href="<?php print CreateURL('index.php','mod=equipment&do=manage');?>">Manage Equipment</a></li>
							<li><a href="<?php print CreateURL('index.php','mod=equipment&do=add');?>">Add Equipment</a></li>
							<li><a href="<?php print CreateURL('index.php','mod=subequipment&do=manage');?>">Manage Sub-Trade</a></li>
							<li><a href="<?php print CreateURL('index.php','mod=subequipment&do=add');?>">Add Sub-Trade</a></li>
						</ul>
					</li>
				<?
				}
				else
				{
				?>
					<li><a href="<?php print CreateURL('index.php','mod=equipment&do=manage&master_nav=1');?>">Equipment</a></li>
				<?
				}
			endif;
			if ($_SESSION['admin_user_name'] == $admin_user->user_name ||  $auth->isModuleAccessible('rank_master',$user_id)):
				if ($mod=='rank_master') {?>
				<li><span>Rank</span>
					<ul id="secondary">
						<li><a href="<?php print CreateURL('index.php','mod=rank_master&do=manage');?>">Rank Details</a></li>
						<li><a href="<?php print CreateURL('index.php','mod=rank_master&do=add');?>">Add Rank</a></li>
					</ul>
				</li>
				<? }
				else
				{
				?>
				<li><a href="<?php print CreateURL('index.php','mod=rank_master&do=manage');?>">Rank</a></li>
				<? }

			endif;
			if ($_SESSION['admin_user_name'] == $admin_user->user_name ||  $auth->isModuleAccessible('unit',$user_id) || $auth->isModuleAccessible('subunit',$user_id) || $auth->isModuleAccessible('arm_master',$user_id) || $auth->isModuleAccessible('command_master',$user_id) || $auth->isModuleAccessible('corps_master',$user_id) ||$auth->isModuleAccessible('division_master',$user_id) || $auth->isModuleAccessible('brigade_master',$user_id)):
				if ($mod=='unit' || $mod == 'subunit' || $mod == 'arm_master' || $mod == 'command_master' || $mod == 'corps_master' || $mod == 'division_master' || $mod == 'brigade_master') {?>
					<li><span>Unit</span>
						<ul id="secondary" style="font-size: 12px;width: 935px;" >

							<li ><a href="<?php print CreateURL('index.php','mod=arm_master&do=manage');?>">Arm</a></li>
							<li><a href="<?php print CreateURL('index.php','mod=arm_master&do=add');?>">Add Arm</a></li>

							<li><a href="<?php print CreateURL('index.php','mod=command_master&do=manage');?>">Command</a></li>
							<li><a href="<?php print CreateURL('index.php','mod=command_master&do=add');?>">Add Command</a></li>

							<li><a href="<?php print CreateURL('index.php','mod=corps_master&do=manage');?>">Corps</a></li>
							<li><a href="<?php print CreateURL('index.php','mod=corps_master&do=add');?>">Add Corps</a></li>

							<li><a href="<?php print CreateURL('index.php','mod=division_master&do=manage');?>">Division</a></li>
							<li><a href="<?php print CreateURL('index.php','mod=division_master&do=add');?>">Add Division</a></li>

							<li><a href="<?php print CreateURL('index.php','mod=brigade_master&do=manage');?>">Brigade</a></li>
							<li><a href="<?php print CreateURL('index.php','mod=brigade_master&do=add');?>">Add Brigade</a></li>

							<li><a href="<?php print CreateURL('index.php','mod=unit&do=manage');?>">Units</a></li>
							<li><a href="<?php print CreateURL('index.php','mod=unit&do=add');?>">Add Unit</a></li>

							<li><a href="<?php print CreateURL('index.php','mod=subunit&do=manage');?>">Subunit</a></li>
							<li><a href="<?php print CreateURL('index.php','mod=subunit&do=add');?>">Add Subunit</a></li>
						</ul>
					</li>
				<? }
				else
				{
				?>
					<li><a href="<?php print CreateURL('index.php','mod=unit&do=manage&master_nav=1');?>">Unit</a></li>
				<?
				 }
			endif;
			if ($_SESSION['admin_user_name'] == $admin_user->user_name ||  $auth->isModuleAccessible('report',$user_id)):
				if ($mod=='report') {?>
				<li><span>Reports</span>
					<ul id="secondary">
						<li><a href="<?php print CreateURL('index.php','mod=report&do=result_sheet');?>">Result Sheets</a></li>
						<li><a href="<?php print CreateURL('index.php','mod=report&do=candidate_detail');?>">Candidate Details</a></li>
						<li><a href="<?php print CreateURL('index.php','mod=report&do=test_detail');?>">IAFD - 931</a></li>
						<li><a href="<?php print CreateURL('index.php','mod=report&do=candidate_performance');?>">Candidate Performance</a></li>
						<li><a href="<?php print CreateURL('index.php','mod=report&do=intelligence_performance');?>">intelligent test performance</a></li>
						<li><a href="<?php print CreateURL('index.php','mod=report&do=sample_test_performance');?>">Sample Test Performance</a></li>
					</ul>
				</li>
				<? }
				else
				{
				?>
				<li><a href="<?php print CreateURL('index.php','mod=report&do=result_sheet');?>">Reports</a></li>
				<? }
			endif;
			if ($_SESSION['admin_user_name'] == $admin_user->user_name ||  $auth->isModuleAccessible('candidate_master',$user_id)):
				if ($mod=='candidate_master') {?>
				<li><span>Candidate</span>
					<ul id="secondary">
						<li><a href="<?php print CreateURL('index.php','mod=candidate_master&do=manage');?>">Candidates Details</a></li>
						<li><a href="<?php print CreateURL('index.php','mod=candidate_master&do=add');?>">Add Candidate</a></li>
						<li><a href="<?php print CreateURL('index.php','mod=candidate_master&do=upload_candidate');?>">Upload Candidate Details</a></li>
						<li><a href="<?php print CreateURL('index.php','mod=candidate_master&do=query');?>">Query</a></li>
					</ul>
				</li>
				<? }
				else
				{
				?>
					<li><a href="<?php print CreateURL('index.php','mod=candidate_master&do=manage');?>">Candidate</a></li>
				<? }
			endif;
			?>
			<?php
			if ($_SESSION['admin_user_name'] == $admin_user->user_name ||  $auth->isModuleAccessible('backup',$user_id)):
					if ($mod == 'backup')
					{
						echo '<li><span>Backup</span>
								<ul id="secondary">
									<li><a href="'.CreateURL('index.php','mod=backup&do=list').'">Backup Details</a></li>
									<li><a href="'.CreateURL('index.php','mod=backup&do=takeBackup').'">Take Backup</a></li>
									<li><a href="'.CreateURL('index.php','mod=backup&do=upload').'">Restore Backup</a></li>
								</ul>
							</li>';
					}
					else
					{
						echo '<li><a href="'.CreateURL('index.php','mod=backup').'">Backup</a></li>';
					}
			endif;
			?>
			<?php
			if ($_SESSION['admin_user_name'] == $admin_user->user_name ||  $auth->isModuleAccessible('role',$user_id)):
				if ($mod == 'role' || $mod == 'assignrole' || $mod == 'users')
				{
					echo '<li><span>Role</span>
							<ul id="secondary">
								<li><a href="'.CreateURL('index.php','mod=role').'">Manage Role</a></li>
								<li><a href="'.CreateURL('index.php','mod=role&do=add').'">Add Role</a></li>
								<li><a href="'.CreateURL('index.php','mod=assignrole&do=assign').'">Assign Role</a></li>
								<li><a href="'.CreateURL('index.php','mod=assignrole').'">Admin Users</a></li>';
								$admin_user = $DB->SelectRecord('admin_users');

									if (($_SESSION['admin_user_name'] == $admin_user->user_name)){
									echo '<li><a href="'.CreateURL('index.php','mod=users&do=cpass&nameID='.$admin_info->id).'">Change Admin Password</a></li>';
								}
					echo '	</ul>
						</li>';
				}
				else
				{
					echo '<li><a href="'.CreateURL('index.php','mod=role').'">Role</a></li>';
				}
			endif;
			?>
			<?php //if ($mod=='board_member') {?>
			<!--<li><span>Board Member</span>
				<ul id="secondary">
					<li><a href="<?php print CreateURL('index.php','mod=board_member&do=manage');?>">Member Details</a></li>
					<li><a href="<?php print CreateURL('index.php','mod=board_member&do=add');?>">Add Member</a></li>
				</ul>
			</li>
			--><? /*}
			else
			{*/
			?>
<!--			<li><a href="<?php print CreateURL('index.php','mod=board_member&do=manage');?>">Board Member</a></li>-->
			<? //} ?>
			<?php
			if ($_SESSION['admin_user_name'] == $admin_user->user_name ||  $auth->isModuleAccessible('preferences',$user_id)):
			?>

				<?php if ($mod=='preferences') {?>
				<!--<li><span>Configuration</span></li>-->
				<? }
				else
				{
				?>
				<!--<li><a href="<?php print CreateURL('index.php','mod=preferences&do=view');?>">Configuration</a></li>-->
				<? } ?>
			<?php
			endif;
			?>
			<?php
			 //Call the isModuleAccessible($module,$user_id) and decide whether to show the module in navigation or not
			 if ($_SESSION['admin_user_name'] == $admin_user->user_name || $auth->isModuleAccessible('feedback',$user_id)):
			 ?>
				<?php if ($mod=='feedback') {
				echo '<li><span>Feedback</span>
							<ul id="secondary">
								<li><a href="'.CreateURL('index.php','mod=feedback').'">Manage Feedback</a></li>
								<li><a href="'.CreateURL('index.php','mod=feedback&do=archive').'">Archive Feedback</a></li>';

					echo '	</ul>
						</li>'; }
				else
				{
				?>
				<li><a href="<?php print CreateURL('index.php','mod=feedback');?>">Feedback</a></li>
				<? }?>
			<?php
			endif;
			?>

			<li><a href="index.php?pid=103" style="color:black">Logout</a></li>
			<?php
				 if ($_SESSION['admin_user_name'] == $admin_user->user_name || $auth->isModuleAccessible('feedback',$user_id)):
			?>
			<?php if ($mod=='help') {?>
				<li><span>help</span>
				<ul id="secondary" style="right: 10px;">
					<li style="float:right;padding-right:20px;"><a href="<?php echo ROOTURL."/docs/flow_chart.docx"?>" >Flow Chart</a></li>
					</ul>
				</li>
				<? }
				else
				{
				?>
				<li ><a href="<?php print CreateURL('index.php','mod=help');?>"><img src="images/help.gif" title="help" height="14"></a></li>
				<? }?>
				<?php endif; ?>

		</ul>

</div>
<?php
}
?>