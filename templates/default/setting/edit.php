
<script>
function deleteConfirm()
{
	if(confirm("Are you really want to delete ? "))
		return true;
	else
		return false;
}

</script>

<?php
$obj = new passEncDec;
$pwd  = $obj->decrypt_password($Row[0][0]->password);
?>
    <section>
        <form method="post" class="form-horizontal" name="lang_edit" id="lang_edit" enctype="multipart/form-data">

        <div class="container-fluid page-wrapper">
        <div class="row nomargin">
            <div style="height: 100vh" class="col-sm-12 drop-shadow nopadding">
                <div class="user-heading text-left fixedHeader">
                    <div class="row">
                        <div class="col-md-3 col-sm-4 col-xs-6">
                    <span class="side-nav-open">
                                <i class="fa fa-bars" aria-hidden="true"></i>
                            </span>
                    <span style="vertical-align: text-bottom">Feedback Setting </span>
                            </div>
                        <div class="col-md-5 col-sm-2 hidden-xs"></div>
                        <div style="margin-top:0px !important;" class="col-md-2 col-sm-3 hidden-xs">

                        </div>
                        <div class="col-md-2 col-sm-3 col-xs-6">
                    <?php
                    include_once 'user_profile.php';
                    ?>
                            </div>
                        </div>
                </div>
                <div class="userbg">

                    <?php
                    $lang = $language->english($lang);
                    if(isset($_SESSION['error']))
                    {
                        echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" ><tbody><tr><td colspan="6"  align="center">
            <div class="alert alert-danger alert-dismissable">
               <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
                        echo $_SESSION['error'];
                        echo '</div></td></tr></tbody></table><br>';
                        unset($_SESSION['error']);
                    }
                    if(isset($_SESSION['success']))
                    {
                        echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" ><tbody><tr><td colspan="6"  align="center">
        <div class="alert alert-success alert-dismissable">
           <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
                        echo $_SESSION['success'];
                        echo '</div></td></tr></tbody></table><br>';
                        unset($_SESSION['success']);
                    }
                    ?>

                    <div id="users">
                    <h4 class="update-user">Update Feedback Setting</h4>
                    </div>
                    <div class="plan-category user-page-form">
                        <div class="form-group">
                            <label for="form_heading" class="col-sm-3">Heading Title
                                <sup>*</sup></label>
                            <div class="col-sm-9">
                                <textarea name="form_heading" title="Enter Heading Title" class="form-control"><?php if($_POST['form_heading']) echo $_POST['form_heading']; else echo $Row[0][0]->form_heading;?></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="form_footer" class="col-sm-3">Footer Title
                                <sup>*</sup></label>
                            <div class="col-sm-9">
                                <textarea name="form_footer" title="Enter footer title" class="form-control"><?php if($_POST['form_footer']) echo $_POST['form_footer']; else echo $Row[0][0]->form_footer;?></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3">Capture Extra Comment <sup>*</sup></label>
                            <div class="col-sm-9">
                                <select name="capture_extra_comments" class="form-control">
                                    <option value="">Select Option</option>
                                    <option value="Y" <?php if($_POST['capture_extra_comments'] == 'Y'){?> selected <?php }elseif($Row[0][0]->capture_extra_comments == 'Y'){?> selected <?php }?>>YES</option>
                                    <option value="N" <?php if($_POST['capture_extra_comments'] == 'N'){?> selected <?php }elseif($Row[0][0]->capture_extra_comments == 'N'){?> selected <?php }?>>NO</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="comment_section_title" class="col-sm-3">Comment Section Title
                                <sup>*</sup></label>
                            <div class="col-sm-9">
                                <textarea name="comment_section_title" title="Enter comment section title" class="form-control"><?php if($_POST['comment_section_title']) echo $_POST['comment_section_title']; else echo $Row[0][0]->comment_section_title;?></textarea>
                            </div>
                        </div>

<!--                        <div class="form-group">-->
<!--                            <label for="attach_survey" class="col-sm-3">Attach Survey <sup>*</sup></label>-->
<!--                            <div class="col-sm-9">-->
<!--                                <select name="attach_survey" class="form-control">-->
<!--                                    <option value="">Select Option</option>-->
<!--<!--                                    <option value="Y" <?php ////if($_POST['attach_survey'] == 'Y'){?><!--<!-- selected <?php ////}elseif($Row[0][0]->attach_survey == 'Y'){?><!--<!-- selected --><?php ////}?><!--<!--><!--YES</option>-->
<!--                                    <option value="N" --><?php ////if($_POST['attach_survey'] == 'N'){?><!-- selected --><?php //}elseif($Row[0][0]->attach_survey == 'N'){?><!-- selected --><?php //}?><!--><!--NO</option>-->
<!--                                </select>-->
<!--                            </div>-->
<!--                        </div>-->
                        <!--<div class="form-group">
                            <label for="survey_position" class="col-sm-3">Survey Position <sup>*</sup></label>
                            <div class="col-sm-9">
                                <select name="survey_position" class="form-control">
                                    <option value="">Select Option</option>
                                    <option value="first" <?php /*if($_POST['survey_position'] == 'first'){*/?> selected <?php /*}elseif($Row[0][0]->survey_position == 'first'){*/?> selected <?php /*}*/?>>FIRST</option>
                                    <option value="last" <?php /*if($_POST['survey_position'] == 'last'){*/?> selected <?php /*}elseif($Row[0][0]->survey_position == 'last'){*/?> selected <?php /*}*/?>>LAST</option>
                                    <option value="random" <?php /*if($_POST['survey_position'] == 'random'){*/?> selected <?php /*}elseif($Row[0][0]->survey_position == 'random'){*/?> selected <?php /*}*/?>>RANDOM</option>
                                </select>
                            </div>
                        </div>-->
                        <!--<div class="form-group">
                            <label for="layout" class="col-sm-3">Layout <sup>*</sup></label>
                            <div class="col-sm-9">
                                <select name="layout" class="form-control">
                                    <option value="">Select Option</option>
                                    <option value="list" <?php /*if($_POST['layout'] == 'list'){*/?> selected <?php /*}elseif($Row[0][0]->layout == 'list'){*/?> selected <?php /*}*/?>>LIST</option>
                                    <option value="tab" <?php /*if($_POST['layout'] == 'tab'){*/?> selected <?php /*}elseif($Row[0][0]->layout == 'tab'){*/?> selected <?php /*}*/?>>TAB</option>
                                    <option value="grid" <?php /*if($_POST['layout'] == 'grid'){*/?> selected <?php /*}elseif($Row[0][0]->layout == 'grid'){*/?> selected <?php /*}*/?>>GRID</option>
                                    <option value="slide" <?php /*if($_POST['layout'] == 'slide'){*/?> selected <?php /*}elseif($Row[0][0]->layout == 'slide'){*/?> selected <?php /*}*/?>>SLIDE</option>
                                </select>
                            </div>
                        </div>-->
                        <!--<div class="form-group">
                            <label for="assign_users" class="col-sm-3">Assign Users <sup>*</sup></label>
                            <div class="col-sm-9">
                                <select name="assign_users" class="form-control">
                                    <option value="">Select Option</option>
                                    <option value="Y" <?php /*if($_POST['assign_users'] == 'Y'){*/?> selected <?php /*}elseif($Row[0][0]->assign_users == 'Y'){*/?> selected <?php /*}*/?>>YES</option>
                                    <option value="N" <?php /*if($_POST['assign_users'] == 'N'){*/?> selected <?php /*}elseif($Row[0][0]->assign_users == 'N'){*/?> selected <?php /*}*/?>>NO</option>
                                </select>
                            </div>
                        </div>-->

                        <div class="form-group">
                            <label for="capture_percent_chart" class="col-sm-3">Capture Percent Chart <sup>*</sup></label>
                            <div class="col-sm-9">
                                <select name="capture_percent_chart" class="form-control">
                                    <option value="">Select Option</option>
                                    <option value="Y" <?php if($_POST['capture_percent_chart'] == 'Y'){?> selected <?php }elseif($Row[0][0]->capture_percent_chart == 'Y'){?> selected <?php }?>>YES</option>
                                    <option value="N" <?php if($_POST['capture_percent_chart'] == 'N'){?> selected <?php }elseif($Row[0][0]->capture_percent_chart == 'N'){?> selected <?php }?>>NO</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="capture_contact_detail" class="col-sm-3">Capture Contact Detail <sup>*</sup></label>
                            <div class="col-sm-9">
                                <select name="capture_contact_detail" class="form-control">
                                    <option value="">Select Option</option>
                                    <option value="Y" <?php if($_POST['capture_contact_detail'] == 'Y'){?> selected <?php }elseif($Row[0][0]->capture_contact_detail == 'Y'){?> selected <?php }?>>YES</option>
                                    <option value="N" <?php if($_POST['capture_contact_detail'] == 'N'){?> selected <?php }elseif($Row[0][0]->capture_contact_detail == 'N'){?> selected <?php }?>>NO</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="field_type" class="col-sm-3"> Option Symbol Type<sup>*</sup></label>
                            <div class="col-sm-9">
                                <select name="field_type" class="form-control">
                                    <option value="">Select Option</option>
                                    <?php if ($_SESSION['company_id'] == 98 ) {
                                        ?>
                                    <option value="face" <?php if($_POST['field_type'] == 'face'){?> selected <?php }elseif($Row[0][0]->field_type == 'face'){?> selected <?php }?>>Face</option>
                                    <?php } ?>
                                    <?php if ($_SESSION['company_id'] == 99 ) { ?>
                                    <option value="star" <?php if($_POST['field_type'] == 'star'){?> selected <?php }elseif($Row[0][0]->field_type == 'star'){?> selected <?php }?>>Star</option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>

                        <!--<div class="form-group">
                            <label for="total_option" class="col-sm-3"> Number of Options <sup>*</sup></label>
                            <div class="col-sm-9">
                                <select name="total_option" class="form-control">
                                    <option value="">Select Option</option>

                                    <?php
/*                                    $number = range(1,6);
                                    foreach($number as $number_key => $number_val){ */?>
                                        <option value="<?php /*echo $number_val */?>" <?php /*if($_POST['total_option'] == $number_val ){*/?> selected <?php /*}elseif($Row[0][0]->total_option == $number_val){*/?> selected <?php /*}*/?>><?php /*echo $number_val */?></option>
                                    <?php /*} */?>
                                </select>
                            </div>
                        </div>-->

                        <div class="form-group">
                                <div class="col-sm-12">
                                    <a class="btn btn-danger cancel_button pull-right add-company" href="<?php echo ROOTADMINURL.'/index.php?mod='.$_GET['mod'] ?>" >Cancel</a>&nbsp;
                                    <button type="submit" name="update_feedback_setting" class="btn btn-danger add-company pull-right">Update Setting</button>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
        </form>

    </section>
<script>
    $(document).ready(function() {
        $('#uploadButton').click(function(){

            $('#fileUpload').click();
        });
    });

</script>