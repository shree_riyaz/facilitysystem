<?php
	/*======================================
	Developer	-	Jaishree Sahal
	Module      -   User
	SunArc Tech. Pvt. Ltd.
	======================================
	******************************************************/
	$lang = $language->english($lang);
?>
<script>
//    $( document ).ready(function() {
//$( window ).on( "load", function() {
        function show_or_hide(elem){
            if(elem.value == 'N'){
                document.getElementById('survey_position_div').style.display = "none";
            }else{
                document.getElementById('survey_position_div').style.display = "block";
            }
        }
//    });
</script>

<section>
    <form method="post" name="user_add" class="form-horizontal" id="user_add" enctype="multipart/form-data">


        <div class="container-fluid page-wrapper">
            <div class="row nomargin">
                <div class="col-sm-12 drop-shadow nopadding">
                    <div class="user-heading text-left fixedHeader">
                        <span class="side-nav-open">
                                <i class="fa fa-bars" aria-hidden="true"></i>
                            </span>
                        <span>Settings</span>
                        <?php
                        include_once 'user_profile.php';
                        ?>
                    </div>
                    <div class="userbg">

                        <?php

                        if(isset($_SESSION['error']))
                        {
                            echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" ><tbody><tr><td colspan="6"  align="center">
        <div class="alert alert-danger alert-dismissable">
           <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
                            echo $_SESSION['error'];
                            echo '</div></td></tr></tbody></table><br>';
                            unset($_SESSION['error']);
                        }
                        if(isset($_SESSION['success']))
                        {
                            echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" ><tbody><tr><td colspan="6"  align="center">
				<div class="alert alert-success alert-dismissable">
				   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
                            echo $_SESSION['success'];
                            echo '</div></td></tr></tbody></table><br>';
                            unset($_SESSION['success']);
                        }
                        ?>

                        <div id="users">
                            <h4 class="update-user">Add New Setting</h4>
                        </div>
                        <div class="plan-category user-page-form">
                            <div class="form-group">
                                <label for="form_heading" class="col-sm-3">Heading Title
                                    <sup>*</sup></label>
                                <div class="col-sm-9">
                                    <textarea name="form_heading" title="Enter Heading Title" class="form-control"><?php echo $_POST['form_heading'];?></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="form_footer" class="col-sm-3">Footer Title
                                    <sup>*</sup></label>
                                <div class="col-sm-9">
                                    <textarea name="form_footer" title="Enter footer title" class="form-control"><?php echo $_POST['form_footer'];?></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3">Capture Extra Comment <sup>*</sup></label>
                                <div class="col-sm-9">
                                    <select name="capture_extra_comments" class="form-control">
                                        <option value="" >Select Option</option>
                                        <option value="Y" <?php if($_POST['capture_extra_comments']=='Y') echo 'selected="selected"';?>>YES</option>
                                        <option value="N" <?php if($_POST['capture_extra_comments']=='N') echo 'selected="selected"';?>>NO</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="comment_section_title" class="col-sm-3">Comment Section Title
                                    <sup>*</sup></label>
                                <div class="col-sm-9">
                                    <textarea name="comment_section_title" title="Enter comment section title" class="form-control"><?php echo $_POST['comment_section_title'];?></textarea>
                                </div>
                            </div>

                            <!--<div class="form-group">
                                <label for="attach_survey" class="col-sm-3">Attach Survey <sup>*</sup></label>
                                <div class="col-sm-9">
                                    <select name="attach_survey" class="form-control" onchange="show_or_hide(this)">
                                        <option value="">Select Option</option>
<!--                                        <option value="Y" --><?php /*//if($_POST['attach_survey']=='Y') echo 'selected="selected"';*/?><!--><!--YES</option>-->
                                     <!--   <option value="N" <?php //if($_POST['attach_survey']=='N') echo 'selected="selected"';*/*/?>>NO</option>
                                    </select>
                                </div>
                            </div>-->-->
                            <!--<div class="form-group" id="survey_position_div">
                                <label for="survey_position" class="col-sm-3">Survey Position <sup>*</sup></label>
                                <div class="col-sm-9">
                                    <select name="survey_position" class="form-control">
                                        <option value="">Select Option</option>
                                        <option value="first"<?php /*if($_POST['survey_position']=='first') echo 'selected="selected"';*/?>>First</option>
                                        <option value="last"<?php /*if($_POST['survey_position']=='last') echo 'selected="selected"';*/?>>Last</option>
                                        <option value="random"<?php /*if($_POST['survey_position']=='random') echo 'selected="selected"';*/?>>Random</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="layout" class="col-sm-3">Layout <sup>*</sup></label>
                                <div class="col-sm-9">
                                    <select name="layout" class="form-control">
                                        <option value="">Select Option</option>
                                        <option value="list" <?php /*if($_POST['layout']=='list') echo 'selected="selected"';*/?>>List</option>
                                        <option value="tab" <?php /*if($_POST['layout']=='tab') echo 'selected="selected"';*/?>>Tab</option>
                                        <option value="gride" <?php /*if($_POST['layout']=='gride') echo 'selected="selected"';*/?>>Grid</option>
                                        <option value="slide" <?php /*if($_POST['layout']=='slide') echo 'selected="selected"';*/?>>Slide</option>
                                    </select>
                                </div>
                            </div>-->
                           <!-- <div class="form-group">
                                <label for="assign_users" class="col-sm-3">Assign Users<sup>*</sup></label>
                                <div class="col-sm-9">
                                    <select name="assign_users" class="form-control">
                                        <option value="">Select Option</option>
                                        <option value="Y" <?php /*if($_POST['assign_users']=='Y') echo 'selected="selected"';*/?>>YES</option>
                                        <option value="N" <?php /*if($_POST['assign_users']=='N') echo 'selected="selected"';*/?>>NO</option>
                                    </select>
                                </div>
                            </div>
-->
                            <div class="form-group">
                                <label for="capture_percent_chart" class="col-sm-3">Capture Percent Chart <sup>*</sup></label>
                                <div class="col-sm-9">
                                    <select name="capture_percent_chart" class="form-control">
                                        <option value="">Select Option</option>
                                        <option value="Y" <?php if($_POST['capture_percent_chart']=='Y') echo 'selected="selected"';?>>YES</option>
                                        <option value="N" <?php if($_POST['capture_percent_chart']=='N') echo 'selected="selected"';?>>NO</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="capture_contact_detail" class="col-sm-3">Capture Contact Detail <sup>*</sup></label>
                                <div class="col-sm-9">
                                    <select name="capture_contact_detail" class="form-control">
                                        <option value="">Select Option</option>
                                        <option value="Y" <?php if($_POST['capture_contact_detail']=='Y') echo 'selected="selected"';?>>YES</option>
                                        <option value="N" <?php if($_POST['capture_contact_detail']=='N') echo 'selected="selected"';?>>NO</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="field_type" class="col-sm-3"> Option Symbol Type<sup>*</sup></label>
                                <div class="col-sm-9">
                                    <select name="field_type" class="form-control">
                                        <option value="">Select Option</option>
                                        <?php if ($_SESSION['company_id'] == 98 ) { ?>
                                            <option value="face" <?php if($_POST['field_type']=='face') echo 'selected="selected"';?>>Face</option>
                                        <?php } ?>
                                        <?php if ($_SESSION['company_id'] == 99 ) { ?>
                                            <option value="star" <?php if($_POST['field_type']=='star') echo 'selected="selected"';?>>Star</option>
                                        <?php } ?>

                                    </select>
                                </div>
                            </div>
                            <!--<div class="form-group">
                                <label for="total_option" class="col-sm-3"> Number of Options <sup>*</sup></label>
                                <div class="col-sm-9">
                                    <select name="total_option" class="form-control">
                                        <option value="">Select Option</option>

                                        <?php
/*                                        $number = range(1,6);
                                        foreach($number as $number_key => $number_val){ */?>
                                            <option value="<?php /*echo $number_val */?>" <?php /*if($_POST['total_option']== $number_val ) echo 'selected="selected"';*/?>><?php /*echo $number_val */?></option>
                                        <?php /*} */?>
                                    </select>
                                </div>
                            </div>-->

                        <div class="form-group">
                            <div class="col-sm-12">
                                <button type="submit" name="add_setting" class="btn btn-danger add-company pull-right">Save Setting</button>
                            </div>
                        </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    </section>

<script>
    $(document).ready(function() {
        $('#uploadButton').click(function(){

            $('#fileUpload').click();
        });
    });

</script>