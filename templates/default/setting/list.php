<?php
/*======================================
Developer	-	Jaishree Sahal
Module      -   USer
SunArc Tech. Pvt. Ltd.
======================================
******************************************************/
?>

<script>

    function checkAll(ele) {
        var checkboxes = document.getElementsByTagName('input');

        if (ele.checked) {
            for (var i = 0; i < checkboxes.length; i++) {
                if (checkboxes[i].type == 'checkbox') {
                    checkboxes[i].checked = true;
                }
            }
        } else {
            for (var i = 0; i < checkboxes.length; i++) {
                console.log(i);
                if (checkboxes[i].type == 'checkbox') {
                    checkboxes[i].checked = false;
                }
            }
        }
    }

    function showpassdiv(id) {
        $("#password_" + id).css("display", "block");
    }
    function Check() {
        if (document.getElementById('keyword').value == '') {
            alert('Please enter any value for search.');
            return false;
        }
        else {
            return true;
        }
    }
    function Clear() {
        document.getElementById('keyword').value = '';
        location.href = "index.php?mod=user&do=list";
        return false;
    }
</script>


    <section>


        <div style="height: 100vh;" class="col-sm-12 drop-shadow nopadding">

            <form method="post" name="frmlist" id="frmlist">
                <?php
                if (isset($_SESSION['ActionAccess'])) {
                    echo '<table cellspacing="0" cellpadding="0" border="0" align="center" width="90%" ><tbody><tr><td colspan="6"  align="center"><div class="errormsg">';
                    echo $_SESSION['ActionAccess'];
                    echo '</div></td></tr></tbody></table>';
                    unset($_SESSION['ActionAccess']);

                }
                if (isset($_SESSION['error'])) {
                    echo '<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" ><tbody><tr><td colspan="6"  align="center">
				<div class="alert alert-danger alert-dismissable">
				   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
                    echo $_SESSION['error'];
                    echo '</div></td></tr></tbody></table>';
                    unset($_SESSION['error']);
                }
                if (isset($_SESSION['success'])) {
                    echo '<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" ><tbody><tr><td colspan="6"  align="center">
            <div class="alert alert-success alert-dismissable">
               <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
                    echo $_SESSION['success'];
                    echo '</div></td></tr></tbody></table>';
                    unset($_SESSION['success']);
                }
                ?>
            <div class="user-heading fixedHeader">
                <div class="row">
                    <div class="col-xs-3">
                 <span class="side-nav-open">
                                <i class="fa fa-bars" aria-hidden="true"></i>
                            </span>
                <span style="vertical-align: text-bottom"><?php echo  $lang['Setting'] ?> </span>
                        </div>
                <div class="col-md-5 col-sm-3 col-xs-2"></div>
                <div style="margin-top:0px !important;" class="col-md-2 col-sm-3 col-xs-4">

                </div>
                <div class="col-md-2 col-xs-3">
                <?php
                include_once 'user_profile.php';
                ?>
                    </div>
                </div>
            </div>
            <div class="userbg">

                <div class="row">


                    <div style="padding:0px 15px 30px 0px; overflow: hidden;" class="col-sm-12">
                        <div class="pull-right">
                            <?php  if(($_SESSION['usertype'])=='company_admin' && $count_record == 0) { ?>
                                <a title="Add New Setting" href="index.php?mod=setting&do=add" class="btn btn-danger add-company "><?php echo $lang['Add Setting'] ?></a>
                            <?php } echo '<br>';  ?>

                        </div>
                    </div>

                </div>
                <?php
                $srNo = $frmdata['from'];
                $count = count($Row);
                ?>
                <div id="users">
                    <h4 class="update-user">FeedBack Setting Details</h4>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="table-responsive">
                            <table class="table dashboard-table table-bordered">
                                <thead>
                                <tr>
                                    <th class="text-center"><a onClick="OrderPage('form_heading');"  style="text-decoration:none; cursor:pointer" >Heading
                                            <?php if($frmdata['orderby']=='form_heading') {print '<img src="'.IMAGEURL.'/asc.gif">';} elseif($frmdata['orderby']=='form_heading desc'){print '<img src="'.IMAGEURL.'/desc.gif">';}?>
                                        </a></th>
                                        <th class="text-center">
                                            <a onClick="OrderPage('form_footer');" style="text-decoration:none; cursor:pointer" >Footer
                                                <?php if($frmdata['orderby']=='form_footer') {print '<img src="'.IMAGEURL.'/asc.gif">';} elseif($frmdata['orderby']=='form_footer desc'){print '<img src="'.IMAGEURL.'/desc.gif">';}?>
                                            </a>
                                        </th>

                                    <th class="text-center">
                                        <a onClick="OrderPage('comment_section_title');" style="text-decoration:none; cursor:pointer" >Comment Section Title
                                            <?php if($frmdata['orderby']=='comment_section_title') {print '<img src="'.IMAGEURL.'/asc.gif">';} elseif($frmdata['orderby']=='comment_section_title desc'){print '<img src="'.IMAGEURL.'/desc.gif">';}?>
                                    </th>

                                    <!--<th class="text-center">
                                        <a onClick="OrderPage('attach_survey');" style="text-decoration:none; cursor:pointer" >Attach Survey
                                            <?php /*if($frmdata['orderby']=='attach_survey') {print '<img src="'.IMAGEURL.'/asc.gif">';} elseif($frmdata['orderby']=='attach_survey desc'){print '<img src="'.IMAGEURL.'/desc.gif">';}*/?>
                                    </th>
                                    <th class="text-center">
                                        <a onClick="OrderPage('assign_users');" style="text-decoration:none; cursor:pointer" >Assign Users
                                            <?php /*if($frmdata['orderby']=='assign_users') {print '<img src="'.IMAGEURL.'/asc.gif">';} elseif($frmdata['orderby']=='assign_users desc'){print '<img src="'.IMAGEURL.'/desc.gif">';}*/?>
                                    </th>
                                    <th class="text-center">
                                        <a onClick="OrderPage('layout');" style="text-decoration:none; cursor:pointer" >Layout
                                            <?php /*if($frmdata['orderby']=='layout') {print '<img src="'.IMAGEURL.'/asc.gif">';} elseif($frmdata['orderby']=='layout desc'){print '<img src="'.IMAGEURL.'/desc.gif">';}*/?>
                                    </th>-->
                                    <th class="text-center">
                                        <a onClick="OrderPage('capture_percent_chart');" style="text-decoration:none; cursor:pointer" >Percent Pie Chart
                                            <?php if($frmdata['capture_percent_chart']=='is_active') {print '<img src="'.IMAGEURL.'/asc.gif">';} elseif($frmdata['orderby']=='capture_percent_chart desc'){print '<img src="'.IMAGEURL.'/desc.gif">';}?>
                                    </th>
                                    <th class="text-center">
                                        <a onClick="OrderPage('capture_contact_detail');" style="text-decoration:none; cursor:pointer" >Contact Us
                                            <?php if($frmdata['orderby']=='capture_contact_detail') {print '<img src="'.IMAGEURL.'/asc.gif">';} elseif($frmdata['orderby']=='capture_contact_detail desc'){print '<img src="'.IMAGEURL.'/desc.gif">';}?>
                                    </th>

                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                if($Row)
                                {
                                $objpassEncDec = new passEncDec;
                                for($counter=0;$counter<$count;$counter++)
                                {
                                    $srNo++;
                                    if(($counter%2)==0)
                                    {
                                        $trClass="tdbggrey";
                                    }
                                    else
                                    {
                                        $trClass="tdbgwhite";
                                    }

                                    $confirmDelete = 'All the related data of this user will be deleted . Do you really want to delete this User ?';
                                    $obj = new passEncDec;
                                    ?>
                                    <tr>

                                        <td><?php echo isset($Row[$counter]->form_heading) ? ucfirst($Row[$counter]->form_heading) : 'NA'; ?></td>
                                        <td><?php echo isset($Row[$counter]->form_footer) ? $Row[$counter]->form_footer : 'NA'; ?></td>
                                        <td><?php echo isset($Row[$counter]->comment_section_title) ? $Row[$counter]->comment_section_title : 'NA'; ?></td>
<!--                                        <td> --><?php //if ($Row[$counter]->attach_survey == 'N'){echo 'NO';} else { echo 'YES' ;}?><!--</td>-->
<!--                                        <td> --><?php //if ($Row[$counter]->assign_users == 'N'){echo 'NO';} else { echo 'YES' ;}?><!--</td>-->
<!--                                        <td>--><?php //echo isset($Row[$counter]->layout) ? ucfirst($Row[$counter]->layout) : 'NA'; ?><!--</td>-->
                                        <td> <?php if ($Row[$counter]->capture_percent_chart == 'N'){echo 'NO';} else { echo 'YES' ;}?></td>
                                        <td> <?php if ($Row[$counter]->capture_contact_detail == 'N'){echo 'NO';} else { echo 'YES' ;}?></td>
                                        <td>

                                            <a class="fontstyle" href='<?php print CreateURL('index.php','mod=setting&do=edit&id='.$Row[$counter]->id);?>' title="Edit" ><img src="<?php echo IMAGEURL."/b_edit.png" ?>" border=0 />
                                            </a>

                                        </td>
                                    </tr>
                                    <?php $sno++; } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="text-center">
                            <?php
                            PaginationDisplay($totalCount);
                            ?>
                        </div>
                    </div>
                </div>
            <?php
            }
            elseif($_SESSION['keywords'] == 'Y') {
                $frmdata['message']="Sorry! No record found for the selected criteria";
                unset ($_SESSION['keywords']);
                ShowMessage(); }
            else {
                $frmdata['message']="Sorry! No feedback setting created yet.";
                ShowMessage(); }
            ?>
            </div>
                <input name="pageNumber" type="hidden" value="<?php print $frmdata['pageNumber'] ?>">
                <input name="orderby" type="hidden"
                       value="<?php if ($frmdata['orderby'] != '') echo $frmdata['orderby']; else echo 'u.user_id desc'; ?>">
                <input name="order" type="hidden" value="<?php print $frmdata['order'] ?>">
                <input name="actUserID" type="hidden" value=""/>
            </form>
        </div>

    </section>

<script>
    $parent.find('button[name=download]').click(function () {
        window.location.href = 'download.php';
    });
</script>