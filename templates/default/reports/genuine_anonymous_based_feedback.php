<?php

$lang = $language->english($lang);
$DBFilter = New $DBFilter();
//print_r($_SESSION);
//echo "Hello";
//print_r($Row);
$id = $_SESSION['user_id'];
$date= date("d-M-Y");
$expiary_date = date("d-M-Y", strtotime($Row->expiary_date));

//print_r( $DBFilter->SelectRecord('faults'));
//echo count($feedbk[0]);
//echo "<pre>"; print_r($count_data); exit;
?>
<style>text.highcharts-credits {
        display: none;
    }</style>

<section>
    <div class="col-sm-9 drop-shadow nopadding">
        <div class="user-heading">
            <span>Dashboard</span>
            <?php
            include_once 'user_profile.php';
            ?>
        </div>
        <div class="userbg">

            <?php if(($_SESSION['usertype']) == 'company_admin' ) { ?>

                <div class="row">

                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <i class="fa fa-bar-chart-o fa-fw"></i> Genuine vs Anonymous Based Feedbacks
                            </div>
                            <!-- /.panel-heading -->
                            <div class="panel-body">
                                <div class="">
                                    <span style="font-weight: bold; color: darkolivegreen">Count - <?php echo $count_data[1]->total_genuine.'/'.$count_data[0]->total_anonymous; ?></span>
                                    <br>

                                    <!--                                        <span style="font-weight: bold; color: darkolivegreen">Ratio  -  --><?php //echo getRatio($count_data[1]->total_genuine,$count_data[0]->total_anonymous); ?><!--</span>-->
                                    <br>
                                    <br>
                                    <br>
                                    <table class="table table-responsive table-striped table-hover" id="example">
                                        <thead>
                                        <tr>
                                            <th>Location Name</th>
                                            <th>Number of feedbacks</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        foreach ($Row[0] as $row_list) { ?>

                                            <tr>
                                                <td><?php echo $row_list->location_name; ?></td>
                                                <td><?php echo $row_list->feedbacks; ?></td>

                                            </tr>
                                        <?php } //} ?>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>

</section>




<script type="text/javascript">
    $(document).ready(function() {
        $('#example').DataTable();
    } );
</script>

