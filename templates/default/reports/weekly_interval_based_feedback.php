<?php

$lang = $language->english($lang);
$DBFilter = New $DBFilter();
//print_r($_SESSION);
//echo "Hello";
//print_r($Row);
$id = $_SESSION['user_id'];
$date= date("d-M-Y");
$expiary_date = date("d-M-Y", strtotime($Row->expiary_date));
//echo "<pre>";
//print_r($Row);
//print_r( $DBFilter->SelectRecord('faults'));
//echo count($feedbk[0]);
//echo "<pre>"; print_r($Row[0]['rating_options_detail'][0]->option_sequence); exit;

?>
<style>text.highcharts-credits {
        display: none;
    }



</style>

<section>
    <div style="height: 100vh" class="col-sm-12 drop-shadow nopadding">
        <div class="user-heading fixedHeader">
            <div class="row">
                <div class="col-md-3 col-sm-4 col-xs-6">
            <span class="side-nav-open">
                                <i class="fa fa-bars" aria-hidden="true"></i>
                            </span>
            <span style="vertical-align: text-bottom">Reports</span>
                    </div>
                <div class="col-md-5 col-sm-2 hidden-xs"></div>
                <div style="margin-top:0px !important;" class="col-md-2 col-sm-3 hidden-xs">

                </div>
                <div class="col-md-2 col-sm-3 col-xs-6">
            <?php
            include_once 'user_profile.php';
            ?>
                    </div>
                </div>

        </div>
        <div class="userbg">

            <?php if(($_SESSION['usertype']) == 'company_admin' ) { ?>

                <div class="row">
                    <div style="padding:0px 0px 30px 0px; overflow: hidden;" class="col-sm-12 demo">
                        <?php
                        include_once(CURRENTTEMP."/"."date_picker.php");
                        ?>
                    </div>
                    <div class="col-lg-12">
                        <div class="panel panel-default report-panel">
                            <div class="panel-heading">
                                <i class="fa fa-calendar" aria-hidden="true"></i> Weekly Based Feedback
                            </div>
                            <!-- /.panel-heading -->
                            <div class="panel-body">
                                <div class="table-responsive post_data_weekly" id="post-data">
                                    <table class="table table-bordered table-striped table-hover interval-feedback" id="example" >

                                        <thead>
                                        <tr>

                                            <th>Week</th>
                                            <th>Total Feedback</th>
                                            <?php foreach ($Row[0]['rating_options_detail'] as $row_list_key => $row_list_th) { ?>
                                            <th><?php echo $row_list_th->title ?></th>
                                            <?php }  ?>
                                        </tr>
                                        </thead>
                                        <tbody class="" >

                                        <?php
                                        if (count($Row)){
                                        foreach ($Row[1] as $row_list_key =>$row_list) { ?>
                                            <tr>
                                                <th colspan="7" class="text-center" style="padding: 10px!important;background: lavender;"><?php echo isset($row_list_key) ? strtoupper(date('Y - F',strtotime($row_list_key.'-01'))): 'NA'; ?></th>
                                            </tr>

                                            <?php
                                            foreach ($row_list as $row_list_key_two =>$row_list_val_two) {
                                                $serial_key =  explode('-',$row_list_key_two);
                                                ?>
                            <tr>

                                <td class="text-center" ><?php echo 'WEEK -'.$serial_key[2] ; ?></td>
                                <td class="text-center"><?php echo isset($row_list_val_two['total_feedback']) ? $row_list_val_two['total_feedback'] : 0; ?></td>
                                <?php
                                    $get_options = [];
                                    foreach ($Row[0]['rating_options_detail'] as $row_list_key => $row_list_th) {
                                    $get_options[] = $row_list_th->title;
                                    $get_options_all = array_flip($get_options);
                                    }

                                foreach ($get_options_all as $get_options_all_key=>$get_options_all_val) {
                                        ?>
                                                <td class="text-center"><?php echo isset($row_list_val_two['number_of_feedback'][$get_options_all_key]) ? $row_list_val_two['number_of_feedback'][$get_options_all_key] : 0 ?></td>

                                            <?php } ?>

                            </tr>

                                                <?php
//                                                $k++;
                                            } } } else{ ?>
                                <tr><td colspan="7">No data available now</td></tr>
                                        <?php } ?>
                                        </tbody>
                                    </table>
                                    <?php if($total_pages > 1 ) { ?>
                                        <div class="pagination_weekly_div text-center">
                                            <nav aria-label="...">
                                                <ul class="pagination">

                                                    <?php if($total_pages > 10 ) { ?>

                                                        <li class="page-item">
                                                            <a class="page-link" href="<?php echo ROOTADMINURL.'/index.php?mod=reports&do=weekly_interval&page_no=1'; ?>" tabindex="-1">First</a>
                                                        </li>
                                                    <?php } ?>
                                                    <?php
                                                    if($total_pages > 1 ) {
                                                        for ($i=1;$i<=$total_pages;$i++){ ?>
                                                            <li class="page-item <?php if($_GET['page_no'] == $i || ($_GET['page_no'] == '' && $i==1 ) ) echo 'active'; ?> ">
                                                                <a class="page-link" href="<?php echo ROOTADMINURL.'/index.php?mod=reports&do=weekly_interval&page_no='.$i; ?>"><?php echo $i; ?> </a>
                                                            </li>
                                                        <?php } } ?>
                                                    <?php if($total_pages > 10) { ?>

                                                        <li class="page-item">
                                                            <a class="page-link" href="<?php echo ROOTADMINURL.'/index.php?mod=reports&do=weekly_interval&page_no='.$total_pages; ?>" tabindex="-1">Last</a>
                                                        </li>
                                                    <?php } ?>

                                                </ul>
                                            </nav>
                                        </div>
                                    <?php } ?>

                                </div>

                            </div>

                        </div>

                        <!-- <div class="ajax-load text-center" style="display:none">

                             <p><img src="http://demo.itsolutionstuff.com/plugin/loader.gif">Loading More post</p>

                         </div>-->
                    </div>

                </div>
            <?php } ?>
        </div>

    </div>

    <input type="hidden" class="company_id_load_more" value="<?php echo $total_pages ?>">


</section>


<?php

// include_once(JS . "/app.js");

?>


