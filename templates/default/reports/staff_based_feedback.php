<?php

$lang = $language->english($lang);
$DBFilter = New $DBFilter();
//print_r($_SESSION);
//echo "Hello";
//print_r($Row);
$id = $_SESSION['user_id'];
$date= date("d-M-Y");
$expiary_date = date("d-M-Y", strtotime($Row->expiary_date));

//print_r( $DBFilter->SelectRecord('faults'));
//echo count($feedbk[0]);
//echo "<pre>"; print_r($Row); exit;
?>
<style>text.highcharts-credits {
        display: none;
    }</style>

<section>
    <div class="col-sm-12 drop-shadow nopadding">
        <div class="user-heading fixedHeader">
            <div class="row">
                <div class="col-md-3 col-sm-4 col-xs-6">
            <span class="side-nav-open">
                                <i class="fa fa-bars" aria-hidden="true"></i>
                            </span>
            <span style="vertical-align: text-bottom">Reports</span>
                    </div>
                <div class="col-md-5 col-sm-2 hidden-xs"></div>
                <div style="margin-top:0px !important;" class="col-md-2 col-sm-3 hidden-xs">

                </div>
                <div class="col-md-2 col-sm-3 col-xs-6">
            <?php
            include_once 'user_profile.php';
            ?>
                    </div>
                </div>
        </div>
        <div class="userbg">

            <?php if(($_SESSION['usertype']) == 'company_admin' ) { ?>

                <div class="row">
                    <div style="padding:0px 0px 30px 0px; overflow: hidden;" class="col-sm-12 demo">
                        <?php
                        include_once(CURRENTTEMP."/"."date_picker.php");
                        ?>
                    </div>
                    <div class="col-lg-12">
                        <div class="panel panel-default report-panel">
                            <div class="panel-heading">
                                <i class="fa fa-tint" aria-hidden="true"></i> Staff Based Feedback
                            </div>
                            <!-- /.panel-heading -->
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered interval-feedback table-striped table-hover" id="example">
                                        <thead>
                                        <tr>
                                            <th>Staff Name</th>
                                            <th>Total Feedback</th>
                                            <?php foreach ($Row[0][0] as $row_list_key => $row_list_th) { ?>
                                                <th><?php echo $row_list_th->title ?></th>
                                            <?php }  ?>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        foreach ($Row[1] as $row_list_key=> $row_list) {

                                            $get_options = [];
                                            foreach ($row_list['rating_options_detail'] as $row_list_keys => $row_list_th) {
                                                $get_options[] = $row_list_th->title;
                                                $get_options_all = array_flip($get_options);
                                            }
//                                            echo '<pre>'; print_r($get_options_all); exit;
                                            ?>

                                            <tr>
                                                <td><?php echo isset($row_list['name']) ? $row_list['name'] : 'NA' ?></td>
                                                <td><?php echo isset($row_list['total_feedback']) ? $row_list['total_feedback'] : 'NA' ?></td>
                                                <?php
                                                foreach ($get_options_all as $get_options_all_key_one=>$get_options_all_val) {
                                                    ?>
                                                    <td><?php echo isset($row_list['number_of_feedback'][$get_options_all_key_one]) ? $row_list['number_of_feedback'][$get_options_all_key_one] : 0 ?></td>
                                                <?php   } ?>
                                            </tr>
                                        <?php } ?>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-bar-chart-o fa-fw"></i> Breakdown By Staff Wise
                        </div>
                        <!-- /.panel-heading -->
                        <?php if (count($get_data_for_pie_data_cleaner_pie)){ ?>

                            <div class="panel-body">
                                <div class="">

                                    <script type="text/javascript">

                                        $(function () {

                                            $('#staff_based_graph_chart').highcharts({

                                                chart: {
                                                    renderTo: 'container',
                                                    type: 'pie'
                                                },
                                                title: {
                                                    text: 'Feedback graph on basis of staff'
                                                },
                                                tooltip: {
                                                    pointFormat: '{series.name}: <b>{point.y:.f}</b>'
                                                },
                                                plotOptions: {
                                                    pie: {
                                                        borderColor: '#000000',
                                                        innerSize: '80%'
                                                    }
                                                },

                                                series: [{
                                                    type: 'pie',
                                                    name: 'Feedback',
                                                    size: '100%',
                                                    innerSize: '60%',
                                                    data: <?php echo json_encode($get_data_for_pie_data_cleaner_pie);?>,
                                                    showInLegend: true,
                                                    dataLabels: {
                                                        enabled: false
                                                    }
                                                }]
                                            });

                                        });

                                    </script>

                                    <div id="staff_based_graph_chart"></div>

                                </div>
                            </div>
                        <?php } else { ?>
                            <div class="panel-body">
                                <br>
                                <div class="text-center">
                                    <span  style="font-size: 15px;">Oops! No data available to show staff based feedback graph chart.</span>
                                </div>
                                <br>
                            </div>

                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>

<script type="text/javascript">
    $(document).ready(function() {
        $('#example').DataTable();
    } );
</script>

