<?php

$lang = $language->english($lang);
$DBFilter = New $DBFilter();
//print_r($_SESSION);
//echo "Hello";
//print_r($Row);
$id = $_SESSION['user_id'];
$date= date("d-M-Y");
$expiary_date = date("d-M-Y", strtotime($Row->expiary_date));

//print_r( $DBFilter->SelectRecord('faults'));
//echo count($feedbk[0]);
//echo "<pre>"; print_r($Row); exit;
?>
<style>text.highcharts-credits {
        display: none;
    }</style>

<section>
    <div class="col-sm-9 drop-shadow nopadding">
        <div class="user-heading">
            <span>Feedback Nature Wise</span>
            <?php
            include_once 'user_profile.php';
            ?>
        </div>
        <div class="userbg">

            <?php if(($_SESSION['usertype']) == 'company_admin' ) { ?>

                <div class="row">
                    <div class="col-sm-12 demo">
                        <?php
                        include_once(CURRENTTEMP."/"."date_picker.php");
                        ?>
                    </div>
                </div>
            <?php } ?>
            <div class="row">

                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-bar-chart-o fa-fw"></i> Breakdown By Month on Month
                        </div>
                        <!-- /.panel-heading -->
                        <?php if (count($month_on_array) ){ ?>
                            <div class="panel-body">
                                <div class="overall-feedback-div">

                                    <script type="text/javascript">

                                        $(function () {
                                            var chart = Highcharts.chart('container_fault_month_on', {
                                                chart: {
                                                    type: 'column'
                                                },
                                                title: {
                                                    text: 'Monthly Average Rainfall'
                                                },
                                                subtitle: {
                                                    text: 'Source: WorldClimate.com'
                                                },
                                                xAxis: {
                                                    categories: [
                                                        'Jan',
                                                        'Feb',
                                                        'Mar'
                                                    ],
                                                    crosshair: true
                                                },
                                                yAxis: {
                                                    min: 0,
                                                    title: {
                                                        text: 'Rainfall (mm)'
                                                    }
                                                },
                                                tooltip: {
                                                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                                                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                                                    '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
                                                    footerFormat: '</table>',
                                                    shared: true,
                                                    useHTML: true
                                                },
                                                plotOptions: {
                                                    column: {
                                                        pointPadding: 0.2,
                                                        borderWidth: 0
                                                    }
                                                },
                                                series: [
                                                            {
                                                                name: 'Positive',
                                                                data: [49.9, 71.5, 106.4]
                                                            },
                                                            {
                                                                name: 'Negative',
                                                                data: [83.6, 78.8, 98.5]
                                                            },
                                                            {
                                                                name: 'Neutral',
                                                                data: [48.9, 38.8, 205]
                                                            },
                                                        ]
                                            });
                                        });

                                    </script>

                                    <div id="container_fault_month_on"></div>

                                </div>
                            </div>
                        <?php } else { ?>
                            <div class="panel-body">
                                <br>
                                <div class="text-center">
                                    <span  style="font-size: 15px;">Oops! No data available to show month on month based feedback graph chart.</span>
                                </div>
                                <br>
                            </div>

                        <?php } ?>
                        <!-- /.panel-body -->
                    </div>
                </div>

            </div>
        </div>
    </div>

</section>




<script type="text/javascript">

    $(document).ready(function() {
        $('#example').DataTable();
    } );
</script>

