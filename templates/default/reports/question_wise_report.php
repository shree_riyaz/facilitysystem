<?php

$lang = $language->english($lang);
$DBFilter = New $DBFilter();
//print_r($_SESSION);
//echo "Hello";
//print_r($Row);
$id = $_SESSION['user_id'];
$date= date("d-M-Y");
$expiary_date = date("d-M-Y", strtotime($Row->expiary_date));

//print_r( $DBFilter->SelectRecord('faults'));
//echo count($feedbk[0]);
//echo "<pre>"; print_r($Row); exit;
?>
<style>text.highcharts-credits {
        display: none;
    }</style>

<section>
    <div style="height: 100vh" class="col-sm-12 drop-shadow nopadding ">
        <div class="user-heading fixedHeader">
            <div class="row">
                <div class="col-md-3 col-sm-4 col-xs-6">
             <span class="side-nav-open">
                                <i class="fa fa-bars" aria-hidden="true"></i>
                            </span>
            <span style="vertical-align: text-bottom">Reports</span>
            </div>
            <div class="col-md-5 col-sm-2 hidden-xs"></div>
            <div style="margin-top:0px !important;" class="col-md-2 col-sm-3 hidden-xs">

            </div>
            <div class="col-md-2 col-sm-3 col-xs-6">
            <?php
            include_once 'user_profile.php';
            ?>
                </div>
            </div>
        </div>
        <div class="userbg">

            <?php if(($_SESSION['usertype']) == 'company_admin' ) { ?>

                <div class="row">
                    <div style="padding:0px 0px 30px 0px; overflow: hidden;" class="col-sm-12 demo">
                        <?php
                        include_once(CURRENTTEMP."/"."date_picker.php");
                        ?>
                    </div>
                    <div class="col-lg-12">
                        <div class="panel panel-default report-panel">
                            <div class="panel-heading">
                                <i class="fa fa-calendar" aria-hidden="true"></i> Question Wise Report
                            </div>
                            <!-- /.panel-heading -->
                            <div class="panel-body">
                                <div class="table-responsive">

                                    <table class="table table-bordered interval-feedback table-striped table-hover" id="example">
                                        <thead>
                                        <tr>
                                            <th class="text-center">Question Title</th>
                                            <th class="text-center">Number of Feedback</th>
                                            <?php foreach ($Row[0][0] as $row_list_key => $row_list_th) { ?>
                                            <th class="text-center"><?php echo $row_list_th->title ?></th>
                                            <?php }  ?>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        foreach ($Row[1] as $row_list_key => $row_list) { ?>

                                            <tr>
                                                <th colspan="7" class="text-center" style="padding: 10px!important;background: lavender;"><?php echo isset($row_list_key) ? strtoupper(date('Y - F',strtotime($row_list_key.'-01'))): 'NA'; ?></th>
                                            </tr>

                                            <?php

//                                                    echo '<pre>'; print_r($get_options_all);

                                            foreach ($row_list as $row_list_question_details_key => $row_list_question_details_value) {

                                                $get_options = [];
                                                foreach ($row_list_question_details_value['rating_options_detail'] as $row_list_keys => $row_list_th) {
                                                    $get_options[] = $row_list_th->title;
                                                    $get_options_all = array_flip($get_options);
                                                }
//                                                echo $row_list_question_details_key;

                                                ?>


                                            <tr>
                                                <td class="text-center"><?php echo $row_list_question_details_key  ?></td>

                                                <td class="text-center"><?php echo isset($row_list_question_details_value['total_feedback']) ? $row_list_question_details_value['total_feedback']: 0 ?></td>

                                                <?php
                                                foreach ($get_options_all as $get_options_all_key=>$get_options_all_val) { ?>
                                                    <td class="text-center">
                                                        <?php echo isset($row_list_question_details_value['number_of_feedback'][$get_options_all_key]) ? $row_list_question_details_value['number_of_feedback'][$get_options_all_key] : 0 ?>
                                                    </td>
                                                <?php   }  ?>

                                            </tr>
                                        <?php  } } ?>
                                        </tbody>

                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>

</section>
<script type="text/javascript">

    $(document).ready(function() {
//        $('#example').DataTable();
    } );
</script>

