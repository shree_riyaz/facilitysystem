<?php

$lang = $language->english($lang);
$DBFilter = New $DBFilter();
//print_r($_SESSION);
//echo "Hello";
//print_r($Row);
$id = $_SESSION['user_id'];
$date= date("d-M-Y");
$expiary_date = date("d-M-Y", strtotime($Row->expiary_date));

//print_r( $DBFilter->SelectRecord('faults'));
//echo count($feedbk[0]);
//echo "<pre>"; print_r($Row); exit;
?>
<style>text.highcharts-credits {
        display: none;
    }</style>

<section>
    <div style="height: 100vh" class="col-sm-12 drop-shadow nopadding">
        <div class="user-heading fixedHeader">
            <div class="row">
                <div class="col-md-3 col-sm-4 col-xs-6">
             <span class="side-nav-open">
                                <i class="fa fa-bars" aria-hidden="true"></i>
                            </span>
            <span style="vertical-align: text-bottom">Reports</span>
                    </div>
                <div class="col-md-5 col-sm-2 hidden-xs"></div>
                <div style="margin-top:0px !important;" class="col-md-2 col-sm-3 hidden-xs">

                </div>
                <div class="col-md-2 col-sm-3 col-xs-6">
            <?php
            include_once 'user_profile.php';
            ?>
                    </div>
                </div>
        </div>
        <div class="userbg">

            <?php if(($_SESSION['usertype']) == 'company_admin' ) { ?>

                <div class="row">
                    <div style="padding:0px 0px 30px 0px; overflow: hidden;" class="col-sm-12 demo">
                        <?php
                        include_once(CURRENTTEMP."/"."date_picker.php");
                        ?>
                    </div>
                    <div class="col-lg-12">
                        <div class="panel panel-default report-panel">
                            <div class="panel-heading">
                                <i class="fa fa-star" aria-hidden="true"></i> Breakdown By Feedback Type
                            </div>
                            <?php if (count($get_data_for_pie_feedback_type)){ ?>
                                <div class="panel-body">
                                    <div class="poor-feedback-div">
                                        <!--                        https://jsfiddle.net/gh/get/library/pure/highcharts/highcharts/tree/master/samples/highcharts/demo/line-basic/-->
                                        <script type="text/javascript">
                                            $(function () {

                                                $('#containers_poor').highcharts({

                                                    chart: {
                                                        plotBackgroundColor: null,
                                                        plotBorderWidth: 0,//null,
                                                        plotShadow: false
                                                    },

                                                    title: {
                                                        text: ''
                                                    },
                                                    tooltip: {
                                                        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                                                    },
                                                    plotOptions: {
                                                        pie: {
                                                            allowPointSelect: true,
                                                            cursor: 'pointer',

                                                            dataLabels: {
                                                                enabled: true,
                                                                format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                                                style: {
                                                                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                                                }
                                                            },
                                                            showInLegend: true
                                                        }
                                                    },
                                                    series: [{
                                                        type: 'pie',
                                                        name: 'Feedback',
                                                        size: '90%',
                                                        data: <?php echo json_encode($get_data_for_pie_feedback_type);?>
                                                    }]
                                                });
                                            });

                                        </script>

                                        <div id="containers_poor"></div>
                                    </div>
                                </div>
                            <?php } else { ?>
                                <div class="panel-body">
                                    <br>
                                    <div class="text-center">
                                        <span  style="font-size: 15px;">Oops! No data available to show rating based feedback graph chart.</span>
                                    </div>
                                    <br>
                                </div>

                            <?php } ?>
                            <!-- /.panel-body -->
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>

</section>




<script type="text/javascript">

    $(document).ready(function() {
        $('#example').DataTable();
    } );
</script>

