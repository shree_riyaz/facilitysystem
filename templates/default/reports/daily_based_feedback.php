<?php

$lang = $language->english($lang);
$DBFilter = New $DBFilter();
//print_r($_SESSION);
//echo "Hello";
//print_r($Row);
$id = $_SESSION['user_id'];
$date= date("d-M-Y");
$expiary_date = date("d-M-Y", strtotime($Row->expiary_date));

//print_r( $DBFilter->SelectRecord('faults'));
//echo count($feedbk[0]);
//echo "<pre>"; print_r($Row); exit;
?>
<style>text.highcharts-credits {
        display: none;
    }</style>

<section>
    <div class="container-fluid page-wrapper">
        <div class="row nomargin">
            <div style="height: 100vh" class="col-sm-12 drop-shadow nopadding">
                <div class="user-heading fixedHeader">
                    <div class="row">
                        <div class="col-md-3 col-sm-4 col-xs-6">
                    <span class="side-nav-open">
                                <i class="fa fa-bars" aria-hidden="true"></i>
                            </span>
                    <span style="vertical-align: text-bottom">Reports</span>
                    </div>
                    <div class="col-md-5 col-sm-2 hidden-xs"></div>
                    <div style="margin-top:0px !important;" class="col-md-2 col-sm-3 hidden-xs">

                    </div>
                    <div class="col-md-2 col-sm-3 col-xs-6">
                    <?php
                    include_once 'user_profile.php';
                    ?>
                        </div>
                    </div>
                </div>
                <div class="userbg">

                    <div class="row">
                        <div style="padding:0px 0px 30px 0px; overflow: hidden;" class="col-sm-12 demo">
                            <?php
                            include_once(CURRENTTEMP."/"."date_picker.php");
                            ?>
                        </div>
                        <div class="col-lg-12">
                            <div class="panel panel-default report-panel">
                                <div class="panel-heading">
                                    <i class="fa fa-sun-o" aria-hidden="true"></i> Daily Feedback
                                </div>
                                <?php if (count($data_overall)){ ?>
                                <!-- /.panel-heading -->
                                <div class="panel-body">
                                    <div class="overall-feedback-div">

                                        <div id="container_overall"></div>

                                        <script type="text/javascript">

                                            // create the chart
                                            var chart = Highcharts.chart('container_overall', {
                                                chart: {
                                                    events: {
                                                        addSeries: function () {
                                                            var label = this.renderer.label('A series was added, about to redraw chart', 100, 120)
                                                                .attr({
                                                                    fill: Highcharts.getOptions().colors[0],
                                                                    padding: 10,
                                                                    r: 5,
                                                                    zIndex: 8
                                                                })
                                                                .css({
                                                                    color: '#FFFFFF'
                                                                })
                                                                .add();

                                                            setTimeout(function () {
                                                                label.fadeOut();
                                                            }, 1000);
                                                        }
                                                    }
                                                },
                                                title: {
                                                    text: ''
                                                },
                                                xAxis: {
                                                    categories: <?php echo $overall_feedback_by_date; ?>
                                                },

                                                series: [{
                                                    data: <?php echo $overall_feedback_count; ?>,
                                                    name: "Daily Feedback Record"

                                                }]
                                            });

                                        </script>


                                    </div>
                                </div>
                                <?php } else { ?>
                                    <div class="panel-body">
                                        <br>
                                        <div class="text-center">
                                            <span  style="font-size: 15px;">Oops! No data available to show fault based feedback graph chart.</span>
                                        </div>
                                        <br>
                                    </div>

                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>




<script type="text/javascript">

    $(document).ready(function() {
        $('#example').DataTable();
    } );
</script>

