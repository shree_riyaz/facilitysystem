<?php

$lang = $language->english($lang);
$DBFilter = New $DBFilter();
//print_r($_SESSION);
//echo "Hello";
//print_r($Row);
$id = $_SESSION['user_id'];
$date= date("d-M-Y");
$expiary_date = date("d-M-Y", strtotime($Row->expiary_date));

//https://jsfiddle.net/gh/get/library/pure/highcharts/highcharts/tree/master/samples/highcharts/demo/pie-legend/

//print_r( $DBFilter->SelectRecord('faults'));
//echo count($feedbk[0]);
//echo "<pre>"; print_r($Row); exit;
?>
<style>text.highcharts-credits {
        display: none;
    }</style>

<section>
    <div style="height: 100vh" class="col-sm-12 drop-shadow nopadding">
        <div class="user-heading fixedHeader">
            <div class="row">
                <div class="col-md-3 col-sm-4 col-xs-6">
            <span class="side-nav-open">
                                <i class="fa fa-bars" aria-hidden="true"></i>
                            </span>
            <span style="vertical-align: text-bottom">Reports</span>
                    </div>
                    <div class="col-md-5 col-sm-2 hidden-xs"></div>
                    <div style="margin-top:0px !important;" class="col-md-2 col-sm-3 hidden-xs">

                    </div>
                    <div class="col-md-2 col-sm-3 col-xs-6">
            <?php
            include_once 'user_profile.php';
            ?>
                        </div>
                    </div>
        </div>
        <div class="userbg">

            <?php if(($_SESSION['usertype']) == 'company_admin' ) { ?>

                <div class="row">
                    <div style="padding:0px 0px 30px 0px; overflow: hidden;" class="col-sm-12 demo">
                        <?php
                        include_once(CURRENTTEMP."/"."date_picker.php");
                        ?>
                    </div>
                    <div class="col-lg-12">
                        <div class="panel panel-default report-panel">
                            <div class="panel-heading">
                                <i class="fa fa-bar-chart-o fa-fw"></i> Genuine Based Feedback By Month
                            </div>
                            <!-- /.panel-heading -->
                            <div class="panel-body">
                                <?php
                                $gen_number = isset($count_data[1]->total_genuine) ? $count_data[1]->total_genuine : 0;
                                $ann_number = isset($count_data[0]->total_anonymous) ? $count_data[0]->total_anonymous : 0;

                                ?>
                                <span style="font-weight: bold; color: darkolivegreen">Count - <?php echo 'Genuine('. $gen_number.') / Anonymous('. $ann_number.')'; ?></span>

                                <br>

                                <!--                                        <span style="font-weight: bold; color: darkolivegreen">Ratio  -  --><?php //echo getRatio($count_data[1]->total_genuine,$count_data[0]->total_anonymous); ?><!--</span>-->
                                <br>
                                <div class="table-responsive ">
                                    <table class="table table-bordered interval-feedback table-striped table-hover" id="example">
                                        <thead>
                                        <tr>
                                            <th>Date</th>
                                            <th>Number of feedback</th>
                                            <?php foreach ($Row[0][0] as $row_list_key => $row_list_th) { ?>
                                                <th><?php echo $row_list_th->title ?></th>
                                            <?php }  ?>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        foreach ($Row[1] as $row_list_key => $row_list) {
                                            $get_options = [];
                                            foreach ($row_list['rating_options_detail'] as $row_list_key => $row_list_th) {
                                                $get_options[] = $row_list_th->title;
                                                $get_options_all = array_flip($get_options);
                                            }
                                            ?>

                                            <tr>
                                                <td><?php echo isset($row_list['month_name']) ? $row_list['month_name']  : 0 ?></td>
                                                <td><?php echo isset($row_list['total_feedback']) ? $row_list['total_feedback']: 0 ?></td>
                                                <?php
                                                foreach ($get_options_all as $get_options_all_key=>$get_options_all_val) {
                                                    ?>
                                                    <td><?php echo isset($row_list['number_of_feedback'][$get_options_all_key]) ? $row_list['number_of_feedback'][$get_options_all_key] : 0 ?></td>
                                                <?php   } ?>
                                            </tr>
                                        <?php }  ?>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-bar-chart-o fa-fw"></i> Genuine vs Anonymous Graph Chart
                        </div>
                        <!-- /.panel-heading -->
                        <?php if ($gen_number > 0 ){ ?>

                            <div class="panel-body">
                                <div class="">
                                    <script type="text/javascript">
                                        $(function () {

//                                                Highcharts.chart('container', {
                                            var chart = Highcharts.chart('combined_genuine_vs_anno', {

                                                chart: {
                                                    plotBackgroundColor: null,
                                                    plotBorderWidth: null,
                                                    plotShadow: false,
                                                    type: 'pie'
                                                },
                                                title: {
                                                    text: 'Genuine vs Annonymous comparison chart'
                                                },
                                                tooltip: {
                                                    pointFormat: '{series.name}: <b>{point.y:.f}</b>'
                                                },
                                                plotOptions: {
                                                    pie: {
                                                        allowPointSelect: true,
                                                        cursor: 'pointer',
                                                        dataLabels: {
                                                            enabled: false
                                                        },
                                                        showInLegend: true
                                                    }
                                                },
                                                series: [{
                                                    name: 'Count',
                                                    colorByPoint: true,
                                                    data: [{
                                                        name: 'Genuine',
                                                        y: <?php echo $count_data[1]->total_genuine ; ?>,

                                                    }, {
                                                        name: 'Annonymous',
                                                        y: <?php echo $count_data[0]->total_anonymous ; ?>,
                                                    }]
                                                }]
                                            });
                                        });
                                    </script>
                                    <div id="combined_genuine_vs_anno"></div>
                                </div>
                            </div>
                        <?php } else { ?>
                            <div class="panel-body">
                                <br>
                                <div class="text-center">
                                    <span  style="font-size: 15px;">Oops! No data available to show genuine based feedback graph chart.</span>
                                </div>
                                <br>
                            </div>

                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>

<script type="text/javascript">
    $(document).ready(function() {
        $('#example').DataTable();
    } );
</script>

