<?php 
	/*======================================
	Developer	-	Jaishree Sahal
	Module      -   Plan
	SunArc Tech. Pvt. Ltd.
	======================================		
	******************************************************/
?>

<script>

function checkAll(ele) {
     var checkboxes = document.getElementsByTagName('input');
	 
	if (ele.checked) {
         for (var i = 0; i < checkboxes.length; i++) {
             if (checkboxes[i].type == 'checkbox') {
                 checkboxes[i].checked = true;
             }
         }
     } else {
         for (var i = 0; i < checkboxes.length; i++) {
             console.log(i);
             if (checkboxes[i].type == 'checkbox') {
                 checkboxes[i].checked = false;
             }
         }
     }
 }

function showpassdiv(id){
$("#password_"+id).css("display", "block");
}
function Check()
{
	if(document.getElementById('keyword').value=='')
	{
		alert('Please enter any value for search.');
		return false;
	}
	else
	{
		return true;
	}
}
function Clear()
{
	document.getElementById('keyword').value='';
	location.href="index.php?mod=plan";
	return false;
}
</script>
		
<br />
<?php 
$lang = $language->english($lang);
?>
<form method="post" name="frmlist" id="frmlist" >
<?php
	if (isset($_SESSION['ActionAccess']))
	{
				echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="90%" ><tbody><tr><td colspan="6"  align="center"><div class="errormsg">';
				echo $_SESSION['ActionAccess'];
				echo '</div></td></tr></tbody></table><br>';
				unset($_SESSION['ActionAccess']);
			
	}
	if(isset($_SESSION['error']))
			{
				echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" ><tbody><tr><td colspan="6"  align="center">
				<div class="alert alert-danger alert-dismissable">
				   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
				  echo $_SESSION['error'];
				echo '</div></td></tr></tbody></table><br>';
				unset($_SESSION['error']);
			}
    if(isset($_SESSION['success']))
			{
				echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" ><tbody><tr><td colspan="6"  align="center">
				<div class="alert alert-success alert-dismissable">
				   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
				echo $_SESSION['success'];
				echo '</div></td></tr></tbody></table><br>';
				unset($_SESSION['success']);
			}
			?><center>
	
<div style=" border: 1px #ddd solid; width:50% !important; display: table; text-align : center;">
	<table class="table" style="width:100% !important;">
		<tbody>
		<tr valign="middle" align="center"> 
		  <th height="30" class="thColor" colspan="2" style="padding-left:5px;"><font color="#FFFFFF"><?php echo $lang['Search Plan']?></font></th>
		</tr>
	</table>

	<div class="form-group" style="width:100% !important;">
		<label for="username" class="control-label col-xs-4"><?php echo $lang['Search Plan']?></label>
	   <input type="text" class="form-control" id="keyword" name="keyword" value="<?php echo (isset($frmdata['keyword'])?$frmdata['keyword']:'');?>" onbuler="return  KeywordSearch()" >
	   <span style="font-size:11px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*Search By Plan Name, Plan Description, Plan Price, No Of Supervisors, No Of Cleaners</span>
	</div>

	 <!--<br/><br/><br/><br/>-->
	<div class="col-xs-offset-2 col-xs-10" style="width:100% !important;  margin-left:0px; padding-bottom:10px;">
		<button type="submit" class="btn btn-primary" name="search"  onclick="return Check()"><?php echo $lang['Search']?></button>
		<button type="reset" class="btn btn-primary" name="clearsearch" id="clear" onclick="return Clear()"><?php echo $lang['Reset']?></button>
		<br/>
	</div> 
</div>

	<br/>	<br/>
	<table width="90%" border="0" cellspacing="0" cellpadding="0" align="center"  >
  	
		<tr>
  		<td align="right" height='12'>&nbsp;
  		
		<a title="Add New Plan" style=" font-family:Arial, Helvetica, sans-serif;cursor:hand;font-size:14px" href="index.php?mod=plan&do=add "><?php echo $lang['Add New Plan']?>
        </a>
        </td>
  		</tr>
</table>
<?php
   // echo '<pre>';print_r($Row);
		$srNo=$frmdata['from'];
		$count=count($Row);
		?>
		<table width="90%" align="center" cellpadding="0"  cellspacing="0" border="0" class="table table-striped" >
		<tr class="trwhite"> 
			<?php
				if(($_SESSION['usertype'])=='super_admin' ||($_SESSION['usertype'])=='company_admin' )
				{
				?>
                <td align="left">
                <select name="actions" onchange="this.form.submit()" class="form-control">
                <option value="">Select Action</option>
                <option value="Delete">Delete</option>
                <option value="Active">Active</option>
                <option value="Inactive">In-Active</option>
                </select>
               </td>
               <?php
          		 }
          		?>
			<td colspan="6" align='right' style= "padding-right: 10px;" >
					<?php	
						if($totalCount > 0)
						   print "Showing Results:".($frmdata['from']+1).'-'.($frmdata['from']+$count)." of ".$totalCount; 
						else
                           print "Showing Results:0-0 of 0";
						?>
			</td>		
						
				<td colspan="1"  align="right" width="115"><span style="float: left; margin-top: 9px; ">Show :</span><?php echo getPageRecords();?></td>
				

		</tr>
	<tr class="tblheading">
		<th class="anth"height="36" align="center"><?php
		if(($_SESSION['usertype'])=='super_admin'){
		?><input type="checkbox" name="chkAll[]" id="chkAll"  onchange="checkAll(this)" title="Select All"/><?php } echo ' '.$lang['S.No.']?></th>
		<th  align="center" class="anth"> 
		<a  style="cursor:pointer;color:#FFF;" onClick="OrderPage('p.plan_name');"><?php echo $lang['Plan Name']?>
		<?php if($frmdata['orderby']=='p.plan_name') {print '<img src="'.IMAGEURL.'/asc.gif">';} elseif($frmdata['orderby']=='p.plan_name desc'){print '<img src="'.IMAGEURL.'/desc.gif">';}?>
		</a>
		</th>
		<th  align="center" class="anth"> 
		<a  style="cursor:pointer;color:#FFF;" onClick="OrderPage('p.is_active');"><?php echo $lang['Status']?>
		<?php if($frmdata['orderby']=='p.is_active') {print '<img src="'.IMAGEURL.'/asc.gif">';} elseif($frmdata['orderby']=='p.is_active desc'){print '<img src="'.IMAGEURL.'/desc.gif">';}?>
		</a>
		</th>
		<th  align="center" class="anth"> 
		<a  style="cursor:pointer;color:#FFF;" onClick="OrderPage('p.plan_description');"><?php echo $lang['Plan Description']?>
		<?php if($frmdata['orderby']=='p.plan_description') {print '<img src="'.IMAGEURL.'/asc.gif">';} elseif($frmdata['orderby']=='p.plan_description desc'){print '<img src="'.IMAGEURL.'/desc.gif">';}?>
		</a>
		</th>
		<th  align="center" class="anth"> 
		<a  style="cursor:pointer;color:#FFF;" onClick="OrderPage('p.no_manager');"><?php echo $lang['No Of Supervisors']?>
		<?php if($frmdata['orderby']=='p.no_manager') {print '<img src="'.IMAGEURL.'/asc.gif">';} elseif($frmdata['orderby']=='p.no_manager desc'){print '<img src="'.IMAGEURL.'/desc.gif">';}?>
		</a>
		</th>
		<th  align="center" class="anth"> 
		<a  style="cursor:pointer;color:#FFF;" onClick="OrderPage('p.no_officer');"><?php echo $lang['No Of Cleaners']?> 
		<?php if($frmdata['orderby']=='p.no_officer') {print '<img src="'.IMAGEURL.'/asc.gif">';} elseif($frmdata['orderby']=='p.no_officer desc'){print '<img src="'.IMAGEURL.'/desc.gif">';}?>
		</a>
		</th>	
        <th  align="center" class="anth"> 
		<a  style="cursor:pointer;color:#FFF;" onClick="OrderPage('p.plan_price');"><?php echo $lang['Price']?> 
		<?php if($frmdata['orderby']=='p.plan_price') {print '<img src="'.IMAGEURL.'/asc.gif">';} elseif($frmdata['orderby']=='p.plan_price desc'){print '<img src="'.IMAGEURL.'/desc.gif">';}?>
		</a>
		</th>		
		
		<th class="anth"><?php echo $lang['Edit / Delete']?></th>
	</tr>	
<?php
if($Row)
{	


			
    $objpassEncDec = new passEncDec;
	for($counter=0;$counter<$count;$counter++)
	{
				$srNo++;
				if(($counter%2)==0)
				{
					$trClass="tdbggrey";
				}
				else
				{
					$trClass="tdbgwhite";
				}
				
		
		
          $confirmDelete = 'Do you really want to delete this Plan ?';		
		  $obj = new passEncDec;
		  ?>
		
	<tr>
		<td align='center'><?php if(($_SESSION['usertype'])=='super_admin') { ?><input type="checkbox" name="chkbox[]" id="chkbox" value="<?php echo $Row[$counter]->plan_id;?>"/><?php } echo ' '.$srNo; ?></td>
		<td align='center'><?php echo ucfirst($Row[$counter]->plan_name); ?>&nbsp;</td>
		<td align='center'><?php if($Row[$counter]->is_active == 'Y')echo ucfirst('active'); else echo ucfirst('in-Active'); ?>&nbsp;</td>
		<td align='center'><?php echo substr($Row[$counter]->plan_description,0,20)."..."; ?>&nbsp;</td>
		<td align='center'><?php echo ucfirst($Row[$counter]->no_manager.' '.$Row[$counter]->last_name); ?></td>
		<td align='center'><?php echo ucfirst($Row[$counter]->no_officer); ?>&nbsp;</td>
        <td align='center'><?php if($Row[$counter]->plan_price=='') {echo '-';} else { echo ucfirst($Row[$counter]->plan_price); }?>&nbsp;</td>
        <td align='center'>
		<a class="fontstyle" href='<?php print CreateURL('index.php','mod=plan&do=edit&id='.$Row[$counter]->plan_id);?>' title="Edit" ><img src="<?php echo IMAGEURL."/b_edit.png" ?>" border=0 /></a>
		<a title="Delete"  href='<?php print CreateURL('index.php','mod=plan&do=del&id='.$Row[$counter]->plan_id);?>' onclick="return confirm('<?php echo $confirmDelete ?>')"><img src="<?php print IMAGEURL ?>/b_drop.png"></a>
		</td>
		
	</tr>
	<?php
		$sno++;
	}
?>
</table>
   <table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">		
		 <tr>
			  <td align="center" colspan="12">
				  <?php
					PaginationDisplay($totalCount);
				   ?>
			  </td>
		</tr>
	 </table>
	<!--</div>-->
<?php 
}
elseif($_SESSION['keywords'] == 'Y')
{
	$frmdata['message']="Sorry ! Plan not found for the selected criteria";
	unset ($_SESSION['keywords']);
	ShowMessage();
}
else
{

$frmdata['message']="Sorry ! No plan. Please add plan first.";
		ShowMessage();
?>
<style> .tblheading,.trwhite{ display:none; }

</style>
<?php

}?>


      <input name="pageNumber" type="hidden" value="<?php print $frmdata['pageNumber']?>" >
	  <input name="orderby" type="hidden" value="<?php if($frmdata['orderby']!='')  echo $frmdata['orderby']; else echo 'p.plan_id';?>" >
	  <input name="order" type="hidden" value="<?php print $frmdata['order']?>" >
	  <input name="actUserID" type="hidden" value="" /></form>	
	
</center>