<?php
/*======================================
Developer	-	Jaishree Sahal
Module      -   Plan
SunArc Tech. Pvt. Ltd.
======================================
******************************************************/
?>

<script>

    function checkAll(ele) {
        var checkboxes = document.getElementsByTagName('input');

        if (ele.checked) {
            for (var i = 0; i < checkboxes.length; i++) {
                if (checkboxes[i].type == 'checkbox') {
                    checkboxes[i].checked = true;
                }
            }
        } else {
            for (var i = 0; i < checkboxes.length; i++) {
                console.log(i);
                if (checkboxes[i].type == 'checkbox') {
                    checkboxes[i].checked = false;
                }
            }
        }
    }

    function showpassdiv(id) {
        $("#password_" + id).css("display", "block");
    }
    function Check() {
        if (document.getElementById('keyword').value == '') {
            alert('Please enter any value for search.');
            return false;
        }
        else {
            return true;
        }
    }
    function Clear() {
        document.getElementById('keyword').value = '';
        location.href = "index.php?mod=plan";
        return false;
    }
</script>

<?php
$lang = $language->english($lang);
?>

    <section>

        <div class="col-sm-12 drop-shadow nopadding">
                    <form method="post" class="form-horizontal" name="frmlist" id="frmlist">
                        <?php

                        if(isset($_SESSION['error']))
                        {
                            echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" ><tbody><tr><td colspan="6"  align="center">
				<div class="alert alert-danger alert-dismissable">
				   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
                            echo $_SESSION['error'];
                            echo '</div></td></tr></tbody></table><br>';
                            unset($_SESSION['error']);
                        }
                        if(isset($_SESSION['success']))
                        {
                            echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" ><tbody><tr><td colspan="6"  align="center">
				<div class="alert alert-success alert-dismissable">
				   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
                            echo $_SESSION['success'];
                            echo '</div></td></tr></tbody></table><br>';
                            unset($_SESSION['success']);
                        }
                        ?>
                    <div class="user-heading fixedHeader">
                        <div class="row">
                            <div class="col-xs-3">
                                 <span class="side-nav-open">
                                <i class="fa fa-bars" aria-hidden="true"></i>
                            </span>
                                <span class=""><?php echo $lang['Plans']?></span>
                            </div>
                            <div class="col-md-4 col-sm-3 col-xs-2"></div>
                            <div style="margin-top:0px !important;" class="col-sm-3 col-xs-4 select-caret">
                                <select name="select_locale" onchange="this.form.submit()" class="form-control show-result select_locale">
                                    <option style="color: white;" value="">Select Language</option>
                                    <?php foreach ($get_language_list[0] as $get_language_list_list_value) { ?>
                                        <option <?php if ($get_language_list_list_value->short_code == $_SESSION['selected_language']){?> selected="selected" <?php } ?> style="color: white;" value="<?php echo $get_language_list_list_value->short_code ?>"> <?php echo trim($get_language_list_list_value->language_name); ?> </option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="col-md-2 col-xs-3">
                                <?php
                                include_once 'user_profile.php';
                                ?>
                                </div>
                        </div>



                    </div>
                    <div class="userbg">

                        <div class="row">
                            <div class="col-sm-12 ">
                                <div class="icon-group">
                                    <input type="text" id="keyword"
                                           value="<?php echo(isset($frmdata['keyword']) ? $frmdata['keyword'] : ''); ?>"
                                           onblur="return  KeywordSearch()" name="keyword" class="form-control search"
                                           placeholder="<?php echo $lang['Search Plan'] ?>">
                                    <i class="fa fa-search search-icon" aria-hidden="true"></i>
                                    <p style="margin: 12px 0px; text-align: center;">
                                       <span style="font-weight:bold; font-size:12px;">
                                        * <?php echo $lang['Search By Plan Name, Plan Description, Plan Price, No Of Supervisors, No Of Cleaners']?>
                                   </span>
                                    </p>
                                </div>
                            </div>
                            <!--<div class="col-xs-offset-2 col-xs-10" style="width:100% !important;  margin-left:0px; padding-bottom:10px;">
                                <button type="submit" class="btn btn-primary" name="search"  onclick="return Check()"><?php /*echo $lang['Search']*/ ?></button>
                                <button type="reset" class="btn btn-primary" name="clearsearch" id="clear" onclick="return Clear()"><?php /*echo $lang['Reset']*/ ?></button>
                                <br/>
                            </div>-->
                        </div>

                        <div class="row">
                            <div class="col-sm-3">

                            </div>

                            <div class="col-sm-9">
                                <div class="pull-right">
                                    <a title="Add New Plan" href="index.php?mod=plan&do=add"
                                       class="btn btn-danger add-company margin_30"><?php echo $lang['Add New Plan'] ?></a>

                                </div>
                            </div>
                        </div>
                        <?php
                        $srNo = $frmdata['from'];
                        $count = count($Row);
                        ?>
                        <div class="user-heading">
                            <span class=""></span>
                        </div>
                        <div class="plan-category">
                            <div class="row">
                                <?php
                                if($Row)
                                {
                                $objpassEncDec = new passEncDec;
                                for($counter=0;$counter<$count;$counter++)
                                {
                                $srNo++;
                                if(($counter%2)==0) {
                                    $trClass="tdbggrey"; } else { $trClass = "tdbgwhite";
                                }

                                $confirmDelete = 'All the related data of this user will be deleted .Do you really want to delete this User ?';
                                $obj = new passEncDec;

                                    $color = isset($Row[$counter]->paid) && $Row[$counter]->paid == 'Y'  ? '#ffbb51' : '#5db85b';
                                    $border_top_color = isset($Row[$counter]->paid) && $Row[$counter]->paid == 'Y'  ? '4px solid #ffbb51' : '4px solid #5db85b';
                                    $btn_border_color = isset($Row[$counter]->paid) && $Row[$counter]->paid == 'Y'  ? '#ffbb51' : '#5db85b';

                                ?>

                                <div class="col-lg-3 col-md-4 col-sm-6 plan-template">
                                    <div style="border-top: <?php echo $border_top_color; ?>" class="basic text-center plan-template-child">
                                        <h3><?php echo ucfirst($Row[$counter]->plan_name); ?></h3>
                                        <h2 style="background-color:<?php echo $color; ?>"><?php echo isset($Row[$counter]->paid) && $Row[$counter]->paid == 'Y'  ? 'Rs. '.$Row[$counter]->plan_price : 'Free'; ?> </h2>
                                        <p><?php echo $lang['Status']?> - <?php if($Row[$counter]->is_active == 'N'){echo $lang[ucfirst('in-Active')];} else {echo $lang[ucfirst('active')];}?></p>
                                        <p><?php echo $lang['Description']?>- <?php echo substr($Row[$counter]->plan_description,0,25)."..."; ?> </p>
                                        <p><?php echo $lang['No Of Supervisors']?>- <?php echo ucfirst($Row[$counter]->no_manager); ?></p>
                                        <p class="no-border"><?php echo $lang['No Of Cleaners']?>- <?php echo ucfirst($Row[$counter]->no_officer); ?></p>
                            <a style="background-color: <?php echo $color; ?>; border-color: <?php echo $btn_border_color; ?>" href="<?php print CreateURL('index.php','mod=plan&do=edit&id='.$Row[$counter]->plan_id);?>" type="button" class="btn btn-success"><?php echo $lang['Edit']?></a>
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="text-center">
                                        <?php
                                        PaginationDisplay($totalCount);
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>

                    <?php
                    }
                    elseif ($_SESSION['keywords'] == 'Y'){
                    $frmdata['message'] = "Sorry ! Company not found for the selected criteria";
                        unset ($_SESSION['keywords']);
                        ShowMessage();
                    } else {
                        $frmdata['message'] = "Sorry ! No company found. Please add company first.";
                        ShowMessage();
                    }
                    ?>
                    </div>
                        <input name="pageNumber" type="hidden" value="<?php print $frmdata['pageNumber'] ?>">
                        <input name="orderby" type="hidden"
                               value="<?php if ($frmdata['orderby'] != '') echo $frmdata['orderby']; else echo 'p.plan_id'; ?>">
                        <input name="order" type="hidden" value="<?php print $frmdata['order'] ?>">
                        <input name="actUserID" type="hidden" value=""/></form>
                </div>

    </section>

