<?php
	/*======================================
	Developer	-	JAishree Sahal
	Module      -   Plan
	SunArc Tech. Pvt. Ltd.
	======================================		
	******************************************************/
	

?>

<script>
$(document).ready(function(e) {
    $("#plan_price").hide();
});
function showPaid() {
	$("#plan_price").show();
}
function hidePaid() {

    document.getElementById("plan_price").style.display = "none";
}

 $(function() {
		
		$("#creation_date").datepicker();
		$("#expiary_date").datepicker();
	});

</script>
<?php 
$lang = $language->english('eng');
?>

<section>

    <div class="col-sm-12 drop-shadow nopadding">
        <form method="post" class="form-horizontal" name="company_add" id="company_add" enctype="multipart/form-data">
            <?php

            if(isset($_SESSION['error']))
            {
                echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" ><tbody><tr><td colspan="6"  align="center">
				<div class="alert alert-danger alert-dismissable">
				   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
                echo $_SESSION['error'];
                echo '</div></td></tr></tbody></table>';
                unset($_SESSION['error']);
            }
            if(isset($_SESSION['success']))
            {
                echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" ><tbody><tr><td colspan="6"  align="center">
				<div class="alert alert-success alert-dismissable">
				   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
                echo $_SESSION['success'];
                echo '</div></td></tr></tbody></table>';
                unset($_SESSION['success']);
            }
            ?>
        <div class="user-heading fixedHeader">
             <span class="side-nav-open">
                                <i class="fa fa-bars" aria-hidden="true"></i>
                            </span>
            <span><?php echo $lang['Add Plan'] ?></span>
            <?php
            include_once 'user_profile.php';
            ?>
        </div>
        <div class="userbg">
            <div id="users">
                <h4 class="update-user"></h4>
            </div>
            <div class="plan-category user-page-form">

                <form class="form-horizontal">
                    <div class="form-group">
                        <label for="RelatedTo" class="col-sm-3"><?php echo $lang['Plan Name'] ?>
                            <sup>*</sup></label>
                        <div class="col-sm-9">
                            <input type="text" title="Enter Plan Name" class="form-control" id="plan_name" name="plan_name" value="<?php echo $_POST['plan_name'];?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="Role" class="col-sm-3"><?php echo $lang['No Of Supervisors'] ?>
                            <sup>*</sup></label>
                        <div class="col-sm-9">
                            <input type="text" name="no_manager" id="no_manager"  class="form-control" value="<?php echo $_POST['no_manager']?>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="FirstName" class="col-sm-3"><?php echo $lang['No Of Cleaners'] ?>
                            <sup>*</sup></label>
                        <div class="col-sm-9">
                            <input type="text" name="no_officer" id="no_officer" class="form-control" value="<?php echo $_POST['no_officer']?>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="LastName" class="col-sm-3"><?php echo $lang['Payment Option'] ?>
                            <sup>*</sup></label>
                        <div class="col-sm-4">
                            <input type="radio" name="paid" value="Y" id="paidY" onclick="showPaid()"/> <?php echo $lang['Paid'] ?> &nbsp;&nbsp;&nbsp;&nbsp;
                            <input type="radio" name="paid" value="N"  id="paidN" onclick="hidePaid()" checked="checked"/> <?php echo $lang['Un-Paid'] ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="EmailId" class="col-sm-3"><?php echo $lang['Price'] ?>
                            <sup>*</sup></label>
                        <div class="col-sm-9">
                            <input type="text" name="plan_price"  class="form-control" value="<?php echo $_POST['plan_price']?>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="Password" class="col-sm-3"><?php echo $lang['Plan Description'] ?>
                            <sup>*</sup></label>
                        <div class="col-sm-9">
                            <textarea  class="form-control" id="plan_description" name="plan_description"><?php echo $_POST['plan_description']; ?></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-12">
                            <button type="submit" name="add_plan" class="btn btn-danger add-company pull-right"><?php echo $lang['Add Plan'] ?></button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        </form>
    </div>



</section>

