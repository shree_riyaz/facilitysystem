<?php
	/*======================================
	Developer	-	JAishree Sahal
	Module      -   Plan
	SunArc Tech. Pvt. Ltd.
	======================================		
	******************************************************/
	

?>

<script>
$(document).ready(function(e) {
    $("#plan_price").hide();
});
function showPaid() {
	$("#plan_price").show();
}
function hidePaid() {

    document.getElementById("plan_price").style.display = "none";
}

 $(function() {
		
		$("#creation_date").datepicker();
		$("#expiary_date").datepicker();
	});

</script>
<?php 
$lang = $language->english('eng');
?>
<form method="post" name="company_add" id="company_add" enctype="multipart/form-data">

<center>
	<?php 
				
			if(isset($_SESSION['error']))
			{
				echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" ><tbody><tr><td colspan="6"  align="center">
				<div class="alert alert-danger alert-dismissable">
				   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
				  echo $_SESSION['error'];
				echo '</div></td></tr></tbody></table><br>';
				unset($_SESSION['error']);
			}
			if(isset($_SESSION['success']))
			{
				echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" ><tbody><tr><td colspan="6"  align="center">
				<div class="alert alert-success alert-dismissable">
				   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
				echo $_SESSION['success'];
				echo '</div></td></tr></tbody></table><br>';
				unset($_SESSION['success']);
			}
			?>

 <table width="80%" border="0" align="center" cellpadding="0" cellspacing="0" class="table table-bordered">
    <tbody><tr valign="middle" align="center"> 
      <th height="30" class="thColor" colspan="2" style="padding-left:5px;"><font color="#FFFFFF"><?php echo $lang['Add New Plan']?></font></th>
    </tr>
	<tr>
		<td   colspan="2"  style="font-size:10px; color:red;" align="right"  class="fontstyle">*<?php echo $lang['All fields are mandatory']?></td>
	</tr>
	
	<tr> 
		<td class="fontstyle" align="right">
		 <div class="form-group" style="width:50% !important;">
            <label for="username" class="control-label col-xs-10"><?php echo $lang['Plan Name'].MANDATORYMARK ?></label></td>
		<td align="left"><div class="col-xs-4">
		   <input type="text" title="Enter Plan Name" class="form-control" id="plan_name" name="plan_name" value="<?php echo $_POST['plan_name'];?>"> 
           </div>
     </td>
     	</tr>
	<tr>
		<td class="fontstyle" align="right">
        <label for="managers" class="control-label col-xs-10"><?php echo $lang['No Of Supervisors'].MANDATORYMARK ?></label></td>
		<td align="left"><div class="col-xs-4">
        <input type="text" name="no_manager" id="no_manager"  class="form-control" value="<?php echo $_POST['no_manager']?>" />
 </div></td>
	</tr>
	
	 <tr>
		<td class="fontstyle" align="right">
        <label for="officers" class="control-label col-xs-10"><?php echo $lang['No Of Cleaners'].MANDATORYMARK ?></label></td>
		<td align="left"><div class="col-xs-4">
        <input type="text" name="no_officer" id="no_officer" class="form-control" value="<?php echo $_POST['no_officer']?>" />
 </div></td>
	</tr>
     <tr>
		<td class="fontstyle" align="right">
        <label for="officers" class="control-label col-xs-10"><?php echo $lang['Payment Option'].MANDATORYMARK ?></label></td>
		<td align="left"><div class="col-xs-6">
   			<input type="radio" name="paid" value="Y" id="paidY" onclick="showPaid()"/> Paid &nbsp;&nbsp;&nbsp;&nbsp;
            <input type="radio" name="paid" value="N"  id="paidN" onclick="hidePaid()" checked="checked"/> Un-Paid 
         </div></td>
	 <tr id="plan_price">
		<td class="fontstyle" align="right">
        <label for="officers" class="control-label col-xs-10"><?php echo $lang['Price'].MANDATORYMARK ?></label></td>
		<td align="left"><div class="col-xs-4">
        <input type="text" name="plan_price"  class="form-control" value="<?php echo $_POST['plan_price']?>" />
 </div></td>
	</tr>
    <tr> 
		<td align="right" class="fontstyle" width="30%"> <label for="plan_description" class="control-label col-xs-10"><?php echo $lang['Plan Description'].MANDATORYMARK ?></label></td>
		<td align="left">
		<div class="col-xs-4">
		        <textarea  class="form-control" id="plan_description" name="plan_description"><?php echo $_POST['plan_description']; ?></textarea>
		  </div> </td>
	</tr>
	<tr class="alt">
		<td colspan=2 style="text-align: center;" align="left">
		<div class="col-xs-offset-2 col-xs-10" style="width:50% !important; margin-left: 24.6667%;">
		
		<button type="submit" class="btn btn-primary" name="add_plan"><?php echo $lang['Add']?></button>
		<button type="reset" class="btn btn-primary" name="Reset" onclick="this.form.reset"><?php echo $lang['Reset']?></button>
		<button type="reset" class="btn btn-primary" name="Back" onClick="window.location.href='<?php print CreateURL('index.php','mod=plan');?>'"><?php echo $lang['Back']?></button>
     </div>
		</td>
	</tr>
</table>
<b class="xbottom"><b class="xb4"></b><b class="xb3"></b><b class="xb2"></b><b
	class="xb1"></b></b>

	
</form>

</center>
</body>

</html>
