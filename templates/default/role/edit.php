<?php
	/*======================================
	Developer	-	JAishree Sahal
	Module      -   Role 
	SunArc Tech. Pvt. Ltd.
	======================================		
	******************************************************/
?>

<section>


                <div class="col-sm-12 drop-shadow nopadding">
                    <form method="post" class="form-horizontal" name="frmlist" id="frmlist" enctype="multipart/form-data">

                        <?php

                        if(isset($_SESSION['error']))
                        {
                            echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" ><tbody><tr><td colspan="6"  align="center">
				<div class="alert alert-danger alert-dismissable">
				   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
                            echo $_SESSION['error'];
                            echo '</div></td></tr></tbody></table><br>';
                            unset($_SESSION['error']);
                        }
                        if(isset($_SESSION['success']))
                        {
                            echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" ><tbody><tr><td colspan="6"  align="center">
				<div class="alert alert-success alert-dismissable">
				   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
                            echo $_SESSION['success'];
                            echo '</div></td></tr></tbody></table><br>';
                            unset($_SESSION['success']);
                        }
                        ?>

                        <div class="user-heading fixedHeader">
                             <span class="side-nav-open">
                                <i class="fa fa-bars" aria-hidden="true"></i>
                            </span>
                        <span>Edit Role</span>
                        <?php
                        include_once 'user_profile.php';
                        ?>
                    </div>
                    <div class="userbg">
                        <div id="users">
                            <h4 class="update-user">Edit Role</h4>
                        </div>
                        <div class="plan-category user-page-form">
                                        <label for="role_name" class="control-label edit-role-label">
                                            <?php echo $lang['Role Name'].MANDATORYMARK?>
                                        </label>
                                        <input title="Enter Role Name" type="text" name="role_name" id="role_name" size=30 value="<?php echo $Row->role_name; ?>" class="form-control">

                            <br/>
                            <div class="table-responsive">
                            <table class="table dashboard-table table-bordered table-hover">
                                <tbody>
                                <tr>
                                    <td>
                                        <label for="role_name" class="control-label">
                                            <?php echo $lang['Module Access']?>
                                        </label>
                                    </td>
                                    <td>
                                        <div>
                                            <span style=""><a style="cursor:pointer;font-weight:bold" onclick="selectAllModule()">Select All</a></span>
                                            &nbsp;&nbsp;
                                            <span style=""><a style="cursor:pointer;font-weight:bold" onclick="unselectAllModule()">Unselect All</a></span>
                                        </div>
                                    </td>
                                </tr>

                                <?php
                                $mdcnt=count($modules[0]);

                                for($counter =0; $counter < $mdcnt; $counter++)
                                {
                                    if ($modules[0][$counter]->module_id != '')
                                    {
                                        //echo 'in';
                                        ($counter == ($mdcnt -1)) ? $colspan='2' : $colspan ='';
                                        $checkeda = '';
                                        $checkede = '';
                                        $checkedd = '';
                                        $checkedr ='';
                                        $checkeded = '';
                                        $checkedl='';
                                        $check = '';
                                        $display = 'display:none';

                                        $addPerm = $DB->SelectRecords('rolepermission','role_id='.$Row->role_id.' and module_id='.$modules[0][$counter]->module_id.' and permission_id='.$add);
                                        //echo '<pre>';print_r($addPerm);
                                        if ($addPerm[0])
                                        {
                                            //echo 'in <br/>';
                                            $checkeda = ' checked ';
                                        }

                                        $editPerm = $DB->SelectRecords('rolepermission','role_id='.$Row->role_id.' and module_id='.$modules[0][$counter]->module_id.' and permission_id='.$edit);
                                        //echo '<pre>';print_r($editPerm);
                                        if ($editPerm[0])
                                        {
                                            // echo 'in <br/>';
                                            $checkede = ' checked ';
                                        }

                                        $exportPerm = $DB->SelectRecords('rolepermission','role_id='.$Row->role_id.' and module_id='.$modules[0][$counter]->module_id.' and permission_id='.$export);

                                        if ($exportPerm[0])
                                        {
                                            // echo 'in <br/>';
                                            $checkeded = ' checked ';
                                        }

                                        $readPerm = $DB->SelectRecords('rolepermission','role_id='.$Row->role_id.' and module_id='.$modules[0][$counter]->module_id.' and permission_id='.$read);

                                        if ($readPerm[0])
                                        {
                                            // echo 'in <br/>';
                                            $check = ' checked';
                                            $checkedr = ' checked ';
                                            $display = 'display:""';
                                        }
                                        $deletePerm = $DB->SelectRecords('rolepermission','role_id='.$Row->role_id.' and module_id='.$modules[0][$counter]->module_id.' and permission_id='.$delete);

                                        if ($deletePerm[0])
                                        {
                                            // echo 'in <br/>';
                                            $checkedd = ' checked ';
                                            $display = 'display:""';
                                        }
                                        $listPerm = $DB->SelectRecords('rolepermission','role_id='.$Row->role_id.' and module_id='.$modules[0][$counter]->module_id.' and permission_id='.$list);

                                        if ($listPerm[0])
                                        {
                                            // echo 'in <br/>';
                                            $checkedl = ' checked ';
                                            $display = 'display:""';
                                        }
                                        echo ($counter % 2 == 0) ? '<tr>' : '';
                                      echo '<td colspan="'.$colspan.'"">
                                        
					<input type="checkbox" '.$check.' name="modules['.$modules[0][$counter]->module_id.']" id="'.$modules[0][$counter]->module_id.'" value="'.$modules[0][$counter]->module_id.'" onclick="showhidePer(this,this.id)"><label for="'.$modules[0][$counter]->module_id.'">&nbsp;&nbsp;'.ucfirst($modules[0][$counter]->module_name).'</label>';
                                        echo ' <div style="'.$display.'" id="div:'.$modules[0][$counter]->module_id.'">';
                                        if($modules[0][$counter]->module_name!='dashboard' && $modules[0][$counter]->module_name!='help')
                                        {
                                            echo '<input type="checkbox" '.$checkeda.' name="modules['.$modules[0][$counter]->module_id.'][]" value="A" id="'.$modules[0][$counter]->module_id.':A" >&nbsp;&nbsp;Add
								 <input type="checkbox" '.$checkede.' name="modules['.$modules[0][$counter]->module_id.'][]" value="E" id="'.$modules[0][$counter]->module_id.':E">&nbsp;&nbsp;Edit
							 <input type="checkbox" '.$checkedd.' name="modules['.$modules[0][$counter]->module_id.'][]" value="D" id="'.$modules[0][$counter]->module_id.':D">&nbsp;&nbsp;Delete
							 <input type="checkbox" '.$checkedr.' name="modules['.$modules[0][$counter]->module_id.'][]" value="R" id="'.$modules[0][$counter]->module_id.':R" >&nbsp;&nbsp;View
							  <input type="checkbox" '.$checkedl.' name="modules['.$modules[0][$counter]->module_id.'][]" value="L" id="'.$modules[0][$counter]->module_id.':L" >&nbsp;&nbsp;List';

                                        }

                                        if (in_array($modules[0][$counter]->module_name, array('question_master','backup','report')))
                                        {
                                            echo '<input type="checkbox" '.$checkeded.' name="modules['.$modules[0][$counter]->module_id.'][]" value="ED" id="'.$modules[0][$counter]->module_id.':ED" >Export Data';
                                        }
                                        echo   	'</div>					
							</td>';
                                        echo ($counter % 2 != 0) ? '</tr>' : '';

                                    }
                                }
                                ?>
                            </table>
                                </div>

                            <div class="form-group">
                                <div class="col-sm-12">
                                    <button type="submit" class="btn btn-danger add-company pull-right role-edit-btn" name="editrole"><?php echo $lang['Update']?></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>

</section>


<b class="xbottom"><b class="xb4"></b><b class="xb3"></b><b class="xb2"></b><b
	class="xb1"></b></b>
<style>
    button.btn.btn-danger.add-company.pull-right.role-edit-btn {
        margin-bottom: 25px;
        margin-right: 71px;
    }
</style>
<script>
function showhidePer(module, id)
{
	div = 'div:'+id;
	//alert(div);
	if (module.checked)
	{	
		document.getElementById(div).style.display='';
	}
	else
	{
		document.getElementById(id+':A').checked=false;
		document.getElementById(id+':E').checked=false;
		document.getElementById(div).style.display='none';
		if (document.getElementById(id+':ED'))
		{
			document.getElementById(id+':ED').checked = false;
		}
	}	
}
</script>