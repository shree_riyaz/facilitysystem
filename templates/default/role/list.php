<?php 
	/*======================================
	Developer	-	Jaishree Sahal
	Module      -   Role
	SunArc Tech. Pvt. Ltd.
	======================================		
	******************************************************/

?>

<script>

function Check()
{
	if(document.getElementById('keyword').value=='')
	{
		alert('Please enter any value for search.');
		return false;
	}
	else
	{
		return true;
	}
}
function Clear()
{
	document.getElementById('keyword').value='';
	location.href="index.php?mod=role&do=list";
	return false;
}
</script>

<?php 
$lang = $language->english($lang);
?>

      <section>
          <div class="col-sm-12 drop-shadow nopadding">
              <form method="post" name="frmlist" id="frmlist" >
                  <?php
                  if (isset($_SESSION['ActionAccess']))
                  {
                      echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="90%" ><tbody><tr><td colspan="6"  align="center"><div class="errormsg">';
                      echo $_SESSION['ActionAccess'];
                      echo '</div></td></tr></tbody></table>';
                      unset($_SESSION['ActionAccess']);

                  }
                  if(isset($_SESSION['error']))
                  {
                      echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" ><tbody><tr><td colspan="6"  align="center">
				<div class="alert alert-danger alert-dismissable">
				   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
                      echo $_SESSION['error'];
                      echo '</div></td></tr></tbody></table>';
                      unset($_SESSION['error']);
                  }
                  if(isset($_SESSION['success']))
                  {
                      echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" ><tbody><tr><td colspan="6"  align="center">
				<div class="alert alert-success alert-dismissable">
				   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
                      echo $_SESSION['success'];
                      echo '</div></td></tr></tbody></table><br>';
                      unset($_SESSION['success']);
                  }
                  ?>
              <div class="user-heading fixedHeader">
                  <div class="row">
                      <div class="col-xs-3">
                          <span class="side-nav-open">
                                <i class="fa fa-bars" aria-hidden="true"></i>
                            </span>
                          <span class=""><?php echo $lang['Roles']?></span>
                      </div>
                      <div class="col-md-4 col-sm-3 col-xs-2"></div>
                      <div style="margin-top:0px !important;" class="col-sm-3 col-xs-4 select-caret">
                           <select name="select_locale" onchange="this.form.submit()" class="form-control show-result select_locale">
                              <option style="color: white;" value="">Select Language</option>
                              <?php foreach ($get_language_list[0] as $get_language_list_list_value) { ?>
                                  <option <?php if ($get_language_list_list_value->short_code == $_SESSION['selected_language']){?> selected="selected" <?php } ?> style="color: white;" value="<?php echo $get_language_list_list_value->short_code ?>"> <?php echo trim($get_language_list_list_value->language_name); ?> </option>
                              <?php } ?>
                          </select>
                      </div>
                      <div class="col-md-2 col-xs-3">
                          <?php
                          include_once 'user_profile.php';
                          ?>
                      </div>
                  </div>



              </div>
              <div class="userbg">

                  <div class="row">
                      <div class="col-sm-12 ">
                          <div class="icon-group">
                              <input type="text" id="keyword" value="<?php echo(isset($frmdata['keyword']) ? $frmdata['keyword'] : ''); ?>"
                                     onblur="return  KeywordSearch()" name="keyword" class="form-control search"
                                     placeholder="<?php echo $lang['Search Role'] ?>">
                              <i class="fa fa-search search-icon" aria-hidden="true"></i>
                              <p style="margin: 12px 0px; text-align: center;">
                                       <span style="font-weight:bold; font-size:12px;">
                                        *<?php echo $lang['Search By Role Name'] ?>
                                   </span>
                              </p>
                          </div>
                      </div>
                      <!--<div class="col-xs-offset-2 col-xs-10" style="width:100% !important; margin-left: 0px; padding-bottom:10px;">

                                <button type="submit" class="btn btn-primary" name="search"  onclick="return Check()"><?php /*echo $lang['Search']*/?></button>
                                <button type="reset" class="btn btn-primary" name="clearsearch" id="clear" onclick="return Clear()"><?php /*echo $lang['Reset']*/?></button>
                            </div>-->
                  </div>

                  <div class="row">
                      <div class="col-sm-3">

                      </div>
                      <div class="col-sm-9">
                          <div class="pull-right">
                              <?php if (($_SESSION['usertype']) == 'super_admin' ) { ?>
                                  <a title="Add New Role" href="index.php?mod=role&do=add" class="btn btn-danger add-company margin_30"><?php echo $lang['Add Role']?></a>
                              <?php } echo '<br>';  ?>

                          </div>
                      </div>
                  </div>
                  <?php
                  $srNo = $frmdata['from'];
                  $count = count($Row);
                  ?>
                  <div class="user-heading">
                      <div class="row">
                          <div class="col-xs-3">
                          </div>
                          <div class="col-xs-6">
                              <p class="pull-right margin_4">
                                  <?php
                                  if($totalCount > 0)
                                      print $lang['Showing Results'].' '.($frmdata['from']+1).'-'.($frmdata['from']+$count)." of ".$totalCount;
                                  else
                                      print $lang['Showing Results'];
                                  ?>
                              </p>
                          </div>
                          <div class="col-xs-3 select-caret">
                              <?php echo getPageRecords(); ?>
                          </div>
                      </div>
                  </div>
                  <div class="row">
                      <div class="col-sm-12">
                          <div class="table-responsive">
                              <table class="table dashboard-table table-bordered">
                                  <thead>
                                  <tr>
                                      <th class="text-center">
                                          <?php echo $lang['S.No.'];?>
                                      </th>
                                      <th class="text-center">
                                          <a  style="text-decoration:none; cursor:pointer" onClick="OrderPage('role_name');"><?php echo $lang['Role Name']?>
                                              <?php if($frmdata['orderby']=='role_name') {print '<img src="'.IMAGEURL.'/asc.gif">';} elseif($frmdata['orderby']=='role_name desc'){print '<img src="'.IMAGEURL.'/desc.gif">';}?>
                                          </a>
                                      </th>



                                      <th class="text-center"><?php echo $lang['Action']?></th>
                                  </tr>
                                  </thead>
                                  <tbody>
                                  <?php
                                  if($Row)
                                  {
                                  for($counter=0;$counter<$count;$counter++)
                                  {
                                      if($Row[$counter]->role_name != 'Super Admin') //Added By Shivender
                                      {
                                          $srNo++;
                                          if(($counter%2)==0)
                                          {
                                              $trClass="tdbggrey";
                                          }
                                          else
                                          {
                                              $trClass="tdbgwhite";
                                          }
                                          $confirmDelete = 'Do you really want to delete this Role ?';
                                          ?>
                                          <tr>

                                              <td class="text-center"><?php echo $srNo; ?></td>

                                              <td class="text-center"><?php echo ucfirst($Row[$counter]->role_name); ?></td>

                                              <td class="text-center">
                                                  <?php
                                                  if(($_SESSION['usertype'])=='super_admin' ||($_SESSION['usertype'])=='company_admin')
                                                  {
                                                      ?>
                                                      <a href='<?php print CreateURL('index.php','mod=role&do=edit&id='.$Row[$counter]->role_id);?>' title="Edit">

                                                          <i class="fa fa-pencil" aria-hidden="true"></i></a>

                                                      <a href="#" data-toggle="modal" data-message="<?php echo $confirmDelete;  ?>" delete-link="<?php print CreateURL('index.php','mod=role&do=del&id='.$Row[$counter]->role_id) ?>" data-target="#myModal" class="delete-modal" ><i class="fa fa-trash" aria-hidden="true"></i></a>

                                                  <?php }
                                                  else
                                                  {
                                                  ?>
                                                  <a href='<?php print CreateURL('index.php','mod=user&do=edit&id='.$Row[$counter]->user_id);?>' title="Edit">
                                                      <?php } ?>
                                              </td>
                                          </tr>
                                          <?php $sno++; } } ?>
                                  </tbody>
                              </table>
                          </div>
                      </div>
                  </div>
                  <div class="row">
                      <div class="col-sm-12">
                          <div class="text-center">
                              <?php
                              PaginationDisplay($totalCount);
                              ?>
                          </div>
                      </div>
                  </div>
              <?php
              }
              elseif
              ($_SESSION['keywords'] == 'Y'){
                  $frmdata['message'] = "Sorry ! Company not found for the selected criteria";
                  unset ($_SESSION['keywords']);
                  ShowMessage();
              }
              else {
                  $frmdata['message'] = "Sorry ! No company found. Please add company first.";
                  ShowMessage();
              }
              ?>
              </div>

                  <input name="pageNumber" type="hidden" value="<?php print $frmdata['pageNumber']?>" >
                  <input name="orderby" type="hidden" value="<?php if($frmdata['orderby']!='')  echo $frmdata['orderby']; else echo 'role_id';?>" >
                  <input name="order" type="hidden" value="<?php print $frmdata['order']?>" >
                  <input name="actUserID" type="hidden" value="" /></form>
          </div>

      </section>

	
