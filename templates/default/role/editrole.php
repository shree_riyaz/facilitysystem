<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Online Examination - Edit Role</title>

</head>
<?php
include_once(ROOT."/incajax.php");
$xajax->printJavascript(); 
?>
<body>
<div id="outerwrapper">
	<table border="0" cellspacing="0" cellpadding="0" width="980" id="tbl_outer">
	  <tr>
		<td>
			<?php 
			include_once(CURRENTTEMP."/"."header.php"); ?>
		</td>
	  </tr>
	  <tr>
		<td>
			<div id="content">
				<?php include_once(CURRENTTEMP."/"."navigation.php"); ?>
					
	<div id="main">
		<div id="contents">
			<form action="" method="post" name="frmlist" id="frmlist">
			<fieldset class="rounded"><legend>Edit Role</legend>
			<?php 
			// Show particular Messages
			if(isset($_SESSION['error']))
			{
				echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="40%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="center" style="padding:3px 3px 3px 3px;color:red;">';
				echo $_SESSION['error'];
				echo '</td></tr></tbody></table>';
				unset($_SESSION['error']);
			}
			if(isset($_SESSION['success']))
			{
				echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="40%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="center" style="padding:3px 3px 3px 3px;color:green;">';
				echo $_SESSION['success'];
				echo '</td></tr></tbody></table>';
				unset($_SESSION['success']);
			}
			
			//print_r($frmdata);
			?>
			<table width="55%" border="0" cellspacing="1" style="margin-top:10px"  cellpadding="3" align="center" class="tableccss">
			<tr > 
				<th height="30" style=" font-family:Arial, Helvetica, sans-serif;font-size:14px" colspan="8">Edit Role</th>
			</tr> 
			<tr>
				<td   colspan="4"  style="font-size:9px"align="right"  class="fontstyle">Fields marked with ( <span style="color:red">*</span> ) are mandatory</td>
			</tr>	   
			
			<tr>
				<td align="left" class="fontstyle" colspan="2">Role Name:
					<input name="roleName" type="text" id="roleName" class="rounded" value="<?php echo $role->role_name; ?>"   size="40"   maxlength="50">
					<?php echo MANMES?>
				</td>				
			</tr> 			
			<tr>
				<td colspan="2">Module Access:-
				<div style="float:right">
					<span style=""><a style="cursor:pointer;font-weight:bold" onclick="selectAllModule()">Select All</a></span>
					&nbsp;&nbsp;
					<span style=""><a style="cursor:pointer;font-weight:bold" onclick="unselectAllModule()">Unselect All</a></span>
				</div>	
				</td>
			</tr>	  
			
			<?php
			$mdcnt=count($modules[0]); 
			
			for($counter =0; $counter < $mdcnt; $counter++)
			{
				if ($modules[0][$counter]->moduleID != '')
				{	
					($counter == ($mdcnt -1)) ? $colspan='2' : $colspan ='';	
					$checkeda = '';
					$checkede = '';
					$checkeded = '';
					$check = '';
					$display = 'display:none';
					
					$addPerm = $DB->SelectRecord('rolepermission','role_id='.$role->role_id.' and module_id='.$modules[0][$counter]->module_id.' and permission_id='.$add);
					if ($addPerm)
					{
						$checkeda = ' checked ';
					}
					
					$editPerm = $DB->SelectRecord('rolepermission','role_id='.$role->role_id.' and module_id='.$modules[0][$counter]->module_id.' and permission_id='.$edit);
					if ($editPerm)
					{
						$checkede = ' checked ';
					}
					
					$exportPerm = $DB->SelectRecord('rolepermission','role_id='.$role->role_id.' and module_id='.$modules[0][$counter]->module_id.' and permission_id='.$export);
					
					if ($exportPerm)
					{
						$checkeded = ' checked ';
					}
					
					$readPerm = $DB->SelectRecord('rolepermission','role_id='.$role->role_id.' and module_id='.$modules[0][$counter]->module_id.' and permission_id='.$read);
					
					if ($readPerm)
					{
						$check = ' checked ';
						$display = 'display:""';
					}	
					if ($modules[0][$counter]->moduleName == 'preferences')
					{
						$modules[0][$counter]->moduleName = 'Configuration';
					}
					if ($modules[0][$counter]->moduleName == 'stream_master')
					{
						$modules[0][$counter]->moduleName = 'Trade Master';
					}						
					echo ($counter % 2 == 0) ? '<tr>' : '';								
					echo '<td colspan="'.$colspan.'" width="50%"><input type="checkbox" '.$check.' name="modules['.$modules[0][$counter]->module_id.']" id="'.$modules[0][$counter]->module_id.'" value="'.$modules[0][$counter]->module_id.'" onclick="showhidePer(this,this.id)"> <label for="'.$modules[0][$counter]->module_id.'">'.ucfirst($modules[0][$counter]->module_name).'</label>';
					echo ' <div style="margin-left:25px;'.$display.'" id="div:'.$modules[0][$counter]->module_id.'">';
								if($modules[0][$counter]->module_name!='dashboard' && $modules[0][$counter]->module_name!='help')
								{
						echo '<input type="checkbox" '.$checkeda.' name="modules['.$modules[0][$counter]->module_id.'][]" value="A" id="'.$modules[0][$counter]->module_id.':A" >Add
								 <input type="checkbox" '.$checkede.' name="modules['.$modules[0][$counter]->module_id.'][]" value="E" id="'.$modules[0][$counter]->module_id.':E">Edit';
							}
				 	
					if (in_array($modules[0][$counter]->module_name, array('question_master','backup','report'))) 
					{
						echo '<input type="checkbox" '.$checkeded.' name="modules['.$modules[0][$counter]->module_id.'][]" value="ED" id="'.$modules[0][$counter]->module_id.':ED" >Export Data';
					}
					echo   	'</div>					
							</td>';				
					echo ($counter % 2 != 0) ? '</tr>' : '';
					
				}	 
			}
			?>
			
			<?php 
			?>
			<tr>
				<td colspan="6"><div align="center" style="margin-top:10px">
				<input type="submit" name="editrole"   id="editrole" value="Update Role" class="buttons rounded"  title="Click to edit">
				  &nbsp;&nbsp;
				<input type="button" name="fileback" id="fileback" class="buttons rounded"  value="Back" onClick="location.href='<?php print CreateURL('index.php','mod=role');?>'">
				<br>
				
				</div>
				</td>
			</tr>
		</table>
		<br>
		</form>	
		</div><!--Div Contents closed-->
				</div><!--Div main closed-->

			</div><!--Content div closed-->
		</td>
	  </tr>
	</table>	
</div><!--Outer wrapper closed--> 
</body>
</html>
<script>
function showhidePer(module, id)
{
	div = 'div:'+id;
	//alert(div);
	if (module.checked)
	{	
		document.getElementById(div).style.display='';
	}
	else
	{
		document.getElementById(id+':A').checked=false;
		document.getElementById(id+':E').checked=false;
		document.getElementById(div).style.display='none';
		if (document.getElementById(id+':ED'))
		{
			document.getElementById(id+':ED').checked = false;
		}
	}	
}
</script>