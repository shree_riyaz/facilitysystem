<?php 
	/*======================================
	Developer	-	Jaishree Sahal
	Module      -   Role
	SunArc Tech. Pvt. Ltd.
	======================================		
	******************************************************/

?>

<script>

function Check()
{
	if(document.getElementById('keyword').value=='')
	{
		alert('Please enter any value for search.');
		return false;
	}
	else
	{
		return true;
	}
}
function Clear()
{
	document.getElementById('keyword').value='';
	location.href="index.php?mod=role&do=list";
	return false;
}
</script>
		
<br />
<?php 
$lang = $language->english($lang);
?>
<form method="post" name="frmlist" id="frmlist" >
<?php
	if (isset($_SESSION['ActionAccess']))
	{
				echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="90%" ><tbody><tr><td colspan="6"  align="center"><div class="errormsg">';
				echo $_SESSION['ActionAccess'];
				echo '</div></td></tr></tbody></table><br>';
				unset($_SESSION['ActionAccess']);
			
	}
	if(isset($_SESSION['error']))
			{
				echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" ><tbody><tr><td colspan="6"  align="center">
				<div class="alert alert-danger alert-dismissable">
				   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
				  echo $_SESSION['error'];
				echo '</div></td></tr></tbody></table><br>';
				unset($_SESSION['error']);
			}
			if(isset($_SESSION['success']))
			{
				echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" ><tbody><tr><td colspan="6"  align="center">
				<div class="alert alert-success alert-dismissable">
				   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
				echo $_SESSION['success'];
				echo '</div></td></tr></tbody></table><br>';
				unset($_SESSION['success']);
			}
			?><center>
<div style=" border: 1px #ddd solid; width:50% !important; display: table; text-align : center;">		
    <table class="table" style="width: 100% !important;">
    <tbody>
	<tr valign="middle" align="center"> 
      <th height="30" class="thColor" colspan="2" style="padding-left: 5px;"><font color="#FFFFFF"><?php echo $lang['Search Role']?></font></th>
    </tr></table>
	 <div class="form-group" style="width:100% !important;">
            <label for="role" class="control-label col-xs-4"><?php echo $lang['Search Role']?></label>
		<!--<div class="col-xs-7">-->
		   <input type="text" class="form-control" id="keyword" name="keyword" value="<?php echo (isset($frmdata['keyword'])?$frmdata['keyword']:'');?>" onbuler="return  KeywordSearch()" >
             <span style="font-size:11px">
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*Search By Role Name 
		<!--</div>-->
     </div>
	 <br/>
	 <div class="col-xs-offset-2 col-xs-10" style="width:100% !important; margin-left: 0px; padding-bottom:10px;">
		
		<button type="submit" class="btn btn-primary" name="search"  onclick="return Check()"><?php echo $lang['Search']?></button>
		<button type="reset" class="btn btn-primary" name="clearsearch" id="clear" onclick="return Clear()"><?php echo $lang['Reset']?></button>
     </div>
	 
</div>
	<br/><br/>

	<br>	
<!--	<table width="90%" border="0" cellspacing="0" cellpadding="0" align="center"  >
  	
		<tr>
  		<td class="mainhead">&nbsp;
		<?php //include_once('nav.php');?>
		</td>
  		</tr>
</table><br/>
-->
	<?php
	// if condition modified by Neha Pareek On 16 nov 2015
		if(($_SESSION['usertype'])=='super_admin')
		{
		?>
	<table width="90%" border="0" cellspacing="0" cellpadding="0" align="center"  >
  	
		<tr>
  		<td align="right" height='12'>&nbsp;
  	
		<a title="Add New User" style=" font-family:Arial, Helvetica, sans-serif;cursor:hand;font-size:14px" href="index.php?mod=role&do=add "><?php echo $lang['Add Role']?>
        </a>
      
		</td>
  		</tr>
</table>  <?php
		}
		?>
<?php
 	
	   
		$srNo=$frmdata['from'];
		$count=count($Row);
		?>
		<table width="90%" align="center" cellpadding="0"  cellspacing="0" border="0" class="table table-striped">

		</tr>
			<tr class="trwhite"> 
			<td colspan="2" align='left'>
				<!--<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
					<tr>
						<td width="454" align="left" valign="top">-->
						<?php	
						if($totalCount > 0)
						   print "Showing Results:".($frmdata['from']+1).'-'.($frmdata['from']+$count)." of ".$totalCount; 
						else
                           print "Showing Results:0-0 of 0";
						?>
						</td>		
						<td align="right" width="125" >
					<span style="float: left; margin-top: 9px;">Show :</span><?php echo getPageRecords();?></td>
					</tr>
				<!--</table>
			 </td>
		</tr> -->
		</tr>
	<tr class="tblheading">
		<th class="anth"height="36" ><?php echo $lang['S.No.'];?></th>
		
		<th  align="center" class="anth"> 
		<a  style="color:#ffffff; cursor:pointer" onClick="OrderPage('role_name');"><?php echo $lang['Role Name'];?>
		<?php if($frmdata['orderby']=='role_name') {print '<img src="'.IMAGEURL.'/asc.gif">';} elseif($frmdata['orderby']=='role_name desc'){print '<img src="'.IMAGEURL.'/desc.gif">';}?>
		</a>
		</th>	
		<!--<th class="anth">View</th>-->
		<th class="anth"><?php echo $lang['Edit / Delete'];?></th>
	</tr>
<?php
if($Row)
{
	for($counter=0;$counter<$count;$counter++)
	{	
		if($Row[$counter]->role_name != 'Super Admin') //Added By Shivender
		{
				$srNo++;
				if(($counter%2)==0)
				{
					$trClass="tdbggrey";
				}
				else
				{
					$trClass="tdbgwhite";
				}
				
		
		
		
          $confirmDelete = 'Do you really want to delete this Role ?';		
		?>
		
	<tr>
		<td align='center'><?php echo $srNo; ?></td>
		<td align='center'><?php echo ucfirst($Row[$counter]->role_name);?>&nbsp;</td>
		<!-- <td align='center'><a class="fontstyle" 
		 href='<?php print CreateURL('index.php','mod=role&do=view&id='.$Row[$counter]->role_id);?>' title="View" >View</a></td>-->
		 <td align='center'>
			<a class="fontstyle" 
				 href='<?php print
					CreateURL('index.php','mod=role&do=edit&id='.$Row[$counter]->role_id);?>' title="Edit" >
						<img src="<?php echo IMAGEURL."/b_edit.png" ?>" border=0 />
			</a>
			<a title="Delete"  
				href='<?php print
				CreateURL('index.php','mod=role&do=del&id='.$Row[$counter]->role_id);?>' 
				onclick="return confirm('<?php echo $confirmDelete ?>')">
				<img src="<?php print IMAGEURL ?>/b_drop.png">
			</a>
		</td>
	
	
		
</tr>
	<?php
		$sno++;
		}
	}
?>
</table>
   <table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">		
		 <tr>
			  <td align="center" colspan="12">
				  <?php
					 PaginationDisplay($totalCount);
				   ?>
			  </td>
		</tr>
	 </table>
	<!--</div>-->
<?php 
}
else
{

$frmdata['message']="Sorry ! Role not found for the selected criteria";
		ShowMessage();
?>
<style> .tblheading,.trwhite{ display:none; }

</style>
<?php

}?>


      <input name="pageNumber" type="hidden" value="<?php print $frmdata['pageNumber']?>" >
	  <input name="orderby" type="hidden" value="<?php if($frmdata['orderby']!='')  echo $frmdata['orderby']; else echo 'role_id';?>" >
	  <input name="order" type="hidden" value="<?php print $frmdata['order']?>" >
	  <input name="actUserID" type="hidden" value="" /></form>	
	
</center>