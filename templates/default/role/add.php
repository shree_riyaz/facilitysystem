<?php
/*======================================
Developer	-	JAishree Sahal
Module      -   Role
SunArc Tech. Pvt. Ltd.
======================================
******************************************************/


?>
<script>
    function Clear() {
        document.getElementById('role_name').value = '';
        var checkboxes = document.getElementsByTagName('input');
        for (var i = 0; i < checkboxes.length; i++) {
            if (checkboxes[i].type === 'checkbox') {
                checkboxes[i].checked = false;

            }

        }
    }
</script>
<section>


        <div class="col-sm-12 drop-shadow nopadding">
            <form method="post" class="form-horizontal" name="frmlist" id="frmlist" enctype="multipart/form-data">

                <?php
                if(isset($_SESSION['error']))
                {
                    echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" ><tbody><tr><td colspan="6"  align="center">
                    <div class="alert alert-danger alert-dismissable">
                       <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
                    echo $_SESSION['error'];
                    echo '</div></td></tr></tbody></table>';
                    unset($_SESSION['error']);
                }
                if(isset($_SESSION['success']))
                {
                    echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" ><tbody><tr><td colspan="6"  align="center">
				<div class="alert alert-success alert-dismissable">
				   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
                    echo $_SESSION['success'];
                    echo '</div></td></tr></tbody></table>';
                    unset($_SESSION['success']);
                }
                ?>
            <div class="user-heading fixedHeader">
                 <span class="side-nav-open">
                                <i class="fa fa-bars" aria-hidden="true"></i>
                            </span>
                <span>Add Role</span>
                <?php
                include_once 'user_profile.php';
                ?>
            </div>
            <div class="userbg">

                        <div id="users" >
                            <h4 class="update-user"></h4>
                        </div>
                        <div class="plan-category user-page-form">
                                        <label for="role_name" class="control-label edit-role-label">
                                            <?php echo $lang['Role Name'] . MANDATORYMARK ?></label>

                    <input title="Enter Role Name" type="text" name="role_name"
                           id="role_name" value="<?php echo $_POST['role_name']; ?>"
                           class="form-control">
                    <br/>
                    <div class="table-responsive">
                        <table class="table dashboard-table table-hover table-bordered">
                            <tbody>
                            <tr>

                                <td>
                                    <label for="role_name" class="control-label">
                                        <?php echo $lang['Module Access'] ?>
                                    </label>
                                </td>
                                <td>
                                    <a style="cursor:pointer;font-weight:bold" onclick="selectAllModule()">Select
                                        All</a> &nbsp;&nbsp;&nbsp;
                                    <a style="cursor:pointer;font-weight:bold" onclick="unselectAllModule()">
                                        Unselect All</a>
                                </td>
                            </tr>
                            <tr>
                                <?php
                                //print_r($modules);
                                $mdcnt = count($modules[0]);
                                for ($counter = 0; $counter < $mdcnt; $counter++) {
                                    //echo $counter;
                                    if ($modules[0][$counter]->module_id != '') {
                                        echo $modules[$counter]->module_id;
                                        ($counter == ($mdcnt - 1)) ? $colspan = '2' : $colspan = '';
                                        $checkeda = '';
                                        $checkede = '';
                                        $checkedd = '';
                                        $checkedr = '';
                                        $checkeded = '';
                                        $checkedl = '';
                                        $check = '';
                                        $display = 'display:none';

                                        echo ($counter % 2 == 0) ? '<tr>' : '';
                                        echo '<td colspan="'.$colspan.'"">
						<input type="checkbox" ' . $check . ' name="modules[' . $modules[0][$counter]->module_id . ']" id="' . $modules[0][$counter]->module_id . '" value="' . $modules[0][$counter]->module_id . '" onclick="showhidePer(this,this.id)">
						<label for="' . $modules[0][$counter]->module_id . '">' . ucfirst($modules[0][$counter]->module_name) . '</label>';
                                        echo ' <div style="' . $display . '" id="div:' . $modules[0][$counter]->module_id . '" class="chk">';
                                        if ($modules[0][$counter]->module_name != 'dashboard' && $modules[0][$counter]->module_name != 'help') {
                                            echo '			
						<input type="checkbox" ' . $checkeda . ' name="modules[' . $modules[0][$counter]->module_id . '][]" value="A" id="' . $modules[0][$counter]->module_id . ':A" >Add
						<input type="checkbox" ' . $checkede . ' name="modules[' . $modules[0][$counter]->module_id . '][]" value="E" id="' . $modules[0][$counter]->module_id . ':E">Edit
						<input type="checkbox" ' . $checkedd . ' name="modules[' . $modules[0][$counter]->module_id . '][]" value="D" id="' . $modules[0][$counter]->module_id . ':D">Delete
						<input type="checkbox" ' . $checkedr . ' name="modules[' . $modules[0][$counter]->module_id . '][]" value="R" id="' . $modules[0][$counter]->module_id . ':R" >View
						<input type="checkbox" ' . $checkedl . ' name="modules[' . $modules[0][$counter]->module_id . '][]" value="L" id="' . $modules[0][$counter]->module_id . ':L" >List';
                                        }
                                        if (in_array($modules[0][$counter]->module_name, array('question_master', 'backup', 'report', 'candidate_master', 'stream_master'))) {
                                            echo '<input type="checkbox" ' . $checkeded . ' name="modules[' . $modules[0][$counter]->module_id . '][]" value="ED" id="' . $modules[0][$counter]->module_id . ':ED" >Export Data';
                                        }

                                        echo '</div>
						 </td> ';
                                        echo ($counter % 2 != 0) ? '</tr>' : '';

                                    }
                                }
                                ?>

                            </tr>

                        </table>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-12">
                            <button type="submit" class="btn btn-danger add-company pull-right role-edit-btn"
                                    name="addrole"><?php echo $lang['Add'] ?></button>
                        </div>
                    </div>
                </div>
            </div>
            </form>
        </div>



</section>

<b class="xbottom"><b class="xb4"></b><b class="xb3"></b><b class="xb2"></b><b
        class="xb1"></b></b></div>

<style>
    button.btn.btn-danger.add-company.pull-right.role-edit-btn {
        margin-bottom: 25px;
        margin-right: 71px;
    }
</style>
<script>
    function showhidePer(module, id) {
        div = 'div:' + id;

        if (module.checked) {
            //alert(div);
            //$("#div").show();
            document.getElementById(div).style.display = '';
        }
        else {
            document.getElementById(id + ':A').checked = false;
            document.getElementById(id + ':E').checked = false;
            document.getElementById(div).style.display = 'none';
            if (document.getElementById(id + ':ED')) {
                document.getElementById(id + ':ED').checked = false;
            }
        }
    }
</script>

