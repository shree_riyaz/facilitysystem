<?php
	/*======================================
	Developer	-	Neha Pareek
	Module      -   Device Locations
	SunArc Tech. Pvt. Ltd.
	======================================		
	******************************************************/
	

?>


<?php 
include_once("lib/language.php");
$language = new Language();
$lang = $language->english('eng');

?>

    <section>

                <div style="height:100vh;" class="col-sm-12 drop-shadow nopadding">
                    <form method="post" name="location_add" id="company_add" class="form-horizontal" enctype="multipart/form-data">

                        <?php
                        if(isset($_SESSION['error']))
                        {
                            echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" ><tbody><tr><td colspan="6"  align="center">
                    <div class="alert alert-danger alert-dismissable">
                       <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
                            echo $_SESSION['error'];
                            echo '</div></td></tr></tbody></table>';
                            unset($_SESSION['error']);
                        }
                        if(isset($_SESSION['success']))
                        {
                            echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" ><tbody><tr><td colspan="6"  align="center">
				<div class="alert alert-success alert-dismissable">
				   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
                            echo $_SESSION['success'];
                            echo '</div></td></tr></tbody></table>';
                            unset($_SESSION['success']);
                        }
                        ?>
                    <div class="user-heading fixedHeader">
                        <div class="row">
                            <div class="col-md-3 col-sm-4 col-xs-6">
                        <span class="side-nav-open">
                                <i class="fa fa-bars" aria-hidden="true"></i>
                            </span>
                        <span style="vertical-align: text-bottom"><?php echo $lang['Device Location'] ?></span>
                                </div>
                            <div class="col-md-5 col-sm-2 hidden-xs"></div>
                            <div style="margin-top:0px !important;" class="col-md-2 col-sm-3 hidden-xs">

                            </div>
                            <div class="col-md-2 col-sm-3 col-xs-6">
                        <?php
                        include_once 'user_profile.php';
                        ?>
                                </div>
                            </div>
                    </div>
                    <div class="userbg">

                        <div id="users">
                            <h4 class="update-user">Add New Device Location</h4>
                        </div>
                        <div class="plan-category user-page-form">

                            <div class="form-group">
                                <label for="" class="col-sm-3"><?php echo $lang['Location Name'] ?>
                                    <sup>*</sup></label>
                                <div class="col-sm-9">
                                    <input type="text" title="Enter Location Name"  class="form-control" id="location_name" name="location_name" value="<?php echo $_POST['location_name'];?>"  >
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-3"><?php echo $lang['Location Description'] ?>
                                    <sup>*</sup></label>
                                <div class="col-sm-9">
                                    <textarea name="location_description" title="Enter Device Location" class="form-control"><?php echo $_POST['location_description'];?></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-12">
                                    <a class="btn btn-danger cancel_button pull-right add-company" href="<?php echo ROOTADMINURL.'/index.php?mod='.$_GET['mod'] ?>" >Cancel</a>&nbsp;
                                    <button type="submit" id="add_location" name="add_location" class="btn btn-danger add-company pull-right" value="Add"><?php echo $lang['Add'] ?></button>
                                </div>

                            </div>
                        </div>
                    </div>
                    </form>
                </div>



    </section>