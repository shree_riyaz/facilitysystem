
<script>
function deleteConfirm()
{
	if(confirm("Are you really want to delete ? "))
		return true;
	else
		return false;
}

</script>

<?php
$obj = new passEncDec;
$pwd  = $obj->decrypt_password($Row[0][0]->password);
?>
    <section>
        <form method="post" class="form-horizontal" name="lang_edit" id="lang_edit" enctype="multipart/form-data">
            <?php
            $lang = $language->english($lang);
            if(isset($_SESSION['error']))
            {
                echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" ><tbody><tr><td colspan="6"  align="center">
            <div class="alert alert-danger alert-dismissable">
               <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
                echo $_SESSION['error'];
                echo '</div></td></tr></tbody></table><br>';
                unset($_SESSION['error']);
            }
            if(isset($_SESSION['success']))
            {
                echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" ><tbody><tr><td colspan="6"  align="center">
        <div class="alert alert-success alert-dismissable">
           <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
                echo $_SESSION['success'];
                echo '</div></td></tr></tbody></table><br>';
                unset($_SESSION['success']);
            }
            ?>
        <div class="container-fluid page-wrapper">
        <div class="row nomargin">
            <div class="col-sm-9 drop-shadow nopadding">
                <div class="user-heading text-left">
                    <span class="">Rating Option</span>
                    <?php
                    include_once 'user_profile.php';
                    ?>
                </div>
                <div class="userbg">

<!--                    </form>-->
                    <div id="users" class="">
                    <h2 class="update-user">
                        Update Rating Option
                    </h2>
                    </div>
                    <div class="plan-category user-page-form">

                        <div class="form-group">
                            <label for="title" class="col-sm-3">Rating Title
                                <sup>*</sup></label>
                            <div class="col-sm-9">
                                <input type="text" name="title" title="Enter Question Title" placeholder="Enter Question Title" value="<?php echo $Row[0][0]->title;?>" class="form-control">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="feedback_type" class="col-sm-3">Feedback Type <sup>*</sup></label>
                            <div class="col-sm-9">
                                <select name="feedback_type" class="form-control">
                                    <option value="">Select Option</option>
                                    <option value="rating" <?php if($Row[0][0]->feedback_type == 'rating'){?> selected <?php }?>>Rating</option>
                                    <option value="survey" <?php if($Row[0][0]->feedback_type == 'survey'){?> selected <?php }?>>Survey</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="field_type" class="col-sm-3">Field Type
                                <sup>*</sup></label>
                            <div class="col-sm-9">
                                <select name="field_type" class="form-control">
                                    <option value="">Select Option</option>
                                    <option value="face" <?php if($Row[0][0]->field_type == 'face'){?> selected <?php }?>>Emoji</option>
                                    <option value="star" <?php if($Row[0][0]->field_type == 'star'){?> selected <?php }?>>Star</option>
                                    <option value="checkbox" <?php if($Row[0][0]->field_type == 'checkbox'){?> selected <?php }?>>Checkbox</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="eligible_for_capture_detail" class="col-sm-3">Capture Details <sup>*</sup></label>
                            <div class="col-sm-9">
                                <select name="eligible_for_capture_detail" class="form-control">
                                    <option value="">Select Option</option>
                                    <option value="Y" <?php if($Row[0][0]->eligible_for_capture_detail == 'Y'){?> selected <?php }?>>YES</option>
                                    <option value="N" <?php if($Row[0][0]->eligible_for_capture_detail == 'N'){?> selected <?php }?>>NO</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="feedback_nature" class="col-sm-3">Feedback Nature<sup>*</sup></label>
                            <div class="col-sm-9">
                                <select name="feedback_nature" class="form-control">
                                    <option value="">Select Option</option>
                                    <option value="P" <?php if($Row[0][0]->feedback_nature == 'P'){?> selected <?php }?>>Positive</option>
                                    <option value="N" <?php if($Row[0][0]->feedback_nature == 'N'){?> selected <?php }?>>Negative</option>
                                    <option value="N" <?php if($Row[0][0]->feedback_nature == 'M'){?> selected <?php }?>>Neutral</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="option_image" class="col-sm-3">Option Image<sup>*</sup></label>
                            <div class="col-sm-9">
                                <input id="fileUpload" title="Upload Option Image" class="hidden" type="file" name="option_image" size=20>

                                <div class="profile-image">
                                    <button type="button" id="uploadButton" class="btn btn-danger add-company">
                                        <i class="fa fa-upload" aria-hidden="true"></i>Choose File</button>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="option_image" class="col-sm-3"></label>
                            <div class="col-sm-9">
                                <div class="col-sm-12">
                                    <div class="box">
                                        <?php
                                        if(trim($Row[0][0]->option_image) == '')
                                        {

                                            $image =  IMAGEURL."/user-profile.png";?>
                                            <img src="<?php echo $image ?>" class="img-responsive user-profile" alt="Option Image" >

                                        <?php }
                                        else
                                        {
                                        //$image = IMAGEURL."profile_picture/".$Row[0][0]->profile_image ;
                                        $image = IMAGEURL."option_image/".$Row[0][0]->option_image;?>
                                        <img width="50" height="100" src="<?php echo $image; ?>" title="img-responsive user-profile"/>
                                        <br>
                                        <span>upload only: .jpg, .jpeg, .gif and .png image
                                                    and size should not be more than 2MB.)</span>
                                    </div>
                                </div>

                                <?php } ?>
                                <input type="hidden" name="option_img_name" value="<?php echo $Row[0][0]->option_image;?>" />
                            </div>
                        </div>


                        <div class="form-group">
                                <div class="col-sm-12">
                                    <button type="submit" name="update_rating_option" class="btn btn-danger add-company pull-right">Update Ration Option</button>
                                </div>
                            </div>


                    </div>
                </div>
            </div>
        </div>
        </div>
        </form>

    </section>
<script>
    $(document).ready(function() {
        $('#uploadButton').click(function(){

            $('#fileUpload').click();
        });
    });

</script>