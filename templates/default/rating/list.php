<?php
/*======================================
Developer	-	Jaishree Sahal
Module      -   USer
SunArc Tech. Pvt. Ltd.
======================================
******************************************************/
?>

<script>

    function checkAll(ele) {
        var checkboxes = document.getElementsByTagName('input');

        if (ele.checked) {
            for (var i = 0; i < checkboxes.length; i++) {
                if (checkboxes[i].type == 'checkbox') {
                    checkboxes[i].checked = true;
                }
            }
        } else {
            for (var i = 0; i < checkboxes.length; i++) {
                console.log(i);
                if (checkboxes[i].type == 'checkbox') {
                    checkboxes[i].checked = false;
                }
            }
        }
    }

    function showpassdiv(id) {
        $("#password_" + id).css("display", "block");
    }
    function Check() {
        if (document.getElementById('keyword').value == '') {
            alert('Please enter any value for search.');
            return false;
        }
        else {
            return true;
        }
    }
    function Clear() {
        document.getElementById('keyword').value = '';
        location.href = "index.php?mod=user&do=list";
        return false;
    }
</script>


    <section>


        <div class="col-sm-9 drop-shadow nopadding toggle-close">

            <form method="post" name="frmlist" id="frmlist">
                <?php
                if (isset($_SESSION['ActionAccess'])) {
                    echo '<table cellspacing="0" cellpadding="0" border="0" align="center" width="90%" ><tbody><tr><td colspan="6"  align="center"><div class="errormsg">';
                    echo $_SESSION['ActionAccess'];
                    echo '</div></td></tr></tbody></table>';
                    unset($_SESSION['ActionAccess']);

                }
                if (isset($_SESSION['error'])) {
                    echo '<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" ><tbody><tr><td colspan="6"  align="center">
				<div class="alert alert-danger alert-dismissable">
				   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
                    echo $_SESSION['error'];
                    echo '</div></td></tr></tbody></table>';
                    unset($_SESSION['error']);
                }
                if (isset($_SESSION['success'])) {
                    echo '<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" ><tbody><tr><td colspan="6"  align="center">
            <div class="alert alert-success alert-dismissable">
               <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
                    echo $_SESSION['success'];
                    echo '</div></td></tr></tbody></table>';
                    unset($_SESSION['success']);
                }
                ?>
            <div class="user-heading sticky">
                <span class=""><?php echo $lang['Question'] ?> </span>
                <?php
                include_once 'user_profile.php';
                ?>
            </div>
            <div class="userbg">

                <div><i class="fa fa-bars" aria-hidden="true"></i></div>


                <div class="row">
                    <div class="col-sm-3">

                    </div>

                    <div class="col-sm-9">
                        <div class="pull-right">
                                <a title="Add New Language" href="index.php?mod=rating&do=add" class="btn btn-danger add-company margin_30">Add Rating Option</a>
                            <?php echo '<br>';  ?>

                        </div>
                    </div>

                </div>
                <?php
                $srNo = $frmdata['from'];
                $count = count($Row);
                ?>
                <div class="user-heading">
                    Feedback Setting Details
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="table-responsive">
                            <table class="table dashboard-table table-bordered">
                                <thead>
                                <tr>
                                    <th class="text-center"><a  onClick="OrderPage('title');" style="text-decoration:none; cursor:pointer" >Title
                                            <?php if($frmdata['orderby']=='title') {print '<img src="'.IMAGEURL.'/asc.gif">';} elseif($frmdata['orderby']=='title desc'){print '<img src="'.IMAGEURL.'/desc.gif">';}?>
                                        </a></th>
                                        <th>
                                            <a onClick="OrderPage('feedback_type');" style="text-decoration:none; cursor:pointer" >Feedback Type
                                                <?php if($frmdata['orderby']=='feedback_type') {print '<img src="'.IMAGEURL.'/asc.gif">';} elseif($frmdata['orderby']=='feedback_type desc'){print '<img src="'.IMAGEURL.'/desc.gif">';}?>
                                            </a>
                                        </th>

                                    <th class="text-center">
                                        <a onClick="OrderPage('field_type');" style="text-decoration:none; cursor:pointer" >Field Type
                                            <?php if($frmdata['orderby']=='field_type') {print '<img src="'.IMAGEURL.'/asc.gif">';} elseif($frmdata['orderby']=='field_type desc'){print '<img src="'.IMAGEURL.'/desc.gif">';}?></a>
                                    </th>

                                    <th class="text-center">

                                        <a onClick="OrderPage('eligible_for_capture_detail');" style="text-decoration:none; cursor:pointer" >Eligible For Capture Detail
                                            <?php if($frmdata['orderby']=='eligible_for_capture_detail') {print '<img src="'.IMAGEURL.'/asc.gif">';} elseif($frmdata['orderby']=='eligible_for_capture_detail desc'){print '<img src="'.IMAGEURL.'/desc.gif">';}?></a>
                                        </th>
                                    <th class="text-center">

                                        <a onClick="OrderPage('option_image');" style="text-decoration:none; cursor:pointer" >Option Image
                                            <?php if($frmdata['orderby']=='option_image') {print '<img src="'.IMAGEURL.'/asc.gif">';} elseif($frmdata['orderby']=='option_image desc'){print '<img src="'.IMAGEURL.'/desc.gif">';}?></a>
                                    </th>

                                    <th class="text-center">

                                        <a onClick="OrderPage('feedback_nature');" style="text-decoration:none; cursor:pointer" >Feedback Nature
                                            <?php if($frmdata['orderby']=='feedback_nature') {print '<img src="'.IMAGEURL.'/asc.gif">';} elseif($frmdata['orderby']=='feedback_nature desc'){print '<img src="'.IMAGEURL.'/desc.gif">';}?></a>
                                    </th>
                                    <th class="text-center">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                if($Row)
                                {
                                $objpassEncDec = new passEncDec;
                                for($counter=0;$counter<$count;$counter++)
                                {
                                    $srNo++;
                                    if(($counter%2)==0)
                                    {
                                        $trClass="tdbggrey";
                                    }
                                    else
                                    {
                                        $trClass="tdbgwhite";
                                    }

                                    $confirmDelete = 'All the related data of this user will be deleted . Do you really want to delete this User ?';
                                    $obj = new passEncDec;
                                    ?>
                                    <tr>

                                <td class="text-center"><?php echo isset($Row[$counter]->title) ? ucfirst($Row[$counter]->title) : 'NA'; ?></td>
                                <td class="text-center"><?php echo isset($Row[$counter]->feedback_type) ? ucfirst($Row[$counter]->feedback_type) : 'NA'; ?></td>
                                <td class="text-center"> <?php if ($Row[$counter]->field_type == 'N'){echo 'NO';} else { echo 'YES' ;}?></td>
                                <td class="text-center"> <?php if ($Row[$counter]->eligible_for_capture_detail == 'N'){echo 'NO';} else { echo 'YES' ;}?></td>
                                <td class="text-center"> <img width="20" height="20" src="<?php echo IMAGEURL.'option_image/'.$Row[$counter]->option_image ?>"> </td>
                                <td class="text-center"> <?php if ($Row[$counter]->feedback_nature == 'P'){echo 'Positive';} else { echo 'Negative' ;}?></td>

                                <td class="text-center">
                                    <a class="fontstyle" href='<?php print CreateURL('index.php','mod=rating&do=edit&id='.$Row[$counter]->id);?>' title="Edit" ><img src="<?php echo IMAGEURL."/b_edit.png" ?>" border=0 />
                                    </a>
                                </td>
                                    </tr>
                                    <?php $sno++; } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="text-center">
                            <?php
                            PaginationDisplay($totalCount);
                            ?>
                        </div>
                    </div>
                </div>
            <?php
            }
            elseif($_SESSION['keywords'] == 'Y') {
                $frmdata['message']="Sorry! No record found for the selected criteria";
                unset ($_SESSION['keywords']);
                ShowMessage(); }
            else {
                $frmdata['message']="Sorry! No feedback setting created yet.";
                ShowMessage(); }
            ?>
            </div>
                <input name="pageNumber" type="hidden" value="<?php print $frmdata['pageNumber'] ?>">
                <input name="orderby" type="hidden"
                       value="<?php if ($frmdata['orderby'] != '') echo $frmdata['orderby']; else echo 'id desc'; ?>">
                <input name="order" type="hidden" value="<?php print $frmdata['order'] ?>">
                <input name="actUserID" type="hidden" value=""/>
            </form>
        </div>

    </section>

<script>
    $parent.find('button[name=download]').click(function () {
        window.location.href = 'download.php';
    });
</script>