<?php
	/*======================================
	Developer	-	Jaishree Sahal
	Module      -   User
	SunArc Tech. Pvt. Ltd.
	======================================
	******************************************************/
	$lang = $language->english($lang);
?>
<script>
function Clear()
{	//alert(document.getElementById('role_id')[0].value);
	try{
	document.getElementById('user_phone').value='';
	//alert(document.getElementById('user_phone').value);
	document.getElementById('first_name').value='';
	document.getElementById('last_name').value='';
	document.getElementById('user_email').value='';
	//document.getElementById('confirm_password').value='';
	//document.getElementById('password').value='';
	document.getElementById('role_id').value='';
	//document.getElementById('superviser_row').style.display = 'none';
	if(document.getElementById('assigned_to'))	// to hide Assingned to selectbox on Reset form Added By : Neha Pareek
	{
		document.getElementById('assigned_to').value='';
		document.getElementById('superviser_row').style.display = 'none';
	}
	document.getElementById('profile_image').value='';
	return false;
	}
	catch(e)
	{
		alert("Error: " + e.description);
	}
}
</script>

<section>
    <form method="post" name="user_add" class="form-horizontal" id="user_add" enctype="multipart/form-data">

        <?php

        if(isset($_SESSION['error']))
        {
            echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" ><tbody><tr><td colspan="6"  align="center">
        <div class="alert alert-danger alert-dismissable">
           <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
            echo $_SESSION['error'];
            echo '</div></td></tr></tbody></table><br>';
            unset($_SESSION['error']);
        }
        if(isset($_SESSION['success']))
        {
            echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" ><tbody><tr><td colspan="6"  align="center">
				<div class="alert alert-success alert-dismissable">
				   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
            echo $_SESSION['success'];
            echo '</div></td></tr></tbody></table><br>';
            unset($_SESSION['success']);
        }
        ?>
        <div class="container-fluid page-wrapper">
            <div class="row nomargin">
                <div class="col-sm-9 drop-shadow nopadding">
                    <div class="user-heading text-left">
                        <span>Rating Option</span>
                        <?php
                        include_once 'user_profile.php';
                        ?>
                    </div>
                    <div class="userbg">

                        <div id="users" class="">
                            <h2 class="update-user">Add New Rating Option</h2>
                        </div>
                        <div class="plan-category user-page-form">
                            <div class="form-group">
                                <label for="form_heading" class="col-sm-3">Rating Title
                                    <sup>*</sup></label>
                                <div class="col-sm-9">
                                    <input type="text" name="title" title="" placeholder="Enter Question Title" value="<?php echo $_POST['title'];?>" class="form-control">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="feedback_type" class="col-sm-3">Feedback Type <sup>*</sup></label>
                                <div class="col-sm-9">
                                    <select name="feedback_type" class="form-control">
                                        <option value="">Select Option</option>
                                        <option value="rating">Rating</option>
                                        <option value="survey">Survey</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="field_type" class="col-sm-3">Field Type
                                    <sup>*</sup></label>
                                <div class="col-sm-9">
                                    <select name="field_type" class="form-control">
                                        <option value="">Select Option</option>
                                        <option value="face">Emoji</option>
                                        <option value="star">Star</option>
                                        <option value="checkbox">Checkbox</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="eligible_for_capture_detail" class="col-sm-3">Capture Details <sup>**</sup></label>
                                <div class="col-sm-9">
                                    <select name="eligible_for_capture_detail" class="form-control">
                                        <option value="">Select Option</option>
                                        <option value="Y">YES</option>
                                        <option value="N">NO</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="feedback_nature" class="col-sm-3">Feedback Nature<sup>*</sup></label>
                                <div class="col-sm-9">
                                    <select name="feedback_nature" class="form-control">
                                        <option value="">Select Option</option>
                                        <option value="P">Positive</option>
                                        <option value="N">Negative</option>
                                        <option value="M">Neutral</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="option_image" class="col-sm-3">Option Image<sup>*</sup></label>
                                <div class="col-sm-9">
                                    <input id="fileUpload" title="Upload Option Image" class="hidden" type="file" name="option_image" size=20>

                                    <div class="profile-image">
                                        <button type="button" id="uploadButton" class="btn btn-danger add-company">
                                            <i class="fa fa-upload" aria-hidden="true"></i>Choose File</button>
                                    </div>
                                </div>
                            </div>

                        <div class="form-group">
                            <div class="col-sm-12">
                                <button type="submit" name="add_rating_option" class="btn btn-danger add-company pull-right">Save Rating Option</button>
                            </div>
                        </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    </section>

<script>
    $(document).ready(function() {
        $('#uploadButton').click(function(){
            $('#fileUpload').click();
        });
    });
</script>