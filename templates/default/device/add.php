<?php
	/*======================================
	Developer	-	JAishree Sahal
	Module      -   Device
	SunArc Tech. Pvt. Ltd.
	======================================		
	******************************************************/
?>

<script>
 $(function() {
		
		$("#creation_date").datepicker();
		$("#expiary_date").datepicker();
	});

</script>
<?php 
$lang = $language->english('eng');
?>

    <section>

                <div class="col-sm-9 drop-shadow nopadding toggle-close">
                    <form method="post" name="company_add" id="company_add" class="form-horizontal" enctype="multipart/form-data">

                        <?php
                        if(isset($_SESSION['error']))
                        {
                            echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" ><tbody><tr><td colspan="6"  align="center">
                    <div class="alert alert-danger alert-dismissable">
                       <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
                            echo $_SESSION['error'];
                            echo '</div></td></tr></tbody></table>';
                            unset($_SESSION['error']);
                        }
                        if(isset($_SESSION['success']))
                        {
                            echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" ><tbody><tr><td colspan="6"  align="center">
				<div class="alert alert-success alert-dismissable">
				   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
                            echo $_SESSION['success'];
                            echo '</div></td></tr></tbody></table>';
                            unset($_SESSION['success']);
                        }
                        ?>
                    <div class="user-heading sticky">
                        <span><?php echo $lang['Add New Device'] ?></span>
                        <?php
                        include_once 'user_profile.php';
                        ?>
                    </div>
                    <div class="userbg">
                        <i class="fa fa-bars" aria-hidden="true"></i>
                        <div id="users" class="">
                            <h4 class="update-user"><?php echo $lang['Add New Device'] ?></h4>
                        </div>
                        <div class="plan-category user-page-form">
                            <?php if ($_SESSION['usertype'] == 'super_admin') { ?>
                                <div class="form-group">
                                    <label for="" class="col-sm-3"><?php echo $lang['Related To'] ?>
                                        <sup>*</sup></label>
                                    <div class="col-sm-9">
                                        <select class="form-control" name="company_id">
                                            <option value="">Please Select</option>
                                            <?php
                                            if($_SESSION['company_id'])
                                            {
                                                $selected = 'selected';
                                            }
                                            for($i=0;$i<count($company[0]);$i++)
                                            {
                                                echo '<option value='.$company[0][$i]->company_id.'>'.$company[0][$i]->company_name.'</option>';
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            <?php } else {?> <input type="hidden" class="form-control" name="company_id" value="<?php echo $admin_company->company_id;?>"><?php } ?>
                            <div class="form-group">
                                <label for="" class="col-sm-3"><?php echo $lang['Device Name'] ?>
                                    <sup>*</sup></label>
                                <div class="col-sm-9">
                                    <input type="text" title="Enter Company Name" class="form-control" id="device_name" name="device_name" value="<?php echo $_POST['device_name'];?>" onbuler="return  KeywordSearch()" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-3"><?php echo $lang['Device Location'] ?>
                                    <sup>*</sup></label>
                                <div class="col-sm-9">
                                    <select name="location_id" id="location_id" class="form-control" style="width:170px;">
                                        <option value="">Please Select</option>
                                        <?php

                                        for($i=0;$i<count($loc[0]);$i++)
                                        {
                                            echo '<option value='.$loc[0][$i]->location_id.'>'.$loc[0][$i]->location_name.'</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-3"><?php echo $lang['Device Owner'] ?>
                                    <sup>*</sup></label>
                                <div class="col-sm-4">
                                    <select class="form-control" name="user_id" id="user_id" style="width:170px;">
                                        <option value="" selected="selected">Please Select</option>
                                        <?php
                                        for($i=0;$i<count($users[0]);$i++)
                                        {
                                            echo '<option value='.$users[0][$i]->user_id.'>'.$users[0][$i]->first_name.' '.$users[0][$i]->last_name.'</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-3"><?php echo $lang['Device Description'] ?>
                                    <sup>*</sup></label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" name="device_description"><?php echo $_POST['device_description']?></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-12">
                                    <button type="submit" id="add_device" name="add_device" class="btn btn-danger add-company pull-right"><?php echo $lang['Add New Device'] ?></button>

                                </div>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>



    </section>