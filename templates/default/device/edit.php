<?php
	/*======================================
	Developer	-	JAishree Sahal
	Module      -   Device
	SunArc Tech. Pvt. Ltd.
	======================================		
	******************************************************/
//print_r($_SESSION);exit;
$lang = $language->english('eng');	
?>	

<script>
 $(function() {
		
		$("#creation_date").datepicker();
		$("#expiary_date").datepicker();
	});
</script>


    <section>

                <div style="height:100vh" class="col-sm-12 drop-shadow nopadding">
                    <form method="post" name="company_add" id="company_add" class="form-horizontal" enctype="multipart/form-data">


                        <?php
                        if(isset($_SESSION['error']))
                        {
                            echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" ><tbody><tr><td colspan="6"  align="center">
                    <div class="alert alert-danger alert-dismissable">
                       <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
                            echo $_SESSION['error'];
                            echo '</div></td></tr></tbody></table><br>';
                            unset($_SESSION['error']);
                        }
                        if(isset($_SESSION['success']))
                        {
                            echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" ><tbody><tr><td colspan="6"  align="center">
				<div class="alert alert-success alert-dismissable">
				   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
                            echo $_SESSION['success'];
                            echo '</div></td></tr></tbody></table><br>';
                            unset($_SESSION['success']);
                        }
                        ?>
                    <div class="user-heading text-left fixedHeader">
                        <div class="row">
                            <div class="col-xs-3">
                         <span class="side-nav-open">
                                <i class="fa fa-bars" aria-hidden="true"></i>
                            </span>
                        <span style="vertical-align: text-bottom"><?php echo $lang['Device'] ?></span>
                                </div>
                            <div class="col-md-5 col-sm-3 col-xs-2"></div>
                            <div style="margin-top:0px !important;" class="col-md-2 col-sm-3 col-xs-4">

                            </div>
                            <div class="col-md-2 col-xs-3">
                        <?php
                        include_once 'user_profile.php';
                        ?>
                            </div>
                            </div>
                    </div>
                    <div class="userbg">

                        <div id="users">
                            <h4 class="update-user"><?php echo $lang['Update Device'] ?></h4>
                        </div>
                        <div class="plan-category user-page-form">
                            <form class="form-horizontal">

                                <?php if ($_SESSION['usertype'] == 'super_admin') { ?>
                                    <div class="form-group">
                                        <label for="" class="col-sm-3"><?php echo $lang['Related To'] ?>
                                            <sup>*</sup></label>
                                        <div class="col-sm-9">
                                            <select class="form-control" name="company_id">
                                                <option value="">Please Select</option>
                                                <?php
                                                for($i=0;$i<count($company[0]);$i++)
                                                {
                                                    ?>
                                                    <option value= "<?php echo$company[0][$i]->company_id?>" <?php if($_SESSION['company_id'])echo "selected";?>><?php echo $company[0][$i]->company_name; ?> </option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                <?php } ?> <input type="hidden" class="form-control" name="company_id" value="<?php echo $admin_company->company_id;?>">

                                <div class="form-group">
                                    <label for="" class="col-sm-3"><?php echo $lang['Device Name'] ?>
                                        <sup>*</sup></label>
                                    <div class="col-sm-9">
                                        <input type="text" title="Enter Company Name" class="form-control" id="device_name" name="device_name" value="<?php echo $Row->device_name;?>" onbuler="return  KeywordSearch()" >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-3"><?php echo $lang['Device Location'] ?>
                                        <sup>*</sup></label>
                                    <div class="col-sm-9">
                                        <select class="form-control" name="location_id" id="location_id" style="width:170px;">
                                            <option value="" >Please Select</option>
                                            <?php
                                            for($i=0;$i<count($loc[0]);$i++)
                                            { ?>
                                                <option value="<?php echo $loc[0][$i]->location_id;?>" <?php if($Row->location_id == $loc[0][$i]->location_id) { echo "selected"; } ?>><?php echo $loc[0][$i]->location_name;?>
                                                </option>
                                            <?php  }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-3"><?php echo $lang['Device Owner'] ?>
                                        <sup>*</sup></label>
                                    <div class="col-sm-4">
                                        <select class="form-control" name="user_id" style="width:170px;">
                                            <option value="">Please Select</option>
                                            <?php
                                            for($i=0;$i<count($users[0]);$i++)
                                            { ?>

                                                <option value="<?php echo $users[0][$i]->user_id;?>" <?php if($Row->user_id == $users[0][$i]->user_id) { echo "selected"; } ?>><?php echo $users[0][$i]->first_name.' '.$users[0][$i]->last_name;?>
                                                </option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-3"><?php echo $lang['Device Status']; ?>
                                        <sup>*</sup></label>
                                    <div class="col-sm-9">
                                        <select class="form-control" name="device_status" id="device_status" style="width:170px;">
                                            <option value="" >Please Select</option>
                                            <option value="Active" <?php if($Row->device_status=='Active') { echo "selected"; }?>>Active </option> &nbsp; &nbsp;&nbsp;&nbsp;
                                            <option value="Inactive" <?php if($Row->device_status=='Inactive') { echo "selected"; }?>>In-Active</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-3"><?php echo $lang['Device Description'] ?>
                                        <sup>*</sup></label>
                                    <div class="col-sm-9">
                                        <textarea class="form-control" name="device_description"><?php if($_POST['device_description']) echo $_POST['device_description']; else echo $Row->device_description?></textarea>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <a class="btn btn-danger cancel_button pull-right add-company" href="<?php echo ROOTADMINURL.'/index.php?mod='.$_GET['mod'] ?>" >Cancel</a>&nbsp;
                                        <button type="submit" id="update" name="update" class="btn btn-danger add-company pull-right"><?php echo $lang['Update']?></button>

                                    </div>
                                </div>

                        </div>
                    </div>
                    </form>
                </div>



    </section>