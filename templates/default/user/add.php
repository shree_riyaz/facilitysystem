<?php
	/*======================================
	Developer	-	Jaishree Sahal
	Module      -   User
	SunArc Tech. Pvt. Ltd.
	======================================
	******************************************************/
	$lang = $language->english($lang);
?>
<script>
function Clear()
{	//alert(document.getElementById('role_id')[0].value);
	try{
	document.getElementById('user_phone').value='';
	//alert(document.getElementById('user_phone').value);
	document.getElementById('first_name').value='';
	document.getElementById('last_name').value='';
	document.getElementById('user_email').value='';
	//document.getElementById('confirm_password').value='';
	//document.getElementById('password').value='';
	document.getElementById('role_id').value='';
	//document.getElementById('superviser_row').style.display = 'none';
	if(document.getElementById('assigned_to'))	// to hide Assingned to selectbox on Reset form Added By : Neha Pareek
	{
		document.getElementById('assigned_to').value='';
		document.getElementById('superviser_row').style.display = 'none';
	}
	document.getElementById('profile_image').value='';
	return false;
	}
	catch(e)
	{
		alert("Error: " + e.description);
	}
}
</script>

<section>

        <div class="col-sm-9 drop-shadow nopadding">

            <form method="post" name="user_add" class="form-horizontal" id="user_add" enctype="multipart/form-data">

                <?php

                if(isset($_SESSION['error']))
                {
                    echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" ><tbody><tr><td colspan="6"  align="center">
        <div class="alert alert-danger alert-dismissable">
           <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
                    echo $_SESSION['error'];
                    echo '</div></td></tr></tbody></table>';
                    unset($_SESSION['error']);
                }
                if(isset($_SESSION['success']))
                {
                    echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" ><tbody><tr><td colspan="6"  align="center">
				<div class="alert alert-success alert-dismissable">
				   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
                    echo $_SESSION['success'];
                    echo '</div></td></tr></tbody></table>';
                    unset($_SESSION['success']);
                }
                ?>
            <div class="user-heading">
                <span> <?php echo $lang['Add User'] ?></span>
                <?php
                include_once 'user_profile.php';
                ?>
            </div>
            <div class="userbg">

                <!-- <div class="form-group icon-group">
                     <input type="text" class="form-control search margin_bot_30" placeholder="Search User">
                     <i class="fa fa-search search-icon" aria-hidden="true"></i>
                 </div>-->
                <div id="users" class="">
                    <h4 class="update-user"></h4>
                </div>
                <div class="plan-category user-page-form">
                    <?php if ($_SESSION['usertype'] == 'admin') { ?>
                        <div class="form-group">
                            <label for="FirstName" class="col-sm-3"><?php echo $lang['Related To'] ?> <sup>*</sup></label>
                            <div class="col-sm-9">
                                <select name="company_id" class="form-control">
                                    <option value=""><?php echo $lang['Please Select']?></option>
                                    <?php

                                    for($i=0;$i<count($company[0]);$i++)
                                    { ?>
                                        <option value="<?php echo $company[0][$i]->company_id ?>" <?php if($Row->company_id == $company[0][$i]->company_id){?> selected <?php }?>><?php echo $company[0][$i]->company_name ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>

                            </div>
                        </div>
                    <?php } else { ?>
                        <input class="form-control" type="hidden"
                               name="company_id" id="company_id" size=25 value="<?php echo $_SESSION['company_id'];?>">
                    <?php } ?>


                    <div class="form-group">
                        <label for="FirstName" class="col-sm-2"> <?php echo $lang['First Name'] ?>
                            <sup>*</sup></label>
                        <div class="col-sm-10">
                            <input title="Enter User First Name" class="form-control" type="text"
                                   name="first_name" id="first_name" size=25 value="<?php echo $_POST['first_name'];?>">
                        </div>
                    </div>


                    <div class="form-group">
                        <label for="LastName" class="col-sm-2"><?php echo $lang['Last Name'] ?>
                            <sup>*</sup></label>
                        <div class="col-sm-10">
                            <input title="Enter User Last Name" class="form-control" type="text"
                                   name="last_name" id="last_name" size=25 value="<?php echo $_POST['last_name'];?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="EmailId" class="col-sm-2"><?php echo $lang['Email Id'] ?>
                            <sup>*</sup></label>
                        <div class="col-sm-10">
                            <input title="Enter Email Id" class="form-control" type="text"
                                   name="user_email" id="user_email" size=25 value="<?php echo $_POST['user_email'];?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="PhoneNumber" class="col-sm-2"> <?php echo $lang['Phone Number'] ?>
                            <sup>*</sup></label>
                        <div class="col-sm-10">
                            <input title="Enter User Phone Number" class="form-control" type="text" name="user_phone" id="user_phone" size=25 value="<?php echo $_POST['user_phone'];?>">
                            <p style="margin: 12px 0px;color: #ff625e;">(Enter phone no. in (123) 456-7890 or 123-456-7890 or 10 digits format.)</p>

                        </div>
                    </div>

                    <div class="form-group">
                        <label for="PhoneNumber" class="col-sm-2"> <?php echo $lang['Role'] ?>
                            <sup>*</sup></label>
                        <div class="col-sm-10">

                            <select name="role_id" id="role_id" class="form-control" onchange="this.form.submit()">
                                <option value="">Please Select</option>
                                <?php
                                if($Row[0][0]->user_id != '')//Added By : Neha Pareek. Dated : 02 Dec 2015
                                {
                                    for($i=0;$i<count($roles[0]);$i++)
                                    {
                                        ?>
                                        <option value="<?php echo $roles[0][$i]->role_id?>" <?php if($_POST['role_id']== $roles[0][$i]->role_id) {?> selected="selected" <?php } ?>><?php echo $roles[0][$i]->role_name ?></option>
                                        <?php
                                    }
                                }
                                else//Added By : Neha Pareek. Dated : 02 Dec 2015
                                {
                                    for($i=0;$i<count($roles[0]);$i++)
                                    {
                                        if($roles[0][$i]->role_id == 4)
                                        {
                                            ?>
                                            <option value="<?php echo $roles[0][$i]->role_id?>" <?php if($_POST['role_id']== $roles[0][$i]->role_id) {echo "selected"; } ?>><?php echo $roles[0][$i]->role_name; ?></option>
                                            <?php
                                        }
                                    }
                                }
                                ?>
                            </select>

                            <?php if($Row[0][0]->user_id == '') echo "<span class='col-xs-10' style='margin-left :5px; color : #00688B; font-weight:bold;'>(If no user or no active supervisor present then add supervisor first.)<span>";?>

                        </div>
                    </div>

                    <?php if($_POST['role_id']=='3') { ?>
                        <div class="form-group">
                            <label for="PhoneNumber" class="col-sm-3"><?php echo $lang['Assigned To'] ?>
                                <sup>*</sup></label>
                            <div class="col-sm-9">
                                <select name="assigned_to" id="assigned_to" class="form-control">
                                    <option value="">Please Select</option>
                                    <?php
                                    //echo "<pre>";print_r($supervisor[0]);
                                    for($i=0;$i<count($supervisor[0]);$i++)
                                    {
                                        // if($_POST['assigned_to'] !='')
                                        // {

                                        ?>
                                        <option value="<?php echo $supervisor[0][$i]->user_id?>" <?php if($supervisor[0][$i]->user_id == $_POST['assigned_to']){ echo "selected";} ?>><?php echo $supervisor[0][$i]->first_name.' '.$supervisor[0][$i]->last_name; ?></option>
                                        <?php
                                        //}
                                        // else
                                        // {
                                        ?>

                                        <?php
                                        //}
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                    <?php } ?>

                    <div class="form-group">
                        <label for="UpdateProfilePicture" class="col-sm-2 "><?php echo $lang['Update Profile Picture'] ?>
                        </label>
                        <div class="col-sm-10">
                            <input id="fileUpload" title="Upload Profile Picture" class="hidden" type="file" name="profile_image" size=20>

                            <div class="profile-image">
                                <button type="button" id="uploadButton" class="btn btn-danger add-company">
                                    <i class="fa fa-upload" aria-hidden="true"></i>Choose File</button>
                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="box">
                                        <?php

                                        $image =  IMAGEURL."user-profile.png";?>

                                        <img src="<?php echo $image; ?>" title="" class="img-responsive user-profile"/>
                                        <span> <?php echo $lang['(upload only: .jpg, .jpeg, .gif and .png image and size should not be more than 2MB.)'] ?> </span>
                                    </div>
                                </div>





                            </div>

                        </div>
                    </div>


                    <div class="form-group">
                        <div class="col-sm-12">
                            <button type="submit" name="add_user" class="btn btn-danger add-company pull-right"><?php echo $lang['Add User'] ?></button>
                        </div>
                    </div>
                    <input type="hidden" name="plan_name" value="<?php echo $planName;?>" />

                </div>
            </div>
            </form>
        </div>


    </section>

<script>
    $(document).ready(function() {
        $('#uploadButton').click(function(){

            $('#fileUpload').click();
        });
    });

</script>