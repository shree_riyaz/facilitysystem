<script>
function deleteConfirm()
{
	if(confirm("Are you really want to delete ? "))
		return true;
	else
		return false;
}
function Clear()
{
	document.getElementById('company_id').value='';
	document.getElementById('role_id').value='';
	document.getElementById('first_name').value='';
	document.getElementById('last_name').value='';
	document.getElementById('user_email').value='';
	document.getElementById('confirm_password').value='';
	document.getElementById('password').value='';
	document.getElementById('user_phone').value='';
	document.getElementById('is_activeN').value='';
	document.getElementById('is_activeY').value='';

	return false;
}
</script>

<?php
$obj = new passEncDec;
$pwd  = $obj->decrypt_password($Row[0][0]->password);
?>
    <section>

            <div class="col-sm-12 drop-shadow nopadding">
                <div class="user-heading text-left fixedHeader">
                     <span class="side-nav-open">
                                <i class="fa fa-bars" aria-hidden="true"></i>
                            </span>
                    <span style="vertical-align: text-bottom"> <?php echo $lang['Profile'] ?> </span>
                    <?php
                    include_once 'user_profile.php';
                    ?>
                </div>
                <div class="userbg">

                    <form method="post" class="form-horizontal" name="user_edit" id="user_edit" enctype="multipart/form-data">
                        <?php
                        $lang = $language->english($lang);
                        if(isset($_SESSION['error']))
                        {
                            echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" ><tbody><tr><td colspan="6"  align="center">
            <div class="alert alert-danger alert-dismissable">
               <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
                            echo $_SESSION['error'];
                            echo '</div></td></tr></tbody></table><br>';
                            unset($_SESSION['error']);
                        }
                        if(isset($_SESSION['success']))
                        {
                            echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" ><tbody><tr><td colspan="6"  align="center">
        <div class="alert alert-success alert-dismissable">
           <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
                            echo $_SESSION['success'];
                            echo '</div></td></tr></tbody></table><br>';
                            unset($_SESSION['success']);
                        }
                        ?>

                    <div id="users">
                        <h4 class="update-user"><?php echo $lang['Profile'] ?></h4>
                    </div>
                    <div class="plan-category user-page-form">
                        <?php if ($_SESSION['usertype'] == 'super_admin') { ?>
                            <div class="form-group">
                                <label for="RelatedTo" class="col-md-3 col-sm-4 col-xs-6"><?php echo $lang['Related To'] ?>
                                    <sup>*</sup></label>
                                <div class="col-md-9 col-sm-8 col-xs-6">
                                    <select name="company_id" class="form-control" id="company_id" disabled="disabled" style="width : 197px;">
                                        <option value="">Please Select</option>
                                        <?php
                                        for($i=0;$i<count($company[0]);$i++)
                                        { ?>
                                            <option value="<?php echo $company[0][$i]->company_id ?>" <?php if($Row[0][0]->company_id == $company[0][$i]->company_id){?> selected <?php }?>><?php echo $company[0][$i]->company_name ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                    <input type="hidden" class="form-control" name="company_id" value="<?php echo $Row[0][0]->company_id;?>">

                                </div>
                            </div>
                        <?php } else { ?>
                            <input type="hidden" class="form-control" name="company_id" value="<?php echo $Row[0][0]->company_id;?>">
                        <?php } ?>


                        <?php if ($_SESSION['usertype'] == 'super_admin') { ?>

                            <div class="form-group">
                                <label for="Role" class="col-md-3 col-sm-4 col-xs-6"><?php echo $lang['Role'] ?>
                                    <sup>*</sup></label>
                                <div class="col-md-9 col-sm-8 col-xs-6">
                                    <select class="form-control" name="role_id" disabled = "disabled" style="width : 197px;">
                                        <option value="" >Please Select</option>
                                        <?php
                                        for($i=0;$i<count($role[0]);$i++) { ?>
                                            <option value="<?php echo $role[0][$i]->role_id; ?>" <?php if($Row[0][0]->role_id == $role[0][$i]->role_id){ echo "selected"; }?>><?php echo $role[0][$i]->role_name;?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        <?php } ?>


                        <?php if($_SESSION['usertype']=='super_admin' || ($Row[0][0]->role_id !=2 && $_SESSION['usertype'] == 'company_admin')) { ?>

                            <div class="form-group">
                                <!--                                <label for="Role" class="col-md-3 col-sm-4 col-xs-6">--><?php ////echo $lang['Active'] ?>
                                <!--                                    <sup>*</sup></label>-->
                                <div class="col-md-9 col-sm-8 col-xs-6">
                                    <input type="hidden" name="is_active" value="<?php echo $Row[0][0]->is_active;?>" />
                                </div>
                            </div>
                        <?php } else { ?> <input type="hidden" name="is_active" value="<?php echo $Row[0][0]->is_active;?>" /> <?php } ?>


                        <input type="hidden" class="form-control" name="company_id" value="<?php echo $Row[0][0]->company_id;?>">

                        <?php if (($Row[0][0]->role_id !=2 && $_SESSION['usertype'] == 'company_admin')) { ?>

                            <div class="form-group">
                                <label for="Role" class="col-md-3 col-sm-4 col-xs-6"><?php echo $lang['Role'] ?>
                                    <sup>*</sup></label>
                                <div class="col-md-9 col-sm-8 col-xs-6">
                                    <select class="form-control" name="role_id" disabled="disabled"  style="width:197px;">
                                        <option value="" >Please Select</option>
                                        <?php

                                        for($i=0;$i<count($role[0]);$i++)
                                        {
                                            if($role[0][$i]->role_id != 1)//Added by : Neha Pareek, Dated : 2 Dec 2015
                                            {
                                                ?>
                                                <option value="<?php echo $role[0][$i]->role_id; ?>" <?php if($Row[0][0]->role_id == $role[0][$i]->role_id){?> selected = "selected" <?php }?>><?php echo $role[0][$i]->role_name;?></option>

                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                    <input type="hidden" class="form-control" name="role_id" value="<?php echo $Row[0][0]->role_id;?>">

                                </div>
                            </div>
                        <?php } else { ?>
                            <input type="hidden" class="form-control" name="role_id" value="<?php echo $Row[0][0]->role_id;?>">
                        <?php } ?>


                        <div class="form-group">
                            <label for="FirstName" class="col-md-3 col-sm-4 col-xs-6"><?php echo $lang['First Name'] ?>
                                <sup>*</sup></label>
                            <div class="col-md-9 col-sm-8 col-xs-6">
                                <input title="Enter User First Name" name="first_name" id="first_name" size=25 value="<?php if($_POST['first_name']) echo $_POST['first_name']; else echo $Row[0][0]->first_name; ?>" type="text" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="LastName" class="col-md-3 col-sm-4 col-xs-6"><?php echo $lang['Last Name'] ?>
                                <sup>*</sup></label>
                            <div class="col-md-9 col-sm-8 col-xs-6">
                                <input title="Enter User Last Name" name="last_name" id="last_name" size=25 value="<?php if($_POST['last_name']) echo $_POST['last_name']; else echo $Row[0][0]->last_name; ?>" type="text" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="EmailId" class="col-md-3 col-sm-4 col-xs-6"><?php echo $lang['Email Id'] ?>
                                <sup>*</sup></label>
                            <div class="col-md-9 col-sm-8 col-xs-6">
                                <input readonly title="Enter Email" name="user_email" id="user_email" size=25 value="<?php if($_POST['user_email']) echo $_POST['user_email']; else echo $Row[0][0]->user_email; ?>" type="email" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="Password" class="col-md-3 col-sm-4 col-xs-6"><?php echo $lang['Password'] ?>
                                <sup>*</sup></label>
                            <div class="col-md-9 col-sm-8 col-xs-6">
                                <input title="Enter Password" name="password" id="password" size=25 value="" type="password" class="form-control">
                                <?php  echo "<p style='margin:12px 0px; font-weight:bold; color: #ff625e;'>
".$lang['*Enter password if you want to change password.']."</p>"; ?>
                                <input type="hidden" class="form-control" name="pwd" value="<?php if($_POST['password'] == '') echo $pwd;?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="ConfirmPassword" class="col-md-3 col-sm-4 col-xs-6"> <?php echo $lang['Confirm Password'] ?></label>
                            <div class="col-md-9 col-sm-8 col-xs-6">
                                <input title="Enter Confirm Password" name="confirm_password" id="confirm_password" size=25 value="" type="password" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="PhoneNumber" class="col-md-3 col-sm-4 col-xs-6"><?php echo $lang['Phone Number'] ?>
                                <sup>*</sup></label>
                            <div class="col-md-9 col-sm-8 col-xs-6">
                                <input title="Enter User Phone Number" name="user_phone" id="user_phone" size=25 value="<?php if($_POST['user_phone']) echo $_POST['user_phone']; else echo $Row[0][0]->user_phone;?>" type="text" class="form-control">
                                <p style="margin:12px 0px;font-weight:bold;color: #ff625e;">*<?php echo $lang['Enter phone no. in (123) 456-7890 or 123-456-7890 or 10 digits format.'] ?></p>

                            </div>
                        </div>
                        <div class="form-group">
                            <label for="UpdateProfilePicture" class="col-md-3 col-sm-4 col-xs-6 "><?php echo $lang['Update Company Logo'] ?>
                            </label>
                            <div class="col-md-9 col-sm-8 col-xs-6">
                                <input id="fileUpload" class="hidden" type="file" name="company_logo">
                                <div><button type="button" id="uploadButton" class="btn btn-danger add-company">
                                        <i class="fa fa-upload" aria-hidden="true"></i>Choose File</button></div>
                                <div class="row">
                                    <div class="col-sm-2">
                                        <?php

                                        if(trim($company_logo_edit) == '')
                                        {
                                            $image =  IMAGEURL."company_logo/no-picture.png";
                                        }
                                        else{
                                            $image = IMAGEURL."company_logo/".$company_logo_edit;
                                        }
                                        ?>
                                        <img src="<?php echo $image; ?>" class="img-responsive user-profile" alt="">
                                    <input type="hidden" name="company_logo_hidden" value="<?php echo $company_logo_edit; ?>">
                                    </div>
                                    <div class="col-sm-10">
                                        <p>upload only: .jpg, .jpeg, .gif and .png image
                                            and size should not be more than 2MB.)</p>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <?php if($_SESSION['usertype'] == 'company_admin' && $r_id->role_id == '3' || $_SESSION['usertype'] == 'supervisor' && $r_id->role_id == '3') { ?>
                            <div class="form-group">
                                <label for="Role" class="col-md-3 col-sm-4 col-xs-6">Assigned To
                                    <sup>*</sup></label>
                                <div class="col-md-9 col-sm-8 col-xs-6">
                                    <select class="form-control" name="assigned_to">
                                        <option value="" >Please Select</option>
                                        <?php
                                        for($i=0;$i<count($supervisor[0]);$i++)
                                        { ?>
                                            <option value="<?php echo $supervisor[0][$i]->user_id; ?>" <?php if($Row[0][0]->assigned_to == $supervisor[0][$i]->user_id && $_POST['assigned_to'] == ''){ echo "selected"; } elseif ($_POST['assigned_to'] != ''){echo "selected";} ?>><?php echo $supervisor[0][$i]->first_name.' '.$supervisor[0][$i]->last_name; ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        <?php } else { ?>
                            <input type="hidden" class="form-control" name="assigned_to" value="<?php echo $Row[0][0]->assigned_to;?>">
                        <?php } ?>

                        <div class="form-group">
                            <div class="col-sm-12">
                                <button type="submit" name="update" class="btn btn-danger add-company pull-right"><?php echo $lang['Update Profile'] ?></button>
                            </div>
                        </div>


                    </div>
                </div>
                </form>
            </div>



    </section>


<script>
    $(document).ready(function() {
        $('#uploadButton').click(function(){

            $('#fileUpload').click();
        });
    });

</script>