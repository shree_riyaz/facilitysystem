<?php
/*======================================
Developer	-	Jaishree Sahal
Module      -   USer
SunArc Tech. Pvt. Ltd.
======================================
******************************************************/
?>

<script>

    function checkAll(ele) {
        var checkboxes = document.getElementsByTagName('input');

        if (ele.checked) {
            for (var i = 0; i < checkboxes.length; i++) {
                if (checkboxes[i].type == 'checkbox') {
                    checkboxes[i].checked = true;
                }
            }
        } else {
            for (var i = 0; i < checkboxes.length; i++) {
                console.log(i);
                if (checkboxes[i].type == 'checkbox') {
                    checkboxes[i].checked = false;
                }
            }
        }
    }

    function showpassdiv(id) {
        $("#password_" + id).css("display", "block");
    }
    function Check() {
        if (document.getElementById('keyword').value == '') {
            alert('Please enter any value for search.');
            return false;
        }
        else {
            return true;
        }
    }
    function Clear() {
        document.getElementById('keyword').value = '';
        location.href = "index.php?mod=user&do=list";
        return false;
    }
</script>

<?php
$lang = $language->english($lang);
?>




    <section>


        <div style="height: 100%" class="col-sm-12 drop-shadow nopadding">
            <form method="post" name="frmlist" id="frmlist">
                <?php
                if (isset($_SESSION['ActionAccess'])) {
                    echo '<table cellspacing="0" cellpadding="0" border="0" align="center" width="90%" ><tbody><tr><td colspan="6"  align="center"><div class="errormsg">';
                    echo $_SESSION['ActionAccess'];
                    echo '</div></td></tr></tbody></table>';
                    unset($_SESSION['ActionAccess']);

                }
                if (isset($_SESSION['error'])) {
                    echo '<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" ><tbody><tr><td colspan="6"  align="center">
				<div class="alert alert-danger alert-dismissable">
				   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
                    echo $_SESSION['error'];
                    echo '</div></td></tr></tbody></table>';
                    unset($_SESSION['error']);
                }
                if (isset($_SESSION['success'])) {
                    echo '<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" ><tbody><tr><td colspan="6"  align="center">
            <div class="alert alert-success alert-dismissable">
               <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
                    echo $_SESSION['success'];
                    echo '</div></td></tr></tbody></table>';
                    unset($_SESSION['success']);
                }
                ?>
            <div class="user-heading fixedHeader">
                <div class="row">
                    <div class="col-xs-3">
                         <span class="side-nav-open">
                                <i class="fa fa-bars" aria-hidden="true"></i>
                            </span>
                        <span class="">
                            <?php echo  $_SESSION['usertype'] =='super_admin' ? $lang['Companies'] : $lang['Users'] ?>
                        </span>
                    </div>

                <div class="col-md-4 col-sm-3 col-xs-2"></div>
                <div style="margin-top:0px !important;" class="col-sm-3 col-xs-4 select-caret">
                    <select name="select_locale" onchange="this.form.submit()" class="form-control show-result select_locale">
                        <option style="color: white;" value="">Select Language</option>
                        <?php foreach ($get_language_list[0] as $get_language_list_list_value) { ?>
                            <option <?php if ($get_language_list_list_value->short_code == $_SESSION['selected_language']){?> selected="selected" <?php } ?> style="color: white;" value="<?php echo $get_language_list_list_value->short_code ?>"> <?php echo trim($get_language_list_list_value->language_name); ?> </option>
                        <?php } ?>
                    </select>
                </div>
                <div class="col-md-2 col-xs-3">
                <?php
                include_once 'user_profile.php';
                ?>
                </div>
                </div>
            </div>
            <div class="userbg">

                <!--<div id="google_translate_element"></div>
                <script type="text/javascript">
                    function googleTranslateElementInit() {
                        new google.translate.TranslateElement({pageLanguage: 'en'}, 'google_translate_element');
                    }
                </script>-->
<!--                <script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>-->

                <div class="row">
                    <div class="col-sm-12 ">
                        <div class="icon-group">
                            <input type="text" id="keyword" value="<?php echo(isset($frmdata['keyword']) ? $frmdata['keyword'] : ''); ?>"
                                   onblur="return  KeywordSearch()" name="keyword" class="form-control search"
                                   placeholder="<?php echo $lang['Search User'] ?>">
                            <i class="fa fa-search search-icon" aria-hidden="true"></i>
                            <p style="text-align: center; margin: 12px 0px;">
                                        <span style="font-size:12px;font-weight: bold;">
                                   <?php if(($_SESSION['usertype'])=='super_admin')
                                   { ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*<?php echo $lang['Search By Name ,Role,Phone No,Email Id,Company Name'] ?> <?php }
                                   else
                                   { ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*<?php echo $lang['Search By Name , Role, Phone No, Email ID'] ?><?php } ?>
                                   </span>
                            </p>
                        </div>
                    </div>
                    <!--<div class="col-xs-offset-2 col-xs-10" style="width:100% !important; margin-left: 0px; padding-bottom:10px;">

                                <button type="submit" class="btn btn-primary" name="search"  onclick="return Check()"><?php /*echo $lang['Search']*/?></button>
                                <button type="reset" class="btn btn-primary" name="clearsearch" id="clear" onclick="return Clear()"><?php /*echo $lang['Reset']*/?></button>
                            </div>-->
                </div>

                <div class="row">
                    <div class="col-sm-3">

                    </div>
                    <?php if(($_SESSION['usertype'])=='super_admin'){ ?>
                    <br><br>
                    <?php } ?>

                    <div class="col-sm-9">
                        <div class="pull-right">
                            <?php if (($_SESSION['usertype']) == 'company_admin' ) { ?>
                                <a title="Add New User" href="index.php?mod=user&do=add" class="btn btn-danger add-company margin_30"><?php echo $lang['Add New User']?></a>
                            <?php } echo '<br>';  ?>

                        </div>
                    </div>
                </div>
                <?php
                $srNo = $frmdata['from'];
                $count = count($Row);
                ?>
                <div class="user-heading">
                    <div class="row">
                        <div class="col-sm-3 select-caret">
                            <?php if (($_SESSION['usertype']) == 'super_admin' ||($_SESSION['usertype'])=='company_admin' ) { ?>
                                <select name="actions" id="action_list_id" class="form-control show-result">
                                    <option value=""><?php echo $lang['Select Action']?></option>
                                    <option value="Delete"><?php echo $lang['Delete']?></option>
                                    <option value="Active"><?php echo $lang['Active']?></option>
                                    <option value="Inactive"><?php echo $lang['In-Active']?></option>
                                </select>
                            <?php } ?>
                        </div>
                        <div class="col-xs-6">
                            <p class="showing-results margin_4">
                                <?php
                                if($totalCount > 0)
                                    print print $lang['Showing Results'].' '.($frmdata['from']+1).'-'.($frmdata['from']+$count)." of ".$totalCount;
                                else
                                    print print $lang['Showing Results'];
                                ?>
                            </p>
                        </div>
                        <div class="col-sm-3 col-xs-6 select-caret">
                            <?php echo getPageRecords(); ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="table-responsive">
                            <table class="table dashboard-table table-bordered">
                                <thead>
                                <tr>
                                    <th>
                                        <?php
                                        if(($_SESSION['usertype'])=='super_admin' ||($_SESSION['usertype'])=='company_admin' ) { ?>
                                            <input type="checkbox" name="chkAll[]" id="chkAll"  onchange="checkAll(this)"/>&nbsp;
                                        <?php } echo $lang['S.No.']; ?>
                                    </th>
                                    <th><a  style="text-decoration:none; cursor:pointer" onClick="OrderPage('first_name');"><?php echo $lang['Name']?>
                                            <?php if($frmdata['orderby']=='first_name') {print '<img src="'.IMAGEURL.'/asc.gif">';} elseif($frmdata['orderby']=='first_name desc'){print '<img src="'.IMAGEURL.'/desc.gif">';}?>
                                        </a></th>
                                    <?php  if($_SESSION['usertype']=='super_admin') { ?>
                                        <th>
                                            <a style="text-decoration:none; cursor:pointer" onClick="OrderPage('company_name');"><?php echo $lang['Company Name']?>
                                                <?php if($frmdata['orderby']=='company_name') {print '<img src="'.IMAGEURL.'/asc.gif">';} elseif($frmdata['orderby']=='company_name desc'){print '<img src="'.IMAGEURL.'/desc.gif">';}?>
                                            </a>
                                        </th>
                                    <?php } ?>
                                    <th>
                                        <a style="text-decoration:none; cursor:pointer" onClick="OrderPage('role_name');"><?php echo $lang['Role']?>
                                            <?php if($frmdata['orderby']=='role_name') {print '<img src="'.IMAGEURL.'/asc.gif">';} elseif($frmdata['orderby']=='role_name desc'){print '<img src="'.IMAGEURL.'/desc.gif">';}?>
                                        </a>
                                    </th>
                                    <th>
                                        <a style="text-decoration:none; cursor:pointer" onClick="OrderPage('is_active');"><?php echo $lang['Status']?>
                                            <?php if($frmdata['orderby']=='is_active') {print '<img src="'.IMAGEURL.'/asc.gif">';} elseif($frmdata['orderby']=='is_active desc'){print '<img src="'.IMAGEURL.'/desc.gif">';}?>
                                    </th>
                                    <th>
                                        <a style="text-decoration:none; cursor:pointer" onClick="OrderPage('user_phone');"><?php echo $lang['Phone Number']?>
                                            <?php if($frmdata['orderby']=='user_phone') {print '<img src="'.IMAGEURL.'/asc.gif">';} elseif($frmdata['orderby']=='user_phone desc'){print '<img src="'.IMAGEURL.'/desc.gif">';}?>
                                    </th>
                                    <th>
                                        <a style="text-decoration:none; cursor:pointer" onClick="OrderPage('user_email');"><?php echo $lang['Email Id']?>
                                            <?php if($frmdata['orderby']=='user_email') {print '<img src="'.IMAGEURL.'/asc.gif">';} elseif($frmdata['orderby']=='company_email desc'){print '<img src="'.IMAGEURL.'/desc.gif">';}?>
                                        </a>
                                    </th>

                                    <th><?php echo $lang['Action']?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                if($Row)
                                {
                                $objpassEncDec = new passEncDec;
                                for($counter=0;$counter<$count;$counter++)
                                {
                                    $srNo++;
                                    if(($counter%2)==0)
                                    {
                                        $trClass="tdbggrey";
                                    }
                                    else
                                    {
                                        $trClass="tdbgwhite";
                                    }

                                    $confirmDelete = 'All the related data of this user will be deleted . Do you really want to delete this User ?';
                                    $obj = new passEncDec;
                                    ?>
                                    <tr>
                                        <td>
                                            <?php
                                            if(($_SESSION['usertype'])=='super_admin' ||($_SESSION['usertype'])=='company_admin' ) { ?>
                                                <input type="checkbox" name="chkbox[]" id="chkbox" value="<?php echo $Row[$counter]->user_id;?>"/>&nbsp;&nbsp;
                                            <?php } echo $srNo; ?>
                                        </td>
                                        <td><?php echo ucwords($Row[$counter]->first_name.' '.$Row[$counter]->last_name); ?></td>
                                        <?php if($_SESSION['usertype']=='super_admin') {?>
                                            <td><?php echo ucwords($Row[$counter]->company_name); ?>&nbsp;</td>
                                        <?php } ?>
                                        <td><?php echo $Row[$counter]->role_name; ?>&nbsp;</td>
                                        <td><span class="status-bg-<?php if($Row[$counter]->is_active == 'N')echo 'inactive'; else echo 'active'; ?>">
            <?php if($Row[$counter]->is_active == 'N'){echo ucfirst('in-Active');} else {echo ucfirst('active');}?>
        </span></td>

                                        <td><?php echo isset($Row[$counter]->user_phone) ? $Row[$counter]->user_phone : 'NA'; ?>&nbsp;</td>

                                        <td><?php echo $Row[$counter]->user_email; ?></td>



                                        <td>
                                            <?php
                                            if(($_SESSION['usertype'])=='super_admin' ||($_SESSION['usertype'])=='company_admin')
                                            {
                                                ?>
                                                <a href='<?php print CreateURL('index.php','mod=user&do=edit&id='.$Row[$counter]->user_id);?>' title="Edit">

                                                    <i class="fa fa-pencil" aria-hidden="true"></i></a>

                                                <a href="#" data-toggle="modal" data-message="<?php echo $confirmDelete;  ?>" delete-link="<?php print CreateURL('index.php','mod=user&do=del&id='.$Row[$counter]->user_id) ?>" data-target="#myModal" class="delete-modal" ><i class="fa fa-trash" aria-hidden="true"></i>
                                                </a>
                                            <?php } else { ?>

                                            <a href='<?php print CreateURL('index.php','mod=user&do=edit&id='.$Row[$counter]->user_id);?>' title="Edit">
                                                <i class="fa fa-pencil" aria-hidden="true"></i>
                                                <?php } ?>
                                        </td>
                                    </tr>
                                    <?php $sno++; } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="text-center">
                            <?php
                            PaginationDisplay($totalCount);
                            ?>
                        </div>
                    </div>
                </div>
            <?php
            }
            elseif($_SESSION['keywords'] == 'Y') {
                $frmdata['message']="Sorry! No record found for the selected criteria";
                unset ($_SESSION['keywords']);
                ShowMessage(); }
            else {
                $frmdata['message']="Sorry! No user found.";
                ShowMessage(); }
            ?>
            </div>
                <input name="pageNumber" type="hidden" value="<?php print $frmdata['pageNumber'] ?>">
                <input name="orderby" type="hidden"
                       value="<?php if ($frmdata['orderby'] != '') echo $frmdata['orderby']; else echo 'u.user_id desc'; ?>">
                <input name="order" type="hidden" value="<?php print $frmdata['order'] ?>">
                <input name="actUserID" type="hidden" value=""/>
            </form>
        </div>

    </section>

