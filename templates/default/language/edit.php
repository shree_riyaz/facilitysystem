
<script>
function deleteConfirm()
{
	if(confirm("Are you really want to delete ? "))
		return true;
	else
		return false;
}

</script>

<?php
$obj = new passEncDec;
$pwd  = $obj->decrypt_password($Row[0][0]->password);
?>
    <section>
        <form method="post" class="form-horizontal" name="lang_edit" id="lang_edit" enctype="multipart/form-data">
            <?php
            $lang = $language->english($lang);
            if(isset($_SESSION['error']))
            {
                echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" ><tbody><tr><td colspan="6"  align="center">
            <div class="alert alert-danger alert-dismissable">
               <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
                echo $_SESSION['error'];
                echo '</div></td></tr></tbody></table><br>';
                unset($_SESSION['error']);
            }
            if(isset($_SESSION['success']))
            {
                echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" ><tbody><tr><td colspan="6"  align="center">
        <div class="alert alert-success alert-dismissable">
           <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
                echo $_SESSION['success'];
                echo '</div></td></tr></tbody></table><br>';
                unset($_SESSION['success']);
            }
            ?>
        <div class="container-fluid page-wrapper">
        <div class="row nomargin">
            <div style="height: 100vh;" class="col-sm-12 drop-shadow nopadding">
                <div class="user-heading text-left fixedHeader">
                    <span class="side-nav-open">
                                <i class="fa fa-bars" aria-hidden="true"></i>
                            </span>
                    <span class=""><?php echo  $lang['Languages'] ?> </span>
                    <?php
                    include_once 'user_profile.php';
                    ?>
                </div>
                <div class="userbg">
<!--                    </form>-->
                    <div id="users">
                    <h4 class="update-user">
                        <?php echo  $lang['Update Language'] ?>
                    </h4>
                        <div class="backbtn"><button type="button" class="btn btn-danger">Back</button></div>
                    </div>
                    <div class="plan-category user-page-form">


                            <div class="form-group">
                                <label for="RelatedTo" class="col-sm-3"><?php echo  $lang['Language Name'] ?>
                                    <sup>*</sup></label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" name="language_name" value="<?php echo $Row[0][0]->language_name;?>">

                                </div>
                            </div>
                            <div class="form-group">
                                <label for="RelatedTo" class="col-sm-3"><?php echo  $lang['Short Code'] ?>
                                    <sup>*</sup></label>
                                <div class="col-sm-9">
                                    <input type="text" disabled="disabled" class="form-control" name="short_code" value="<?php echo $Row[0][0]->short_code;?>">

                                </div>
                            </div>
            <input type="hidden" name="short_code_available" value="<?php echo $Row[0][0]->short_code;?>">
            <input type="hidden" name="file_name_available" value="<?php echo isset($Row[0][0]->file_name) ? $Row[0][0]->file_name : NULL;?>">
                        <?php
                        $format = ".json";

                        $is_exist  = file_exists(LANG."/".$Row[0][0]->short_code.''.$format);
//                        echo $is_exist;
                        ?>
            <input type="hidden" name="is_file_available" value="<?php echo $is_exist ;?>">

                            <div class="form-group">
                            <label for="UpdateProfilePicture" class="col-sm-3 "><?php echo  $lang['Upload File'] ?> </label>
                                <div class="col-sm-9">
                                <input id="fileUpload" class="hidden" type="file" name="file_name" >

                                <div><button type="button" id="uploadButton" class="btn btn-danger add-company">
                                        <i class="fa fa-upload" aria-hidden="true"></i>Choose File</button></div>

                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="box">
                                            <?php
                                            if(trim($Row[0][0]->file_name) == '')
                                            {

                                                $image =  IMAGEURL."/uploads/user-profile.png";?>
                                                <img src="<?php echo $image ?>" class="img-responsive user-profile" alt="Profile Image" >

                                            <?php }
                                            else
                                            {

                                            //$image = IMAGEURL."profile_picture/".$Row[0][0]->profile_image ;
                                            $image = IMAGEURL."language/".$Row[0][0]->file_name;?>
                                            <a download="" href='<?php echo LANG.$Row[0][0]->file_name;?>' >
                                                &nbsp;&nbsp;&nbsp;  <span>
                                                (<?php echo $Row[0][0]->file_name; ?>)
                                            </span> <i class="fa fa-download" aria-hidden="true"></i>
                                            </a>

                                            <span>upload only: .json file.)</span>
                                        </div>
                                    </div>

                                    <?php } ?>
                                    <input type="hidden" name="lang_file_name" value="<?php echo $Row[0][0]->file_name;?>" />
                                </div>




                            </div>

                        </div>



                            <div class="form-group">
                                <div class="col-sm-12">
                                    <button type="submit" name="update_language" class="btn btn-danger add-company pull-right"><?php echo  $lang['Update Language'] ?></button>
                                </div>
                            </div>


                    </div>
                </div>
            </div>
        </div>
        </div>
        </form>

    </section>
<script>
    $(document).ready(function() {
        $('#uploadButton').click(function(){

            $('#fileUpload').click();
        });
    });

</script>