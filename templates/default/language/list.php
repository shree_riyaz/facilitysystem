<?php
/*======================================
Developer	-	Jaishree Sahal
Module      -   USer
SunArc Tech. Pvt. Ltd.
======================================
******************************************************/
?>

<script>

    function checkAll(ele) {
        var checkboxes = document.getElementsByTagName('input');

        if (ele.checked) {
            for (var i = 0; i < checkboxes.length; i++) {
                if (checkboxes[i].type == 'checkbox') {
                    checkboxes[i].checked = true;
                }
            }
        } else {
            for (var i = 0; i < checkboxes.length; i++) {
                console.log(i);
                if (checkboxes[i].type == 'checkbox') {
                    checkboxes[i].checked = false;
                }
            }
        }
    }

    function showpassdiv(id) {
        $("#password_" + id).css("display", "block");
    }
    function Check() {
        if (document.getElementById('keyword').value == '') {
            alert('Please enter any value for search.');
            return false;
        }
        else {
            return true;
        }
    }
    function Clear() {
        document.getElementById('keyword').value = '';
        location.href = "index.php?mod=user&do=list";
        return false;
    }
</script>


    <section>


        <div style="height: 100vh" class="col-sm-12 drop-shadow nopadding">

            <form method="post" name="frmlist" id="frmlist">
                <?php
                if (isset($_SESSION['ActionAccess'])) {
                    echo '<table cellspacing="0" cellpadding="0" border="0" align="center" width="90%" ><tbody><tr><td colspan="6"  align="center"><div class="errormsg">';
                    echo $_SESSION['ActionAccess'];
                    echo '</div></td></tr></tbody></table>';
                    unset($_SESSION['ActionAccess']);

                }
                if (isset($_SESSION['error'])) {
                    echo '<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" ><tbody><tr><td colspan="6"  align="center">
				<div class="alert alert-danger alert-dismissable">
				   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
                    echo $_SESSION['error'];
                    echo '</div></td></tr></tbody></table>';
                    unset($_SESSION['error']);
                }
                if (isset($_SESSION['success'])) {
                    echo '<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" ><tbody><tr><td colspan="6"  align="center">
            <div class="alert alert-success alert-dismissable">
               <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
                    echo $_SESSION['success'];
                    echo '</div></td></tr></tbody></table>';
                    unset($_SESSION['success']);
                }
                ?>
            <div class="user-heading fixedHeader">
                <span class="side-nav-open">
                                <i class="fa fa-bars" aria-hidden="true"></i>
                            </span>
                <span class=""><?php echo  $lang['Languages'] ?> </span>
                <?php
                include_once 'user_profile.php';
                ?>
            </div>
            <div class="userbg">

                <!--<div id="google_translate_element"></div>
                <script type="text/javascript">
                    function googleTranslateElementInit() {
                        new google.translate.TranslateElement({pageLanguage: 'en'}, 'google_translate_element');
                    }
                </script>-->
                <script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>

                <div class="row">
                    <div class="col-sm-12">
                        <div class="icon-group">
                            <input type="text" id="keyword" value="<?php echo(isset($frmdata['keyword']) ? $frmdata['keyword'] : ''); ?>"
                                   onblur="return  KeywordSearch()" name="keyword" class="form-control search"
                                   placeholder="<?php echo $lang['Search Language'] ?>">
                            <i class="fa fa-search search-icon" aria-hidden="true"></i>
                            <p style="text-align: center; margin: 12px 0px;">
                                        <span style="font-size:12px;font-weight: bold;">
                                   <?php if(($_SESSION['usertype'])=='super_admin')
                                   { ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*<?php echo $lang['Search By Language Name'] ?><?php }
                                   else
                                   { ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*<?php echo $lang['Search By Name , Role, Phone No, Email ID'] ?><?php } ?>
                                   </span>
                            </p>
                        </div>

                    </div>


                    <!--<div class="col-xs-offset-2 col-xs-10" style="width:100% !important; margin-left: 0px; padding-bottom:10px;">

                                <button type="submit" class="btn btn-primary" name="search"  onclick="return Check()"><?php /*echo $lang['Search']*/?></button>
                                <button type="reset" class="btn btn-primary" name="clearsearch" id="clear" onclick="return Clear()"><?php /*echo $lang['Reset']*/?></button>
                            </div>-->
                </div>

                <div class="row">
                    <div class="col-sm-3">
                        <select name="select_locale" onchange="this.form.submit()" class="form-control show-result select_locale">
                            <option style="color: white;" value="">Select Language</option>
                            <?php foreach ($get_language_list[0] as $get_language_list_list_value) { ?>
                                <option <?php if ($get_language_list_list_value->short_code == $_SESSION['selected_language']){?> selected="selected" <?php } ?> style="color: white;" value="<?php echo $get_language_list_list_value->short_code ?>"> <?php echo trim($get_language_list_list_value->language_name); ?> </option>
                            <?php } ?>
                        </select>
                    </div>

                    <div class="col-sm-9">
                        <div class="pull-right">
                            <?php  if(($_SESSION['usertype'])=='super_admin') { ?>
                                <a title="Add New Language" href="index.php?mod=language&do=add" class="btn btn-danger add-company margin_30"><?php echo $lang['Add New Language'] ?></a>
                            <?php } echo '<br>';  ?>

                        </div>
                    </div>

                </div>
                <?php
                $srNo = $frmdata['from'];
                $count = count($Row);
                ?>
                <div class="user-heading">
                    <div class="row">
                        <div class="col-sm-3 select-caret">
                            <?php if (($_SESSION['usertype']) == 'super_admin') { ?>
                                <select name="actions" id="action_list_id" class="form-control show-result">
                                    <option value=""><?php echo $lang['Select Action']?></option>
                                    <option value="Delete"><?php echo $lang['Delete']?></option>
                                    <option value="Active"><?php echo $lang['Active']?></option>
                                    <option value="Inactive"><?php echo $lang['In-Active']?></option>
                                </select>
                            <?php } ?>
                        </div>
                        <div class="col-xs-6">
                            <p class="showing-results margin_4">
                                <?php
                                if($totalCount > 0)
                                    print $lang['Showing Results'].' '.($frmdata['from']+1).'-'.($frmdata['from']+$count)." of ".$totalCount;
                                else
                                    print $lang['Showing Results'];
                                ?>
                            </p>
                        </div>
                        <div class="col-sm-3 col-xs-6 select-caret">
                            <?php echo getPageRecords(); ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="table-responsive">
                            <table class="table dashboard-table table-bordered">
                                <thead>
                                <tr>
                                    <th>
                                        <?php
                                        if(($_SESSION['usertype'])=='super_admin' ||($_SESSION['usertype'])=='company_admin' ) { ?>
                                            <input type="checkbox" name="chkAll[]" id="chkAll"  onchange="checkAll(this)"/>&nbsp;
                                        <?php } echo $lang['S.No.']; ?>
                                    </th>
                                    <th><a  style="text-decoration:none; cursor:pointer" onClick="OrderPage('first_name');"><?php echo $lang['Language Name']?>
                                            <?php if($frmdata['orderby']=='first_name') {print '<img src="'.IMAGEURL.'/asc.gif">';} elseif($frmdata['orderby']=='first_name desc'){print '<img src="'.IMAGEURL.'/desc.gif">';}?>
                                        </a></th>
                                    <?php  if($_SESSION['usertype']=='super_admin') { ?>
                                        <th>
                                            <a style="text-decoration:none; cursor:pointer" onClick="OrderPage('company_name');"><?php echo $lang['Short Code']?>
                                                <?php if($frmdata['orderby']=='company_name') {print '<img src="'.IMAGEURL.'/asc.gif">';} elseif($frmdata['orderby']=='company_name desc'){print '<img src="'.IMAGEURL.'/desc.gif">';}?>
                                            </a>
                                        </th>
                                    <?php } ?>

                                    <th>
                                        <a style="text-decoration:none; cursor:pointer" onClick="OrderPage('is_active');"><?php echo $lang['Status']?>
                                            <?php if($frmdata['orderby']=='is_active') {print '<img src="'.IMAGEURL.'/asc.gif">';} elseif($frmdata['orderby']=='is_active desc'){print '<img src="'.IMAGEURL.'/desc.gif">';}?>
                                    </th>

                                    <th><?php echo $lang['File uploaded ?']?></th>
                                    <th><?php echo $lang['Action']?></th>
                                    <th><?php echo $lang['Download File']?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                if($Row)
                                {
                                $objpassEncDec = new passEncDec;
                                for($counter=0;$counter<$count;$counter++)
                                {
                                    $srNo++;
                                    if(($counter%2)==0)
                                    {
                                        $trClass="tdbggrey";
                                    }
                                    else
                                    {
                                        $trClass="tdbgwhite";
                                    }

                                    $confirmDelete = 'All the related data of this user will be deleted . Do you really want to delete this User ?';
                                    $obj = new passEncDec;
                                    ?>
                                    <tr>
                                        <td>
                                            <?php
                                            if(($_SESSION['usertype'])=='super_admin' ||($_SESSION['usertype'])=='company_admin' ) { ?>
                                                <input type="checkbox" name="chkbox[]" id="chkbox" value="<?php echo $Row[$counter]->user_id;?>"/>&nbsp;&nbsp;
                                            <?php } echo $srNo; ?>
                                        </td>
                                        <td><?php echo isset($Row[$counter]->language_name) ? ucfirst($Row[$counter]->language_name) : 'NA'; ?></td>
                                        <td><?php echo isset($Row[$counter]->short_code) ? $Row[$counter]->short_code : 'NA'; ?></td>


                                        <td><span class="status-bg-<?php if($Row[$counter]->is_active == 'N')echo 'inactive'; else echo 'active'; ?>">
            <?php if($Row[$counter]->is_active == 'N'){echo ucfirst('in-Active');} else {echo ucfirst('active');}?>

                                        <td>
                                            <?php
                                            $format = ".json";
                                            $file_name = "sample.json";
//

                                            $is_exist  = file_exists(LANGUAGEURL."/".$Row[$counter]->short_code.''.$format);

                                            if ($is_exist == 1 && isset($Row[$counter]->short_code) ) {  ?>
                                                <span> YES </span>
                                            <?php  }else{ ?>
                                                <span> NO </span>

                                            <?php  } ?>

                                        </td>


                                        <td>
                                            <?php
                                            if(($_SESSION['usertype'])=='super_admin')
                                            {
//                                                echo $is_exist; exit;
                                                ?>

                                                <a href="#" data-toggle="modal" data-message="<?php echo $confirmDelete;  ?>" delete-link="<?php print CreateURL('index.php','mod=language&do=del&id='.$Row[$counter]->id) ?>" data-target="#myModal" class="delete-modal" ><i class="fa fa-trash" aria-hidden="true"></i>
                                                </a>

                                            <a href='<?php print CreateURL('index.php','mod=language&do=edit&lang_id='.$Row[$counter]->id);?>' title="Edit">
                                                <i class="fa fa-pencil" aria-hidden="true"></i>
                                            <?php } ?>



                                        </td>
                                        <td>
                                    <?php if ($is_exist == 1 && isset($Row[$counter]->short_code) ) {  ?>
                                        <span>Not Available</span>
                                <?php  }else{ ?>
<!--                                    <a href='--><?php //print CreateURL('index.php','mod=language&do=edit&download=sample.json') ?><!--' >-->
<!--                                        &nbsp;&nbsp;&nbsp; Download sample file <i class="fa fa-download" aria-hidden="true"></i>-->
<!--                                    </a>-->
                                        <a download="" href='<?php echo LANG.'sample.json';?>' >
                                        &nbsp;&nbsp;&nbsp; <?php echo $lang['Download sample file'] ?> <i class="fa fa-download" aria-hidden="true"></i>
                                        </a>

                                <?php  } ?>
                                        </td>
                                    </tr>
                                    <?php $sno++; } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="text-center">
                            <?php
                            PaginationDisplay($totalCount);
                            ?>
                        </div>
                    </div>
                </div>
            <?php
            }
            elseif($_SESSION['keywords'] == 'Y') {
                $frmdata['message']="Sorry! No record found for the selected criteria";
                unset ($_SESSION['keywords']);
                ShowMessage(); }
            else {
                $frmdata['message']="Sorry! No user found.";
                ShowMessage(); }
            ?>
            </div>
                <input name="pageNumber" type="hidden" value="<?php print $frmdata['pageNumber'] ?>">
                <input name="orderby" type="hidden"
                       value="<?php if ($frmdata['orderby'] != '') echo $frmdata['orderby']; else echo 'u.user_id desc'; ?>">
                <input name="order" type="hidden" value="<?php print $frmdata['order'] ?>">
                <input name="actUserID" type="hidden" value=""/>
            </form>
        </div>

    </section>

<script>
    $parent.find('button[name=download]').click(function () {
        window.location.href = 'download.php';
    });
</script>