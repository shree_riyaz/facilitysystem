<?php
	/*======================================
	Developer	-	Jaishree Sahal
	Module      -   User
	SunArc Tech. Pvt. Ltd.
	======================================
	******************************************************/
	$lang = $language->english($lang);
?>
<script>
function Clear()
{	//alert(document.getElementById('role_id')[0].value);
	try{
	document.getElementById('user_phone').value='';
	//alert(document.getElementById('user_phone').value);
	document.getElementById('first_name').value='';
	document.getElementById('last_name').value='';
	document.getElementById('user_email').value='';
	//document.getElementById('confirm_password').value='';
	//document.getElementById('password').value='';
	document.getElementById('role_id').value='';
	//document.getElementById('superviser_row').style.display = 'none';
	if(document.getElementById('assigned_to'))	// to hide Assingned to selectbox on Reset form Added By : Neha Pareek
	{
		document.getElementById('assigned_to').value='';
		document.getElementById('superviser_row').style.display = 'none';
	}
	document.getElementById('profile_image').value='';
	return false;
	}
	catch(e)
	{
		alert("Error: " + e.description);
	}
}
</script>

<section>
    <form method="post" name="user_add" class="form-horizontal" id="user_add" enctype="multipart/form-data">


        <div class="container-fluid page-wrapper">
            <div class="row nomargin">
                <div style="height:100vh;" class="col-sm-12 drop-shadow nopadding">
                    <div class="user-heading text-left fixedHeader">
                         <span class="side-nav-open">
                                <i class="fa fa-bars" aria-hidden="true"></i>
                            </span>
                        <span>Language</span>
                        <?php
                        include_once 'user_profile.php';
                        ?>
                    </div>
                    <div class="userbg">
                        <?php

                        if(isset($_SESSION['error']))
                        {
                            echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" ><tbody><tr><td colspan="6"  align="center">
        <div class="alert alert-danger alert-dismissable">
           <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
                            echo $_SESSION['error'];
                            echo '</div></td></tr></tbody></table><br>';
                            unset($_SESSION['error']);
                        }
                        if(isset($_SESSION['success']))
                        {
                            echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" ><tbody><tr><td colspan="6"  align="center">
				<div class="alert alert-success alert-dismissable">
				   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
                            echo $_SESSION['success'];
                            echo '</div></td></tr></tbody></table><br>';
                            unset($_SESSION['success']);
                        }
                        ?>
                        <div id="users">
                            <h4 class="update-user">Add new Language</h4>
                        </div>
                        <div class="plan-category user-page-form">



                        <div class="form-group">
                            <label for="language_name" class="col-sm-3"><?php echo $lang['Language Name'] ?>
                                <sup>*</sup></label>
                            <div class="col-sm-9">
                                <input title="Enter language name" class="form-control" type="text"
                                       name="language_name" id="language_name" size=30 value="<?php echo $_POST['language_name'];?>">
                            </div>
                        </div>

                            <div class="form-group">
                                <label for="FirstName" class="col-sm-3"><?php echo $lang['Select Language'] ?> <sup>*</sup></label>
                                    <div class="col-sm-9">
                                        <select name="short_code" class="form-control">
                                            <option value="">Select Language</option>
                                            <?php foreach ($get_language_list[0] as $get_language_list_list_value) { ?>
                                                <option  value="<?php echo $get_language_list_list_value->short_code ?>">
                                                    <?php echo $get_language_list_list_value->language_name; ?>
                                                </option>
                                            <?php } ?>
                                        </select>
                                    </div>
                            </div>

                        <div class="form-group">
                            <div class="col-sm-12">
                                <button type="submit" name="add_language" class="btn btn-danger add-company pull-right"><?php echo $lang['Add Language'];?></button>
                            </div>
                        </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    </section>

<script>
    $(document).ready(function() {
        $('#uploadButton').click(function(){

            $('#fileUpload').click();
        });
    });

</script>