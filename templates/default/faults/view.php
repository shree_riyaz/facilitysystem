<?php
/*======================================
Developer    -    Neha Pareek
Module      -  Facility & Services
SunArc Tech. Pvt. Ltd.
======================================
*/


?>

<!--<script type="text/javascript" src="--><?php //echo ROOTADMINURL?><!--/js/jquery.min.js"></script>-->

<?php
$lang = $language->english($lang);
//echo '<pre>'; print_r($Row); exit;
?>

<section>
    <form method="post" name="frmlist" id="add_faults" class="form-horizontal" enctype="multipart/form-data" onsubmit="return validation()" >


        <?php
        if(isset($_SESSION['error']))
        {
            echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" ><tbody><tr><td colspan="6"  align="center">
                    <div class="alert alert-danger alert-dismissable">
                       <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
            echo $_SESSION['error'];
            echo '</div></td></tr></tbody></table>';
            unset($_SESSION['error']);
        }
        if(isset($_SESSION['success']))
        {
            echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" ><tbody><tr><td colspan="6"  align="center">
				<div class="alert alert-success alert-dismissable">
				   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
            echo $_SESSION['success'];
            echo '</div></td></tr></tbody></table>';
            unset($_SESSION['success']);
        }
        ?>


                <?php
                $srNo = $frmdata['from'];
                $count = count($Row);
                ?>
                <div class="col-sm-9 drop-shadow nopadding toggle-close">
                    <div class="user-heading sticky">
                        <span><?php echo $lang['Facility & Faults'] ?></span>
                        <?php
                        include_once 'user_profile.php';
                        ?>
                    </div>
                    <div class="userbg">
                        <div><i class="fa fa-bars" aria-hidden="true"></i></div>
                        <div class="user-heading ">
                            <div class="row">
                                <div class="col-sm-3">
                                    <h4 ><?php echo $lang['Add & View Faults'] ?></h4>
                                </div>
                                <div class="col-xs-6">
                                    <!--<p class="showing-results margin_4">
                                        <?php
/*                                        if($totalCount > 0)
                                            print "Showing Results:".($frmdata['from']+1).'-'.($frmdata['from']+$count)." of ".$totalCount;
                                        else
                                            print "Showing Results:0-0 of 0";
                                        */?>
                                    </p>-->
                                </div>
                                <div class="col-sm-3 col-xs-6">
                                    <?php //echo getPageRecords(); ?>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="row">
                                <div class="col-sm-12 ">
                                    <div class="table-responsive">
                                        <table class="table dashboard-table table-bordered">
                                            <thead>
                                            <tr>
                                                <th><?php echo $lang['Service Name'] ?></th>
                                                <th><?php echo $lang['Service Description'] ?></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td><?php echo ucfirst($service->service_name); ?></td>
                                                <td><?php echo $lang['Service Description']?></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>


                                <div class="col-sm-12">
                                    <?php
                                    $srNo=$frmdata['from'];
                                    $count=count($Row);

                                    if($Row)
                                    {
                                    ?>
                                    <div class="table-responsive show_all_fault">
                                        <table class="table dashboard-table table-bordered">
                                            <thead>
                                            <tr>
                                                <th><?php echo $lang['S.No.']?></th>
                                                <th>
                                                    <a style="cursor:pointer; color: #05709b; text-decoration : none;" onClick="OrderPageField('fault_name');"><?php echo $lang['Fault Name']?>
                                                        <?php if($frmdata['orderby']=='fault_name') {print '<img src="'.IMAGEURL.'/asc.gif">';} elseif($frmdata['orderby']=='fault_name desc'){print '<img src="'.IMAGEURL.'/desc.gif">';}?>
                                                    </a>
                                                </th>
                                                <th>
                                                    <?php echo $lang['Fault Image']?>&nbsp;&nbsp;
                                                </th>
                                                <th>
                                                    <?php echo $lang['Active Image']?> &nbsp;&nbsp;
                                                </th>
                                                <th ><?php echo $lang['Remove / Add']?></th>

                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php

                                            static $n;
                                            $objpassEncDec = new passEncDec;
                                            for($counter=0;$counter<$count;$counter++)
                                            {
                                            $srNo++;
                                            if(($counter%2)==0)
                                            {
                                                $trClass="tdbggrey";
                                            }
                                            else
                                            {
                                                $trClass="tdbgwhite";
                                            }

                                            $confirmDelete = 'Do you really want to delete this Fault. ?';
                                            $obj = new passEncDec;
                                            ?>
                                            <tr>
                                                <td><?php echo $srNo; ?></td>
                                                <td><?php echo ucwords($Row[$counter]->fault_name); ?></td>
                                                <td class="width_equal">
<!--                                                    <img src="images/device.png" alt="" ></td>-->
                                                <?php if($Row[$counter]->fault_image)
                                                {
                                                    $pos = strrpos($Row[$counter]->image,'/');
                                                    $img = substr($Row[$counter]->image,$pos+1);
                                                    //echo IMAGEURL."faults_images/".$Row[$counter]->fault_image;
//echo file_exists( "images/faults_images/".$Row[$counter]->fault_image);
                                                    if(file_exists( "images/faults_images/".$Row[$counter]->fault_image))
                                                    {
                                                        ?>
                                                        <div align="center" >
                                                            <img class="img-responsive" src="<?php echo IMAGEURL."faults_images/".$Row[$counter]->fault_image; ?>" alt="Fault Image"  style="height :50px; width:50px;"/>
                                                        </div>
                                                        <?php
                                                    }
                                                    else
                                                    { ?>
                                                        <div align="left">
                                                            <img class="img-responsive" src="<?php echo IMAGEURL."faults_images/no_image.gif"; ?>" alt="Fault Image"  style="height :50px; width:50px;"/>
                                                        </div>
                                                        <?php

                                                    }
                                                }
                                                else
                                                {  ?>
                                                    <div align="left">
                                                        <img class="img-responsive" src="<?php echo IMAGEURL."faults_images/no_image.gif"; ?>" alt="Fault Image"  style="height :50px; width:50px;"/>
                                                    </div>
                                                    <?php

                                                }
                                                ?>

                                                </td>
                                                <td class="width_equal">
<!--                                                    <img src="images/device.png" alt="" ></td>-->
                                                <?php if($Row[$counter]->active_image)
                                                {
                                                    $pos = strrpos($Row[$counter]->image,'/');
                                                    $img = substr($Row[$counter]->image,$pos+1);
                                                    //echo IMAGEURL."faults_images/".$Row[$counter]->fault_image;
//echo file_exists( "images/faults_images/".$Row[$counter]->fault_image);
                                                    if(file_exists( "images/faults_images/".$Row[$counter]->active_image))
                                                    {
                                                        ?>
                                                        <div align="center" >
                                                            <img class="img-responsive" src="<?php echo IMAGEURL."faults_images/".$Row[$counter]->active_image; ?>" alt="Fault Image"  style="height :50px; width:50px;"/>
                                                        </div>
                                                        <?php
                                                    }
                                                    else
                                                    { ?>
                                                        <div align="left">
                                                            <img class="img-responsive" src="<?php echo IMAGEURL."faults_images/no_image.gif"; ?>" alt="Fault Image"  style="height :50px; width:50px;"/>
                                                        </div>
                                                        <?php

                                                    }
                                                }
                                                else
                                                {  ?>
                                                    <div align="left">
                                                        <img class="img-responsive" src="<?php echo IMAGEURL."faults_images/no_image.gif"; ?>" alt="Fault Image"  style="height :50px; width:50px;"/>
                                                    </div>
                                                    <?php

                                                }
                                                ?>

                                                </td>
                                                <td>


                                                    <a href="#" data-toggle="modal" data-message="<?php echo $confirmDelete;  ?>" delete-link="<?php print CreateURL('index.php','mod=faults&do=del&id='.$Row[$counter]->fault_id) ?>" data-target="#myModal" class="delete-modal" ><i class="fa fa-times" aria-hidden="true"></i></a>

                                                </td>

                                            </tr>
                                                <?php
                                                $sno++;
                                                $n = $sno++;
                                                }
                                                ?>
                                                <?php } else { ShowMessage(); ?>


                                                    <?php
                                                }
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <div class="table-responsive">
                                    <table id="myfields">
                                        <tbody>
                                        <tr>
                                            <td><span style="margin-left:20px;color: #05709b;">Fault Type</span><sup style="font-size:13px; font-weight: bold;color: red;">*</sup></td>
                                            <td><span style="margin-left:20px;color: #05709b;">Fault Image</span><sup style="font-size:13px; font-weight: bold;color: red;">*</sup></td>
                                            <td><span style="margin-left:20px;color: #05709b;">Active Image</span><sup style="font-size:13px; font-weight: bold;color: red;">*</sup></td>
                                        </tr>
                                        <tr class="txt first_row">
                                            <td >
                                                <input type='text' placeholder="Enter Fault Here" style="margin-left:20px;" name='mytextfield[]' class="form-control txt"/>
                                            </td>
                                            <td>
                                                <input type='file' name='myFilesfield[]' class="form-control " style="margin-left:20px; " id="myFilesfield"/>
                                            </td>
                                            <td>
                                                <input type='file' name='active_image[]' class="form-control " style="margin-left:20px;" id="myFilesfield"/>
                                            </td>

<!--                                           -->

                                       <td><a href='javascript:addFields()' style="font-size:14px; margin-left:20px;" id="addmorelink">Add More</a>
                                        </td></tr>

                                        <tfoot>


                                        <tr class="alt" align="center">
                                            <td></td>
                                            <td></td>
                                            <td style="text-align: center;">
                                                <button type="submit" style="margin: 12px 0px;" class="btn btn-primary" name="add"><?php echo $lang['Add']?></button>
                                                <input type="button" style="margin: 12px 0px;" value="<?php echo $lang['Reset']?>" onClick="this.form.reset()" class="btn btn-primary"/>
                                                <!--<button type="reset" class="btn btn-primary" name="Back" onClick="window.location.href='<?php print CreateURL('index.php','mod=facilityandServices&do=view&id=');?>'"><?php echo $lang['Back']?></button>-->
                                            </td>
                                        </tr>
                                        </tfoot>


                                    </table>
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                            <div class="col-sm-12">
                                <div class="text-center">
                                    <?php
                                    PaginationDisplay($totalCount);
                                    ?>
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>


        <input name="pageNumber" type="hidden" value="<?php print $frmdata['pageNumber']?>" >
        <input name="orderby" type="hidden" value="<?php if($frmdata['orderby']!='')  echo $frmdata['orderby']; else echo 'f.feedback_id';?>" >
        <input name="order" type="hidden" value="<?php print $frmdata['order']?>" >
        <input name="actUserID" type="hidden" value="" />

    </form>
</section>

<style type="text/css">
    .frmt
    {
        /*padding-left : 39px;*/
    }
    .user-page-form .form-control {
        width:100%;
    }
    .frmtbtn
    {	align: left;
        text-align:center;
    }

</style>
<script type="text/javascript">

    function addFields()
    {
        var table = document.getElementById('myfields');
        var lastRow = table.rows.length;
        var row = table.insertRow(1);
        row.className="frmtbtn ";
        var cell1 = row.insertCell();
        var cell2 = row.insertCell();
        var cell3 = row.insertCell();
        var cell4 = row.insertCell();
        var cell5 = row.insertCell();


        cell1.innerHTML = "<span style='font-size:16px; text-align: center; color: red; font-weight:bold; padding-left:0px;'>*</span>";

        cell2.innerHTML = " <input type='text' placeholder='Enter Other Fault Here' name='mytextfield[]'  style='margin-left:20px;' class='form-control txt'/>";
        cell2.className = "frmt";

        cell3.innerHTML = "<input type='file' name='myFilesfield[]' style='margin-left:20px;' class='form-control'/> ";
        cell3.className = "frmt";

        cell4.innerHTML = "<input type='file' name='active_image[]' style='margin-left:20px;' class='form-control'/> ";
        cell4.className = "frmt";

        cell5.innerHTML = "<input type='button' value='Remove' class='btn btn-primary' style='text-align: center; ' onclick='SomeDeleteRowFunction(this);'  />";
        cell5.className = "frmtbtn1";
        var newTextContent = " <input type='text' name='mytextfield[]'  class='form-control'/>";
        var newFileContent = "<input type='file' name='myFilesfield[]'  class='form-control' id='myFilesfield'/>";
        // $("#mytextfield").append(newTextContent);

        // $("#myFilesfield").append(newFileContent);
    }

    function doValidate(fault)
    {
        var e = document.getElementsByName(fault_name);
        for(var i = 0; i < e.length; i++)
        {	//var text_fault = e[i].value;
            if(e[i].value == "")
            {
                alert("Please Enter Fault.");
                return;
            }
        }
        alert("all fault fields should have some value.");
    }


    function validation()
    {

        var b=document.getElementsByTagName('input');

        var count=0;
        for (i=0;i<b.length;i++)
        {
            var box=b[i];
            console.log(44444);
            console.log(b);
            if(	box.value =='' && box.type=='text')
            {
                alert('Enter Any Fault in Textbox.');
                box.focus();
                return false;
            }
            else if(((box.value).trim()) != "")//AddedBy Neha Pareek.Dated: 03-11-15
            {
                return true;
            }
            else//AddedBy Neha Pareek.Dated : 03-11-15
            {
                alert("Space don't allow, Enter Any Fault in Textbox.");
                box.focus();
                return false;
            }
        }
    }

    function SomeDeleteRowFunction(btndel)
    {
        if (typeof(btndel) == "object")
        {
            $(btndel).closest("tr").remove();
        }
        else
        {
            return false;
        }
    }


</script>
<!--<input name="pageNumber" type="hidden" value="<?php print $frmdata['pageNumber']?>" >-->
<input name="orderby" type="hidden" value="<?php if($frmdata['orderby']!='')  echo $frmdata['orderby']; else echo 'fault_name';?>" >
<input name="order" type="hidden" value="<?php print $frmdata['order']?>" >
<input name="actUserID" type="hidden" value="" />