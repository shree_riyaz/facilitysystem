<?php
    /*======================================
    Developer    -    Neha Pareek
    Module      -  Facility & Services
    SunArc Tech. Pvt. Ltd.
    ======================================        
    ******************************************************/
    

?>

<script type="text/javascript" src="<?php echo ROOTADMINURL?>js/jquery.min.js"></script>
   

<?php 
$lang = $language->english($lang);
?>
<form method="post" name="fault_add" id="fault_add" enctype="multipart/form-data" onsubmit="return validation()">
<center>
    <?php 
          
        if(isset($_SESSION['error']))
            {
                echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" ><tbody><tr><td colspan="6"  align="center">
                <div class="alert alert-danger alert-dismissable">
                   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
                  echo $_SESSION['error'];
                echo '</div></td></tr></tbody></table><br>';
                unset($_SESSION['error']);
            }
            if(isset($_SESSION['success']))
            {
                echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" ><tbody><tr><td colspan="6"  align="center">
                <div class="alert alert-success alert-dismissable">
                   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
                echo $_SESSION['success'];
                echo '</div></td></tr></tbody></table><br>';
                unset($_SESSION['success']);
            }
            ?>

	<table class="table table-striped" cellpadding="0" style="width:80% !important; margin-bottom: 10px; ">
    
	<tr valign="middle" align="center"> 
      <th height="30" class="thColor" colspan="2" style="padding-left:5px;"><font color="#FFFFFF"><?php echo $lang['View Faults']?></font>
	  </th>
	   </tr>
	<tr height="36">
	<td align='center'  style="text-align : center;  ">
	<font color="black"><?php echo $lang['Service Name']?></font>
	</td>
	<td align='center'>
	<?php echo ucfirst($service->service_name); ?>
	</td>
	<td></td>
	</tr>
	<tr height="36">
	<td align='center'  style="text-align : center; ">
	<font color="black"><?php echo $lang['Service Description']?></font>
	</td>
	<td align='center'>
	<?php echo ucfirst($service->service_description); ?>
	</td>
	<td></td>
	</tr>
	<tr>
	
	<td align="center" colspan="2">

		
		<table class="table-striped" align="center" width="100%" id="myfields" >
		<tbody>
		<tr >
			<td height="20" align="center" ><?php echo $lang['S.No.']?></td>
			<td  height="20"align="center"  ><?php echo $lang['Fault Name']?>
			<!--<a style="cursor:pointer; " onClick="OrderPage('fault_name');">
		<?php if($frmdata['orderby']=='fault_name') {print '<img src="'.IMAGEURL.'/asc.gif">';} elseif($frmdata['orderby']=='fault_name desc'){print '<img src="'.IMAGEURL.'/desc.gif">';}?>
		</a>--></td>
		<td align="center"  > 
		<?php echo $lang['Fault Image']?>&nbsp;&nbsp;
		</td>
		<td height="20" align="center" ><?php echo $lang['Remove / Add']?></td>
		</tr>
		
<?php
  		$srNo=$frmdata['from'];
		$count=count($Row);
		
if($Row)
{	
	static $n;	
    $objpassEncDec = new passEncDec;
	for($counter=0;$counter<$count;$counter++)
	{
				$srNo++;
				if(($counter%2)==0)
				{
					$trClass="tdbggrey";
				}
				else
				{
					$trClass="tdbgwhite";
				}
				
		$confirmDelete = 'Do you really want to delete this Fault ?';		
		  $obj = new passEncDec;
  ?>
		<tr>
		<td align='center'><?php echo $srNo; ?></td>
		<td align="center"  ><?php echo ucfirst($Row[$counter]->fault_name); ?>&nbsp; &nbsp;</td>
		<td align='center'><!--<?php echo $Row[$counter]->fault_image; ?>&nbsp;-->
		
					<?php if($Row[$counter]->fault_image)
						{  
						$pos = strrpos($Row[$counter]->image,'/');
						$img = substr($Row[$counter]->image,$pos+1);
						
									if(file_exists( "images/faults_images/".$Row[$counter]->fault_image))
									{	
									?>
										<div align="center" >
										<img src="<?php echo "images/faults_images/".$Row[$counter]->fault_image; ?>" alt="Fault Image"  style="height :50px; width:50px;"/>
										</div>
								<?php
									}
									else
									{ ?> 
										<div align="left">
										<img src="<?php echo "images/faults_images/no_image.gif"; ?>" alt="Fault Image"  style="height :50px; width:50px;"/>
										</div>
									<?php 
									
									}
						}
						else
						{  ?>
							<div align="left">
							<img src="<?php echo "images/faults_images/no_image.gif"; ?>" alt="Fault Image"  style="height :50px; width:50px;"/>
							</div>
						<?php 
								
						}
						?>
		
		
		
		</td>&nbsp;
		<td align='center'>
		<a title="Delete"  href='<?php print CreateURL('index.php','mod=faults&do=del&id='.$Row[$counter]->fault_id);?>' onclick="return confirm('<?php echo $confirmDelete ?>')"><img src="<?php print IMAGEURL ?>/b_drop.png"></a></td>
		</tr>
	
<?php
$sno++;
$n = $sno++;
	}
?>
		<tr >
			<td align="center" style="font-size:16px; font-weight: bold;">*</td>
			<td align="center">
				   <input type='text' name='mytextfield[]' class="form-control txt"/>
			</td>
			<td align="center" style="padding-right:6px;"><input type='file' name='myFilesfield[]' class="form-control" style="margin-right:20px; "/>
			</td>
			<td align="center">	</td>
			
		</tr>
		<tfoot>
					<tr>
					<td></td>
					<td></td>
					<td></td>
					<td align="center"><a href='javascript:addFields()' style="font-size:14px;" id="addmorelink">Add More Faults</a>
					</td>
					</tr>
					
					<tr class="alt" align="center">
					<td></td>
					<td></td>
					<td  style="text-align: center;" >
				
					<button type="submit" class="btn btn-primary" name="add"><?php echo $lang['Add']?></button>
					<input type="button" value="<?php echo $lang['Reset']?>" onClick="this.form.reset()" class="btn btn-primary"/>
					<button type="reset" class="btn btn-primary" name="Back" onClick="window.location.href='<?php print CreateURL('index.php','mod=facilityandServices&do=view&id=1');?>'"><?php echo $lang['Back']?></button>
					</tr>
				<tfoot>
			<tbody>
		</table>
		</td>
		<td></td>
	</tr>
	</table>
	
<?php 
}
else
{

$frmdata['message']="Sorry ! Faults not found.";
		ShowMessage();
?>
<style> .tblheading,.trwhite{ display:none; }

</style>
<?php
}
?>


<style type="text/css">
.frmt
{
padding-left : 39px;
}
.frmtbtn
{
	text-align:center;
}
</style>
<script type="text/javascript">
function addFields()
 {
     var table = document.getElementById('myfields');
	 var lastRow = table.rows.length;
     var row = table.insertRow(lastRow-3);
	 row.className="frmtbtn";
    var cell1 = row.insertCell();
    var cell2 = row.insertCell();
    var cell3 = row.insertCell();
    var cell4 = row.insertCell();
	
	
    cell1.innerHTML = "<span style='font-size:16px; text-align: center; font-weight:bold; padding-left:0px;'>*</span>";
	
    cell2.innerHTML = " <input type='text' name='mytextfield[]'  style='text-align: center;' class='form-control txt'/>";
	cell2.className = "frmt";
   
    cell3.innerHTML = "<input type='file' name='myFilesfield[]' style='text-align: center;' class='form-control'/> ";
	cell3.className = "frmt";
	cell4.innerHTML = "<input type='button' value='Remove' class='btn btn-primary' style='text-align: center; -align: center;' onclick='SomeDeleteRowFunction(this);' />";
	cell4.className = "frmtbtn";
    var newTextContent = " <input type='text' name='mytextfield[]'  class='form-control'/>";
    var newFileContent = "<input type='file' name='myFilesfield[]'  class='form-control'/>";
      $("#mytextfield").append(newTextContent); 
  
      $("#myFilesfield").append(newFileContent);   
 }
 
 function doValidate(fault)
{
	var e = document.getElementsByName(fault_name);
	for(var i = 0; i < e.length; i++)
	{
		if(e[i].value == "")
		{
			alert("Please Enter Fault");
			return;
		}
	}
	alert("all fields have some value");
}


    function validation()
    {
           
		var b=document.getElementsByTagName('input');
		
		var count=0;
			for (i=0;i<b.length;i++)
			{
				var box=b[i];
				if(	box.value=='' && box.type=='text')
				{
					alert('You put something in the box');
					box.focus();
					return false;
				}
			}
	}
    
	function SomeDeleteRowFunction(btndel) {
    if (typeof(btndel) == "object") {
        $(btndel).closest("tr").remove();
    } else {
        return false;
    }
}


</script>
</center>