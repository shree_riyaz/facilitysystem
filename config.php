<?php




/*======================================
		Developer	-	Seema
	    Date        -   22 june 2012
		Module      -   index.php
		SunArc Tech. Pvt. Ltd.
======================================
******************************************************/

/*@define("ROOT",$_SERVER['DOCUMENT_ROOT']);
@define("ROOTADMINURL","http://".$_SERVER['HTTP_HOST']);
@define("ADMINROOT",$_SERVER['DOCUMENT_ROOT']);
@define("ROOTUSERURL","http://".$_SERVER['HTTP_HOST']);
@define("ADMINSCRIPTROOT","http://".$_SERVER['HTTP_HOST']);
@define("SITEURL","http://".$_SERVER['HTTP_HOST']);
@define("IMAGEURL","http://".$_SERVER['HTTP_HOST']."/images/");*/

@define("ROOT",$_SERVER['DOCUMENT_ROOT']."/facilitysystem");
@define("ROOTADMINURL","http://".$_SERVER['HTTP_HOST']."/facilitysystem");
@define("ADMINROOT",$_SERVER['DOCUMENT_ROOT']."/facilitysystem");
@define("ROOTUSERURL","http://".$_SERVER['HTTP_HOST']."/facilitysystem");
@define("ADMINSCRIPTROOT","http://".$_SERVER['HTTP_HOST']."/facilitysystem");
@define("SITEURL","http://".$_SERVER['HTTP_HOST']."/facilitysystem");
@define("IMAGEURL","http://".$_SERVER['HTTP_HOST']."/facilitysystem/images/");
@define("LANGUAGEURL",ADMINROOT."/language/");
@define("LANG",ROOTADMINURL."/language/");

@define('SHOW_QUERY_FAILURE',true); //Added by bajrang to print the query failure massage.
@define('ACCESS',1);
@define('PREFIX','');
@define('CROPIMG','cropImg_');

@define('HOST','localhost');
@define('USER','root');
//@define('PASSWORD','SunArc@123');
@define('PASSWORD','');
@define('DATABASE','facilitysystem_uat');
//@define('DATABASE','facilitysystem');
//@define('DATABASE','feedbacksystem_live');

/*@define('USER','pukhraj');
@define('PASSWORD','pukhraj123');*/
/*@define('USER','cleanerm_wfsys');
@define('PASSWORD','U-H8-Zo)=W6+');
@define('DATABASE','cleanerm_facilitysystem');*/

@define("BACKUP_DIR",ADMINROOT."/modules/backup/db_backups/");
@define("BACKUP_URL",ROOTADMINURL."/modules/backup/db_backups/");
@define("MY_SQLDUMP_COMMAND","mysqldump");
@define("MY_SQL_COMMAND","mysql");

@define("MOD","modules");
@define("FILEPATH",ADMINROOT."/banimg/");
@define("TEMPLETPATH","templates");
@define("STYLE",ADMINROOT."/style");
@define("JS",ADMINROOT."/js");
@define("CURRENTTEMP",ADMINROOT."/templates/default");
define("IMAGESIZE",33554432);
define("IMAGEEXT","jpg,gif,png,jpeg,pjpeg");
define("FILESIZE",33554432);
define("FILEEXT","docx,doc,txt,xls,pdf");
@define("MANDATORYNOTE","<div ><strong >Note</strong> : Fields marked by (<font color='#FF0000'><strong>*</strong></font>) are mandatory.</div>"); // for mandatory notes

@define("MANDATORYMARK","  <font color='#FF0000' size='3'><strong>*</strong></font>"); // for mandatory mark
@define("NOSTRDATA"," <font> <i> N/A </i></font>");

//This variable holds the tables name which not require to get backups under backup module.
$CONFIG_SETTING_DONT_BACKUP_TABLES_ARR = array("permission","adminlogin","role","rolepermission","users");

//echo $_SERVER['HOST']; exit;
?>