<?php session_start();

/*======================================
		Developer   -	Jaishree Sahal
	        Date        -   29 july 2015
		Module      -   index.php
		SunArc Tech. Pvt. Ltd.
======================================		
******************************************************/	 
ini_set('display_errors',0);
include_once("include.php");
$DBFilter= new DBFilter();
$language = new Language();
if(isset($_GET['mod']))
{
	$mod=$_GET['mod'];
}
else
{
	$mod='';
}
if(isset($_GET['do']))
{
	$do=$_GET['do'];
}
else
{
	$do='list';
}
if($_REQUEST['call']=='API')
{
	global $DBFilter;
	$id= $_REQUEST['id'];

	$access_key =  $DBFilter->SelectRecord('login_detail','user_id=11');
	if($_REQUEST['access_key'] == $access_key->access_key)
	{	
		$ACCESS_TOKEN = 1; 
	}
	else 
	$ACCESS_TOKEN = 1;
}
else
{
	$_SESSION['ACCESS_TOKEN']= $_SESSION['ACCESS_TOKEN'];
}

//echo ROOT;
//echo ROOT."/images/company_logo/";
// print_r($_SESSION); exit;
if($mod || 1)
{ 
		if(($_REQUEST['call']=='API' && $ACCESS_TOKEN==1) || $_SESSION['ACCESS_TOKEN'])
		{
				//don't authorize Super Admin
				//print_r($_POST);exit;
			if (($_SESSION['usertype'] != ''))
			{		//print_r($_SESSION);
				$auth = new Authorise();
				$_SESSION['module_name'] = $mod;
				$_SESSION['action_name'] = $do;
				$user_id = $_SESSION['user_id'];
				//print_r($_SESSION);
				//echo $mod. $do. $user_id;exit;
				$permission = $auth->isAuthorisedAction($user_id, $mod, $do);
				//print_r($permission);//exit;
				 $_SESSION['permission']= $permission[0]['do'];
			//	print_r($_SESSION['permission']);//exit;
				/*if ($auth->isAuthorisedAction($user_id, $mod, $do) == false)
				{
					$redirectURL = $auth->determineRedirection($user_id,$mod, $do);
					if ($mod != 'dashboard' && $auth->showMessage === true)
					{
						$_SESSION['error'] = 'Sorry! you don\'t have sufficient permission to perform this action.';
					}				
					Redirect($redirectURL);
					die();	
				}*/
				
			}


            switch($mod)
				{
					case 'company':
						//echo $mod; exit;
                        include_once(MOD."/company/index.php");
					break;
					case 'login':
						include_once(MOD."/login/index.php");
					break;
					case 'device':
						include_once(MOD."/device/index.php");
					break;
					case 'feedback':

						include_once(MOD."/feedback/index.php");
					break;
					case 'user':
						include_once(MOD."/user/index.php");
					break;
					case 'role':
						include_once(MOD."/role/index.php");
					break;
					case 'facilityandServices':
						include_once(MOD."/facilityandServices/index.php");
					break;
					case 'device_locations':
						include_once(MOD."/device_locations/index.php");
					break;
					case 'faults':
						include_once(MOD."/faults/index.php");
					break;
					case 'plan':
						include_once(MOD."/plan/index.php");
					break;
					case 'contact_us':
                        include_once(MOD."/contact_us/index.php");
					break;
					case 'reports':
                        include_once(MOD."/reports/index.php");
					break;
					case 'language':
                        include_once(MOD."/language/index.php");
					break;
					case 'setting':
                        include_once(MOD."/setting/index.php");
					break;
					case 'question':
                        include_once(MOD."/question/index.php");
					break;
					case 'rating':
                        include_once(MOD."/rating/index.php");
					break;
					default:
						include_once(MOD."/login/index.php");
					break;
				}
		}
		else
		{ 	
			include_once(MOD."/login/index.php");
		}	
} 

?>
