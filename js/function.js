//focus to first field


function getLastFormElem(){
    var fID = document.forms.length -1;
    var f = document.forms[fID];
    var eID = f.elements.length -1;
	//alert(eID);
    return f.elements[1];
  }

function isNumber(n) 
{
	  return !isNaN(parseFloat(n)) && isFinite(n);
}

function evaluteValue(ctrl)
{
    var expressionStr = ctrl.value;
    //alert(expressionStr);
    try
    {
	    var parser = new Parser();
	    var val = parser.parse(expressionStr).evaluate();
	    if(isNumber(val))
	    	val = val;
	    else
	    	val = ''; 
	    ctrl.value = val;
    }
    catch(err)
    {
    	ctrl.value='0';
		alert('Please enter a valid expression or a numeric value.');
		ctrl.focus();
	    return true;
    }
}


//validate login form
function ValidateDeviceForm(form)
	{ 
		var FieldNameArr = new Array('make','model');
			
		var Message = '';
		var FocusField;
		var Error = 0;
		var err = 0;
		var TotalFields = eval(FieldNameArr.length);
		

		//..........check for all mandatory fields....................
		for(var Counter = 0; Counter < TotalFields; Counter++)
		{
			if(document.getElementById(FieldNameArr[Counter]).value == '' 
				|| document.getElementById(FieldNameArr[Counter]).value == '0' 
				|| document.getElementById(FieldNameArr[Counter]).value == 'ps' 
				|| document.getElementById(FieldNameArr[Counter]).value =='null')
			{
				Error = eval(Error)+1;
				
			}
			
		}

		if(Error <= TotalFields && Error > 0)
		{
			Message = Message + "Please enter all mandatory fields.\n";
			err++;
		}
		
		if(false ==  form.factor_to_adjust_rule.value > 0)
		{
			Message = Message + "Please enter numeric value Factor to adjust for 80/20 rule.\n";
			err++;
			if(FocusField == '')
				FocusField = form.factor_to_adjust_rule;
		}
		
		if(false ==  form.assembly_and_testing.value > 0)
		{
			Message = Message + "Please enter numeric value Factor to include assembly and testing.\n";
			err++;
			if(FocusField == '')
				FocusField = form.assembly_and_testing;
			
		}
		
		if(Message != '' && err > 0)
		{
			alert(Message);
			if(FocusField)
				FocusField.focus();
			return false;
		}
		else
		{
			return true;
		}
		
}

function trim(stringToTrim)
{
	var str =  stringToTrim.replace(/^\s+|\s+$/g,"");
	alert(str);
}

//validate login form
function ValidateForm(frmName)
	{ 
		alert(frmName);
		if(frmName=='addComponent')
			var FieldNameArr = new Array('Manufacturer','Model Number');
			
		var Message = '';
		var FocusField = '';
		var Error = 0;
		var TotalFields = eval(FieldNameArr.length);
		

		//..........check for all mandatory fields....................
		for(var Counter = 0; Counter < TotalFields; Counter++)
		{
			if(document.getElementById(FieldNameArr[Counter]).value == '' 
				|| document.getElementById(FieldNameArr[Counter]).value == '0' 
				|| document.getElementById(FieldNameArr[Counter]).value == 'ps' 
				|| document.getElementById(FieldNameArr[Counter]).value =='null')
			{
				Error = eval(Error)+1;
				
			}
			
		}

		if(Error <= TotalFields && Error > 0)
		{

			alert("Please enter all mandatory fields.");
			return false;
		}
		else
		{
			return true;
		}	
}

function IsEmpty(inputObject, title)
{
	var Flag=0;	

	if(inputObject.value=='' || inputObject.value== 'null' || inputObject.value == 'ps' || inputObject.value== 0 )
	{
			Flag=1;
	}
	if(Flag==1)
	{
		alert("Please enter "+title+".");
		setTimeout(function() { inputObject.focus(); }, 10);
		//inputObject.focus();
		return false;
	}
	else 
		return true;
	
}

//validate login form
/*function validateKeyFactors(keyFactors)
	{ 
		alert('test');
		alert(typeof(keyFactors));
		var FieldNameArr = new Array();
		//FieldNameArr = keyFactors;
			
		var Message = '';
		var FocusField = '';
		var Error = 0;
		var TotalFields = eval(FieldNameArr.length);
		alert(TotalFields);

		//..........check for all mandatory fields....................
		for(var Counter = 0; Counter < TotalFields; Counter++)
		{
				alert(document.getElementById(FieldNameArr[Counter]).value);
			if(document.getElementById(FieldNameArr[Counter]).value == '' 
				|| document.getElementById(FieldNameArr[Counter]).value == '0' 
				|| document.getElementById(FieldNameArr[Counter]).value == 'ps' 
				|| document.getElementById(FieldNameArr[Counter]).value =='null')
			{
				Error = eval(Error)+1;
				
			}
			
		}
//alert(Error);
		if(Error == TotalFields)
		{

			alert("Please enter all mandatory fields.");
			return false;
		}
		else
		{
			return true;
		}	
}*/

 //================Function to check all checkboxes in a form=========================================

function Checkall(frmvalue,val)
{
			for(var nfi = 0; nfi < frmvalue.getElementsByTagName('input').length; nfi++) 
			{
				if(frmvalue.getElementsByTagName('input')[nfi].type=='checkbox')
				{ 
					frmvalue.getElementsByTagName('input')[nfi].checked=val;
				}
			}
		
}


//######################################################################################################################
  //======================To check the check box whether it is checked or not============================================	
	function CheckSubmit(RecordCount,Field,Type)
   	{	
		
		 var Flag = 0; //keeps track that atleast one record is selected to change the status
		 var Counter = 0; 
		/* if(RecordCount ==0 )
		 {
			if(confirm("Please Select  atleast one "+Type+" to delete"))return false; 
		 }*/
		 if(RecordCount == 1)
		 { 
		     if(Field.checked==true)
			 {
			    Flag = 1;
			  }
			 else
			 {
			    Flag = 0;
			  } 
		 }	  	
		 else
		 {
		  	for (Counter=0; Counter < RecordCount; Counter++) 
			{
			  if(Field[Counter].checked == true)
				{
					
				   Flag = 1;
				   break;
				 }    
			   else
			   {
				 Flag = 0; 
				}
			}  	
		}
		
	     if(Flag == 0)
		 {
			if(Type == "device")
			{
				alert("Please select atleast one device to delete.");
			}		 
		 
			if(Type == "component")
			{
				alert("Please select atleast one component to delete.");
			}
			else if(Type == "athlete")
			{
				alert("Please select atleast one athlete to delete.");
			}
			else if(Type == "keyfactor")
			{
				alert("Please select atleast one of key factore option value to delete.");
			}
			else if(Type == "game")
			{
				alert("Please select atleast one game to delete.");
			}
			else if(Type == "team")
			{
				alert("Please select atleast one team to delete.");
			}
			else if(Type == "register")
			{
				alert("Please select atleast one user to delete.");
			}
			else if(Type == "state")
			{
				alert("Please select atleast one state to delete.");
			}
			else if(Type == "user")
			{
				alert("Please select atleast one user to delete.");
			}
			else if(Type == "users")
			{
				alert("Please select atleast one user to send mail.");
			}
			else if(Type == "role")
			{
				alert("Please select atleast one role to delete.");
			}
			else if(Type == "pressRelease")
			{
				alert("Please select atleast one press release to delete.");
			}
			else if(Type == "category")
			{
					alert("Please select atleast one category to delete.");
			}
			else if(Type == "subcategory")
			{
					alert("Please select atleast one Subcategory to delete.");
			}
			else if(Type == "company")
			{
					alert("Please select atleast one company to delete.");
			}
			else if(Type == "equipments")
			{
					alert("Please select atleast one equipment to delete.");
			}
			else if(Type == "rigorder")
			{
				alert("Please select atleast one order to delete.");
			}
			else if(Type == "oilprice")
			{
				alert("Please select atleast one oil price to delete.");
			}
			else if(Type == "vendor")
			{
				alert("Please select atleast one vendor master to delete.");
			}
			else if(Type == "model")
			{
				alert("Please select atleast one model to delete.");
			}
			else if(Type == "manufacturer")
			{
				alert("Please select atleast one manufacturer to delete.");
			}
			else if(Type == "user")
			{
				alert("Please select atleast one user to delete.");
			}
			return false;
		 }
		 else
		 	if(confirm("Do you really want to delete this "+Type+"?"))return true;	
			else return false;
  }	
  function selectAllModule()
{
	var node_list = document.getElementsByTagName('input'); 
	for (var i = 0; i < node_list.length; i++) {
    	var node = node_list[i]; 
    	if (node.getAttribute('type') == 'checkbox') {
			divid = node.getAttribute('id');
			if (document.getElementById('div:'+divid))
			document.getElementById('div:'+divid).style.display ='';
			node.checked= true;
    	}
	} 
}
function unselectAllModule()
{
	var node_list = document.getElementsByTagName('input'); 
	for (var i = 0; i < node_list.length; i++) {
    	var node = node_list[i]; 
    	if (node.getAttribute('type') == 'checkbox') {
			divid = node.getAttribute('id');
			if (document.getElementById('div:'+divid))
			document.getElementById('div:'+divid).style.display ='none';
			node.checked= false;
    	}
	} 
}



function getValueFromExpression(expretionVal,id)
{

	 var fileurl = XAJAX_PATH_JS+"regularexpressioncheck.php?";
	 for(i=0;i<=50;i++)
	   expretionVal = expretionVal.replace('+', 'PLUS_SIGN');
	 
	
	if(expretionVal!='')
	{
		 $.ajax({
				url:   fileurl,
				data: "do=regexpcheck&expressionStr="+expretionVal,
				type: 'GET',
			 success: function (resp) 
			 {
				//alert("success \n\n" + resp);
				document.getElementById(id).value = resp;
				//$('elemntId').html(resp);
			 },
				error: function(e){ 
				 //alert(' Error: '+e); 
				  }  
			});
	} 
}

function searchSpecs(params,display_div)
{
    // alert(display_div);
	 var fileurl = XAJAX_PATH_JS+"searchspecs.php?";
	
	if(display_div!='')
	{
		 $.ajax({
				url:   fileurl,
				data: "do=searchspecs"+params,
				type: 'GET',
			 success: function (resp) 
			 {
				//alert("success \n\n" + resp);
				document.getElementById(display_div).innerHTML = resp;
				//$('elemntId').html(resp);
			 },
				error: function(e){ 
				  //alert(' Error: '+e); 
				  }
			});
	} 
}

function configChange(params,display_div)
{
    //alert(params + display_div);
	 var fileurl = XAJAX_PATH_JS+"searchspecs.php?";
	
	if(display_div!='')
	{
		 $.ajax({
				url:   fileurl,
				data: "do=deviceConfigChange"+params,
				type: 'GET',
			 success: function (resp) 
			 {
			 
				 document.getElementById(display_div).innerHTML = resp;
				 
				 var  new_device_id   = document.getElementById('new_device_id').value;
				 var new_component_id = document.getElementById('new_component_id').value;
				 var reloadUrl = ROOTADMINURL_JS + "index.php?mod=specs&do=search&component_id="+new_component_id+"&device_id="+new_device_id+"&reloadpage=1&showHeader=N";
				 document.location.href = reloadUrl;
				 
				//$('elemntId').html(resp);
			 },
				error: function(e){ 
				  //alert(' Error: '+e); 
				  }
			});
	} 
}



function loadmodelCombo(mf_id,model_for,display_div,ctr_name,selectedValue)
{
         //alert(mf_id + "," +model_for + "," + display_div + "," );
	     var Qstr = '';
		 var fileurl = XAJAX_PATH_JS+"regularexpressioncheck.php?";
		 if(selectedValue != '') Qstr = "&model_id="+selectedValue;
		 
		 $.ajax({
				url:   fileurl,
				data: "do=ModelComboByManufecture&manufacturer_id="+mf_id+"&model_for="+model_for+"&ctr_name="+ctr_name+Qstr,
				type: 'GET',
			 success: function (resp) 
			 {
				//alert("success \n\n" + resp);
				document.getElementById(display_div).innerHTML = resp;
				//$('modelcombo_div').html(resp);
			 },
				error: function(e){ 
				 alert(' Error: '+e); 
				  }  
			});
}



function addRowToTable(tblId,icOptionArray,leavRowFromBottom,selectedValue,selectedQuantity)
{

  if(selectedQuantity == '')
	  selectedQuantity = '1';
  
  var tbl = document.getElementById(tblId); 
  var lastRow = tbl.rows.length - (leavRowFromBottom);
  
  //alert( tbl.rows.length + ' , ' +leavRowFromBottom + ' , lastRow=' + lastRow);  
  // if there's no header row in the table, then iteration = lastRow + 1
  var iteration = lastRow;
//  var iteration = lastRow + 1;
  var row = tbl.insertRow(lastRow);
  var RowNumber = row.rowIndex;
  row.id= "row_id"+RowNumber;

  //  cell 0
  var cell0 = row.insertCell(0);
  cell0.align='right';
  
  var lbl = document.createElement('label');
  //el.textContent = 'IC Chips :';
  lbl.appendChild(document.createTextNode('IC Chips :'));
  cell0.appendChild(lbl);
 
  //cell 1
  var cell1 = row.insertCell(1);


  var selector = document.createElement('select');
  selector.id = 'icchips';
  selector.name = 'icchips[]';
  selector.style.width = '200px';
  //alert(selector);

  for (var key in icOptionArray) 
  {   
      if(key == 'containsValue') continue;
	  var option = document.createElement('option');
	  if(key == selectedValue)
	 	 option.selected = true;
	  option.value = key;
	  option.appendChild(document.createTextNode(icOptionArray[key]));
	  selector.appendChild(option);
  }

  cell1.appendChild(selector);

  var space = document.createTextNode(' ');
     cell1.appendChild(space);  
  
  var lbl = document.createElement('label');
  //lbl.textContent = 'Qty :';
  lbl.appendChild(document.createTextNode(' Qty :  '));
  cell1.appendChild(lbl);  

  
  var selector = document.createElement('select');
  selector.id = 'additional_quantity';
  selector.name = 'additional_quantity[]';

  for (var key in QUANTITIES_ARR) 
  {
      if(key == 'containsValue') continue;
      if(! isNumber(key)) continue;
 
	  var option = document.createElement('option');
	  if(key == selectedQuantity)
	 	 option.selected = true;
	  option.value = key;
	  option.appendChild(document.createTextNode(QUANTITIES_ARR[key]));
	  selector.appendChild(option);
  }

  cell1.appendChild(selector);
  
/*  var space = document.createTextNode('  ');
  cell1.appendChild(space); */
/*  var space = document.createTextNode('&nbsp;');
  cell1.appendChild(space);
  var space = document.createTextNode('&nbsp;');
  cell1.appendChild(space);  */
  
/*  var a = document.createElement('a');
  a.textContent = 'Remove';
  a.style.cursor = 'pointer';
  //a.href = "javascript: document.getElementById(tblId).deleteRow(0);"; 
  //a.setAttribute("onclick","alert(RowNumber)"); 
  a.onclick = function(){ element = document.getElementById(row.id);element.parentNode.removeChild(element); };

  cell1.appendChild(a); */
  if(navigator.appName == 'Microsoft Internet Explorer')
 	 var cell2 = row.insertCell(3);
  else
      var cell2 = row.insertCell(2);	 
	  
  var img = document.createElement('img');
  img.src = IMAGEURL_JS+'b_drop.png';
  img.style.cursor = 'pointer';
  img.onclick = function(){ element = document.getElementById(row.id);element.parentNode.removeChild(element); };

  cell2.appendChild(img);   

}
function FormRecord()
{
	document.frmlist.pageNumber.value=1;
	document.frmlist.submit();

}
function GotoPage(Pagename) 
{
	location.href=Pagename;
}
//=============================================================================================================================
//THIS FUNCTION POSTS FORM FOR NEXT PAGE ....
//THIS FUNCTION POSTS FORM FOR NEXT PAGE ....
//=============================================================================================================================
function NextPage()
{
   document.frmlist.pageNumber.value = eval(document.frmlist.pageNumber.value) + 1;
   document.frmlist.submit();
}
//===========================================================================================================================
//=============================================================================================================================
//THIS FUNCTION POSTS FORM FOR PREVIOUS PAGE ....
//THIS FUNCTION POSTS FORM FOR PREVIOUS PAGE ....
//=============================================================================================================================
function PrePage()
{
   
	document.frmlist.pageNumber.value = eval(document.frmlist.pageNumber.value) - 1  ;
	document.frmlist.submit();
}

//=============================================================================================================================
//=============================================================================================================================
//THIS FUNCTION POSTS FORM FOR SPECIFIED PAGE NUMBER....
//THIS FUNCTION POSTS FORM FOR SPECIFIED PAGE NUMBER....
//=============================================================================================================================
function NextPageLink(val)
{
    
	document.frmlist.pageNumber.value = eval(val)-1  ;
	document.frmlist.submit();
}

function setvalue(tar,con,val)
{
	con.value=val;
	tar.submit();
}

function PopupCenter(pageURL, title,w,h)
{
	var left = (screen.width/2)-(w/2);
	var top = (screen.height/2)-(h/2);
	var targetWin = window.open (pageURL, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
} 


function mandatoryfield_check(inputObject_arr)
{
	var error = 0; 
	var massage = '';
	
	   //Make to static to mandatory 
     if(document.getElementById('manufacturer_id'))	
		inputObject_arr['Manufacturer_9999999'] = document.getElementById('manufacturer_id');
     if(document.getElementById('model_id'))
		inputObject_arr['Model Number_9999999'] = document.getElementById('model_id'); 
		//document.editcomponentform.model_id;

		for(var key in inputObject_arr)
		{
		   if(inputObject_arr[key])
		   {
				objVal = inputObject_arr[key].value;
				var val = (objVal);
			}
			
			if(inputObject_arr[key] && ( val == '' || val == 'null' || val == 'ps' ))
			{
				from =key.indexOf('_');
				to=key.length;

				//alert(key + ", Val="+inputObject_arr[key].type);
				  var labelVal = key.substring(0,from);
				  if(error == 0)
					  focusAt=inputObject_arr[key];	
				  error = 1;
				  switch(inputObject_arr[key].type)
				  {
					 case 'select-one':
						 massage = massage + 'Please select '+labelVal+'.\n';
						 break;
					 case 'text':
						 massage = massage + 'Please enter '+labelVal+'.\n';
						 break;		
					 default :
						massage = massage + 'Please enter '+labelVal+'.\n';
						break;
				  }
			}
		}

		if(error == 1)
		{
			alert(massage);
			focusAt.focus();
			return false;
		}
		else
		{
			return true;
		} 
	
}

function OrderPage(OrderBy)
{
	//alert(OrderBy);
	document.frmlist.pageNumber.value = eval(document.frmlist.pageNumber.value);
	
	if(document.frmlist.orderby.value.search(OrderBy)<0)
		document.frmlist.orderby.value = OrderBy;
	else
	{
		if(document.frmlist.orderby.value.search(' desc')<0)
			document.frmlist.orderby.value = OrderBy +' desc';
		else
			document.frmlist.orderby.value = OrderBy;
	}

	document.frmlist.submit();
}
function initialCap(field) {
   field.value = field.value.substr(0, 1).toUpperCase() + field.value.substr(1);
}

/* created by : Neha Parek */
function OrderPageField(OrderBy)
{
	//alert(OrderBy);
	//document.frmlist.pageNumber.value = eval(document.frmlist.pageNumber.value);
	
	if(document.frmlist.orderby.value.search(OrderBy)<0)
		document.frmlist.orderby.value = OrderBy;
	else
	{
		if(document.frmlist.orderby.value.search(' desc')<0)
			document.frmlist.orderby.value = OrderBy +' desc';
		else
			document.frmlist.orderby.value = OrderBy;
	}

	document.frmlist.submit();
}
