<?php
include_once("lib/language.php");
include_once("lib/class.authorise.php");
include_once("lib/dbfilter.php");
$language = new Language();
$lang = $language->english('eng');
$auth = new Authorise();
$DB = new DBFilter();
$user_id = $_SESSION['user_id'];
$module_name = $auth->getOneAccessibleModule($user_id);
$user_name = $DB->SelectRecord('users',"user_id='$user_id'");
// print_r($module_name[0]);exit;
?>
<div class="bs-example">
<br/>
 <ul class="nav nav-tabs">
			<?php
			for($i=0;$i<count($module_name[0]);$i++)
			{
				$module = $module_name[0][$i]->module_name;

				if($module=='device' )
				{
			?>
       			<li class="dropdown" <?php if($_REQUEST['mod']==$module){ ?> class ="active" <?php } ?> >
			 	 <a class="dropdown-toggle" data-toggle="dropdown" href="#"><?php echo ucfirst($module)."s";?> <span class="caret"></span></a>
			 	 <ul class="dropdown-menu">
				<li style="color: #3969ab;"><a href="<?php print CreateURL('index.php','mod=device');?>"><?php echo $lang['Devices']?></a></li>
				<li  style="color: #3969ab;"><a href="<?php print CreateURL('index.php','mod=device_locations');?>"><?php echo $lang['Device Locations']?></a></li></ul>
			<?php }
				else if($module =='faults')
				{
				?>
                <li <?php if($_REQUEST['mod']==$module){ ?> class ="active" <?php } ?>>
				<a href="<?php print CreateURL('index.php','mod=faults&do=view&id='.$service->service_id );?>"><?php echo $lang['Facility & Faults']?></a>
			    </li>
                <?php
				}
			else if($module == 'role')//Added By : Neha Pareek, Dated : 30 Nov 2015
				{	if(($_SESSION['usertype'])=='super_admin')
					{
				?>
                <li <?php if($_REQUEST['mod']==$module){ ?> class ="active" <?php } ?>>
				<a href="<?php print CreateURL('index.php','mod=role');?>"><?php echo $lang['Roles']?></a>
			    </li>
                <?php
					}
					else
					{
						continue;
					}
				}
			 else if($module!='device_locations')
			 { ?>

			<li <?php if($_REQUEST['mod']==$module){ ?> class ="active" <?php } ?>>
				<a href="<?php print CreateURL('index.php','mod='.$module);?>">
				<?php
				if(($_SESSION['usertype'])=='super_admin')
				{
					###### convert singular to plural : For Module Names in Navigation (added by : Neha Pareek) ####
					if(substr($module, -1)!='y')
					{
						echo ucfirst($module)."s";
					}
					else
					{
						echo ucfirst(str_replace(substr($module, -1), 'ies', $module));
					}
				}
				else
				{
					if($module != 'company')
					{
						echo ucfirst($module)."s";
					}
					else
					{
						echo ucfirst('Dashboard');
						//echo str_replace(substr($module, -1), 'ies', $module);
						//ucfirst($module)."ies";
					}
				}
					?>
				</a>
			</li>
		<?php } } ?>

     <!-- Added By : Neha Pareek. Dated : 27 Nov 2015 -->
			 <?php /*if($_SESSION['role_id']!=3 && $_SESSION['user_id']!='') { ?>
			<li class="dropdown" <?php if($_REQUEST['mod']==$module){ ?> class ="active" <?php } ?> >
			 	 <a class="dropdown-toggle" data-toggle="dropdown" href="#"><?php echo $lang['Settings']?><span class="caret"></span></a>
			 	 <ul class="dropdown-menu">
				<li style="color: #3969ab;"><a href="<?php print CreateURL('index.php','mod=user&do=edit&id='.$_SESSION[user_id]);?>"><?php echo $lang['Profile']?></a></li>
				<li  style="color: #3969ab;"><a href="<?php print CreateURL('index.php','mod=login&do=logout'); ?>"><?php echo $lang['Logout']?></a></li>
				</ul>
			</li>
			<?php }
			else if($_SESSION['role_id']==3 && $_SESSION['user_id']!='')
			{
			?>
			<li>
			<a href="<?php print CreateURL('index.php','mod=login&do=logout'); ?>"><?php echo $lang['Logout']?></a>
			</li>
			<?php
			}
			/*if($_SESSION['user_id']!='')
			{
			?>
			<li style="float:right;">
			<?php echo 'Welcome '.$user_name->first_name;  ?>
			</li>
			<?php } */?>

			<!-- Added By : Neha Pareek. Dated : 25 Jan 2016 -->
			<?php if($_SESSION['role_id']!=3 && $_SESSION['user_id']!='') { ?>
			<li class="dropdown" style="float:right;" <?php if($_REQUEST['mod']==$module){ ?> class ="active" <?php } ?> >

			<a class="dropdown-toggle" data-toggle="dropdown" href="#">
			<?php echo 'Welcome '.$user_name->first_name; ?>&nbsp;&nbsp;<span class="glyphicon glyphicon-user" ></span><span class="caret"></span></a>
			<ul class="dropdown-menu">
			<li style="color: #3969ab;" ><a href="<?php print CreateURL('index.php','mod=user&do=edit&id='.$_SESSION[user_id]);?>"><b><?php echo $lang['Profile']?></b></a></li>
			<li  style="color: #3969ab;" ><a href="<?php print CreateURL('index.php','mod=login&do=logout'); ?>"><b><?php echo $lang['Logout']?></b></a></li>
			</ul>
			</li>
			<?php
			}
			else if($_SESSION['role_id']==3 && $_SESSION['user_id']!='')
			{
			?>
			<li class="dropdown" style="float:right;" <?php if($_REQUEST['mod']==$module){ ?> class ="active" <?php } ?> >
			<a class="dropdown-toggle" data-toggle="dropdown" href="#">
			<?php echo 'Welcome '.$user_name->first_name; ?>&nbsp;&nbsp;<span class="glyphicon glyphicon-user" ></span><span class="caret"></span></a>
			<ul class="dropdown-menu">
			<li  style="color: #3969ab;"><a href="<?php print CreateURL('index.php','mod=login&do=logout'); ?>"><b><?php echo $lang['Logout']?></b></a></li>
			</ul>
			</li>
			<?php } ?>

		</ul>

  <div align="right"></div>
  <!-- <?php print_r($_SESSION); ?>-->
	</div>