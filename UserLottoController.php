<?php

namespace App\Http\Controllers\api;

use App\Model\Lotto;
use App\Model\Tournament;
use App\Model\TournamentUser;
use App\Model\User;
use App\Model\UserLotto;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class UserLottoController extends Controller
{
    public function createUserLotto(Request $request)
    {
//        dd($request->all());

        try {
            date_default_timezone_set("Asia/Kolkata");

            $tournament_end_date = Tournament::where('id', $request->tournament_id)->where('completed', '0')->value('end_time');
            $tournament_start_date = Tournament::where('id', $request->tournament_id)->where('completed', '0')->value('start_time');
            $lotto_end_time = Lotto::where("method", 'daily')->where("type", $request->lotto_name)->value('draw_time');
            $lotto_end_date = date('Y-m-d ' . $lotto_end_time);
//                dd($lotto_end_date);

            if (strtotime($tournament_end_date) <= strtotime($lotto_end_date) && $request->is_tournament == 1) {
//                echo 144444; exit;
                $response = ['status' => 'fail', 'message' => 'Sorry, You can not play this game now, because tournament going to be expired soon.'];

            }elseif (strtotime($tournament_start_date) > strtotime($lotto_end_date) && $request->is_tournament == 1){
                $response = ['status' => 'fail', 'message' => 'Sorry, You can not play this game now, because playing duration for this lotto has expired.'];
            }
            else {
//                echo 89899; exit;
                if ($request->all() != null) {
                    $validation = UserLotto::validate($request->all());
                    if ($validation->fails()) {
                        $response = ['status' => 0, 'error' => $validation->errors(), 'data' => 'Errors found.'];
                    } else {

                        $req = $request->all();
                        $cart = json_decode($req['cart']);
                        $totalTickets = 0;
                        foreach ($cart as $cartValue) {
                            $totalTickets += $cartValue->ticket_purchased;
                        }
//                dd($totalTickets);
                        $request->total_amount = $totalTickets * env("TICKET_PRICE");

                        if ($request->is_tournament) {

                            $userTournament = TournamentUser::where('tournament_id', $request->tournament_id)
                                ->where('user_id', Auth::user()->id)
                                ->first();
                            $check_initial_balance = $userTournament->balance;
                        } else {
                            $check_initial_balance = Auth::user()->main_balance;
                        }
                        if ($check_initial_balance >= $request->total_amount) {
                            if ((($check_initial_balance / 100) * 25) >= $request->total_amount) {
                                $lotto_name = $request->lotto_name;
                                $lotto_type = $request->lotto_type;
                                $resultNumber = null;
                                $win = $resultAmount = 0;

                                if ($lotto_name == 'binary') {
                                    $resultNumber = random_int(0, 1);
                                } elseif ($lotto_name == 'dice') {
                                    $resultNumber = random_int(1, 6);
                                } else {
                                    $resultNumber = random_int(0, 9);
                                }

                                $lotto_id = Lotto::where('type', $lotto_name)->where('method', $lotto_type)->pluck('id')->first();

                                foreach ($cart as $cartValue) {
                                    $initial_balance = 0;
                                    $prize = 0;
                                    if ($request->is_tournament) {
                                        $userTournament = TournamentUser::where('tournament_id', $request->tournament_id)
                                            ->where('user_id', Auth::user()->id)
                                            ->first();
                                        $initial_balance = $userTournament->balance;
                                        $initial_balancett = $userTournament->balance;
                                    } else {
                                        $initial_balance = user::where('id', Auth::user()->id)->pluck('main_balance')->first();
                                    }
                                    $resultAmount = 0;

                                    if ($lotto_type == 'instant') {
                                        if ($cartValue->draw_number == $resultNumber) {
                                            $totalWinningTickets = $cartValue->ticket_purchased;
                                            if ($lotto_name == 'binary') {
                                                $resultAmount = ($totalWinningTickets * env("BINARY_PRICE"));
                                            } elseif ($lotto_name == 'dice') {
                                                $resultAmount = ($totalWinningTickets * env("DICE_PRICE"));
                                            } else {
                                                $resultAmount = ($totalWinningTickets * env("DECIMAL_PRICE"));
                                            }
                                        }

                                    }

                                    $ticket_amount = $cartValue->ticket_purchased * env("TICKET_PRICE");
                                    $final_balance = $initial_balance - $ticket_amount;
                                    $draw_number = $cartValue->draw_number;
                                    $ticket_purchased = $cartValue->ticket_purchased;
                                    $result = 'pending';
                                    $description = 'daily lotto ticket amount';
                                    if ($lotto_type == 'instant') {
                                        if ($resultAmount > 0) {
                                            $final_balance = $final_balance + $resultAmount;
                                            $result = 'win';
                                            $prize = $resultAmount;
                                            $description = 'Instant lotto prize and ticket amount';
                                        } else {
                                            $result = 'lost';
                                            $description = 'Instant lotto ticket amount';
                                        }
                                    }

                                    $user = User::whereId(Auth::id())->first();

                                    /*Handling game before expiration of tournament*/


                                    /*Handling game before expiration of tournament*/
                                    if ($request->is_tournament) {

                                        $userTournament = TournamentUser::where('tournament_id', $request->tournament_id)->where('user_id', Auth::user()->id)->first();
                                        $userTournament->balance = $final_balance;
                                        $userTournament->save();

                                        /*if ($request->is_tournament == 1 && strtotime($tournament_end_date) <= strtotime($lotto_end_date)) {

                                            dd('asdddd');

                                            $response = ['status' => 'fail', 'message' => 'Oops! Sorry'. Auth::user()->firstname. 'You can not play this game.'];

                                        }else{ }*/

                                    } else {
                                        $user->main_balance = $final_balance;
                                    }
                                    $user->save();
                                    $method = $request->lotto_type;
                                    $details = array_merge($request->all(), array('user_id' => Auth::user()->id, 'initial_balance' => $initial_balance, 'final_balance' => $final_balance, 'lotto_id' => $lotto_id, 'result' => $result, "method" => $method, 'lotto_type' => $request->lotto_name, "draw_number" => $draw_number, "result_number" => $resultNumber, "total_amount" => $ticket_amount, "ticket_purchased" => $ticket_purchased, 'prize' => $prize, 'description' => $description));


                                    $complete_details[] = $details;
                                    $final_complete_details = $this->createUserLottoDetails($complete_details,$request->lotto_name,$method);
                                    $userLotto = UserLotto::create($details);

                                    if ($userLotto != null) {
                                        if ($method == 'instant') {
                                            $finalInstantDetails = $final_complete_details;
                                            if ($result == 'win') {
                                                $win = 1;
//                                        echo json_encode($finalInstantDetails);

                                                $winData = ['status' => 'success', 'message' => 'You won the game', 'number' => $resultNumber, 'data' => $final_complete_details];
                                            } else {
                                                $response = ['status' => 'success', 'message' => 'Sorry, You lost the game.', 'number' => $resultNumber, 'data' => $final_complete_details];
                                            }

                                        } else {
//                                    $response = ['status' => 'success', 'data' => 'User lotto created successfully.'];
                                            $response = ['status' => 'success', 'message' => 'User lotto created successfully.', 'data' => $final_complete_details];
                                        }
                                    } else {
                                        $response = ['status' => 'fail', 'data' => 'Something went wrong, Try again.'];
                                    }


                                }
                                if (isset($finalInstantDetails) && $finalInstantDetails) {


                                    $winData['data'] = $finalInstantDetails;
//                            print_r($winData['data']);

                                    $get_winning_value = array_filter($winData['data']['win_prize_arr']);
                                    $key_of_win_val = key($get_winning_value);

                                    if (isset($get_winning_value) && count($get_winning_value) > 0) {


                                        $get_winning_value = array_values($get_winning_value);

                                        $winData['data']['win_prize'] = $get_winning_value[0];

                                        $total_ticket_purchased_by_user = $winData['data']['total_ticket_purchased'];
                                        $total_amount_of_ticket = $winData['data']['total_amount_of_ticket'];
//                                $winData['data']['total_final_balance'] += $get_winning_value[0]*$count_of_win_ticket;
                                        $winData['data']['total_final_balance'] = ($winData['data']['initial_balance'] - $total_amount_of_ticket) + ($get_winning_value[0]);

                                    } else {
                                        $winData['data']['win_prize'] = 0;

                                    }
//                            dd($winData['data']['win_prize']);
//                            dd($finalInstantDetails);

                                    $all_winning_prizes = $winData['data']['win_prize'];
                                }
                                if ($win == 1) {
//                            if(isset($finalInstantDetails) && $finalInstantDetails){
//                                $winData['data'] = $finalInstantDetails;
//                            }
                                    $response = $winData;
                                }
                            } else {
//                        $response = ['status' => 'fail', 'data' => 'Ticket amount can not be more than 25% of main amount.'];
                                $response = ['status' => 'fail', 'message' => 'Ticket amount can not be more than 25% of main amount.'];
                            }
                        } else {
                            $response = ['status' => 'fail', 'message' => 'Ticket amount can not be more than available amount.'];
                        }
                    }
                } else {
                    $response = ['status' => 'fail', 'data' => 'No data sent'];
                }
//        dd($complete_details);
            }
            return response()->json($response);

        } catch (\Exception $e) {
            return \Response::json(['status' => 503, 'message' => $e->getMessage(), 'line' => $e->getLine(), 'file' => $e->getFile()]);
        }
    }

    public function createUserLottoDetails($complete_details,$lotto_name,$method)
    {
        date_default_timezone_set("Asia/Kolkata");

        $item = [];
        $total_ticket_purchased = 0;
        $total_initial_balance = 0;
        $total_amount_of_ticket = 0;
        $total_final_balance = 0;
        $win_prize = 0;
//        $win_prize_arr =[];

        foreach ($complete_details as $complete_detail) {
            $kk[] = $complete_detail['initial_balance'];
            $total_initial_balance = $kk[0];


//            $total_initial_balance = $complete_detail['initial_balance'];
            $total_final_balance = $complete_detail['final_balance'];
            $total_ticket_purchased += $complete_detail['ticket_purchased'];
            $total_amount_of_ticket += $complete_detail['total_amount'];
            $win_prize = $complete_detail['prize'];
            $win_prize_arr[] = $complete_detail['prize'];
            $specific_ticket_count_arr[] = $complete_detail['ticket_purchased'];

        }
        $item['total_ticket_purchased'] = $total_ticket_purchased;
        $item['initial_balance'] = $total_initial_balance;
        $item['total_amount_of_ticket'] = $total_amount_of_ticket;
        $item['total_final_balance'] = $total_initial_balance - ($total_ticket_purchased * 11) + $win_prize;
        $kk[0] = $item['total_final_balance'];
        $item['win_prize'] = $win_prize;
        $item['win_prize_arr'] = $win_prize_arr;
        $item['specific_count_ticket_arr'] = $specific_ticket_count_arr;
        $item['time'] = date('Y-m-d H:i:s');
        $item['lotto_name'] = $lotto_name;
        $item['lotto_method'] = $method;

        return $item;
    }

    public function getUserLotto(Request $request)
    {
        try {
            $user_id = Auth::user()->id;
            $userlottos = UserLotto::with(['lotto' => function ($query) {
                $query->select('name as lotto_name');
            }])->where('user_id', $user_id)->select('*', DB::raw("DATE_FORMAT(created_at,'%d %b %Y %h:%i %p') AS game_time"))->orderBy('id', 'DESC')->get();
            $lottoData = [];
            /*foreach($userlottos as $userlotto)
            {
                $draw_number = json_decode($userlotto->draw_number);
                if(count($draw_number)>0)
                {
                    foreach($draw_number as $draw_num)
                    {
                        $userlottoData = $userlotto;
                        $userlottoData->draw_number = $draw_num->draw_number;
                        $userlottoData->ticket_purchased = $draw_num->ticket_purchased;
                        $lottoData[] = $userlottoData;
                    }
                }
            }*/


            if ($userlottos) {
                $response['status'] = 'success';
                //  $response['data'] = (object)$lottoData;
                $response['data'] = $userlottos;
            } else {
                $response['status'] = 'fail';
                $response['data'] = array();
            }
            return response()->json($response);
        } catch (\Exception $e) {
            return \Response::json(['status' => 503, 'message' => $e->getMessage(), 'line' => $e->getLine(), 'file' => $e->getFile()]);
        }
    }
}
