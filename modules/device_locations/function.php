<?php

    if (isset($frmdata['clearsearch']))
    {
        unset($frmdata);
        unset($_SESSION['pageNumber']);
        $CFG->template = "rigs/list.php";
    }
    
	// Add New Device Location
    if (isset($frmdata['add_location']))
    {
		$err = '';
		if (trim($frmdata['location_name']) == '')
		{
            $err .= "Please enter location name .<br>";
			$frmdata['location_name']='';
        }
		else
		{
			if(validatename($frmdata['location_name'])== true)
			{
				$frmdata['location_name']= $frmdata['location_name'];
			}
			else
			{
				$err .= "Only letters and white space allowed location name field.<br>";
			}
		}
		
		if (trim($frmdata['location_description']) == '')
		{
            $err .= "Please enter location description .<br>";
			$frmdata['location_description']='';
        }
		else
		{	
			//if(trim(validatename($frmdata['location_description']))!= true)
			/* if(validatename($frmdata['location_description'])== true)
			{
				$frmdata['location_description']= $frmdata['location_description'];
			}
			else
			{
				$err .= "Only letters and white space allowed in device location description field.<br>";
			}*/
                        $frmdata['location_description']= $frmdata['location_description'];
		}
		if ($err == '')
        {
			$data = array('location_name'=> $frmdata['location_name'],'location_description'=>$frmdata['location_description'],'company_id'=>$_SESSION['company_id']);
			
			if ($DBFilter->InsertRecord('device_locations', $data))
            {
				
//				$lid=mysql_insert_id();
				
				$_SESSION['success'] = "Location added successfully.";
                Redirect(CreateURL('index.php', 'mod=device_locations'));
                die();
            }
			else
            {
				$_SESSION['error'] = "<font color=red>Due to some reasion record could not be saved.</font>";
            }
        }
        else
        {
            $_SESSION['error'] = "<font color=red>" . $err . "</font>";
        }
        
    }
    elseif (isset($frmdata['update'])) //Update Location
    {
     // print_r($frmdata);
	   $err = '';
		if (trim($frmdata['location_name']) == '')
		{
            $err .= "Please enter location field.<br>";
			$frmdata['location_name']='';
        }
		else
		{
			if(validatename($frmdata['location_name'])== true)
			{
				$frmdata['location_name']= $frmdata['location_name'];
			}
			else
			{
				$err .= "Only letters and white space allowed in device location name field.<br>";
			}
		}
		if($frmdata['is_active']=='N')
		{
			$devices = $DBFilter->SelectRecord('device',"location_id ='$id' and is_deleted='N' and is_active='Y'");
			if($devices)
			{
				$err.= "Device location is in use. It can not be inactive";
			}
			else 
			{
				$frmdata['is_active']= $frmdata['is_active'];
			}
			
		}
		if (trim($frmdata['location_description']) == '')
		{
            $err .= "Please enter location description .<br>";
			$frmdata['location_description']='';
        }
		else
		{
			if(validatename($frmdata['location_description'])== true)
			{
				$frmdata['location_description']= $frmdata['location_description'];
			}
			else
			{
				$err .= "Only letters and white space allowed In device location description field.<br>";
			}
		}
		
		//=============
		
		if ($err == '')
        {
			
			$data = array('location_name'=>$frmdata['location_name'],'location_description'=>$frmdata['location_description'],'company_id'=>$_SESSION['company_id'],'is_active'=>$frmdata['is_active']);
		/*	
		 print_r($data );
 		 exit;
		*/
          $DBFilter->UpdateRecord('device_locations', $data, "location_id ='$id'");
          //  print_r($data );exit;
			 $_SESSION['success'] = "Device location updated successfully.";
             Redirect(CreateURL('index.php', 'mod=device_locations'));
             die();
        }
        else
        {
            $_SESSION['error'] = $err;
        }
        
    }

    //============================================================================
        
    //Delete Multiple or Other Actions added by : Neha Pareek
        
    //============================================================================
    if($frmdata['actions'])
	{
		$ids = $frmdata['chkbox'];
		$action = $frmdata['actions'];
		$cond = "location_id=";
		$total_chk = count($frmdata['chkbox']); 
		for($i = 0; $i < $total_chk; $i++) //Create Array of multiple checkbox id's
		{
			$loc_id = $ids[$i];
			$loc = $DBFilter->SelectRecords('device',"is_deleted ='N'");
			$rows= $DBFilter->SelectRecords('device', "location_id='".$loc_id."' and is_deleted = 'N' and company_id='".$_SESSION['company_id']."'");
			//$rows= $DBFilter->SelectRecords('device', "location_id='".$loc_id."' and is_deleted = 'N' and is_active='Y' and company_id='".$_SESSION['company_id']."'");
			
			if ($rows)
			{
				$_SESSION['error'] = 'Device location is in use. Please delete related device first.';
				redirect(CreateURL('index.php', 'mod=device'));
				die();
			}
		}
		if($frmdata['chkbox']=='' && isset($frmdata['actions']))
		{
			$_SESSION['error'] = 'Please select atleast one device location';
		}
		else 
		{		
		multipleAction($ids,'device_locations',$action,$cond);
		$_SESSION['success'] = 'Device locations has been '.$action.'d successfully.';
		Redirect(CreateURL('index.php', 'mod=device_locations'));
		die();
		}
	}

    //============================================================================
        
    //Delete the Device Location added by : Neha Pareek
        
    //============================================================================
    elseif ($do == 'del')
    {
    	//$devices = $DBFilter->SelectRecord('device',"location_id ='$id' and is_deleted='N' and is_active='Y'");
    	$devices = $DBFilter->SelectRecord('device',"location_id ='$id' and is_deleted='N'");
    	$device_id = $devices->device_id;
       	$feedback = $DBFilter->SelectRecords('feedback',"device_id='$device_id' and is_deleted='N'");
       //$feedback = $DBFilter->SelectRecords('feedback',"device_id='$device_id' and is_deleted='N' and is_active='Y'");
    	
		//print_r($feedback);print_r($devices);exit;
		if($devices || $feedback)
		{	
			if($devices)
			{
				$_SESSION['error'] = 'Device location is in use. Please delete the related device first.';
				Redirect(CreateURL('index.php', 'mod=device'));
				die();
			}
			else if($feedback)
			{
				$_SESSION['error'] = 'Device location is in use. Please delete the related feedbacks first.';
				Redirect(CreateURL('index.php', 'mod=feedback'));
				die();
			}
			else 
			{
				$_SESSION['error'] = 'Device location is in use. Please delete the related device and feedbacks first.';
				Redirect(CreateURL('index.php', 'mod=device_location'));
				die();
			}
		}
		else
		{
			$data = array('is_deleted'=>'Y','is_active'=>'N');
			$DBFilter->UpdateRecord('device_locations', $data, "location_id ='$id'");
         	$_SESSION['success'] = 'Device location has been deleted successfully.';
        	Redirect(CreateURL('index.php', 'mod=device_locations'));
	        die();
		} 
}

?>
