<?php 
 $do = (isset($_REQUEST['do']) ? $_REQUEST['do'] : 'list'); 
    $id = (isset($_REQUEST['id']) ? $_REQUEST['id'] : '');
	
	$userAlowedActions = array('view','add','list','edit','del');
	
	if(!in_array($do,$userAlowedActions) and $_SESSION['usertype'] == 'user')	
	{
?> 
		 <script type="text/javascript">
			 document.location.href="<?php echo 'index.php?mod=device_locations&do=list' ?>";
	     </script>
   <?php
	   
	}
//$company= $DBFilter->SelectRecords('device_locations');
//$admin_company = $DBFilter->SelectRecord('device_locations',"location_id=".$_SESSION['location_id']);	
 include_once('function.php');
    switch ($do)
    {	
		case 'add':
			if($_REQUEST['call']=='API')
			{
				$result = registerDevice($frmdata);
			}
			$CFG->template = "device_locations/add.php";
            break;
			
		case 'edit':
            $Row= $DBFilter->SelectRecord('device_locations', "location_id='$id'");
			$CFG->template = "device_locations/edit.php";
            break;
		
        case 'list':
			//echo "hello";
			//exit;
            if ($frmdata['keyword'])
            {
                $frmdata['pageNumber'] = 1;
            }
            PaginationWork();
            $totalCount= 0;
            $Row  = getdevicelocations($totalCount);
            $CFG->template = "device_locations/list.php";
            break;
				
		case 'view':
			if($_REQUEST['call']=='API')
			{
				$result = getAllDeviceLocation($frmdata);
			}
				$Row = $DBFilter->SelectRecord('device_locations', "location_id='$id'");
				$CFG->template = "device_locations/view.php";
				// echo '<pre>';print_r($Row);print_r($faults);
			break; 
    }
    
    function getdevicelocations(&$totalCount)
    {
        global $DB, $frmdata, $DBFilter;
		
        $query = " SELECT * from device_locations";
		$where = " where is_deleted = 'N' ";
		//$where = " where is_deleted = 'N' and is_active = 'Y'";
		if (trim($frmdata['keyword']) != '')
        {
			$frmdata['keyword'] = trim($frmdata['keyword']);
		
            $where .= " and (location_name like  '%".$frmdata['keyword']."%' or location_description like  '%".$frmdata['keyword']."%')";
				
			$_SESSION['keywords'] = 'Y';
        }
		else
		{
			$where.= '';
		}
		
		if($_SESSION['company_id']!='' && $_SESSION['usertype']!='super_admin')
		{
			$company_id = $_SESSION['company_id'];
			
			$where .=" and company_id='$company_id'";
		
		}
        if (isset($frmdata['orderby']) && $frmdata['orderby'] != '')
        {
            $order .= " order by " . $frmdata['orderby'];
        }
        else
        {
            $order .= " order by location_id desc";
        }
		
         $query = $query.$where.$order; 
	//echo $query;
        $result = $DBFilter->RunSelectQueryWithPagination($query,$totalCount);
        return $result;
    } 
   
   /*get company's devices using webservice */
   function getAllDeviceLocation($data)
   {
	   global $DB, $DBFilter;
	   $access_key = $DBFilter->SelectRecord('login_detail',"access_key='".$data['access_key']."'");

	   $user = $DBFilter->SelectRecord('users',"user_id=".$access_key->user_id);
	   //print_r($access_key);
	  // print_r($user);exit;
	   
       if($access_key)	  
	   {
		   $Rows= $DBFilter->SelectRecords('device_locations',"is_deleted='N' and is_active='Y' and company_id=".$user->company_id);
		   $devices = $DBFilter->SelectRecords('device',"is_deleted='N' and company_id=".$user->company_id);
		   if($Rows != '')
		   {	
				if($devices != '')
				{
					//Create Array Of Locations Fetched via Location Table for Particular Company
					if(count($Rows[0]) >0)
					{	$loc_arr1 = array();
						for($k = 0; $k < count($Rows[0]); $k++)
						{
							$loc_arr1[] = $Rows[0][$k]->location_id;
							//$loc_arr1['name'] = $Rows[0][$k]->location_name;
						}
					}
					//Create Array Of Locations Fetched via Device Table for Particular Company
					if(count($devices[0]) >0)
					{	$loc_arr2 = array();
						for($k = 0; $k < count($devices[0]); $k++)
						{
							$loc_arr2[] = $devices[0][$k]->location_id;
						}
					}
					 $locations = array();
					 $free_loc = array();
					 //Free Location Array
					 $free_loc = array_diff(array_unique($loc_arr1),array_unique($loc_arr2));
					//print_r($free_loc);exit;
					 $locs = implode(",",$free_loc);
					 if(count($free_loc) > 0)
					 {
						$Row = $DBFilter->SelectRecords('device_locations',"is_deleted='N' and is_active='Y' and location_id IN (".$locs.") and company_id=".$user->company_id);
						for($i=0;$i<count($Row[0]);$i++)
						{
							$locations[$i]['location_id']= $Row[0][$i]->location_id;
							$locations[$i]['location_name']= $Row[0][$i]->location_name;
						}
					 }
				}
				else
				{
					 $locations = array();
					for($i=0;$i<count($Rows[0]);$i++)
					{
						$locations[$i]['location_id']= $Rows[0][$i]->location_id;
						$locations[$i]['location_name']= $Rows[0][$i]->location_name;
					}
					
				}
				//echo "<pre>";print_r($locations);exit;
				if($locations)
				{
					$final_data = array('result'=>$locations,'status'=>1);
					echo json_encode($final_data);
					exit;
				}
				else
				{
					$final_data = array('result'=>'No free location found.','status'=>0);
					echo json_encode($final_data);
					exit; 
				}	
			}
			else
			{
				$final_data = array('result'=>'No location found.','status'=>0);
				echo json_encode($final_data);
				exit; 
			}
	   }
	   else
	   {
			$final_data = array('result'=>'Access key is expired.Please login again.','status'=>2);
			echo json_encode($final_data);
			exit; 
	   }
	}
   /* register device using webservice */
   function registerDevice($data)
   {
	    global $DB,$DBFilter;
		
		$access_key = $DBFilter->SelectRecord('login_detail',"access_key='".$data['access_key']."'");
		$user = $DBFilter->SelectRecord('users',"user_id=".$access_key->user_id);
       $company = $user->company_id;
       $user_id = $user->user_id;
       $assign_user = $DBFilter->SelectRecord('feedback_form_settings',"company_id=".$company);

//       if ($user->role_id == 2 && $assign_user->assign_users == 'N' ){
       if ($user->role_id == 2 ){

           $cleaner_data = $DBFilter->RunSelectQuery("select * from users where role_id ='2' and is_deleted = 'N' and is_active = 'Y' and company_id=".$company);

       }else{
           $cleaner_data = $DBFilter->SelectRecords("users","role_id ='3' and is_deleted = 'N' and is_active = 'Y' and assigned_to = '".$user_id."' and company_id=".$company);

       }

	   if($user!='') { // This is for supervisor
           if ($assign_user->assign_users == 'Y'){
               /* checks user's role */
               if ($user->role_id == '4') {

                   if (count($cleaner_data[0]) > 0)//Check For Active Cleaners
                   {
                       $company_user_data = array('user_id' => $user->user_id, 'company_id' => $user->company_id, 'role_id' => $user->role_id);

                       $insert_data = array('device_name' => $data['device_id'], 'location_id' => $data['location_id'], 'device_status' => 'Active', 'company_id' => $company_user_data['company_id'], 'user_id' => $company_user_data['user_id']);

                    $insert_device = $DBFilter->InsertRecord('device',$insert_data);
                       if ($insert_device) {
                           $device_ids = $DBFilter->SelectRecord("device", "company_id ='" . $company . "' and is_deleted = 'N' and is_active = 'Y' and user_id='" . $user_id . "' order by device_id desc");
                           $device_id = $device_ids->device_id;

                           $final_data = array('result' => 'Device registered successfully.', 'Device_id' => $device_id, 'status' => 1);
                           echo json_encode($final_data);
                           exit;
                       } else {
                           $final_data = array('result' => 'Device registration failed. Please check.', 'status' => 0);
                           echo json_encode($final_data);
                           exit;
                       }
                   }
                   else {
                           $final_data = array('result' => 'Device registration failed. No staff found for this supervisor.', 'status' => 0);
                           echo json_encode($final_data);
                           exit;
                   }
               } else {
                   $final_data = array('result' => 'You are not allowed to register any device.', 'status' => 0);
                   echo json_encode($final_data);
                   exit;
               }
       }else{ // This is for company admin
               if($user->role_id == '2'){

                   if (count($cleaner_data[0]) > 0)//Check For Active Admin
                   {
                       $company_user_data = array('user_id' => $user->user_id, 'company_id' => $user->company_id, 'role_id' => $user->role_id);

                       $insert_data = array('device_name' => $data['device_id'], 'location_id' => $data['location_id'], 'device_status' => 'Active', 'company_id' => $company_user_data['company_id'], 'user_id' => $company_user_data['user_id']);

                       $insert_device = $DBFilter->InsertRecord('device',$insert_data);
                       if ($insert_device) {
                           $device_ids = $DBFilter->SelectRecord("device", "company_id ='" . $company . "' and is_deleted = 'N' and is_active = 'Y' and user_id='" . $user_id . "' order by device_id desc");
                           $device_id = $device_ids->device_id;

                           $final_data = array('result' => 'Device registered successfully.', 'Device_id' => $device_id, 'status' => 1);
                           echo json_encode($final_data);
                           exit;
                       } else {
                           $final_data = array('result' => 'Device registration failed. Please check.', 'status' => 0);
                           echo json_encode($final_data);
                           exit;
                       }
                   }
                   else {
                       $final_data = array('result' => 'Device registration failed. You are not an active member.', 'status' => 0);
                       echo json_encode($final_data);
                       exit;
                   }
               }else{
                   $final_data = array('result' => 'You are not allowed to register any device.', 'status' => 0);
                   echo json_encode($final_data);
                   exit;
               }
           }
		}
		else 
		{
			$final_data = array('result'=>'Access key is expired. Please login again.','status'=>2);
				echo json_encode($final_data);
				exit; 
			
		}
   }

    include_once(CURRENTTEMP . "/index.php");
?>
