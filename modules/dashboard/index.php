<?php 
/*****************Developed by :- Jaishree Sahal
	                Date         :- 25-Aug-2015
					Module       :- Dashboard
					Purpose      :- Entry file for any action, call in Dashboard module
***********************************************************************************/

defined("ACCESS") or die("Access Restricted");
$DB = new DBFilter();

include_once('class.dashboard.php');
$DashboardOBJ = new Dashboard();
$do = (isset($_REQUEST['do']) ? $_REQUEST['do'] : 'list');


switch($do)
{
	default:
	case 'view':

		$Row= $DBFilter->SelectRecord('company_detail', "company_id=".$_SESSION['company_id']);
		
		$users = $DBFilter->RunSelectQuery("select * from users as u 
				left join roles as r on u.role_id = r.role_id
				where u.company_id=".$_SESSION['company_id']);
			
		$CFG->template = "dashboard/view.php";
		break;	
	
}

include(CURRENTTEMP."/index.php");
exit;
?>