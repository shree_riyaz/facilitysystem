<?php 
/**
 * 	@author	:	Ashwini Agarwal
 * 	@desc	:	Methods to get dashboard data
 */
class Dashboard
{
	/******************************************************************
	Des: A function to count candidate
	******************************************************************/
	function countCandidate()
	{
		global $DB,$frmdata;
		
		$totalCandidate = $DB->SelectRecord('candidate', '', 'count(*) as candidate');
		//print_r($totalCandidate);
		return $totalCandidate;
	}

	function countStreamCandidate()
	{
		global $DB,$frmdata;
		$query = "	select 	e.exam_name, e.id as exam_id,
							count(*) as candidate
							from candidate as c 
							join exam_candidate as ec on ec.candidate_id=c.id
							join examination as e on ec.exam_id=e.id
							group by e.id
							order by candidate desc";
			
		$result = $DB->RunSelectQuery($query);
		
		return $result;
	}
	
	/******************************************************************
	Des: A function to count total question
	******************************************************************/
	function countQuestion()
	{
		global $DB,$frmdata;
		
		//$query="select count(*) as que from question";
				
		$query="select count(*) as que, sum(if(question_level='B', 1, 0)) as totalbeg,
				sum(if(question_level='I', 1, 0)) as totalinter, sum(if(question_level='H', 1, 0)) as totalhigh
				from question";
		$result = $DB->RunSelectQuery($query);
		return $result;
	}
	
	/******************************************************************
	Des: A function to get question acc. to stream
	******************************************************************/
	function countSubjectQuestion()
	{
		global $DB,$frmdata;
	
		$query = "	select 	sub.id as subject_id,
					sub.subject_name, 
					count(*) as question, 
					sum(if(question_level='B', 1, 0)) as beg,
					sum(if(question_level='I', 1, 0)) as inter, 
					sum(if(question_level='H', 1, 0)) as high 
					from question as q 
					left join subject as sub on q.subject_id=sub.id 
					group by q.subject_id 
					order by question desc";
			
		$result = $DB->RunSelectQuery($query);
		
		return $result;
	}
	
	/******************************************************************
	Des: A function to count finished test
	******************************************************************/
	function countfinishedTest()
	{
		global $DB,$frmdata;
		
		$query="select count(*) as finishtest from test where test_date<now()";
				
		$result = $DB->RunSelectQuery($query);
		return $result;
	}
	
	/******************************************************************
	Des: A function to count remaining test
	******************************************************************/
	function countremainingTest()
	{
		global $DB,$frmdata;
		
		$query="select count(*) as test from test where test_date>=now()";
				
		$result = $DB->RunSelectQuery($query);
		return $result;
	}
	
	/******************************************************************
	Des: A function to get last test detail
	******************************************************************/
	function lastTestdetail()
	{
		global $DB,$frmdata;
		$query = "	select test.id, test_name, test_date, test_date_end, e.exam_name 
					from test join examination as e on e.id = test.exam_id
					where test_date<now()
					order by test_date desc limit 0,1";
					
		$result = $DB->RunSelectQuery($query);
		return $result;
	}
	
	/******************************************************************
	Des: A function to get test result
	******************************************************************/
	function TestResult($testid)
	{
		global $DB,$frmdata;
		$query = "select count(*) as totalcandidate, sum(if(cth.result='P', 1, 0)) as pass,
					sum(if(cth.result='F', 1, 0)) as fail
					from `candidate_test_history` as cth where cth.test_id='".$testid."'";
		//exit;			
		$result = $DB->RunSelectQuery($query);
		return $result;
	}
	
	/******************************************************************************************
	 * 		Author		:	Ashwini Agarwal
	 * 		Date		:	March 6, 2012
	 * 		Description	:	To find out total appeared candidates in last test.
	 */
	function lastTestResultTotal(&$totalCount)
	{
		global $DB,$frmdata;
		$query = $this->lastTestQuery();
		$_SESSION['queryForTotal'] = $query;
		$result = $DB->RunSelectQueryWithPagination($query,$totalCount);
		return $result;
	}
	
	/******************************************************************************************
	 * 		Author		:	Ashwini Agarwal
	 * 		Date		:	March 6, 2012
	 * 		Description	:	To find out total passed candidates in last test.
	 */
	function lastTestResultPass(&$totalCount)
	{
		global $DB,$frmdata;
		$query = $this->lastTestQuery('P');
		$_SESSION['queryForPass'] = $query;
		$result = $DB->RunSelectQueryWithPagination($query,$totalCount);
		return $result;
	}
	
	/******************************************************************************************
	 * 		Author		:	Ashwini Agarwal
	 * 		Date		:	March 6, 2012
	 * 		Description	:	To find out total failed candidates in last test.
	 */
	function lastTestResultFail(&$totalCount)
	{
		global $DB,$frmdata;		
		$query = $this->lastTestQuery('F');
		$_SESSION['queryForFail'] = $query;
		$result = $DB->RunSelectQueryWithPagination($query,$totalCount);
		return $result;
	}
	
	function lastTestQuery($for = '')
	{
		global $frmdata;
		$query = " select cand.*, cth.marks_obtained as marks,
					if(cth.result='P', 'PASS', 'FAIL') as result
							
					from candidate as cand
					JOIN candidate_test_history as cth ON cand.id = cth.candidate_id
					
					where cth.test_id= 
						(	select id from test 
							where test_date<now()
							order by test_date desc 
							limit 0,1	) and";
		
		if($for) $query.= " (cth.result = '$for') AND";
		
		if(isset($frmdata['name']) && $frmdata['name'] !='')
		{
			$query.=" (";
			$query.=" cand.first_name like '%".$frmdata['name']."%' or 
						cand.last_name like '%".$frmdata['name']."%' or
						(concat(trim(cand.first_name),' ',trim(cand.last_name)) like '%".addslashes($frmdata['name'])."%')";
			$query.=")";
			$query.=" and";	
		}
		
		$query=substr($query,0,(strlen($query)-4));
		
		if(isset($frmdata['orderby']) && $frmdata['orderby']!='' )
		{
			$query.=" order by ".$frmdata['orderby'];
		}	
		else
		{
			$query.=' order by cand.id desc';
		}
		
		return $query;
	}
	
	function getQuestionXml()
	{
		$totalQue = $this->countSubjectQuestion();
		$subjects = array();
		
		$subjectCategoty = "<categories>";
		$biginnerData = "<dataset seriesName='Beginner'>";
		$intermediateData = "<dataset seriesName='Intermediate'>";
		$higherData = "<dataset seriesName='Higher'>";
		
		foreach($totalQue as $question)
		{
			if(in_array($question->subject_id, $subjects)) continue;
			
			$subjectCategoty .= "<category label='".ucwords($question->subject_name)."' />";
			
			$biginnerData .= "<set value='$question->beg' />";
			$intermediateData .= "<set value='$question->inter' />";
			$higherData .= "<set value='$question->high' />";
			
			$subjects[$question->subject_id] = $question->subject_name;
		}
		
		$biginnerData .= "</dataset>";
		$intermediateData .= "</dataset>";
		$higherData .= "</dataset>";
		$subjectCategoty .= "</categories>";
		
		$FCExporter = ROOTURL . "/lib/FusionCharts/ExportHandlers/JavaScript/index.php";
		$xml = "<chart caption='Question Bank' xAxisName='Subjects' yAxisName='Questions'
					bgColor='E6E6E6,F0F0F0' bgAlpha='100,50' bgRatio='50,100' bgAngle='270' 
					showNames='1' useEllipsesWhenOverflow='0' showBorder='1' exportEnabled='1' 
					html5ExportHandler='$FCExporter' exportFileName='Question_Bank'>";

		$xml .= $subjectCategoty;
		$xml .= $biginnerData;
		$xml .= $intermediateData;
		$xml .= $higherData;
		$xml .= '</chart>';
		
		header('Cache-control: no-cache' and 'pragma: no-cache');
		header('Content-type: text/xml');
		echo $xml;
		exit;
	}
	
	function getTestXml()
	{
		$finishedTest = $this->countfinishedTest();
		$remainingTest = $this->countremainingTest();
	
		$FCExporter = ROOTURL . "/lib/FusionCharts/ExportHandlers/JavaScript/index.php";
		$xml = "<chart caption='Test' showLegend='1' showPercentValues='0' 
					bgColor='E6E6E6,F0F0F0' bgAlpha='100,50' bgRatio='50,100' bgAngle='270' 
					showNames='1' useEllipsesWhenOverflow='0' showBorder='1' exportEnabled='1' 
					html5ExportHandler='$FCExporter' exportFileName='Test'>
					
				   <set label='Conducted Test' value='".$finishedTest[0]->finishtest."' />
				   <set label='Remaining Test' value='".$remainingTest[0]->test."' />
				</chart>";
		
		header('Cache-control: no-cache' and 'pragma: no-cache');
		header('Content-type: text/xml');
		echo $xml;
		exit;
	}
	
	function getCandidateXml()
	{
		$candidates = $this->countStreamCandidate();
		
		$FCExporter = ROOTURL . "/lib/FusionCharts/ExportHandlers/JavaScript/index.php";
		$xml = "<chart caption='Students' showLegend='1' showPercentValues='0'
					bgColor='E6E6E6,F0F0F0' bgAlpha='100,50' bgRatio='50,100' bgAngle='270' 
					showNames='1' useEllipsesWhenOverflow='0' showBorder='1' exportEnabled='1' 
					html5ExportHandler='$FCExporter' exportFileName='Students'>";
		
		foreach( $candidates as $candidate )
		{
			$xml .= "<set label='$candidate->exam_name' value='$candidate->candidate' />";
		}
		
		$xml .= "</chart>";
		
		header('Cache-control: no-cache' and 'pragma: no-cache');
		header('Content-type: text/xml');
		echo $xml;
		exit;
	}
	
	function getStudentLinkRequest()
	{
		global $DB;
		
		$query = 'SELECT pc.*, 
				CONCAT_WS(" ",p.first_name,p.last_name) as parent_name,
				CONCAT_WS(" ",c.first_name,c.last_name) as candidate_name 
				from parent_candidate pc
				JOIN parent p ON pc.parent_id = p.id 
				JOIN candidate c ON pc.candidate_id = c.id
				WHERE pc.is_approved = 0';
		
		return $DB->RunSelectQuery($query);
	}

	function recentlySubmittedHomework()
	{
		global $DB;
		
		
		$query = " SELECT chh.*, c.first_name, c.last_name, h.homework_name, e.exam_name " 
				." FROM candidate_homework_history chh "
				." JOIN candidate c ON c.id = chh.candidate_id "
				." JOIN examination e ON e.id = chh.exam_id "
				." JOIN homework h ON h.id = chh.homework_id "
				." GROUP BY chh.id ORDER BY chh.created_date DESC LIMIT 0,5 ";
		
		return $DB->RunSelectQuery($query);
	}
}
?>