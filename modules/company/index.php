<?php


//ini_set('display_errors',1);
//$get_data = $DBFilter->RunSelectQuery("select created_at,feedback_id,rating,option_id,field_type,company_id from feedback WHERE field_type ='face' and feedback_type='rating' and company_id='54' ORDER BY feedback_id DESC");
//
//foreach ($get_data[0] as $val) {
//    $get_data_from_option = $DBFilter->RunSelectQuery("select id from rating_options WHERE field_type='".$val->field_type."' AND title='".$val->rating."'");
//    if (isset($val->created_at)){
//        $option_id = $get_data_from_option[0][0]->id;
//        $DBFilter->RunSelectQuery("update feedback set option_id=$option_id where feedback_id= ".$val->feedback_id);
//    }
//}
//
//exit;
$permission = $_SESSION['permission'];
if ($_SESSION['usertype'] == 'super_admin') {
    $do = (isset($_REQUEST['do']) ? $_REQUEST['do'] : 'list');
} else {
    $do = (isset($_REQUEST['do']) ? $_REQUEST['do'] : 'view');
}
if ($do == 'del' && $_SESSION['usertype'] == 'super_admin')
    $permission = 'del';

$id = (isset($_REQUEST['id']) ? $_REQUEST['id'] : '');
$userAlowedActions = array($permission, 'logout');
// print_r($userAlowedActions);
if (!in_array($do, $userAlowedActions)) {
    if ($do == 'view') { ?>
        <script type="text/javascript">document.location.href = "<?php
                echo 'index.php?mod=company&do=view&id=' . $_SESSION['company_id']; ?>";
        </script>
    <?php }
    ?>
    <script type="text/javascript">
        alert('Your are not allowed to perform this action');
        document.location.href = "<?php
            echo 'index.php?mod=company&do=view&id=' . $_SESSION['company_id']; ?>";
    </script>
    <?php
    exit;
}

//$plan = $DBFilter->SelectRecords("plan");
$plan = $DBFilter->SelectRecords('plan', "is_deleted='N' and is_active='Y'  order by plan_name");// Added By Neha Pareek, Dated : 02 Nov 2015
$total_feedbacks = number_of_feedback();
$nature_type_positive = 'P';
$total_positive = like_dislike($nature_type_positive);

$nature_type_negative = 'N';
$total_negative = like_dislike($nature_type_negative);

$nature_type_neutral = 'M';
$total_neutral = like_dislike($nature_type_neutral);

include_once('function.php');
switch ($do) {
    case 'logout':
        unset($_SESSION);
        {
            @session_unset();
            @session_destroy();
        }

        $CFG->template = "company/logout.php";
        break;

    case 'add':
        $plan = $DBFilter->SelectRecords('plan', "is_deleted='N' and is_active='Y' order by plan_name");
        $cat = $DBFilter->SelectRecords('company_detail ', '*', 'order by company_name');
        $CFG->template = "company/add.php";
        break;

    case 'is_delete':
        $CFG->template = "company/modal_popup.php";
        break;

    case 'edit':
        //$plan = $DBFilter->SelectRecords('plan',"is_deleted='N' and is_active='Y'"); // Added By Neha Pareek, Dated : 02 Nov 2015
        $Row = $DBFilter->SelectRecord('company_detail', "company_id='$id'");
        $CFG->template = "company/edit.php";
        break;
    case 'view':
        //print_r($_SESSION);
        $company = $_SESSION['company_id'];
        $Row = $DBFilter->SelectRecord('company_detail', "is_active='Y' and is_deleted = 'N' and company_id=" . $_SESSION['company_id']);
        $plan = $DBFilter->SelectRecord('plan', "is_deleted='N' and is_active='Y' and plan_id='" . $Row->plan_id . "'");
        //$feedbk = $DBFilter->SelectRecords('feedback',"is_deleted='N' and is_active='Y' orderby feedback_id desc ");
        $feedbk = getFeedbacks();
        //$fault = $DBFilter->SelectRecords('fault', "fault_id= '$feedbk->feedback_id'");
        $cleaner = $DBFilter->RunSelectQuery("select * from users as u 
					left join roles as r on u.role_id = r.role_id
					where u.is_deleted = 'N' and u.is_active= 'Y' and u.role_id = '3' and u.company_id='" . $_SESSION['company_id'] . "' order by u.creation_date asc");
        $superviser = $DBFilter->RunSelectQuery("select * from users as u 
					left join roles as r on u.role_id = r.role_id
					where u.is_deleted = 'N' and u.is_active= 'Y' and u.role_id ='4' and u.company_id='" . $_SESSION['company_id'] . "' order by u.creation_date asc");
        $comp_admin = $DBFilter->RunSelectQuery("select * from users as u 
					left join roles as r on u.role_id = r.role_id
					where u.is_deleted = 'N' and u.role_id =' 2' and u.is_active= 'Y' and u.company_id='" . $_SESSION['company_id'] . "' order by u.creation_date asc");
        $devices = $DBFilter->SelectRecords('device', " is_deleted = 'N' and is_active= 'Y' and company_id='$company'");
        $users = $DBFilter->RunSelectQuery("select * from users as u 
					left join roles as r on u.role_id = r.role_id
					where u.is_deleted = 'N' and u.role_id!='1' and u.is_active= 'Y' and u.company_id='" . $_SESSION['company_id'] . "' order by u.creation_date asc");
        $r = $DBFilter->SelectRecords('roles');

        if (isset($frmdata['show_chart_submit'])) {

            $_SESSION['selected_date_range_session'] = $_POST['selected_date_range'];
            $selected_date = $_POST['selected_date_range'];

            $custom_date_range_selected = explode('-', $_SESSION['selected_date_range_session']);

            $selected_date_interval = $_POST['selected_date_range_type_hidden'];

            $split_date = explode('-', $selected_date);
            $get_start_date = trim($split_date[0]);
            $get_end_date = trim($split_date[1]);
            $start_date = date('Y-m-d', strtotime($get_start_date));
            $end_date = date('Y-m-d', strtotime($get_end_date));

            if ($selected_date_interval == 'Yesterday') {
                $_SESSION['start_date_by_interval'] = "moment().subtract(1, 'days')";
                $_SESSION['end_date_by_interval'] = "moment().subtract(1, 'days')";
            } elseif ($selected_date_interval == 'Today') {
                $_SESSION['start_date_by_interval'] = "moment()";
                $_SESSION['end_date_by_interval'] = "moment()";
            } elseif ($selected_date_interval == 'Last 7 Days') {
                $_SESSION['start_date_by_interval'] = "moment().subtract(6, 'days')";
                $_SESSION['end_date_by_interval'] = "moment()";
            } elseif ($selected_date_interval == 'Last 30 Days') {
                $_SESSION['start_date_by_interval'] = "moment().subtract(30, 'days')";
                $_SESSION['end_date_by_interval'] = "moment()";
            } elseif ($selected_date_interval == 'This Month') {
                $_SESSION['start_date_by_interval'] = "moment().startOf('month')";
                $_SESSION['end_date_by_interval'] = "moment().endOf('month')";
            } elseif ($selected_date_interval == 'Last Month') {
                $_SESSION['start_date_by_interval'] = "moment().subtract(1, 'month').startOf('month')";
                $_SESSION['end_date_by_interval'] = "moment().subtract(1, 'month').endOf('month')";
            }/*else{
                $_SESSION['start_date_by_interval'] = "moment($start_date,'MMMM D, YYYY')";
                $_SESSION['end_date_by_interval'] = "moment($end_date,'MMMM D, YYYY')";
            }*/

//            $CandidateMasterOBJ->chart_function($nameID);

            $_SESSION['start_date'] = $start_date;
            $_SESSION['end_date'] = $end_date;
        } else {
            $_SESSION['start_date'];
            $_SESSION['end_date'];
//            $end_date = date('Y-m-d');
//            $start_date = date('Y-m-d', strtotime('today - 30 days'));
        }

        /* Fault column chart feedback start*/

        $between_date = check_date_today_yesterday_or_other();

        $fault_feedback = $DBFilter->RunSelectQuery("select flt.fault_name,count(rating) as feedback_count from feedback as fd INNER JOIN faults flt on fd.fault_id = flt.fault_id where fd.is_deleted = 'N' and fd.is_active= 'Y' AND fd.rating != '' AND fd.feedback_type = 'rating'  AND $between_date  and fd.company_id='" . $company . "' GROUP BY flt.fault_name");

        $data_fault = array_values($fault_feedback[0]);
        foreach ($data_fault as $data_list_fault) {
            $get_data_for_pie_data_list_fault[] = [ucfirst($data_list_fault->fault_name), (int)$data_list_fault->feedback_count];
        }

        /*Fault column chart feedback end*/

        /*Overall column chart feedback start*/
        $overall_feedback = $DBFilter->RunSelectQuery("select fd.created_at ,count(fd.rating) as feedback_count from feedback as fd
					where fd.is_deleted = 'N' and fd.is_active= 'Y' AND (fd.created_at BETWEEN '" . $_SESSION['start_date'] . "' AND '" . $_SESSION['end_date'] . "')  AND fd.rating != '' AND fd.feedback_type = 'rating' AND fd.company_id='" . $company . "' GROUP BY fd.created_at");

        $data_overall = array_values($overall_feedback[0]);
        foreach ($data_overall as $data_list_ovellall) {
            $get_count_overall_feedback[] = $data_list_ovellall->feedback_count;
            $get_count_overall_createtion_date[] = date("d-M-Y", strtotime($data_list_ovellall->created_at));
        }

        $overall_feedback_count = json_encode($get_count_overall_feedback, JSON_NUMERIC_CHECK);
        $overall_feedback_by_date = json_encode($get_count_overall_createtion_date, JSON_NUMERIC_CHECK);

        /*Overall column chart feedback end*/

        /*Feedback Type  start*/
        $poor_feedback = $DBFilter->RunSelectQuery("select rating,count(rating) as feedback_count from feedback as fd where fd.is_deleted = 'N' and fd.is_active= 'Y' AND (fd.created_at BETWEEN '" . $_SESSION['start_date'] . "' AND '" . $_SESSION['end_date'] . "') and fd.rating != '' and fd.feedback_type ='rating' and fd.company_id='" . $company . "' GROUP BY rating ");

        $data = array_values($poor_feedback[0]);
        foreach ($data as $data_list) {
            $get_data_for_pie_feedback_type[] = [$data_list->rating, (int)$data_list->feedback_count];
        }
        /*Feedback Type end*/

        /* feedback Nature Pie chart*/
        $nature_feedback = $DBFilter->RunSelectQuery("select feedback_nature,count(rating) as feedback_count from feedback as fd where fd.is_deleted = 'N' and fd.is_active= 'Y' and fd.feedback_nature !='' and fd.rating !='' and fd.feedback_type ='rating' AND (fd.created_at BETWEEN '" . $_SESSION['start_date'] . "' AND '" . $_SESSION['end_date'] . "') and fd.company_id='" . $company . "' GROUP BY feedback_nature ");

        foreach (current($nature_feedback) as $data_list) {
            if ($data_list->feedback_nature == 'P') {
                $nature = 'Positive';
            } elseif ($data_list->feedback_nature == 'N') { $nature = 'Negative'; } else { $nature = 'Neutral'; }
            $get_data_for_pie_feedback_type_nature[] = [$nature, (int)$data_list->feedback_count];
        }
        /* feedback Nature Pie chart*/
      /* feedback survey Pie chart*/
        $survey_feedback = $DBFilter->RunSelectQuery("select rating,count(rating) as feedback_count from feedback as fd where fd.is_deleted = 'N' and fd.is_active= 'Y' and fd.field_type = 'checkbox'  AND (fd.created_at BETWEEN '" . $_SESSION['start_date'] . "' AND '" . $_SESSION['end_date'] . "') and fd.company_id='" . $company . "' GROUP BY rating ");

        foreach (current($survey_feedback) as $data_list) {
            $get_data_for_pie_feedback_type_survey[] = [ucfirst($data_list->rating), (int)$data_list->feedback_count];
        }
//                        echo '<pre>'; print_r($get_data_for_pie_feedback_type_survey); exit;

        /* feedback survey Pie chart*/

        /* Question wise start*/
        $question_details_count = is_rating_option_available();

        $company_id = $_SESSION['company_id'];
        $query_question_based_data = "select  count(*) as number_of_feedback,ro.title as title,ro.option_sequence as option_sequence,fq.title as question,feedback.feedback_nature as feedback_nature from feedback
        INNER JOIN rating_options as ro ON ro.id = feedback.option_id
        INNER JOIN feedback_questions as fq ON fq.id = feedback.feedback_question_id
where fq.is_active='Y' AND fq.is_deleted ='N' AND feedback.is_active='Y' AND feedback.is_deleted ='N' AND created_at!='' AND ro.feedback_type ='rating'  and feedback.company_id = '$company_id'
and created_at BETWEEN '" . $_SESSION['start_date'] . "' AND '" . $_SESSION['end_date'] . "' and created_at !=''
group by title,option_sequence,question,feedback_nature order by question ASC";


        $arr_get_question_based_data = [];
        $question_based_result = $DBFilter->RunSelectQuery($query_question_based_data);
//            echo '<pre>'; print_r($query_question_based_data); exit;

        foreach ($question_based_result[0] as $month_on_month_based_result_key => $month_on_month_based_result_val) {
            $arr_get_question_based_data[$month_on_month_based_result_val->question]['total_feedback'] += $month_on_month_based_result_val->number_of_feedback;
            $arr_get_question_based_data[$month_on_month_based_result_val->question]['number_of_feedback'][$month_on_month_based_result_val->title] += $month_on_month_based_result_val->number_of_feedback;
            $arr_get_question_based_data[$month_on_month_based_result_val->question]['number_of_feedback_nature'][$month_on_month_based_result_val->feedback_nature] += $month_on_month_based_result_val->number_of_feedback;
            $arr_get_question_based_data[$month_on_month_based_result_val->question]['rating_options_detail'] = $question_details_count[0];
        }

        $category = array();
        $category['question'] = [];
        $category['rating'] = [];
        $data_question = [];
        $k = 0;
        foreach($arr_get_question_based_data as $question_key => $question_val) {
            foreach($question_val['number_of_feedback'] as $key => $val) {
                $data_question[$question_key][str_replace(' ','_',strtolower($key))] = $val;
            }
        }

        $que_lists = array();
        $rating_lists = array();
        foreach ($question_details_count[0] as $rating_key=>$rating_val){
            $rating_lists['rating_option_list'][] = str_replace(' ','_',strtolower($rating_val->title));
        }
        /*foreach ($data_question as $data_question_key=>$data_question_val){
                $que_lists['que'][] = [$data_question_key,$data_question_val];
        }*/

        $get_name_data_combined = [];
        $get_x_axis_category = [];
        foreach ($data_question as $data_question_key=>$data_question_val){
        $moo[] = array_values($data_question_val);

        $ddc[] =$data_question_key;
//            $get_x_axis_category = implode(',',$data_question_key);
//            echo '<pre>'; print_r($ddc);

            foreach ($moo as $moo_key=>$moo_val){

                $implode_number_of_feedback = implode(',',$moo_val);
//                $fresh_arr = ['['.$implode_number_of_feedback.']'];
                $fresh_arr = '['.$implode_number_of_feedback.']';
//                echo '<pre>'; print_r($fresh_arr);
                //            $que_lists[$data_question_key]['data'] = [$data_question_val];
                $que_lists[$data_question_key]['name'] = $data_question_key;
                $que_lists[$data_question_key]['data'] = $fresh_arr;
                //            $get_name_data_combined[][] = [$que_lists['name'],$que_lists['data']];
        }
        }
        $get_x_axis_category = implode(',',$ddc);
        $get_x_axis_category = '['.$get_x_axis_category.']';
//        echo '<pre>'; print_r(); exit;

        foreach ($que_lists as $que_lists_key=>$que_lists_val){
            $get_name_data_combined[] = $que_lists_val;
        }

//          echo '<pre>'; print_r($get_x_axis_category);exit;
//          echo '<pre>'; print_r($get_name_data_combined);exit;
//          echo json_encode($get_name_data_combined);exit;
//          echo '<pre>'; print_r(json_encode($get_name_data_combined));exit;

        /* Question wise end*/

        $selected_date_by_user = date('F j, Y', strtotime($_SESSION['start_date'])) . '' . date('F j, Y', strtotime($_SESSION['end_date']));

        $CFG->template = "company/view.php";
        break;

    case 'list':
        if ($frmdata['search']) {
            $frmdata['pageNumber'] = 1;
        }

        PaginationWork();
        $totalCount = 0;
        $Row = getCompany($totalCount);
        //print_r($Row); exit;
        $CFG->template = "company/list.php";
        break;

}

function getCompany(&$totalCount)
{
    global $DB, $frmdata, $DBFilter;

    $query = "Select o.*,p.plan_id,p.plan_name from company_detail as o 
		 join plan as p on o.plan_id = p.plan_id 
		where o.is_deleted = 'N' and (o.is_active = 'Y' or o.is_active = 'N')";
    //echo $frmdata['keyword'];
    // if(isset($frmdata['search']))
    // {
    if ($frmdata['keyword'] != '') {
        $frmdata['keyword'] = trim($frmdata['keyword']);
        $where = " and (o.company_name like  '%" . $frmdata['keyword'] . "%'
				or p.plan_name like '%" . $frmdata['keyword'] . "%' or o.company_email like '%" . $frmdata['keyword'] . "%' or o.user_name like '%" . $frmdata['keyword'] . "%')";
        $_SESSION['keywords'] = 'Y';
    }

    // }
    if (isset($frmdata['orderby']) && $frmdata['orderby'] != '') {
        $order .= " order by " . $frmdata['orderby'];
    } else {
        $order .= " order by o.company_id desc ";
    }


    //$groupby = ' group by o.company_id';
    $query = $query . $where . $order;
    //echo  $query;
    $result = $DBFilter->RunSelectQueryWithPagination($query, $totalCount);
    //echo "<pre>";print_r($result);
    return $result;
}


function getFeedbacks()
{    //print_r($_SESSION);
    global $DB, $frmdata, $DBFilter;
    if ($_SESSION['usertype'] == 'company_admin' and $_SESSION['role_id'] == 2) {
        $query = "Select * from feedback where is_deleted = 'N' and is_active ='Y' and company_id='" . $_SESSION['company_id'] . "'  order by feedback_id desc limit 0,5";
    } else if ($_SESSION['usertype'] == 'company_users' and $_SESSION['role_id'] == 3) {
        $query = "Select * from feedback where is_deleted = 'N' and is_active ='Y' and assigned_to = '" . $_SESSION['user_id'] . "' and company_id='" . $_SESSION['company_id'] . "'  order by feedback_id desc limit 0,5";
    } else if ($_SESSION['usertype'] == 'company_users' and $_SESSION['role_id'] == 4) {
        $query = "Select * from feedback where is_deleted = 'N' and is_active ='Y' and user_id = '" . $_SESSION['user_id'] . "' and company_id='" . $_SESSION['company_id'] . "'  order by feedback_id desc limit 0,5";
    }

    $result = $DBFilter->RunSelectQuery($query);
    //echo "<pre>";print_r($result);
    return $result;
}

function check_date_today_yesterday_or_other()
{
    $yesterday = date('Y-m-d', strtotime("-1 days"));
    $today = date('Y-m-d');
    if ($_SESSION['start_date'] == $today) {

        $between_date = "(DATE(fd.created_at) ='" . $_SESSION['start_date'] . "')";
        return $between_date;
    } elseif ($_SESSION['start_date'] == $yesterday) {
        $between_date = "(DATE(fd.created_at) ='" . $_SESSION['start_date'] . "')";
        return $between_date;
    } else {
        $between_date = "(fd.created_at BETWEEN '" . $_SESSION['start_date'] . "' AND '" . $_SESSION['end_date'] . "')";
        return $between_date;
    }
}

function check_date_today_yesterday_or_other_without_alias()
{

    $yesterday = date('Y-m-d', strtotime("-1 days"));
    $today = date('Y-m-d');
    if ($_SESSION['start_date'] == $today) {

        $between_date = "(DATE(feedback.created_at) ='" . $_SESSION['start_date'] . "')";
        return $between_date;
    } elseif ($_SESSION['start_date'] == $yesterday) {
        $between_date = "(DATE(feedback.created_at) ='" . $_SESSION['start_date'] . "')";
        return $between_date;
    } else {
        $between_date = "(feedback.created_at BETWEEN '" . $_SESSION['start_date'] . "' AND '" . $_SESSION['end_date'] . "')";
        return $between_date;
    }
}

include_once(CURRENTTEMP . "/index.php");
?>

