<?php


if (isset($frmdata['clearsearch'])) {
    unset($frmdata['keyword']);
    unset($_SESSION['pageNumber']);
    $CFG->template = "company/list.php";
}
// print_r($_SESSION);
if (isset($frmdata['add_company'])) {
//        try{


    $err = '';
    /* echo '<pre>';
     print_r($frmdata);
     exit;*/
    if (trim($frmdata['company_name']) == '') {
        $id = 'company_name';
        //SetFocus($id);
        $err .= "Please enter company name.<br>";
        $frmdata['company_name'] = '';
    } else {
        if (validatecompanyname($frmdata['company_name']) == true) {
            if (chkCompanyName(trim($frmdata['company_name'])) == 1) {
                $err .= "Company name already exists.<br>";
                $frmdata['company_name'] = '';
            } else {
                $frmdata['company_name'] = $frmdata['company_name'];
            }
        } else {
            $err .= "Only alpha numeric and white space allowed for company name.<br>";
            $id = 'company_name';
            //SetFocus($id);
        }

    }
    if (trim($frmdata['user_name']) == '') {
        $err .= "Please enter contact person name.<br>";
        $frmdata['user_name'] = '';
        $id = 'user_name';
        //SetFocus($id);
    } else {
        if (validatename($frmdata['user_name']) == true) {
            $frmdata['user_name'] = $frmdata['user_name'];
        } else {
            $err .= "Only alpha numeric and white space allowed for contact person name.<br>";
            $id = 'user_name';
            //SetFocus($id);
        }

    }
    if (trim($frmdata['creation_date']) == '') {
        $err .= "Please select creation date.<br>";
        $frmdata['creation_date'] = '';
        $id = 'creation_date';
        //SetFocus($id);
    }

    if (trim($frmdata['expiary_date']) == '') {
        $err .= "Please select expiry date.<br>";
        $frmdata['expiary_date'] = '';
        $id = 'expiry_date';
        //SetFocus($id);
    } else {

        $frmdata['expiary_date'] = date("Y-m-d", strtotime($frmdata['expiary_date']));

    }

    if ($frmdata['expiary_date'] != '' && $frmdata['creation_date'] != '') {
        if (strtotime($frmdata['expiary_date']) < strtotime($frmdata['creation_date'])) {
            $frmdata['expiary_date'] = '';
            $err .= "Please select valid expiry date.<br>";
            $id = 'expiry_date';
            //SetFocus($id);
        }
    }
    if (trim($frmdata['plan_id']) == '') {
        $err .= "Please select subscription plan .<br>";
        $frmdata['plan_id'] = '';
        $id = 'subscription_plan';
        //SetFocus($id);
    } else {
        $frmdata['plan_id'] = $frmdata['plan_id'];
    }

    if (trim($frmdata['company_email']) != '') {
        if (!filter_var($frmdata['company_email'], FILTER_VALIDATE_EMAIL)) {
            $err .= "Please enter a valid email address.<br>";
            $frmdata['company_email'] = '';
            $id = 'company_email';
            //SetFocus($id);
        }
        /**********Added By Shivender***********/
        /*$email_check = $DBFilter->SelectRecord('users',"user_email='".$frmdata['company_email']."'");
        if($email_check)
        {
            $err.="Email is already exists.<br>";
            $frmdata['company_email']='';
            $id = 'company_email';
            //SetFocus($id);
        } */
        /******Ended By Shivender******/
        else {
            if (checkMail(trim($frmdata['company_email'])) == 1) {
                $err .= "Email is already exists.<br>";
                $frmdata['company_email'] = '';
            } else {
                $frmdata['company_email'] = trim($frmdata['company_email']);
            }
        }
    } else {
        $err .= "Please enter company email .<br>";
        $frmdata['company_email'] = '';
        $id = 'company_email';
        //SetFocus($id);
    }

    if (validateimage($_FILES["company_logo"]["name"]) == true) {
        if ($_FILES['company_logo']['size'] > 2097152)//added by : Neha Pareek. (02/12/2015)
        {
            $err .= 'Logo size must be less than 2 MB' . '<br/>';
        } else {
            $_FILES["company_logo"]["name"] = $_FILES["company_logo"]["name"];
        }
    } else {    //$frmdata['plan_id'] = $frmdata['plan_id'];
        $id = 'company_logo';
        //SetFocus($id);
        $err .= "Please select company logo.<br>";
        $_FILES["company_logo"]["name"] = '';
    }

    if ($_FILES["company_logo"]["name"] != '') {
        $image_name = $_FILES["company_logo"]["name"];
        $source_image = $_FILES["company_logo"]["tmp_name"];
        $filename = time() . "_" . $image_name;
        $folderpath = ROOT . "/images/company_logo/" . $filename;
       $aa = move_uploaded_file($source_image, $folderpath);
    }
    if ($err == '') {
        $nowdate = date("Y-m-d");
        $updatedate = date("Y-m-d");
        $pass = MakeNewpassword(6);
        $obj = new passEncDec;
        $password = $obj->encrypt_password($pass);
        $company_data = array('company_name' => $frmdata['company_name'], 'plan_id' => $frmdata['plan_id'], 'creation_date' => $frmdata['creation_date'], 'expiary_date' => $frmdata['expiary_date'], 'user_name' => $frmdata['user_name'], 'company_email' => $frmdata['company_email'], 'password' => $password, 'company_logo' => $filename);

//        echo $aa;
//echo '<pre>'; print_r($company_data); exit;

        if ($id = $DBFilter->InsertRecord('company_detail', $company_data)) {
            $company_id = $id;
            /* code is for adding company_admin */

            $admin_data = array('company_id' => $company_id, 'role_id' => '2', 'creation_date' => $nowdate,'first_name' => $frmdata['user_name'], 'user_email' => $frmdata['company_email'], 'password' => $password, 'assigned_to' => $_SESSION['user_id']);

            $company_admin = $DBFilter->InsertRecord('users', $admin_data);

            /* code is for adding one defualt service */

            $service_data = array('company_id' => $company_id, 'service_name' => 'Washroom', 'service_description' => 'Washroom Feedbacks');
            $company_services = $DBFilter->InsertRecord('services', $service_data);
            //exit;

            if ($company_admin) {
                if ($password != '' && trim($frmdata['company_email']) != '') {
                    $name = ucfirst($frmdata['user_name']);
                    $userid = $frmdata['user_name'];
                    $email = $frmdata['company_email'];
                    $passwd = $pass;
                    $to = $email;
                    send_mail_to_company_admin_cleaner_supervisor($to, $passwd, $name);
                }
            }
//			$_SESSION['success'] = "Company added successfully, Password for company admin is : <b>$pass.'-'. $aa </b>";
            $_SESSION['success'] = "Company added successfully, Password for company admin is : <b>$pass</b>";
            Redirect(CreateURL('index.php', 'mod=company'));
            die();
        } else {
            $_SESSION['error'] = "<font color=red>Please insert correct values for company details.</font>";
        }
    } else {
        $_SESSION['error'] = "<font color=red>" . $err . "</font>";
    }
    // echo '<pre>';
    // print_r($frmdata);
    // exit;
    /*}
    catch (Exception $e){
        CreateLog($e->getMessage());
    }*/
}
elseif (isset($frmdata['update'])) {
    $err = '';

    //print_r($_FILES);
    if (trim($frmdata['company_name']) == '') {
        $err .= "Please enter company name.<br>";
        $frmdata['company_name'] = '';
    } else {
        if (validatecompanyname($frmdata['company_name']) == true) {
            if (chkCompanyName(trim($frmdata['company_name'])) == 1) {
                $err .= "Company name already exists.<br>";
                $frmdata['company_name'] = '';
            } else {
                $frmdata['company_name'] = $frmdata['company_name'];
            }
        } else {
            $err .= "Only alpha numeric and white space allowed for company name.<br>";
        }

    }

    if (trim($frmdata['plan_id']) != '') {
        $frmdata['plan_id'] = $frmdata['plan_id'];
    } else {
        $err .= "Please select subscription plan .<br>";
        $frmdata['plan_id'] = '';
    }

    if (trim($frmdata['user_name']) == '') {
        $err .= "Please enter contact person name.<br>";
        $frmdata['user_name'] = '';
    } else {
        if (validatename($frmdata['user_name']) == true) {
            $frmdata['user_name'] = $frmdata['user_name'];
        } else {
            $err .= "Only alpha numeric and white space allowed for contact person name.<br>";
        }

    }

    if (trim($frmdata['expiary_date']) == '') {
        $err .= "Please select expiry date.<br>";
        $frmdata['expiary_date'] = '';
    }

    if (trim($frmdata['creation_date']) == '') {
        $err .= "Please select creation date.<br>";
        $frmdata['creation_date'] = '';
    }

    if (trim($frmdata['company_email']) != '') {
        if (!filter_var($frmdata['company_email'], FILTER_VALIDATE_EMAIL)) {
            $err .= "Please enter a valid email address.<br>";
            $frmdata['company_email'] = '';

        } else {
            /**********Added By Shivender***********/
            /*$email_check = $DBFilter->SelectRecord('users',"user_email='".$frmdata['company_email']."' and company_id!=".$id);

            if($email_check)
            {
                $err.="Email is already exists.<br>";
                $frmdata['company_email']='';
            }
            /******Ended By Shivender******/
            if (checkMail(trim($frmdata['company_email'])) == 1)//Added By : Neha Pareek . Dated : 25-01-2016
            {
                $err .= "Email is already exists.<br>";
                $frmdata['company_email'] = '';
            } else {
                $frmdata['company_email'] = trim($frmdata['company_email']);
            }
        }
    } else {
        $err .= "Please enter company email .<br>";
        $frmdata['company_email'] = '';
    }

    if (strtotime($frmdata['expiary_date']) < strtotime($frmdata['creation_date'])) {
        $err .= "Please enter valid expiry date.<br>";
    }

    if ($_FILES["company_logo"]["name"] == '') {
        $filename = $frmdata['logo'];
    } else {
        if (validateimage($_FILES["company_logo"]["name"]) == true) {
            if ($_FILES['company_logo']['size'] > 2097152)//added by : Neha Pareek. (02/12/2015)
            {
                $err .= 'Logo size must be less than 2 MB' . '<br/>';
            } else {
                $_FILES["company_logo"]["name"] = $_FILES["company_logo"]["name"];
            }
        } else {
            $err .= "Please select only an image.<br>";

        }
    }
    if ($_FILES["company_logo"]["name"] != '') {
        $image_name = $_FILES["company_logo"]["name"];
        $source_image = $_FILES["company_logo"]["tmp_name"];
        $filename = time() . "_" . $image_name;
        $folderpath = ROOT . "/images/company_logo/" . $filename;
        $oldfile = ROOT . "/images/company_logo/" . $frmdata['company_logo_hidden'];
        unlink($oldfile);
        $aa = move_uploaded_file($source_image, $folderpath);
    } else {
        $filename = $frmdata['company_logo_hidden'];
    }
    if ($err == '') {
        $frmdata['expiary_date'] = date("Y-m-d", strtotime($frmdata['expiary_date']));
        $frmdata['creation_date'] = date("Y-m-d", strtotime($frmdata['creation_date']));
        $obj = new passEncDec;
        $password = $obj->encrypt_password($frmdata['password']);

        $company_data = array('company_name' => $frmdata['company_name'], 'plan_id' => $frmdata['plan_id'], 'creation_date' => $frmdata['creation_date'], 'expiary_date' => $frmdata['expiary_date'], 'user_name' => ucwords($frmdata['user_name']), 'company_email' => $frmdata['company_email'], /*'is_active' => $frmdata['is_active'],*/ 'company_logo' => $filename);
//       echo "<pre>"; 	print_r($frmdata);exit;
        if ($DBFilter->UpdateRecord('company_detail', $company_data, "company_id ='$id'")) {

            //$company_id=$id;
            $name = array();
            $name = explode(" ", ucwords(trim($frmdata['user_name'])));
            $user_data = $DBFilter->SelectRecord('users', "company_id='$id' and role_id='2'");
            $admin_data = array('company_id' => $id, 'role_id' => '2', 'user_email' => $frmdata['company_email'], 'first_name' => $name[0], 'last_name' => $name[1], 'is_active' => $frmdata['is_active'], 'is_deleted' => 'N');
            // echo "<pre>"; print_r($company_data);
            // print_r($admin_data);
            // exit;
            $company_admin = $DBFilter->UpdateRecord('users', $admin_data, "company_id ='$id' and role_id='2'");

            $_SESSION['success'] = "Company updated successfully.";
            Redirect(CreateURL('index.php', 'mod=company'));
            die();
        }
    } else {
        $_SESSION['error'] = $err;
    }

}
//============================================================================

//Delete Multiple or Other Actions added by : Neha Pareek

//============================================================================

if ($frmdata['actions']) {
    if ($frmdata['chkbox'] == '' && isset($frmdata['actions'])) {
        $_SESSION['error'] = 'Please select atleast one company.';
    } else {
        $ids = $frmdata['chkbox'];
        //print_r($ids);exit;
        $action = $frmdata['actions'];
        $cond = "company_id=";
        multipleAction($ids, 'company_detail', $action, $cond);
        $_SESSION['success'] = "Companies has been " . $action . "d successfully.";
        Redirect(CreateURL('index.php', 'mod=company'));
        die();
    }
}
//============================================================================

//Delete the company

//============================================================================
elseif ($do == 'del') {

//		echo 'in'.$id;exit;
    $related_detail = DeleteRelatedData($id, $_SESSION['usertype'], 'company_detail');
    if ($related_detail == true) {
        $_SESSION['success'] = 'Company has been deleted successfully.';
        Redirect(CreateURL('index.php', 'mod=company'));
        die();
    } else {
        $_SESSION['error'] = 'There is problem in deletion. Try again later.';
        Redirect(CreateURL('index.php', 'mod=company'));
        die();

    }
}

?>