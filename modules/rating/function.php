<?php
//$rows = $DBFilter->SelectRecord('feedback',"user_id='7'");
//print_r($rows);
//exit;
//print_r($_SESSION);
//include('class.encryptdecrypt');
if (isset($frmdata['clearsearch'])) {
    echo $frmdata['clearsearch'];
    unset($frmdata);
    unset($_SESSION['pageNumber']);
    $CFG->template = "rigs/list.php";
}

if (isset($frmdata['add_rating_option'])) {

//    echo '<pre>'; print_r($frmdata); exit;

    $err = '';
    if (trim($frmdata['title']) == '') {
        $err .= "Please enter question title.<br>";
        $frmdata['title'] = '';
    }
    if (trim($frmdata['feedback_type']) == '') {
            $err .= "Please enter in feedback type.<br>";
            $frmdata['feedback_type'] = '';
        }

    if (trim($frmdata['field_type']) == '') {
        $err .= "Please select option for field type.<br>";
        $frmdata['field_type'] = '';
    }
    if (trim($frmdata['eligible_for_capture_detail']) == '') {
        $err .= "Please select option for capture details.<br>";
        $frmdata['eligible_for_capture_detail'] = '';
    }

    if (trim($frmdata['feedback_nature']) == '') {
        $err .= "Please select option for capture fault detail.<br>";
        $frmdata['feedback_nature'] = '';
    }

    if (validateimage($_FILES["option_image"]["name"]) == true) {
        if ($_FILES['option_image']['size'] > 2097152)//added by : Neha Pareek. (02/12/2015)
        {
            $err .= 'Image size must be less than 2 MB' . '<br/>';
        } else {
            $_FILES["option_image"]["name"] = $_FILES["option_image"]["name"];
        }
    }else{
            $err .= 'Image format should be jpg,jpeg,gif,png only' . '<br/>';
    }

    if ($_FILES["option_image"]["name"] != '') {
        $image_name = $_FILES["option_image"]["name"];
        $source_image = $_FILES["option_image"]["tmp_name"];
        $filename = time() . "_" . $image_name;
        //$folderpath = ROOT."images/uploads/".$filename;
        $folderpath = ROOT . "/images/option_image/" . $filename;
        move_uploaded_file($source_image, $folderpath);
    }

//    echo '<pre>'; print_r($frmdata); exit;

    if ($err == '') {


        $company_id = $_SESSION['company_id'];
        $data = array('title' => $frmdata['title'], 'feedback_type' => $frmdata['feedback_type'],'field_type' => $frmdata['field_type'], 'eligible_for_capture_detail' => $frmdata['eligible_for_capture_detail'],'feedback_nature' => $frmdata['feedback_nature'],'option_image' => $filename);

//        echo '<pre>'; print_r($data); exit;

        if ($DBFilter->InsertRecord('rating_options', $data)) {

            $_SESSION['success'] = "Ration option added successfully.";
            Redirect(CreateURL('index.php', 'mod=rating'));
            die();

        } else {
            $_SESSION['error'] = "<font color=red>Oops! Due to some reason record could not be saved.</font>";
        }
    } else {
        $_SESSION['error'] = "<font color=red>" . $err . "</font>";
    }

}

elseif (isset($frmdata['update_rating_option'])) {

    $err = '';
//    echo '<pre>'; print_r($frmdata); exit;
    $option_id = $_GET['id'];

    if (trim($frmdata['title']) == '') {
        $err .= "Please enter question title.<br>";
        $frmdata['title'] = '';
    }
    if (trim($frmdata['feedback_type']) == '') {
        $err .= "Please enter in feedback type.<br>";
        $frmdata['feedback_type'] = '';
    }

    if (trim($frmdata['field_type']) == '') {
        $err .= "Please select option for field type.<br>";
        $frmdata['field_type'] = '';
    }
    if (trim($frmdata['eligible_for_capture_detail']) == '') {
        $err .= "Please select option for capture details.<br>";
        $frmdata['eligible_for_capture_detail'] = '';
    }

    if (trim($frmdata['feedback_nature']) == '') {
        $err .= "Please select option for capture fault detail.<br>";
        $frmdata['feedback_nature'] = '';
    }
    if ($_FILES["option_image"]["name"] == '') {
        $filename = $frmdata['option_img_name'];
    }
    if (validateimage($_FILES["option_image"]["name"]) == true) {
        if ($_FILES['option_image']['size'] > 2097152)//Added by : Neha Pareek. (02/12/2015)
        {
            $err .= 'Image size must be less than 2 MB' . '<br/>';
        } else {
            $_FILES["option_image"]["name"] = $_FILES["option_image"]["name"];
        }
    }
    if ($_FILES["option_image"]["name"] != '') {
        $image_name = $_FILES["option_image"]["name"];
        $source_image = $_FILES["option_image"]["tmp_name"];
        $filename = time() . "_" . $image_name;
        //$folderpath = ROOT."images/uploads/".$filename;
        $folderpath = ROOT . "/images/option_image/" . $filename;
        move_uploaded_file($source_image, $folderpath);
    }


    if ($err == '') {

        $data = array('title' => $frmdata['title'], 'feedback_type' => $frmdata['feedback_type'],'field_type' => $frmdata['field_type'], 'eligible_for_capture_detail' => $frmdata['eligible_for_capture_detail'],'feedback_nature' => $frmdata['feedback_nature'],'option_image' => $filename);
//        echo '<pre>'; print_r($data); exit;

        $DBFilter->UpdateRecord('rating_options', $data, "id =" . $option_id);

                $_SESSION['success'] = "Rating option updated successfully.";
                Redirect(CreateURL('index.php', 'mod=rating'));
                die();


        } else {
            $_SESSION['error'] = "<font color=red>" . $err . "</font>";
        }
}
//============================================================================

//Delete Multiple or Other Actions added by : Neha Pareek

//============================================================================
if ($frmdata['actions']) {

    if ($frmdata['chkbox'] == '' && isset($frmdata['actions'])) {
        $_SESSION['error'] = 'Please select atleast one user';
    } else {
        $ids = $frmdata['chkbox'];
        $action = $frmdata['actions'];
        $cond = "user_id=";
        foreach ($ids as $id) {
            # code...
            //echo "<pre>";
            //print_r($DBFilter->SelectRecords('feedback',"user_id='4'"));
            /*$row = $DBFilter->SelectRecords('feedback',"user_id='$id' or assigned_to='$id'");
            if($row)
            {
            // 	print_r($row);
            // 	exit;
                $_SESSION['error'] = 'Please delete user related feedback first.';
                Redirect(CreateURL('index.php', 'mod=feedback'));
                die();
            }
            else
            {*/
            //exit;
            multipleAction($ids, 'users', $action, $cond);
            $_SESSION['success'] = 'Users has been ' . $action . 'd successfully.';
            Redirect(CreateURL('index.php', 'mod=user'));
            die();
            //}
        }

    }
}
//============================================================================

//Delete the User

//============================================================================
elseif ($do == 'del') {
    $data = array('is_deleted' => 'Y', 'is_active' => 'N');
    $related_detail = DeleteRelatedData($id, $_SESSION['usertype'], 'users');
    if ($related_detail == true) {
        $result = $DBFilter->UpdateRecord('users', $data, "user_id ='" . $id . "'");
        //exit;
        $_SESSION['success'] = 'User has been deleted successfully.';

        Redirect(CreateURL('index.php', 'mod=user'));
        die();
    }
}

/* function is used to allowed number of users according to plan */


?>