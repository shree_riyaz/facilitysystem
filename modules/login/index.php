<?php
/*======================================
		Developer	-	Jaishree Sahal
	    Date        -   5 August 2015
		Module      -   login
		SunArc Tech. Pvt. Ltd.
======================================		
******************************************************/

defined("ACCESS") or die("Access Restricted");

include_once("include.php");
//================ For getting URL variables ===================================================
//print_r($_SESSION);
if (isset($_REQUEST['do'])) {
    $do = $_REQUEST['do'];
} else {
    $do = '';
}


if (isset($frmdata['Reset'])) {

    unset ($frmdata);

}

if (isset($frmdata['submit_login'])) {
    if (validateUserLogin() == 0) {
        ShowMessage();

    } else {
        if (validateMemberLogin() == 1 && $_SESSION['access_key'] != '') {
            session_start();
//			print_r($_SESSION); exit;
            if ($chk_user == '') {
                $frmdata['message'] = " <span style=' color:red'>You are no longer to access this system.</span>";
            }
            if ($_SESSION['usertype'] == 'super_admin') {
                ?>
                <script type="text/javascript">
                    // alert('super admin');
                    document.location.href = "<?php echo 'index.php?mod=company'; ?>";
                </script>
                <?php
            } else if ($_SESSION['usertype'] == 'company_admin' || $_SESSION['usertype'] == 'company_user') {
                ?>
                <script type="text/javascript">
                    //alert('company user');
                    document.location.href = "<?php echo 'index.php?mod=dashboard&do=view&id=' . $_SESSION['company_id']; ?>";
                </script>
                <?php
            }
        }
    }
}
//echo '<pre>'; print_r($_REQUEST['mod']); exit;

switch ($do) {

    case 'logout':
        unset($_SESSION);
        @session_unset();
        @session_destroy();
        include_once(MOD . "/login/logout.php");
        break;

    case 'reset_password':
        if ($_REQUEST['mod'] == 'login' && isset($_REQUEST['mod']) && isset($_REQUEST['key'])) {
            global $DB, $frmdata, $DBFilter;
            if (isset($_POST['submit_reset_password'])) {
                $random_string = $_REQUEST['key'];
                $get_row = $DBFilter->RunSelectQuery("select user_mail, random_key  from reset_password where is_deleted ='N' AND random_key='" . $random_string . "'");

                $get_email_from_db = $get_row[0][0]->user_mail;
                $get_random_key_from_db = $get_row[0][0]->random_key;

                if (validateUserLoginForResetPassword() == 0) {
                    ShowMessageResetPassword();

                }else{

                    if(count($get_random_key_from_db) == 1 && !empty($get_random_key_from_db)) {

                        $get_row_user = $DBFilter->RunSelectQuery("select user_id from users where is_active ='Y' AND is_deleted ='N' AND user_email='" . $get_email_from_db . "'");

                        $get_row_user_reset = $DBFilter->RunSelectQuery("select id from reset_password where is_deleted ='N' AND random_key='" . $get_random_key_from_db . "'");
                        $obj = new passEncDec;
                        $password_updated = $obj->encrypt_password($frmdata['password']);
                        $company_data = array('password'=>$password_updated);
                        $company_data_reset = array('is_deleted'=>'Y');
//                        echo '<pre>'; print_r($company_data); exit;

                        $DBFilter->UpdateRecord('users', $company_data, "user_id =".$get_row_user[0][0]->user_id);
                        $DBFilter->UpdateRecord('reset_password', $company_data_reset, "id =".$get_row_user_reset[0][0]->id);
//                        $_SESSION['message_reset_psd'] = 'Password Reset Successfully.';
                        $_SESSION['message_forgot_success'] = 'Password reset successfully.';
                        ShowMessageForgotSuccess();
                        Redirect(CreateURL('index.php', 'mod=login'));
                        exit;

                    }else {
                        $_SESSION['message_reset_psd'] = 'Oops! It seems session expired now.';
                        ShowMessageResetPassword();
                    }
                }
            }

        }
    include_once(MOD . "/login/reset_password.php");
    break;

    case 'forgot':

        if ($_REQUEST['mod'] == 'login' && isset($_REQUEST['mod'])) {

            global $DB, $frmdata, $DBFilter;
            if (isset($_POST['forgot_password'])) {
                $email_forgot = $_POST['email_forgot'];
                $get_row = $DBFilter->RunSelectQuery("select user_email,first_name as name from users where is_active ='Y' AND is_deleted ='N' AND user_email='" . $email_forgot . "'");

                $get_email_from_db = $get_row[0][0]->user_email;
                $get_name_from_db = $get_row[0][0]->name;

                if (validateUserLoginForForgotPage() == 0) {
                    ShowMessageForgot();

                }else{

                    if(count($get_email_from_db) == 1 && !empty($get_email_from_db)) {
//                    require_once("forgot-password-recovery-mail.php");
//                        $aa = Redirect(CreateURL('index.php', 'mod=login&do=reset_password'));
                        $random_key = randomString(50);;

                        $is_send = send_mail($get_email_from_db,$random_key,ucfirst($get_name_from_db));
                        if ($is_send == 1){
                            $data = array('random_key'=>$random_key,'user_mail'=>$get_email_from_db,'is_deleted'=>'N','created_at'=>date('Y-m-d H:i:s'));
                            $DBFilter->InsertRecord('reset_password', $data);
                            $_SESSION['message_forgot_success'] = 'Reset password link send to you, please check your inbox.';
                            ShowMessageForgotSuccess();
                        }else{
                            $_SESSION['message_forgot'] = 'Oops! Something went wrong while mail to user. ';
                            ShowMessageForgot();
                        }
                    } else {
                        $_SESSION['message_forgot'] = 'Oops! It seems you are not a member of Feedback System Team.';
                        ShowMessageForgot();

                    }
                }
            }

        }
        include_once(MOD . "/login/forgot.php");
        break;


    case 'web_login':

        if ($_REQUEST['call'] == 'API' && $_SERVER['REQUEST_METHOD'] == 'POST') {
            global $DB, $frmdata, $DBFilter;
            $obj = new passEncDec;
            //echo $frmdata['password'];
            $user_data = array();
            if ($frmdata['user_email'] != '' && $frmdata['password'] != '') {
                $login_result = $DBFilter->SelectRecord('users', "is_deleted='N' and is_active='Y' and user_email='" . $frmdata['user_email'] . "'");
                $pwd = $obj->decrypt_password($login_result->password);
                //echo $pwd;
                //print_r($login_result);exit;

                //$login_result = $DBFilter->SelectRecord('users',"user_email='".$frmdata['user_email']."' and password='".md5($frmdata['password'])."'");
                $randomString = substr("abcdefghijklmnopqrstuvwxyz", mt_rand(0, 10), 1) . substr(md5(time()), 1);
                $data = array('user_id' => $login_result->user_id, 'access_key' => $randomString);
                $role = $DBFilter->SelectRecord('roles', "role_id='" . $login_result->role_id . "'");
                //$data= array('user_id'=>$login_result->user_id,'password'=>$login_result->password,'access_key'=>$randomString);
                //print_r($login_result);
                //print_r($data);exit;
                $key = insertAccessKey('login_detail', $data);
                //$accessKey = getAccessKeylog($login_result->user_id,$login_result->password);
                $accessKey = getAccessKey($login_result->user_id);

                if ($pwd != $frmdata['password'] && $login_result->user_email == $frmdata['user_email']) {
                    $final_data = array('result' => 'Please enter valid password.', 'status' => 0);
                    echo json_encode($final_data);
                    exit;
                } else {
                    if ($login_result != '') {
                        $user_data['user_id'] = $login_result->user_id;
                        //$user_data['password'] = $login_result->password;
                        $user_data['first_name'] = $login_result->first_name;
                        $user_data['last_name'] = $login_result->last_name;
                        $user_data['user_email'] = $login_result->user_email;
                        $user_data['company_id'] = $login_result->company_id;
                        $user_data['role_id'] = $login_result->role_id;
                        $user_data['role_name'] = $role->role_name;
                        $user_data['ACCESS_TOKEN'] = $accessKey['access_key'];
                        $user_data['status'] = 1;

                        echo json_encode($user_data);
                        exit;
                    } else {
                        $final_data = array('result' => 'Please enter valid email id and password.', 'status' => 0);
                        echo json_encode($final_data);
                        exit;
                    }
                }
            } else if ($frmdata['user_email'] == '' && $frmdata['password'] != '') {
                $final_data = array('result' => 'Please enter both email id and password.', 'status' => 0);
                echo json_encode($final_data);
                exit;
            } else if ($frmdata['user_email'] == '' && $frmdata['password'] == '') {
                $final_data = array('result' => 'Please enter both email id and password.', 'status' => 0);
                echo json_encode($final_data);
                exit;
            } else {
                $final_data = array('result' => 'Please enter password.', 'status' => 0);
                echo json_encode($final_data);
                exit;
            }
        }

    default:
        $_SESSION['msg'] = $frmdata['message'];

        $_SESSION['start_date'] = date('Y-m-d', strtotime('today - 30 days'));
        $_SESSION['end_date'] = date('Y-m-d');;
        $all_time = [

            "Today" => "moment(); moment()",
            "Yesterday" => "moment().subtract(1, 'days'); moment().subtract(1, 'days')",
            "Last 7 Days" => "moment().subtract(6, 'days'); moment()",
            "Last 30 Days" => "moment().subtract(30, 'days'); moment()",
            "This Month" => "moment().startOf('month'); moment().endOf('month')",
            "Last Month" => "moment().subtract(1, 'month').startOf('month'); moment().subtract(1, 'month').endOf('month')",
        ];
        $_SESSION['all_time_list'] = $all_time;

        include_once(MOD . "/login/login.php");
        break;

}
?>
