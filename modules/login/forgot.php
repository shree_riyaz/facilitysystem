
<?php
/*======================================
		Developer	-	Jaishree Sahal
	    Date        -   5 june 2015
		Module      -   login
		view		-   login form
		SunArc Tech. Pvt. Ltd.
======================================
******************************************************/
if($_SESSION['usertype']!= '')
{
    ?>
    <script type="text/javascript">
        location.href="index.php?mod=company";
    </script>
    <?php
}
/*
if(isset($_POST['Reset']))
{
	unset($_SESSION['msg']);
}*/
?>
<!DOCTYPE html>
<html>
<head>
    <title>Forgot Password</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="style/new_design.css">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Raleway:400,500,600,700" rel="stylesheet">

    <style type="text/css">
        .bs-example{
            margin: 20px;
            width:80%;
        }
        /* Fix alignment issue of label on extra small devices in Bootstrap 3.2 */
        .form-horizontal .control-label{
            padding-top: 7px;
        }
    </style>
</head>
<body style="background-color: #fff;">
<section class="login-form">
    <div class="container">
        <div class="main-wrapper">
            <div class="row">
                <div class="col-sm-12">
                    <img src="images/logo.png" alt="" class="img-responsive logo center-block">
                </div>
                <div class="col-sm-12">

                    <form id="forgot-form" class="" name="forgotpasswordform" method="post">
                        <div class="form-group icon-group">
                <input type="email" class="form-control icon" id="email_forgot" value="<?php echo isset($_POST['email_forgot']) ?  $_POST['email_forgot'] : ''; ?>" placeholder="Enter email" name="email_forgot">
                            <i class="fa fa-user ic1" aria-hidden="true"></i>
                        </div>
                        <div>
                            <button type="submit" name="forgot_password" class="loginBtn btn btn-primary center-block">Submit</button>
                        </div>
                        <div>
                            <a href="<?php print CreateURL('index.php'); ?>" class="text-center center-block help login-link">Log in</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

</section>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</body>
</html>

