
<?php

$lang = $language->english();
//echo '<pre>';print_r($lang['Reports']);

$get_language_list= $DBFilter->RunSelectQuery("select * from language_support where is_file_uploaded='Y' AND is_deleted='N' and is_active='Y'");

/*======================================
		Developer	-	Jaishree Sahal
	    Date        -   5 june 2015
		Module      -   login
		view		-   login form
		SunArc Tech. Pvt. Ltd.
======================================
******************************************************/
if($_SESSION['usertype']!= '')
{
    ?>
    <script type="text/javascript">
        location.href="index.php?mod=company";
    </script>
    <?php
}
/*
if(isset($_POST['Reset']))
{
	unset($_SESSION['msg']);
}*/
?>
<!DOCTYPE html>
<html>
<head>
    <title>Login</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="style/new_design.css">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Raleway:400,500,600,700" rel="stylesheet">
    <script>
        function Clear()
        {
            document.loginform.user_email.value='';
            document.loginform.password.value='';
            document.loginform.user_email.focus();
            // document.getElementById('user_name').value='';
            // document.getElementById('company_name').value='';
            // document.getElementById('creation_date').value='';
            // document.getElementById('expiary_date').value='';
            // document.getElementById('subscription_plan').value='';
            // document.getElementById('company_email').value='';
            location.href="index.php?mod=login";
            return false;
        }
    </script>
    <style type="text/css">
        .bs-example{
            margin: 20px;
            width:80%;
        }
        /* Fix alignment issue of label on extra small devices in Bootstrap 3.2 */
        .form-horizontal .control-label{
            padding-top: 7px;
        }
    </style>
</head>
<body style="background-color: #fff;">
<!--<div class="sel-lang">
    <form id="login-forms"  action="" method="post">

        <select name="select_locale" onchange="this.form.submit()" class="form-control show-result select_locale">
            <option style="color: white;" value="">Select Language</option>
            <?php foreach ($get_language_list[0] as $get_language_list_list_value) { ?>
                <option <?php if ($get_language_list_list_value->short_code == $_SESSION['selected_language']){?> selected="selected" <?php } ?> style="color: white;" value="<?php echo $get_language_list_list_value->short_code ?>"> <?php echo trim($get_language_list_list_value->language_name); ?> </option>
            <?php } ?>
        </select>
        </form>
</div>-->

<section class="login-form">



    <?php

    //    ob_start();


    //    ob_flush();

    ?>

    <div class="container">

        <div class="main-wrapper">
            <form id="login-form" name="loginform" action="" method="post">

                <div class="row">

                    <div class="col-sm-12">
                        <img src="images/logo.png" alt="" class="img-responsive logo center-block">
                    </div>
                    <div class="col-sm-12">
                        <div class="form-group icon-group">
                            <input type="email" class="form-control icon" autocomplete="off" value="<?php echo isset($saved_cookie_email) ? $saved_cookie_email : '' ?>" id="user_email" placeholder="Enter email" name="user_email">

                            <!--                            <input type="email" class="form-control icon" value="--><?php //if($_POST['user_email'] != '') echo $_POST['user_email']?><!--" id="user_email" placeholder="Enter email" name="user_email">-->
                            <i class="fa fa-user ic1" aria-hidden="true"></i>
                        </div>
                        <div class="form-group icon-group">
                            <input type="password" class="form-control icon" autocomplete="off" value="<?php echo isset($saved_cookie_password) ? $saved_cookie_password : '' ?>" id="password" placeholder="Enter password" name="password">
                            <i class="fa fa-lock ic2" aria-hidden="true"></i>
                        </div>
                        <div class="pull-left">
                            <label class="switch">
                                <input type="checkbox" name="remember_me" value="on" <?php echo isset($saved_cookie_is_remember) ? 'checked' : '' ?>>
                                <span class="slider round"></span>
                            </label>
                        </div>
                        <div class="pull-left">
                            <p class="checked-label"><?php echo $lang['Keep me Logged in'] ?></p>
                        </div>

                        <div>
                            <button type="submit" name="submit_login" class="loginBtn btn btn-primary center-block"><?php echo $lang['Log In'] ?></button>
                        </div>
                        <div>
                            <a href="<?php print CreateURL('index.php', 'mod=login&do=forgot'); ?>" class="text-center center-block help forgot-link"><?php echo $lang['Forgot Password?'] ?></a>
                        </div>

                    </div>
                </div>
            </form>

        </div>
    </div>
    <div></div>
</section>
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>-->
<!--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>-->

</body>
</html>

