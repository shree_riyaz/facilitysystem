<?php
//$rows = $DBFilter->SelectRecord('feedback',"user_id='7'");
//print_r($rows);
//exit;
//print_r($_SESSION);
//include('class.encryptdecrypt');
if (isset($frmdata['clearsearch'])) {
    echo $frmdata['clearsearch'];
    unset($frmdata);
    unset($_SESSION['pageNumber']);
    $CFG->template = "rigs/list.php";
}

if (isset($frmdata['add_user'])) {    //print_r($_POST);

//    echo '<pre>'; print_r($frmdata); exit;

    $err = '';
    if (trim($frmdata['first_name']) == '') {
        $err .= "Please enter in user first name field.<br>";
        $frmdata['first_name'] = '';
    } else {
        if (validatename($frmdata['first_name']) == true) {
            $frmdata['first_name'] = $frmdata['first_name'];
        } else {
            $err .= "Only letters and white space allowed in user first name field.<br>";
        }
    }
    if (trim($frmdata['user_phone']) == '') {
        $err .= "Please enter user's phone number.<br>";
        $frmdata['user_phone'] = '';
    } else {
        if (validatePhNum($frmdata['user_phone']) == true) {
            $frmdata['user_phone'] = $frmdata['user_phone'];
        } else {
            $err .= "Please enter phone number in valid given format.<br>";
        }
    }
    if ($frmdata['company_id'] == '') {
        $err .= "Please select company name.<br>";
        $frmdata['company_id'] = '';
    } else {

        if ($frmdata['company_id'] != '') {
            $frmdata['company_id'] = $frmdata['company_id'];
        } else {
            $err .= "Please select company name.<br>";
        }
    }
    if ($frmdata['role_id'] == '') {
        $err .= "Please select role.<br>";
        $frmdata['role_id'] = '';
    } else {
        $frmdata['role_id'] = $frmdata['role_id'];
        if (checkUsers($frmdata['plan_name'], $frmdata['role_id']) == true) {
            $err .= "You are not allowed to add more users. Please contact to administrator for upgrading plan.";
            $_SESSION['error'] = $err;
            Redirect(CreateURL('index.php', 'mod=user'));
            die();
        }
    }

    if (trim($frmdata['assigned_to']) == '' && $frmdata['role_id'] != '4') {
        $err .= "Please assign cleaner to one supervisior .<br>";
        $frmdata['assigned_to'] = '';
    } else {
        if ($frmdata['role_id'] != '') {
            if ($frmdata['assigned_to'] == '' && $frmdata['role_id'] == '4') {
                $frmdata['assigned_to'] = $_SESSION['user_id'];
            } else {
                $frmdata['assigned_to'] = $frmdata['assigned_to'];
            }
        }
    }

    if (trim($frmdata['last_name']) == '') {
        $err .= "Please enter user last name.<br>";
        $frmdata['last_name'] = '';
    } else {
        if (validatename($frmdata['last_name']) == true) {
            $frmdata['last_name'] = $frmdata['last_name'];
        } else {
            $err .= "Only letters and white space allowed in user last name field.<br>";
        }
    }

    if (trim($frmdata['user_email']) == '') {
        $err .= "Please select email.<br>";
        $frmdata['user_email'] = '';
    } else {
        if (trim($frmdata['user_email']) != '') {
            if (!filter_var($frmdata['user_email'], FILTER_VALIDATE_EMAIL)) {
                $err .= "Please provide a valid email address.<br>";
                $frmdata['user_email'] = '';
            }
            /**********Added By Shivender***********/
            /*$email_check = $DBFilter->SelectRecord('users',"user_email='".$frmdata['user_email']."'");
            if($email_check)
            {
                $err.="Email is already exists.<br>";
                $frmdata['user_email']='';
            }*/
            /******Ended By Shivender******/
            else {
                if (checkMail(trim($frmdata['user_email'])) == 1) {
                    $err .= "Email is already exists.<br>";
                    $frmdata['user_email'] = '';
                } else {
                    $frmdata['user_email'] = $frmdata['user_email'];
                }
            }
        }
    }

    if (validateimage($_FILES["profile_image"]["name"]) == true) {
        if ($_FILES['profile_image']['size'] > 2097152)//added by : Neha Pareek. (02/12/2015)
        {
            $err .= 'Image size must be less than 2 MB' . '<br/>';
        } else {
            $_FILES["profile_image"]["name"] = $_FILES["profile_image"]["name"];
        }
    }

    if ($_FILES["profile_image"]["name"] != '') {
        $image_name = $_FILES["profile_image"]["name"];
        $source_image = $_FILES["profile_image"]["tmp_name"];
        $filename = time() . "_" . $image_name;
        //$folderpath = ROOT."images/uploads/".$filename;
        $folderpath = ROOT . "/images/uploads/" . $filename;
        move_uploaded_file($source_image, $folderpath);
    }


    if ($err == '') {
        $obj = new passEncDec;
        $user_password = MakeNewpassword(8);
        $password = $obj->encrypt_password($user_password);
        $nowdate = date("Y-m-d");
        $updatedate = date("Y-m-d H:i:s");
        $data = array('first_name' => $frmdata['first_name'], 'last_name' => $frmdata['last_name'], 'company_id' => $frmdata['company_id'], 'password' => $password, 'user_email' => $frmdata['user_email'], 'creation_date' => $nowdate, 'role_id' => $frmdata['role_id'], 'is_active' => 'Y', 'user_phone' => $frmdata['user_phone'], 'profile_image' => $filename, 'assigned_to' => $frmdata['assigned_to']);
        // print_r($data); exit;

        if ($DBFilter->InsertRecord('users', $data)) {

            //$pid=mysql_insert_id();
            $_SESSION['success'] = "User added successfully and your password is : <b>$user_password</b>";

            if (trim($frmdata['user_email']) != '') {
                $name = ucfirst($frmdata['first_name']);
                $email = $frmdata['user_email'];
                $password_mail = $user_password;
                $to = "$email";
                send_mail_to_company_admin_cleaner_supervisor($to, $password_mail, $name);
            }

            Redirect(CreateURL('index.php', 'mod=user'));
            die();
        } else {
            $_SESSION['error'] = "<font color=red>Due to some reason record could not be saved.</font>";
        }
    } else {
        $_SESSION['error'] = "<font color=red>" . $err . "</font>";
    }

}

elseif (isset($frmdata['update'])) {

    $err = '';
    if (trim($frmdata['first_name']) == '') {
        $err .= "Please enter in user first name field.<br>";
        $frmdata['first_name'] = '';
    } else {
        if (validatename($frmdata['first_name']) == true) {
            $frmdata['first_name'] = $frmdata['first_name'];
        } else {
            $err .= "Only letters and white space allowed in user first name field.<br>";
        }
    }

    if ($_SESSION['usertype'] == 'admin') {
        if (trim($frmdata['company_id']) == '') {
            $err .= "Please select company name.<br>";
            $frmdata['company_id'] = '';
        } else {
            if ($frmdata['company_id'] != '') {
                $frmdata['company_id'] = $frmdata['company_id'];
            } else {
                $err .= "Please select company name.<br>";
            }
        }
    }
    if (trim($frmdata['last_name']) == '') {
        $err .= "Please enter in user last name field.<br>";
        $frmdata['last_name'] = '';
    } else {
        if (validatename($frmdata['last_name']) == true) {
            $frmdata['last_name'] = $frmdata['last_name'];
        } else {
            $err .= "Only letters and white space allowed in user last name field.<br>";
        }
    }

    if (trim($frmdata['role_id']) == '') {
        $err .= "Please select role.<br>";
        $frmdata['role_id'] = '';
    } else {
        if ($frmdata['role_id'] != '') {
            $frmdata['role_id'] = $frmdata['role_id'];
        } else {
            $err .= "Please select role.<br>";
        }
    }

    if (trim($frmdata['assigned_to']) == '' && $frmdata['role_id'] == '2') {
        $err .= "Please assign cleaner to one supervisor .<br>";
        $frmdata['assigned_to'] = '';
    } else {
        //if($frmdata['assigned_to']=='' && $frmdata['role_id'] =='4')
        if ($frmdata['role_id'] == '4') {
            $frmdata['assigned_to'] = $_SESSION['user_id'];
        } else {
            $frmdata['assigned_to'] = $frmdata['assigned_to'];
        }
    }
    //Added By : Neha Pareek. Dated : 05 Nov 2015. for edit super admin profile
    if ($_SESSION['usertype'] == 'super_admin' && ($_SESSION['user_id'] == $_GET['id'])) {

        $cmp = $DBFilter->SelectRecord('users', "is_deleted = 'N' and is_active = 'Y' and user_id=" . $_SESSION['user_id']);
        $frmdata['company_id'] = $cmp->company_id;
        $frmdata['assigned_to'] = $cmp->assigned_to;
    }

    //Added By : Neha Pareek. Dated : 05 Nov 2015. for empty password check
    // if(trim($frmdata['password']) == '')
    // {
    // $err .= "Please enter user password.<br>";
    // $frmdata['password']='';
    // $frmdata['confirm_password']='';
    // }
    // else
    // {

    if (trim($frmdata['password']) == '' and trim($frmdata['confirm_password']) == '') {
        $flag = 1;
        unset($_SESSION['change_pass']);
        //$frmdata['password'] = $frmdata['pwd'];
    } else {
        /*if(trim($frmdata['password']) != '')
        {
            if(trim($frmdata['confirm_password']) == '')
            {
                $err .= "Please enter user password again with confirm password.<br>";
                $frmdata['password']='';
                $frmdata['confirm_password']='';
            }
        }*/
        $flag = 0;
        if (trim($frmdata['password']) != '' && trim($frmdata['confirm_password']) == '') {
            $err .= "Please enter user confirm password.<br>";
            $frmdata['confirm_password'] = '';
        }

        if (($frmdata['confirm_password'] != '') && (trim($frmdata['confirm_password']) != trim($frmdata['password']))) {
            $err .= "Confirm password doesn't match.<br>";
            $frmdata['confirm_password'] = '';
        } else {
            $frmdata['password'] = $frmdata['password'];
            $_SESSION['change_pass'] = " And changed password is : " . $frmdata['password'];
        }
    }
    $pvalue = strlen(trim($frmdata['password']));
    if (!empty($pvalue) && $pvalue < 6 || $pvalue > 16) {
        $err .= 'Password should be between 6 to 16 characters.<br>';
    }

    if (trim($frmdata['user_email']) == '') {
        $err .= "Please select email.<br>";
        $frmdata['user_email'] = '';
    } else {
        if (trim($frmdata['user_email']) != '') {
            if (!filter_var($frmdata['user_email'], FILTER_VALIDATE_EMAIL)) {
                $err .= "Please provide a valid email address.<br>";
                $frmdata['user_email'] = '';
            } else {
                /**********Added By Shivender***********/
                //$email_check = $DBFilter->SelectRecord('company_detail',"company_email='".$frmdata['company_email']."'");
                /* $user_email_check = $DBFilter->SelectRecord('users',"user_email='".$frmdata['user_email']."'");//edited by : Neha Pareek
                if($user_email_check)
                {
                    $err.="Email is already exists.<br>";
                    $frmdata['user_email']='';
                }
                /******Ended By Shivender******/
                if (checkMail(trim($frmdata['user_email'])) == 1) {
                    $err .= "Email is already exists.<br>";
                    $frmdata['user_email'] = '';
                } else {
                    $frmdata['user_email'] = trim($frmdata['user_email']);
                }
            }
        }
    }
    if ($frmdata['user_phone'] == '') {
        $err .= "Please enter user phone number.<br>";
        $frmdata['user_phone'] = '';
    } else {
        if (validatePhNum($frmdata['user_phone']) == true) {
            $frmdata['user_phone'] = $frmdata['user_phone'];
        } else {
            $err .= "Please enter phone number in valid given format.<br>";
        }
    }
   /*profile picture*/
    
    if ($_FILES["company_logo"]["name"] != '') {
        $image_name = $_FILES["company_logo"]["name"];
        $source_image = $_FILES["company_logo"]["tmp_name"];
        $filename = time() . "_" . $image_name;
        $folderpath = ROOT . "/images/company_logo/" . $filename;
        $oldfile = ROOT . "/images/company_logo/" . $frmdata['logo'];
        unlink($oldfile);
        $aa = move_uploaded_file($source_image, $folderpath);
    } else {
        $filename = $frmdata['company_logo_hidden'];
    }

    if ($err == '') {

        $updatedate = date("Y-m-d H:i:s");
        if ($_SESSION['company_id'] != '') {
            $frmdata['company_id'] = $_SESSION['company_id'];
        }
        if ($frmdata['password']) {
            $obj = new passEncDec;
            $password = $obj->encrypt_password($frmdata['password']);
            $data = array('first_name' => $frmdata['first_name'], 'last_name' => $frmdata['last_name'], 'password' => $password, 'user_email' => $frmdata['user_email'], 'company_id' => $frmdata['company_id'], 'role_id' => $frmdata['role_id'], 'is_active' => $frmdata['is_active'],'user_phone' => $frmdata['user_phone'], 'assigned_to' => $frmdata['assigned_to']);
            $company_data_logo = array('company_logo' => $filename);
        } else {
            $obj = new passEncDec;
            $password = $obj->encrypt_password($frmdata['pwd']);
            $data = array('first_name' => $frmdata['first_name'], 'last_name' => $frmdata['last_name'], 'password' => $password, 'user_email' => $frmdata['user_email'], 'company_id' => $frmdata['company_id'], 'role_id' => $frmdata['role_id'], 'is_active' => $frmdata['is_active'],'user_phone' => $frmdata['user_phone'], 'assigned_to' => $frmdata['assigned_to']);
            $company_data_logo = array('company_logo' => $filename);
        }

        if (($_SESSION['usertype'] == 'super_admin' && $frmdata['role_id'] == '2') or ($_SESSION['usertype'] == 'company_admin' && $frmdata['role_id'] == '2') or ($_SESSION['usertype'] == 'super_admin' && $frmdata['role_id'] == '1')) {
            $user_name = ucwords($frmdata['first_name'] . ' ' . $frmdata['last_name']);
            //echo $user_name; exit;
            $cmp_detail = $DBFilter->SelectRecord('company_detail', "company_id =" . $_SESSION['company_id']);
            $company_data = array('company_name' => $cmp_detail->company_name, 'plan_id' => $cmp_detail->plan_id, 'creation_date' => $cmp_detail->creation_date, 'expiary_date' => $cmp_detail->expiary_date, 'user_name' => $user_name, 'company_email' => $frmdata['user_email'], 'password' => $password, 'company_logo' => $cmp_detail->company_logo);


            try {
                $DBFilter->UpdateRecord('users', $data, "user_id =$id");
                $DBFilter->UpdateRecord('company_detail', $company_data, "company_id =" . $_SESSION['company_id']);
                if (($_SESSION['usertype'] == 'company_admin')){
                    $DBFilter->UpdateRecord('company_detail', $company_data_logo, "company_id =" . $_SESSION['company_id']);
                }

            } catch (Exception $e) {
                echo $e;
                exit;
            }

        } else {

            $DBFilter->UpdateRecord('users', $data, "user_id ='$id'");
        }
        //  print_r($data );exit;
        $user_pass = $frmdata['password'];

        if ($_SESSION['user_id'] == $_GET['id'] && $_SESSION['change_pass'] == '')//Added By : Neha Pareek. Dated : 02 dec 2015
        {
            $_SESSION['success'] = "User updated successfully.";
            Redirect(CreateURL('index.php', 'mod=user&do=edit&id=' . $_GET['id']));
            die();
        } elseif ($_SESSION['user_id'] == $_GET['id'] && $_SESSION['change_pass'] != '')//Added By : Neha Pareek. Dated : 02 dec 2015
        {
//            $_SESSION['success'] = "User updated successfully and password for this user is : <b>$user_pass</b> ";
            $_SESSION['success'] = "Password updated successfully.";
            Redirect(CreateURL('index.php', 'mod=user&do=edit&id=' . $_GET['id']));
            die();
        } elseif ($_SESSION['change_pass'] != '') {
            $_SESSION['success'] = "User updated successfully.";
        } else {
            $_SESSION['success'] = "User updated successfully.";
        }

        if (trim($frmdata['user_email']) != '' && $_SESSION['change_pass'] != '') {

            $name = ucfirst($frmdata['first_name']);
            $email = $frmdata['user_email'];
            $password_mail = $frmdata['password'];
            $to = "$email";
            $subject = "Facility System Password";
            send_mail_to_company_admin_cleaner_supervisor($to, $password_mail, $name);
        }


        Redirect(CreateURL('index.php', 'mod=user'));
        die();
    } else {
        $_SESSION['error'] = $err;
    }

}

//============================================================================

//Delete Multiple or Other Actions added by : Neha Pareek

//============================================================================
if ($frmdata['actions']) {

    if ($frmdata['chkbox'] == '' && isset($frmdata['actions'])) {
        $_SESSION['error'] = 'Please select atleast one user';
    } else {
        $ids = $frmdata['chkbox'];
        $action = $frmdata['actions'];
        $cond = "user_id=";
        foreach ($ids as $id) {
            # code...
            //echo "<pre>";
            //print_r($DBFilter->SelectRecords('feedback',"user_id='4'"));
            /*$row = $DBFilter->SelectRecords('feedback',"user_id='$id' or assigned_to='$id'");
            if($row)
            {
            // 	print_r($row);
            // 	exit;
                $_SESSION['error'] = 'Please delete user related feedback first.';
                Redirect(CreateURL('index.php', 'mod=feedback'));
                die();
            }
            else
            {*/
            //exit;
            multipleAction($ids, 'users', $action, $cond);
            $_SESSION['success'] = 'Users has been ' . $action . 'd successfully.';
            Redirect(CreateURL('index.php', 'mod=user'));
            die();
            //}
        }

    }
}
//============================================================================

//Delete the User

//============================================================================
elseif ($do == 'del') {
    $data = array('is_deleted' => 'Y', 'is_active' => 'N');
    $related_detail = DeleteRelatedData($id, $_SESSION['usertype'], 'users');
    if ($related_detail == true) {
        $result = $DBFilter->UpdateRecord('users', $data, "user_id ='" . $id . "'");
        //exit;
        $_SESSION['success'] = 'User has been deleted successfully.';

        Redirect(CreateURL('index.php', 'mod=user'));
        die();
    }
}

/* function is used to allowed number of users according to plan */


?>