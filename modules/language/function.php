<?php
//$rows = $DBFilter->SelectRecord('feedback',"user_id='7'");
//print_r($rows);
//exit;
//print_r($_SESSION);
//include('class.encryptdecrypt');
if (isset($frmdata['clearsearch'])) {
    echo $frmdata['clearsearch'];
    unset($frmdata);
    unset($_SESSION['pageNumber']);
    $CFG->template = "rigs/list.php";
}

if (isset($frmdata['add_language'])) {    //print_r($_POST);

//    echo '<pre>'; print_r($frmdata); exit;

    $err = '';
    if (trim($frmdata['language_name']) == '') {
        $err .= "Please enter in language name field.<br>";
        $frmdata['language_name'] = '';
    } else {
        if (validatename($frmdata['language_name']) == true) {
            $frmdata['language_name'] = $frmdata['language_name'];
        } else {
            $err .= "Only letters and white space allowed in user first name field.<br>";
        }
    }

    if (trim($frmdata['short_code']) == '') {
        $err .= "Please enter in language name field.<br>";
        $frmdata['short_code'] = '';
    }


    if ($err == '') {
        $obj = new passEncDec;
        $user_password = MakeNewpassword(8);
        $password = $obj->encrypt_password($user_password);
        $nowdate = date("Y-m-d");
        $updatedate = date("Y-m-d H:i:s");
        $data = array('language_name' => $frmdata['language_name'], 'short_code' => $frmdata['short_code'], 'is_active' => 'Y','is_file-uploaded' => 'N','is_deleted' => 'N', );
        // print_r($data); exit;

        if ($DBFilter->InsertRecord('language_support', $data)) {

            $_SESSION['success'] = "Language added successfully.";
            Redirect(CreateURL('index.php', 'mod=language'));
            die();

        } else {
            $_SESSION['error'] = "<font color=red>Oops! Due to some reason record could not be saved.</font>";
        }
    } else {
        $_SESSION['error'] = "<font color=red>" . $err . "</font>";
    }

}

elseif (isset($frmdata['update_language'])) {

    $err = '';
//    echo '<pre>'; print_r($frmdata); exit;
    $lang_id = $_GET['lang_id'];
    if (trim($frmdata['language_name']) == '') {
        $err .= "Please enter in language name field.<br>";
        $frmdata['language_name'] = '';
    } else {
        if (validatename($frmdata['language_name']) == true) {
            $frmdata['language_name'] = $frmdata['language_name'];
        } else {
            $err .= "Only letters and white space allowed in user first name field.<br>";
        }
    }


    if ($_FILES["file_name"]["name"] == '' && !empty($frmdata['file_name_available'])) {
        $image_name = $frmdata['file_name_available'];
    }


    if (empty($frmdata['file_name_available'])) {
        $extension = end(explode(".", $_FILES["file_name"]["name"]));

        if ($_FILES["file_name"]["name"] == '') {
            $err .= "Please choose file.<br>";

        }elseif ($extension != "json") {
            $err .= 'File type must be in json format only.' . '<br/>';
        }
        else{
                $_FILES['file_name']['name'] = $_FILES['file_name']['name'];
        }
    }

    if (!empty($frmdata['file_name_available']) && !empty($_FILES["file_name"]["name"]) ) {
        $extension = end(explode(".", $_FILES["file_name"]["name"]));

       if ($extension != "json") {
            $err .= 'File type must be in json format only.' . '<br/>';
        }
        else{
            $_FILES['file_name']['name'] = $_FILES['file_name']['name'];
        }
    }

    if ($err == '') {
        if ($_FILES["file_name"]["name"] != '') {
            $source_image = $_FILES["file_name"]["tmp_name"];
//            $filename = time() . "_" . $image_name;
            $image_name = $frmdata['short_code_available'].'.json';
            $folderpath = ROOT . "/language/" . $image_name;
            move_uploaded_file($source_image, $folderpath);

        }
            $data = array('language_name' => $frmdata['language_name'], 'file_name' => $image_name, 'is_file_uploaded' => 'Y');
            $DBFilter->UpdateRecord('language_support', $data, "id =" . $lang_id);

                $_SESSION['success'] = "Language updated successfully.";
                Redirect(CreateURL('index.php', 'mod=language'));
                die();


        } else {
            $_SESSION['error'] = "<font color=red>" . $err . "</font>";
        }
}
//============================================================================

//Delete Multiple or Other Actions added by : Neha Pareek

//============================================================================
if ($frmdata['actions']) {

    if ($frmdata['chkbox'] == '' && isset($frmdata['actions'])) {
        $_SESSION['error'] = 'Please select atleast one user';
    } else {
        $ids = $frmdata['chkbox'];
        $action = $frmdata['actions'];
        $cond = "user_id=";
        foreach ($ids as $id) {
            # code...
            //echo "<pre>";
            //print_r($DBFilter->SelectRecords('feedback',"user_id='4'"));
            /*$row = $DBFilter->SelectRecords('feedback',"user_id='$id' or assigned_to='$id'");
            if($row)
            {
            // 	print_r($row);
            // 	exit;
                $_SESSION['error'] = 'Please delete user related feedback first.';
                Redirect(CreateURL('index.php', 'mod=feedback'));
                die();
            }
            else
            {*/
            //exit;
            multipleAction($ids, 'users', $action, $cond);
            $_SESSION['success'] = 'Users has been ' . $action . 'd successfully.';
            Redirect(CreateURL('index.php', 'mod=user'));
            die();
            //}
        }

    }
}
//============================================================================

//Delete the User

//============================================================================
elseif ($do == 'del') {
    $data = array('is_deleted' => 'Y', 'is_active' => 'N');
    $related_detail = DeleteRelatedData($id, $_SESSION['usertype'], 'users');
    if ($related_detail == true) {
        $result = $DBFilter->UpdateRecord('users', $data, "user_id ='" . $id . "'");
        //exit;
        $_SESSION['success'] = 'User has been deleted successfully.';

        Redirect(CreateURL('index.php', 'mod=user'));
        die();
    }
}

/* function is used to allowed number of users according to plan */


?>