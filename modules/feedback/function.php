<?php

    if (isset($frmdata['clearsearch']))
    {
        unset($frmdata);
        unset($_SESSION['pageNumber']);
        $CFG->template = "feedback/list.php";
    }
   $frmdata['company_id'] = $_SESSION['company_id']; //Added By Neha Pareek. Dated : 03 Nov 2015
	
    if (isset($frmdata['add_feedback']))
    {
        date_default_timezone_set('Asia/Kolkata');

//        echo '<pre>'; print_r($super_visor_id_by_device);exit;

        $err = '';
		if (trim($frmdata['service_id']) == '')
		{
            $err .= "Please select feedback type.<br>";
			$frmdata['service_id']='';
        }
		else
		{
			if($frmdata['service_id']!='')
			{
				$frmdata['service_id']= $frmdata['service_id'];
			}
			
		}
		if (trim($frmdata['device_id']) == '')
		{
            $err .= "Please select device.<br>";
			$frmdata['device_id']='';
        }
		if (trim($frmdata['rating']) == '')
		{
            $err .= "Please select rating type.<br>";
			$frmdata['rating']='';
        }
		else
		{
			if($frmdata['rating']!='')
			{
				$frmdata['rating']= $frmdata['rating'];
			}
			
		}
	/*	if (trim($frmdata['device_status']) == '')
		{
            $err .= "Please Select Device Status.<br>";
			$frmdata['device_status']='';
        }
		else
		{
			if($frmdata['device_status']!='')
			{
				$frmdata['device_status']= $frmdata['device_status'];
			}
			
		}*/
		if (trim($frmdata['assigned_to']) == '')
		{
            $err .= "Please assigned feedback to any supervisor.<br>";
			$frmdata['assigned_to']='';
        }
		else
		{
			if($frmdata['assigned_to']!='')
			{
				$frmdata['assigned_to']= $frmdata['assigned_to'];
			}
			
		}
		if (trim($frmdata['fault_id']) == '')
		{
            $err .= "Please select any fault.<br>";
			$frmdata['fault_id']='';
        }
		else
		{
			if($frmdata['fault_id']!='')
			{
				$frmdata['fault_id']= $frmdata['fault_id'];
			}
			
		}
		if (trim($frmdata['status']) == '')
		{
            $err .= "Please select status of feedback.<br>";
			$frmdata['status']='';
        }
		else
		{
			if($frmdata['status']!='')
			{
				$frmdata['status']= $frmdata['status'];
			}
			
		}
        $super_visor_id_by_device_query = "select user_id from device where device_id=". $frmdata['device_id'];
        $super_visor_id_by_device = $DBFilter->RunSelectQuery($super_visor_id_by_device_query);

        $super_visor_id_by_cleaner_id_query = "select assigned_to from users where user_id=". $frmdata['assigned_to'];
        $super_visor_id_by_cleaner_id = $DBFilter->RunSelectQuery($super_visor_id_by_cleaner_id_query);
        if ($super_visor_id_by_device != $super_visor_id_by_cleaner_id){

            $err .= "Selected cleaner not belongs to this device, please change device.<br>";
        }
		
		if ($err == '')
        {	//print_r($_SESSION);
//			$date = date('m/d/Y');
            $date = date('Y-m-d H:i:s');
			$updatedate = date('Y-m-d');
			//Added By : Neha Pareek. Dated : 30 Nov 2015. For getting service_id of particular company
			$srv_name = $frmdata['service_id'];
			$service = $DBFilter->SelectRecords('services',"is_deleted='N' and is_active = 'Y' and service_name='".$srv_name."' and company_id=".$_SESSION['company_id']);
			//echo "<pre>";print_r($service);exit;
            $interval_margin = get_hour_by_creation_date($date);

            if($frmdata['rating'] == 'Excellent' || $frmdata['rating'] == 'Good'  ){
                $fault_id = 0;
                $feedback_status = 1;
            }else{
                $fault_id = $frmdata['fault_id'];
                $feedback_status = $frmdata['status'];
            }
			$data = array('service_id'=>$service[0][0]->service_id,'device_id'=>$frmdata['device_id'], 'rating'=>$frmdata['rating'],'creation_date'=>$date,'user_id'=>$_SESSION['user_id'],'company_id'=>$frmdata['company_id'],'fault_id'=>$fault_id,'feedback_status'=>$feedback_status,'assigned_to'=>$frmdata['assigned_to'],'interval_time'=>$interval_margin,'created_at'=>date('Y-m-d'));
//				echo '<pre>';print_r($data);exit;
			if ($DBFilter->InsertRecord('feedback', $data))
            {			
//				$pid=mysql_insert_id();
				$pid=mysqli_insert_id();
				$_SESSION['success'] = "Feedback added successfully.";
                Redirect(CreateURL('index.php', 'mod=feedback'));
                die();
            }
			else
            {
				$_SESSION['error'] = "<font color=red>Please insert correct value.</font>";
            }
        }
        else
        {
            $_SESSION['error'] = "<font color=red>" . $err . "</font>";
        }
        
    }
    elseif (isset($frmdata['update']))
    {
       $err = '';
//	 echo '<pre>'; print_r($frmdata);exit;
		if (trim($frmdata['service_id']) == '')
		{
            $err .= "Please select feedback type.<br>";
			$frmdata['service_id']='';
        }
		else
		{
			if($frmdata['service_id']!='')
			{
				$frmdata['service_id']=$frmdata['service_id'];
			}
			$frmdata['tc_feedback_type']= $frmdata['tc_feedback_type'];
		}
		if($frmdata['device_id']!='')
		{
			$frmdata['device_id']=$frmdata['device_id'];
		}
		if (trim($frmdata['device_id']) == '')
		{
            $err .= "Please select device.<br>";
			$frmdata['device_id']='';
        }
		if (trim($frmdata['rating']) == '')
		{
            $err .= "Please select rating.<br>";
			$frmdata['rating']='';
        }
		else
		{
			if($frmdata['rating']!='')
			{
				$frmdata['rating']= $frmdata['rating'];
			}
			
		}
		if (trim($frmdata['fault_id']) == '')
		{
            $err .= "Please select fault type.<br>";
			$frmdata['fault_id']='';
        }
		else
		{
			if($frmdata['fault_id']!='')
			{
				$frmdata['fault_id']= $frmdata['fault_id'];
			}
			
		}
		if (trim($frmdata['device_status']) == '')
		{
            $err .= "Please select device status.<br>";
			$frmdata['device_status']='';
        }
		else
		{
			if($frmdata['device_status']!='')
			{
				$frmdata['device_status']= $frmdata['device_status'];
			}
			
		}
		if (trim($frmdata['status']) == '')
		{
            $err .= "Please select status of feedback.<br>";
			$frmdata['status']='';
        }
		else
		{
			if($frmdata['status']!='')
			{
				$frmdata['status']= $frmdata['status'];
			}
			
		}
		if (trim($frmdata['assigned_to']) == '')
		{
            $err .= "Please assign feedback to any supervisor.<br>";
			$frmdata['assigned_to']='';
        }
		else
		{
			if($frmdata['assigned_to']!='')
			{
				$frmdata['assigned_to']= $frmdata['assigned_to'];
			}
			
		}
		//=============
		
		if ($err == '')
        {
			$updatedate = date("Y-m-d");
            if($frmdata['rating'] == 'Excellent' || $frmdata['rating'] == 'Good'  ){
                $fault_id = 0;
                $feedback_status = 1;
            }else{
                $fault_id = $frmdata['fault_id'];
                $feedback_status = $frmdata['status'];
            }
			$data = array('service_id'=>$frmdata['service_id'],'device_id'=>$frmdata['device_id'], 'rating'=>$frmdata['rating'],'device_status'=>$frmdata['device_status'],'user_id'=>$_SESSION['user_id'],'company_id'=>$frmdata['company_id'],'fault_id'=>$fault_id,'feedback_status'=>$feedback_status,'modified_date'=>$updatedate,'is_active'=>$frmdata['is_active'],'assigned_to'=>$frmdata['assigned_to']);
			$id = $_REQUEST['id'];
			/*	print_r($data);
			   exit;*/
			$DBFilter->UpdateRecord('feedback', $data, "feedback_id ='$id'");
			//  print_r($data );
			 $_SESSION['success'] = "Feedback updated successfully.";
             Redirect(CreateURL('index.php', 'mod=feedback'));
             die();
            
        }
        else
        {
            $_SESSION['error'] = $err;
        }
        
    }

    //============================================================================
        
    //Delete Multiple or Other Actions added by : Neha Pareek
        
    //============================================================================
    if($frmdata['actions'])
	{
		if($frmdata['chkbox']=='' && isset($frmdata['actions']))
		{
			$_SESSION['error'] = 'Please select atleast one feedback.';
		}
		else
		{
			$total_chk= count($frmdata['chkbox']);// Count Selected Checkboxes
			//exit;
			for($i = 0; $i < $total_chk; $i++) //Create Array of multiple checkbox id's
			{
			$ids = $frmdata['chkbox'];
			}
		$action = $frmdata['actions'];
		$cond = "feedback_id=";
		//echo "<pre>"; print_r($ids);
		//exit;
		multipleAction($ids,'feedback',$action,$cond);
		$_SESSION['success'] = 'Feedbacks has been '.$action.'d successfully.';
		Redirect(CreateURL('index.php', 'mod=feedback'));
		die();
			/*foreach ($ids as $id)
			{
				# code...
				$row = $DBFilter->SelectRecord('feedback',"device_id ='$id'");
				if($row)
				{
					$_SESSION['error'] = 'Device is in use. Please delete device related feedback first.';
					Redirect(CreateURL('index.php', 'mod=feedback'));
					die();
				}
				else
				{
					multipleAction($ids,'device',$action,$cond);
					$_SESSION['success'] = 'Devices has been updated successfully.';
					Redirect(CreateURL('index.php', 'mod=device'));
					die();
				}
			}*/
		
		}
	}

    //============================================================================
        
    //Delete the feedback
        
    //============================================================================
    elseif ($do == 'del')
    {
       $data = array('is_deleted'=>'Y','is_active'=>'N');
	   $DBFilter->UpdateRecord('feedback', $data, "feedback_id ='$id'");
       $_SESSION['success'] = 'Feedback has been deleted successfully.';
        
        Redirect(CreateURL('index.php', 'mod=feedback'));
        die();
    }

?>