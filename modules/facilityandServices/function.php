<?php

	if (isset($frmdata['clearsearch']))
    {
        unset($frmdata['keyword']);
		unset($_SESSION['pageNumber']);
        $CFG->template = "facilityandServices/list.php";
    }
    
    if (isset($frmdata['add']))
    {
		$err = '';
		// echo '<pre>';
		// print_r($frmdata);
		// exit;
		if (trim($frmdata['service_name']) == '')
		{
            $err .= "Please enter service name.<br>";
			$frmdata['service_name']='';
        }
		else
		{
			if(validatename($frmdata['service_name'])== true)
			{
				$frmdata['service_name']= $frmdata['service_name'];
			}
			else
			{
				$err .= "Only alpha numeric and white space allowed for service name.<br>";
			}
		
		}
		if (trim($frmdata['service_description']) == '')
		{
            $err .= "Please enter service description.<br>";
			$frmdata['service_name']='';
        }
		else
		{
			if(validatename($frmdata['service_description'])== true)
			{
				$frmdata['service_description']= $frmdata['service_description'];
			}
			else
			{
				$err .= "Only alpha numeric and white space allowed for service description.<br>";
			}
		
		}
		if (trim($frmdata['company_id']) == '')
		{
            $err .= "Please Select Company.<br>";
			$frmdata['company_id']='';
        }
		else
		{
			$frmdata['company_id']= $frmdata['company_id'];
		}
	    if(isset($_FILES["image"]["name"])== true)
		{
			$_FILES["image"]["name"] = $_FILES["image"]["name"];
		}
		else
		{
			$err .= "Please select only an image.<br>";
		}
	
		if ($err == '')
        {
			if($_FILES["image"]["name"]!='')
			
			{
				$image_name = $_FILES["image"]["name"];
				$source_image=$_FILES["image"]["tmp_name"];
			}
		
			$filename= time()."_".$image_name;
			$folderpath = ROOT."images/uploads/".$filename;
				move_uploaded_file($source_image,$folderpath);
			if(!move_uploaded_file($source_image,$folderpath))
			{
				$err.="There is problem to upload the Image please try again";
			} 
						
			$data = array('service_name'=>$frmdata['service_name'],'service_description'=>$frmdata['service_description'],'company_id'=>$frmdata['company_id'],'image'=>$filename);
						
			if ($id= $DBFilter->InsertRecord('services', $data))
            {
				/* loop add faults with related service in service-fault table*/
				for($i=0;$i<count($frmdata['faults']);$i++)
				{
					$faults_data = array('service_id'=>$id, 'fault_id' => $frmdata['faults'][$i]);
					$add_faults = $DBFilter->InsertRecord('service_faults', $faults_data);
				}
				$_SESSION['success'] = "Facility/Service added successfully.";
                Redirect(CreateURL('index.php', 'mod=facilityandServices'));
                die();
            }
			else
            {
				$_SESSION['error'] = "<font color=red>Please insert correct values in service description.</font>";
            }
        }
        else
        {
            $_SESSION['error'] = "<font color=red>" . $err . "</font>";
        }
		
        
    }
    elseif (isset($frmdata['update']))
    {
        $err = '';
		// echo '<pre>';
		// print_r($frmdata);exit;
		if (trim($frmdata['service_name']) == '')
		{
            $err .= "Please enter service name.<br>";
			$frmdata['service_name']='';
        }
		else
		{
			if(validatename($frmdata['service_name'])== true)
			{
				$frmdata['service_name']= $frmdata['service_name'];
			}
			else
			{
				$err .= "Only alpha numeric and white space allowed for service name.<br>";
			}
		
		}
		if (trim($frmdata['service_description']) == '')
		{
            $err .= "Please enter service description.<br>";
			$frmdata['service_name']='';
        }
		else
		{
			if(validatename($frmdata['service_description'])== true)
			{
				$frmdata['service_description']= $frmdata['service_description'];
			}
			else
			{
				$err .= "Only alpha numeric and white space allowed for service description.<br>";
			}
		
		}
		if (trim($frmdata['company_id']) == '')
		{
            $err .= "Please Select Company.<br>";
			$frmdata['company_id']='';
        }
		else
		{
			$frmdata['company_id']= $frmdata['company_id'];
		}
		
		
		if (trim($_FILES["image"]["name"]) == '')
		{
           $filename = $frmdata['img_name'];
		}
		else
		{
			if(validateimage($_FILES["image"]["name"])== true)
			{
				$_FILES["image"]["name"] = $_FILES["image"]["name"];
			}
			else
			{
			 	$err .= "Please select only an image.<br>";
			}
		}
		if($_FILES["image"]["name"]!='')
		{
			$image_name = $_FILES["image"]["name"];
			$source_image=$_FILES["image"]["tmp_name"];
			$filename= time()."_".$image_name;
			$folderpath = ROOT."images/uploads/".$filename;
			move_uploaded_file($source_image,$folderpath);
		} 
		
		if ($err == '')
        {
			$data = array('service_name'=>$frmdata['service_name'],'service_description'=>$frmdata['service_description'],'company_id'=>$frmdata['company_id'],'image'=>$filename);

            if($DBFilter->UpdateRecord('services',$data,"service_id ='$id'"))
            {
				$update_data = array('is_deleted'=>'Y');
				$update_faults = $DBFilter->UpdateRecord('service_faults',$update_data,"service_id='$id'");
				
				/* loop updates faults with related service in service-fault table*/
				for($i=0;$i<count($frmdata['faults']);$i++)
				{
					$faults_data = array('service_id'=>$id, 'fault_id' => $frmdata['faults'][$i]);
					$add_faults = $DBFilter->InsertRecord('service_faults', $faults_data);
				}
				//exit;
				$_SESSION['success'] = "Facility / Services updated successfully.";
                Redirect(CreateURL('index.php', 'mod=facilityandServices'));
                die();
            }
        }
        else
        {
            $_SESSION['error'] = $err;
        }
        
    }
    //============================================================================
        
    //Delete the role
        
    //============================================================================
    elseif ($do == 'del')
    {
        $data = array('is_deleted'=>'Y');
			
        $DBFilter->UpdateRecord('services', $data, "service_id ='$id'");
         
        $_SESSION['success'] = 'Facility/Services has been deleted successfully.';
        
        Redirect(CreateURL('index.php', 'mod=facilityandServices'));
        die();
    }

?>