<?php 

	 $do = (isset($_REQUEST['do']) ? $_REQUEST['do'] : 'list'); 
	 $id = (isset($_REQUEST['id']) ? $_REQUEST['id'] : '');
	 $userAlowedActions = array('view','edit','list','add','logout');
	 
	if(!in_array($do,$userAlowedActions) and $_SESSION['usertype'] == 'user')	
	{
?> 
	<script type="text/javascript">
		document.location.href="<?php
		echo 'index.php?mod=facilityandServices&do=list&id='.$_SESSION['company_id']; ?>";
	</script>
   <?php
	   exit;
	}
		include_once('function.php');
		if($_SESSION['company_id']!='')
		{
			$company = $DBFilter->SelectRecord('company_detail',"company_id='".$_SESSION['company_id']."'");
			$faults = $DBFilter->SelectRecords('faults');
		}
		else {
		$company = $DBFilter->SelectRecords('company_detail'); }
		$faults = $DBFilter->SelectRecords('faults');
		//echo '<pre>'; //print_r($faults);
		switch ($do)
    {
		case 'logout':
		unset($_SESSION);
			{
				@session_unset();
				@session_destroy();
			}
						
		$CFG->template = "facilityandServices/logout.php";			
		break;	
						
		case 'add':
			$CFG->template = "facilityandServices/add.php";
            break;
		
		case 'edit':
		    $Row= $DBFilter->SelectRecord("services","service_id ='$id'");
				 
			$faults_row= $DBFilter->RunSelectQuery("select f.fault_name,f.fault_id from service_faults as sf
			left join faults as f on sf.fault_id = f.fault_id where sf.service_id = '$id' and sf.is_deleted='N'");
						
			$CFG->template = "facilityandServices/edit.php";
            break;
		 
		case 'view':
		PaginationWork();
            $totalCount = 0;
            $Row  = getfacilityandServicesFaults($totalCount);
						
			if($_REQUEST['call']=='API')
			{
				/*print_r($_POST);
				echo 'ohk';*/
//				$company_id = $frmdata['company_id'];
				$company_id = $_REQUEST['company_id'];
				$Row= $DBFilter->SelectRecord('services',"company_id=".$company_id);
				$company_services = array();
				$company_services['service_name'] = $Row->service_name;
				$faults_row= $DBFilter->RunSelectQuery("select f.fault_name,f.fault_id,f.fault_image,f.active_image from faults as f
			    left join services as s on f.service_id = s.service_id where f.service_id = '".$Row->service_id."' and f.is_deleted='N' ");

				$fault_array = array();
				for($i=0;$i<count($faults_row[0]);$i++)
				{
					$fault_array[$i]['fault_id']= $faults_row[0][$i]->fault_id;
					$fault_array[$i]['fault_name']= $faults_row[0][$i]->fault_name;
					$fault_array[$i]['fault_image']= IMAGEURL."faults_images/".trim($faults_row[0][$i]->fault_image);
					$fault_array[$i]['fault_image_active']= isset($faults_row[0][$i]->active_image) ? IMAGEURL."faults_images/".trim($faults_row[0][$i]->active_image): null;
				}
				
				$company_services['faults']=$fault_array;
//				echo "<pre>"; print_r($company_services); exit;
				$company_services['status']=1;
				echo json_encode($company_services);
				exit;
			}
			 
			
				$service = $DBFilter->SelectRecord('services', "service_id='$id'");
				$CFG->template = "facilityandServices/view.php";
				// echo '<pre>';print_r($Row);print_r($faults);
			break;	
       
        case 'list':
            if ($frmdata['search'])
            {
                $frmdata['pageNumber'] = 1;
            }
		
            PaginationWork();
            $totalCount = 0;
            $Row  = getfacilityandServices($totalCount);
			$CFG->template = "facilityandServices/list.php";
            break;
            
    }
    
    function getfacilityandServices(&$totalCount)
    {
        global $DB, $frmdata,$DBFilter;
		
        $query = "Select * from services as s left join company_detail as cd on s.company_id = cd.company_id 
		where s.is_deleted='N'";
		
		if($_SESSION['company_id'])
		{
			$query.= " and s.company_id ='".$_SESSION['company_id']."'";
		
		}
		//echo $frmdata['keyword'];
		// if(isset($frmdata['search']))
		// {
        	if ($frmdata['keyword'] != '')
        	{ 
				$frmdata['keyword'] = trim($frmdata['keyword']);
				$where = " and cd.company_name like  '%".$frmdata['keyword']."%'
				or s.service_name like '%".$frmdata['keyword']."%'";
			}
			
		//}
        if (isset($frmdata['orderby']) && $frmdata['orderby'] != '')
        {
            $order .= " order by s." . $frmdata['orderby'];
        }
        else
        {
            $order .= " order by s.service_name desc ";
        }
		$groupby = ' group by s.service_id';
       $query = $query.$where.$groupby.$order; 
	
		// echo  $query;
		
        $result = $DBFilter->RunSelectQueryWithPagination($query,$totalCount);
        return $result;
    }
  
  ##### function to fetch Faults ##### Created By : Neha Pareek ###
	function getfacilityandServicesFaults(&$totalCount)
    {
        global $DB, $frmdata,$DBFilter;
		
        $query = "Select * from faults 
		where is_deleted='N'";
		
		if (isset($frmdata['orderby']) && $frmdata['orderby'] != '')
        {
            $order .= " order by " . $frmdata['orderby'];
        }
        else
        {
            $order .= " order by fault_name desc ";
        }
		//$groupby = ' group by service_id';
       $query = $query.$where.$order; 
	
		// echo  $query;
		
        $result = $DBFilter->RunSelectQueryWithPagination($query,$totalCount);
        return $result;
    }
  
  
    include_once(CURRENTTEMP . "/index.php");
?>
