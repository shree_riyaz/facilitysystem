<?php
//$rows = $DBFilter->SelectRecord('feedback',"user_id='7'");
//print_r($rows);
//exit;
//print_r($_SESSION);
//include('class.encryptdecrypt');
if (isset($frmdata['clearsearch'])) {
    echo $frmdata['clearsearch'];
    unset($frmdata);
    unset($_SESSION['pageNumber']);
    $CFG->template = "rigs/list.php";
}

if (isset($frmdata['add_setting'])) {

//    echo '<pre>'; print_r($frmdata); exit;

    $err = '';
    if (trim($frmdata['form_heading']) == '') {
        $err .= "Please enter in heading title.<br>";
        $frmdata['form_heading'] = '';
    }
    if (trim($frmdata['form_footer']) == '') {
            $err .= "Please enter in footer title .<br>";
            $frmdata['form_footer'] = '';
        }

    if (trim($frmdata['capture_extra_comments']) == '') {
        $err .= "Please select option for capture extra comment.<br>";
        $frmdata['capture_extra_comments'] = '';
    }
    if (trim($frmdata['comment_section_title']) == '') {
        $err .= "Please select comment section title.<br>";
        $frmdata['comment_section_title'] = '';
    }

    /*if (trim($frmdata['attach_survey']) == '') {
        $err .= "Please select option for attach survey.<br>";
        $frmdata['attach_survey'] = '';
    }
    if (trim($frmdata['attach_survey']) == 'Y') {
        if (trim($frmdata['survey_position']) == '') {
            $err .= "Please select option for survey position.<br>";
            $frmdata['survey_position'] = '';
        }
    }
    if (trim($frmdata['layout']) == '') {
        $err .= "Please select option for layout.<br>";
        $frmdata['layout'] = '';
    }
    if (trim($frmdata['assign_users']) == '') {
        $err .= "Please select option for assign users.<br>";
        $frmdata['assign_users'] = '';
    }*/
    if (trim($frmdata['capture_percent_chart']) == '') {
        $err .= "Please select option for capture percentage chart.<br>";
        $frmdata['capture_percent_chart'] = '';
    }
    if (trim($frmdata['capture_contact_detail']) == '') {
        $err .= "Please select option for capture  contact details.<br>";
        $frmdata['capture_contact_detail'] = '';
    }
    if (trim($frmdata['field_type']) == '') {
        $err .= "Please select Option Symbol Type field.<br>";
        $frmdata['field_type'] = '';
    }
    /*if (trim($frmdata['total_option']) == '') {
        $err .= "Please select Number of questions field.<br>";
        $frmdata['total_option'] = '';
    }*/


    if ($err == '') {
        $company_id = $_SESSION['company_id'];
        if ($frmdata['attach_survey'] == 'N'){
            $attach_survey = 'last';
        }else{
            $attach_survey = $frmdata['survey_position'];
        }
        $data = array('form_heading' => $frmdata['form_heading'], 'form_footer' => $frmdata['form_footer'],'capture_extra_comments' => $frmdata['capture_extra_comments'], 'comment_section_title' => $frmdata['comment_section_title']/*,'attach_survey' => $frmdata['attach_survey'], 'survey_position' => $attach_survey,'layout' => $frmdata['layout'], 'assign_users' => $frmdata['assign_users']*/,'capture_percent_chart' => $frmdata['capture_percent_chart'], 'capture_contact_detail' => $frmdata['capture_contact_detail'],'company_id' => $company_id,'field_type' => $frmdata['field_type']/*,'total_option' => $frmdata['total_option']*/);
//        echo '<pre>'; print_r($data); exit;

        if ($DBFilter->InsertRecord('feedback_form_settings', $data)) {

            $_SESSION['success'] = "Setting added successfully.";
            Redirect(CreateURL('index.php', 'mod=setting'));
            die();

        } else {
            $_SESSION['error'] = "<font color=red>Oops! Due to some reason record could not be saved.</font>";
        }
    } else {
        $_SESSION['error'] = "<font color=red>" . $err . "</font>";
    }

}

elseif (isset($frmdata['update_feedback_setting'])) {

    $err = '';
//    echo '<pre>'; print_r($frmdata); exit;
    $feedback_setting_id = $_GET['id'];

    if (trim($frmdata['form_heading']) == '') {
        $err .= "Please enter in heading title.<br>";
        $frmdata['form_heading'] = '';
    }
    if (trim($frmdata['form_footer']) == '') {
        $err .= "Please enter in footer title .<br>";
        $frmdata['form_footer'] = '';
    }

    if (trim($frmdata['capture_extra_comments']) == '') {
        $err .= "Please select option for capture extra comment.<br>";
        $frmdata['capture_extra_comments'] = '';
    }
    if (trim($frmdata['comment_section_title']) == '') {
        $err .= "Please select comment section title.<br>";
        $frmdata['comment_section_title'] = '';
    }

    /*if (trim($frmdata['attach_survey']) == '') {
        $err .= "Please select option for attach survey.<br>";
        $frmdata['attach_survey'] = '';
    }
    if (trim($frmdata['attach_survey']) == 'Y') {
        if (trim($frmdata['survey_position']) == '') {
            $err .= "Please select option for survey position.<br>";
            $frmdata['survey_position'] = '';
        }
    }

    if (trim($frmdata['layout']) == '') {
        $err .= "Please select option for layout.<br>";
        $frmdata['layout'] = '';
    }
    if (trim($frmdata['assign_users']) == '') {
        $err .= "Please select option for assign users.<br>";
        $frmdata['assign_users'] = '';
    }*/
    if (trim($frmdata['capture_percent_chart']) == '') {
        $err .= "Please select option for capture percentage chart.<br>";
        $frmdata['capture_percent_chart'] = '';
    }
    if (trim($frmdata['capture_contact_detail']) == '') {
        $err .= "Please select option for capture  contact details.<br>";
        $frmdata['capture_contact_detail'] = '';
    }

    if (trim($frmdata['field_type']) == '') {
        $err .= "Please select Option Symbol Type field.<br>";
        $frmdata['field_type'] = '';
    }
    /*if (trim($frmdata['total_option']) == '') {
        $err .= "Please select Number of questions field.<br>";
        $frmdata['total_option'] = '';
    }*/

    if ($err == '') {
        $company_id = $_SESSION['company_id'];

        if ($frmdata['attach_survey'] == 'N'){
            $survey_position = 'last';
        }else{
            $survey_position = $frmdata['survey_position'];
        }

        $feedback_setting_id = $_GET['id'];
            $data = array('form_heading' => $frmdata['form_heading'], 'form_footer' => $frmdata['form_footer'],'capture_extra_comments' => $frmdata['capture_extra_comments'], 'comment_section_title' => $frmdata['comment_section_title']/*,'attach_survey' => $frmdata['attach_survey'], 'survey_position' => $survey_position ,'layout' => $frmdata['layout'], 'assign_users' => $frmdata['assign_users']*/,'capture_percent_chart' => $frmdata['capture_percent_chart'], 'capture_contact_detail' => $frmdata['capture_contact_detail'],'company_id' => $company_id,'field_type' => $frmdata['field_type']/*,'total_option' => $frmdata['total_option']*/);

//        echo '<pre>'; print_r($data); exit;

        $kk = $DBFilter->UpdateRecord('feedback_form_settings', $data, "id =" . $feedback_setting_id);


                $_SESSION['success'] = "Feedback setting updated successfully.";
                Redirect(CreateURL('index.php', 'mod=setting'));
                die();


        } else {
            $_SESSION['error'] = "<font color=red>" . $err . "</font>";
        }
}
//============================================================================

//Delete Multiple or Other Actions added by : Neha Pareek

//============================================================================
if ($frmdata['actions']) {

    if ($frmdata['chkbox'] == '' && isset($frmdata['actions'])) {
        $_SESSION['error'] = 'Please select atleast one user';
    } else {
        $ids = $frmdata['chkbox'];
        $action = $frmdata['actions'];
        $cond = "user_id=";
        foreach ($ids as $id) {
            # code...
            //echo "<pre>";
            //print_r($DBFilter->SelectRecords('feedback',"user_id='4'"));
            /*$row = $DBFilter->SelectRecords('feedback',"user_id='$id' or assigned_to='$id'");
            if($row)
            {
            // 	print_r($row);
            // 	exit;
                $_SESSION['error'] = 'Please delete user related feedback first.';
                Redirect(CreateURL('index.php', 'mod=feedback'));
                die();
            }
            else
            {*/
            //exit;
            multipleAction($ids, 'users', $action, $cond);
            $_SESSION['success'] = 'Users has been ' . $action . 'd successfully.';
            Redirect(CreateURL('index.php', 'mod=user'));
            die();
            //}
        }

    }
}
//============================================================================

//Delete the User

//============================================================================
elseif ($do == 'del') {
    $data = array('is_deleted' => 'Y', 'is_active' => 'N');
    $related_detail = DeleteRelatedData($id, $_SESSION['usertype'], 'users');
    if ($related_detail == true) {
        $result = $DBFilter->UpdateRecord('users', $data, "user_id ='" . $id . "'");
        //exit;
        $_SESSION['success'] = 'User has been deleted successfully.';

        Redirect(CreateURL('index.php', 'mod=user'));
        die();
    }
}

/* function is used to allowed number of users according to plan */


?>