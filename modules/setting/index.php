<?php 


//$backup =  new Backup;
 include_once('function.php');
 //print_r($_SESSION);

 //$company = $DBFilter->SelectRecords('company_detail');
 //Added By Neha Pareek, Dated : 02 Nov 15 (for all Active Non deleted companies)
 $company = $DBFilter->SelectRecords('company_detail',"is_active= 'Y' and is_deleted= 'N'");
 $roles = $DBFilter->SelectRecords('roles');
 //Added By Neha Pareek, Dated : 02 Nov 15 (for all Active Non deleted plans)
 $role = $DBFilter->SelectRecords('roles',"is_active= 'Y' and is_deleted= 'N'"); 
 $delete_permission = $DBFilter->SelectRecord('rolepermission',"permission_id='4' and role_id='".$_SESSION['role_id']."'");
 if($_SESSION['company_id'])
 {
	$supervisor =  $DBFilter->SelectRecords('users',"role_id='4' and is_active= 'Y' and is_deleted= 'N' and company_id=".$_SESSION['company_id']);
 }
else
{
	//$supervisor =  $DBFilter->SelectRecords('users',"role_id='4');
	//added by Neha Pareek. Dated : 03 Nov 2015
	$supervisor =  $DBFilter->SelectRecords('users',"role_id='4' and is_active='Y' and is_deleted = 'N'");
}
 if($_SESSION['usertype']=='company_admin')
 {
	$roles= $DBFilter->SelectRecords('roles',"role_id in('3','4')");
	$plans = $DBFilter->SelectRecords('company_detail',"company_id=".$_SESSION['company_id']);
	$plan_name = $DBFilter->SelectRecords('plan',"plan_id=".$plans[0][0]->plan_id);
	$planName =  $plan_name[0][0]->plan_name;
 }


    switch ($do)
    {
		case 'add':
            $field_type = get_feedback_setting_detail('field_type',$_SESSION['company_id']);
//            echo '<pre>'; print_r($field_type); exit;

            $company_id = $_SESSION['company_id'];//Added By : Riyaz Khan Dated : 05 Feb 2019

            $CFG->template = "setting/add.php";
            break;

		case 'edit':
            $get_feeback_setting_id = $_GET['id'];
			$Row= $DBFilter->RunSelectQuery("SELECT * from feedback_form_settings as ffs where ffs.id='$get_feeback_setting_id'");
            $CFG->template = "setting/edit.php";
			break;
		
        case 'list':

            if ($frmdata['search'])
            {
                $frmdata['pageNumber'] = 1;
            }
            $get_setting_count= $DBFilter->RunSelectQuery("select COUNT(*) as count from feedback_form_settings where company_id=".$_SESSION['company_id']);
//            echo '<pre>'; print_r($count_record); exit;

            $count_record = current(current($get_setting_count))->count;

            PaginationWork();
            $totalCount= 0;
			//print_r($_SESSION);

            $Row  = get_feedback_setting_list($totalCount);
//            echo '<pre>'; print_r($totalCount);exit;

            $CFG->template = "setting/list.php";
            break;
			

		}
		
    /* function is used to get all user details */
    function get_feedback_setting_list(&$totalCount)
    {
        global $DB, $frmdata,$DBFilter;

		$query = "select * from feedback_form_settings as ffs where company_id=".$_SESSION['company_id'];
        $order = '';
		if (isset($frmdata['orderby']) && $frmdata['orderby'] != '') {
            $order .= " order by " . $frmdata['orderby'];
        } else {
            $order .= " order by id desc";
        }
		
         $query = $query.$order;
//		echo $query;
        $result = $DBFilter->RunSelectQueryWithPagination($query,$totalCount);
		//echo count($result);
//        echo '<pre>'; print_r($result);exit;

        return $result;
    }
    
	/* function is used for showing profile detail using webservice */
	function profileDetail($data)
	{
		$profile_data = array();
		global $DB, $frmdata,$DBFilter;
		$access_key = $DBFilter->SelectRecord('login_detail',"access_key='".$data['access_key']."'");
		if($access_key)
		{
			$user_data = $DBFilter->SelectRecord("users","user_id=".$access_key->user_id);	
			$user_role = $DBFilter->SelectRecord('roles','role_id='.$user_data->role_id);
			$profile_data['user_id'] = $user_data->user_id;
			$profile_data['first_name'] = $user_data->first_name;
			$profile_data['last_name'] = $user_data->last_name;
			$profile_data['user_email'] = $user_data->user_email;
			$profile_data['user_phone_number'] =$user_data->user_phone;
			$profile_data['user_type'] = $user_role->role_name;
			$profile_data['user_profile_image'] = IMAGEURL."uploads/".trim($user_data->profile_image);
		//	$profile_data['user_profile_image'] = IMAGEURL."profile_picture/".trim($user_data->profile_image);
			$profile_data['status'] = 1;
			echo json_encode($profile_data);	
			exit;
		}
		else
		{
			$final_data = array('result'=>'Access key is expired. Please login again','status'=>2);
			echo json_encode($final_data);
			exit; 
		}		
	}
	
 /* function used to update user's details  using webservice */
   function updateUser($data)
    {	//print_r($data);
		// print_r($_FILES); exit;
		 global $DB, $frmdata,$DBFilter;
		 $access_key = $DBFilter->SelectRecord('login_detail',"access_key='".$data['access_key']."'");
		//print_r($access_key);
		 if($access_key)
		 {
			 $updateDate = date('m/d/Y');
			 $user_result = $DBFilter->SelectRecord('users',"user_id='".$access_key->user_id."'");
			 // if detail for login user is not filled for update old info for that blank field will be same as in old
				if($data['first_name']=='')
				$data['first_name'] = $user_result->first_name;
				else  $data['first_name'] = $data['first_name'];
				if($_POST['last_name']=='')
				$data['last_name'] = $user_result->last_name;
				else  $data['last_name'] = $data['last_name'];
				if($data['role_id']=='')
				$data['role_id'] = $user_result->role_id;
				else  $data['role_id'] = $data['role_id'];
				if($data['user_phone']=='')
				$data['user_phone'] = $user_result->user_phone;
				else  $data['user_phone'] = $data['user_phone'];
				//Edited By : Neha Pareek . Dated : 03-02-2016
				if($data['user_email']=='')
				$data['user_email'] = $user_result->user_email;
				else 
				{
					if (trim($data['user_email']) != '')
					{
						if (!filter_var($data['user_email'], FILTER_VALIDATE_EMAIL ) )
						{
							$final_data = array('result'=>'Please provide a valid email address.','status'=>0);
							echo json_encode($final_data);
							exit;
						}
						else
						{
							if(updateEmail(trim($data['user_email']),$user_result->user_id) == 1)
							{
								$final_data = array('result'=>'Email already exists.','status'=>0);
								echo json_encode($final_data);
								exit;
								
							}
							else
							{
								$data['user_email'] = $data['user_email'];
							}
						}
					}
				}

				
				$data['company_id']=$user_result->company_id;
				$data['password']=$user_result->password;
				if($_FILES['profile_image']['name']!='')
				{
					//unlink(ROOT."images/profile_picture/".$user_result->profile_image);
					unlink(ROOT."/images/uploads/".$user_result->profile_image);
					$image_name = $_FILES["profile_image"]["name"];
					$source_image=$_FILES["profile_image"]["tmp_name"];
					$filename= time()."_".trim($image_name);
					$folderpath = ROOT."/images/uploads/".$filename;
					//$folderpath = ROOT."/images/profile_picture/".$filename;
					move_uploaded_file($source_image,$folderpath);
					$data['profile_image'] = $filename;
				}
				else
				{
					$data['profile_image'] = $user_result->profile_image;
					
				}
			//print_r($data); exit;
		    $update_user_profile = $DBFilter->UpdateRecord('users',$data,"user_id=".$access_key->user_id);
		
			 if($update_user_profile ==1)
			{
				$update_user_data = $DBFilter->SelectRecord('users',"user_id=".$access_key->user_id);
				$user_data = array();
				$user_data['first_name'] = $update_user_data->first_name;
				$user_data['last_name'] = $update_user_data->last_name;
				$user_data['user_email'] = $update_user_data->user_email;
				$user_data['phone_number'] =$update_user_data->user_phone;
				$user_data['profile_image'] = IMAGEURL."profile_picture/".trim($update_user_data->profile_image);
				$user_data['status'] = 1;
				echo json_encode($user_data);	
				exit;
			}
			else {
			$final_data = array('result'=>'User updation fail. please check again','status'=>0);
			echo json_encode($final_data);
			exit;}

		 }
		 else
		 {
			$final_data = array('result'=>'Access key is expired. Please login again.','status'=>2);
			echo json_encode($final_data);
			exit;
		 } 
		    
	}
	/* function used to update password using webservice */
	function updateUserpassword($data)
	{
		 global $DB, $frmdata,$DBFilter;
		 $access_key = $DBFilter->SelectRecord('login_detail',"access_key='".$data['access_key']."'");
		
		 if($access_key)
		 {
			
			 if($data['password']!='')
			 {
				 $obj = new passEncDec;
				 $password = $obj->encrypt_password($data['password']);
				// $p = $obj->encrypt_password('demo123');
				// echo $p;
				 $user_data = array('password'=>$password);
				 $update_user_password = $DBFilter->UpdateRecord('users',$user_data,"user_id=".$access_key->user_id);
				 if($update_user_password ==1)
				 {
					$final_data = array('result'=>'Password has been updated successfully','status'=>1);
					echo json_encode($final_data);
					exit;
				 }
				else {
				$final_data = array('result'=>'Password has not been updated. please check again','status'=>0);
				echo json_encode($final_data);
				exit;}
			 }
			 else 
			 	{ 
					$final_data = array('result'=>'Please enter password','status'=>0);
					echo json_encode($final_data);
					exit;
				}
		}
		else
		{
			$final_data = array('result'=>'Access key is expired. Please login again.','status'=>2);
			echo json_encode($final_data);
			exit;
		}
		 
		
	}
	
	/* function is used to show all cleaners to their respective supervisiors */
	function getAllCleaners($data)
	{
		 global $DB, $frmdata,$DBFilter;
		 $access_key = $DBFilter->SelectRecord('login_detail',"access_key='".$data['access_key']."'");
		 $user_data = $DBFilter->SelectRecord('users',"user_id='".$access_key->user_id."'");
		 if($user_data->role_id=='3')
		{
			$final_data = array('result'=>'You are not allowed to access this service','status'=>0);
			echo json_encode($final_data);
			exit;
		}
		 if($access_key)
		 {	
			$cleaner_data = $DBFilter->SelectRecords('users',"is_deleted = 'N' and is_active = 'Y' and assigned_to=".$access_key->user_id);
			
			if($cleaner_data)
			{
				$cleaner_array = array();
				for($i=0;$i<count($cleaner_data[0]);$i++)
				{
					$cleaner_array[$i]['cleaner_id'] = $cleaner_data[0][$i]->user_id;
					$cleaner_array[$i]['cleaner_name'] = $cleaner_data[0][$i]->first_name.' '.$cleaner_data[0][$i]->last_name;
				}
				$final_data = array('result'=>$cleaner_array,'status'=>1);
			echo json_encode($final_data);
			exit;
				
			}
			else
			{
				$final_data = array('result'=>'No Cleaner Found','status'=>0);
				echo json_encode($final_data);
				exit;
			}
		 }
		else
		{
			$final_data = array('result'=>'Access key is expired. Please login again.','status'=>2);
			echo json_encode($final_data);
			exit;
		}
		
		
	}
	
	//Check Email On Update Via Webservice. Created By : Neha Pareek. Dated : 03 Feb 2016
	function updateEmail($email,$user_id)
	{	 	global $DBFilter;
			$id = $user_id; //Get current record's id, which we going to edit
			$users = $DBFilter->SelectRecords("users","user_id NOT LIKE '%{$id}%' and is_deleted='N' and (is_active='Y' or is_active='N')");
			$count_user = count($users[0]);
			$flag = 0;
			for($i = 0; $i < $count_user ; $i++)
			{
				if($users[0][$i]->user_email == trim($email))
				{
					$flag = 1;
				}
			}
			
			if($flag == 1)
			{
				return 1;
			}
			else
			{
				return 0;
			}

	}
		
    include_once(CURRENTTEMP . "/index.php");
?>