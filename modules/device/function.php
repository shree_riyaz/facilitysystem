<?php
$DBFilter = new $DBFilter();
    if (isset($frmdata['clearsearch']))
    {
        unset($frmdata);
        unset($_SESSION['pageNumber']);
        $CFG->template = "rigs/list.php";
    }
    
    if (isset($frmdata['add_device']))
    {
		$err = '';
		//print_r($frmdata);exit;
		if (trim($frmdata['device_name']) == '')
		{
            $err .= "Please enter in device name field.<br>";
			$frmdata['device_name']='';
        }
		else
		{
			if(validatename($frmdata['device_name'])== true)
			{
				$frmdata['device_name']= $frmdata['device_name'];
			}
			else
			{
				$err .= "Only letters and white space allowed in device name field.<br>";
			}
		}
		
		if (trim($frmdata['location_id']) == '')
		{
            $err .= "Please enter in device location field.<br>";
			$frmdata['location_id']='';
        }
		else
		{
			if($frmdata['location_id']!= '')
			{
				$frmdata['location_id']= $frmdata['location_id'];
			}
			else
			{
				$err .= "Only letters and white space allowed in device location field.<br>";
			}
		}
		
		if (trim($frmdata['user_id']) == '')
		{
            $err .= "Please select device owner.<br>";
			$frmdata['user_id']='';
        }
		else
		{
			if(validatename($frmdata['user_id'])== true)
			{
				$frmdata['user_id']= $frmdata['user_id'];
			}
			else
			{
				$err .= "Only letters and white space allowed in device owner name field.<br>";
			}
		}
		if (trim($frmdata['device_description']) == '')
		{
            $err .= "Please enter device description.<br>";
			$frmdata['device_description']='';
        }
		else
		{
			if(validatename($frmdata['device_description'])== true)
			{
				$frmdata['device_description']= $frmdata['device_description'];
			}
			else
			{
				$err .= "Only letters and white space allowed in device description field.<br>";
			}
		}
		
		if ($err == '')
        {
			$nowdate = date("Y-m-d");
			$updatedate = date("Y-m-d H:i:s");
			//print_r($DBFilter->SelectRecord('device_locations',"location_id='".$frmdata['location_id']."'"));
			$frmdata['company_id'] = $_SESSION['company_id']; //Added By Neha Pareek. Dated : 02 Nov 2015
			// print_r($frmdata);
			// exit;
			$data = array('device_name'=>$frmdata['device_name'], 'user_id'=>$frmdata['user_id'],'location_id'=>$frmdata['location_id'],'company_id'=>$frmdata['company_id'],'device_description'=>$frmdata['device_description']);
			//print_r($_SESSION);
			//print_r($data);
			//exit;
			if ($DBFilter->InsertRecord('device', $data))
            {
//				$pid=mysql_insert_id();
				$_SESSION['success'] = "Device added successfully.";
                Redirect(CreateURL('index.php', 'mod=device'));

                die();
            }
			else
            {
				$_SESSION['error'] = "<font color=red>Due to some reasion record could not be saved.</font>";
            }
        }
        else
        {
            $_SESSION['error'] = "<font color=red>" . $err . "</font>";
        }
        
    }
    elseif (isset($frmdata['update'])) // Edit Record
    {
       $err = '';
		if (trim($frmdata['device_name']) == '')
		{
            $err .= "Please enter device name field.<br>";
			$frmdata['device_name']='';
        }
		else
		{
			if(validatename($frmdata['device_name'])== true)
			{
				$frmdata['device_name']= $frmdata['device_name'];
			}
			else
			{
				$err .= "Only letters and white space allowed in device name field.<br>";
			}
		}
		if (trim($frmdata['location_id']) == '')
		{
            $err .= "Please enter device location.<br>";
			$frmdata['location_id']='';
        }
		else
		{
			if($frmdata['location_id']!= '')
			{
				$frmdata['location_id']= $frmdata['location_id'];
			}
			else
			{
				$err .= "Only letters and white space allowed in device location field.<br>";
			}
		}
		if (trim($frmdata['user_id']) == '')
		{
            $err .= "Please enter device owner.<br>";
			$frmdata['user_id']='';
        }
		
		if (trim($frmdata['device_status']) == '')
		{
            $err .= "Please select device status and update.<br>";
			$frmdata['device_status']='';
        }
		
		if (trim($frmdata['device_description']) == '')
		{
            $err .= "Please enter device description.<br>";
			$frmdata['device_description']='';
        }
		else
		{
			if(validatename($frmdata['device_description'])!= '')
			{
				$frmdata['device_description']= $frmdata['device_description'];
			}
			else
			{
				$err .= "Only letters and white space allowed in device description field.<br>";
			}
		}
		//=============
		
		if ($err == '')
        {
			$updatedate = date("Y-m-d H:i:s");
			$loc_name= $frmdata['location_id'];
			//$loc = $DBFilter->SelectRecord('device_locations',"location_id = 39");
			$loc = $DBFilter->SelectRecord('device_locations',"location_name='".$loc_name."'");
			$id = $_GET['id']; //Added By Neha Pareek. Dated : 02 Nov 2015
			$frmdata['company_id'] = $_SESSION['company_id']; //Added By Neha Pareek. Dated : 02 Nov 2015
			
			//print_r($loc);
			//exit;
			/*$data = array('device_name'=>$frmdata['device_name'], 'user_id'=>$frmdata['user_id'],'location_id'=>$loc->location_id,'company_id'=>$frmdata['company_id'],'device_status'=>$frmdata['device_status'],'device_description'=>$frmdata['device_description']);*/
			
			//Added By Neha Pareek. Dated : 02 Nov 2015
			$data = array('device_name'=>$frmdata['device_name'], 'user_id'=>$frmdata['user_id'],'location_id'=>$frmdata['location_id'],'company_id'=>$frmdata['company_id'],'device_status'=>$frmdata['device_status'],'device_description'=>$frmdata['device_description']);
		
		//print_r($_GET['id']);
		// print_r($data );
		// exit;
			   
         $DBFilter->UpdateRecord('device', $data, "device_id ='$id'");
         //print_r($data );exit;
		 $_SESSION['success'] = "Device updated successfully.";
		 Redirect(CreateURL('index.php', 'mod=device'));
		 die();
            
        }
        else
        {
            $_SESSION['error'] = $err;
        }
        
    }

    //============================================================================
        
    //Delete Multiple or Other Actions added by : Neha Pareek
        
    //============================================================================
    if($frmdata['actions'])
	{
		if($frmdata['chkbox']=='' && isset($frmdata['actions']))
		{
			$_SESSION['error'] = 'Please select atleast one device';
		}
		else
		{	
			$total_chk= count($frmdata['chkbox']);// Count Selected Checkboxes
			$ids = $frmdata['chkbox'];
			$action = $frmdata['actions'];
		    $cond = "device_id=";
			/*print_r($ids);
			exit;*/
			$flag = 0;

			if($frmdata['actions']=='Delete' || $frmdata['actions']=='Inactive')
			{	
				 $rows_= $DBFilter->SelectRecords('feedback', "device_id IN(".implode(',', $ids).") and company_id='".$_SESSION['company_id']."' and is_deleted = 'N' or is_active='Y' ");
				//$rows= $DBFilter->SelectRecords('feedback', "device_id in (".implode(',', $ids).") and is_deleted = 'N' and is_active='Y' ");
				//echo "<pre>";print_r($rows);exit;
				if ($rows)
				{
					$_SESSION['error'] = 'Device is in use. Please delete device related feedback first.';
					Redirect(CreateURL('index.php', 'mod=feedback'));
					die();
				}
			}
			
			if($flag == 0)
			{
				//exit;
				multipleAction($ids,'device',$action,$cond);
				$_SESSION['success'] = 'Devices has been '.$action.'d successfully.';
				Redirect(CreateURL('index.php', 'mod=device'));
				die();
			}
		/* echo "<pre>"; //print_r($DBFilter->SelectRecords('feedback', "location_id = '$loc_id'"));
				//print_r($DBFilter->SelectRecords('feedback', "device_id= '$feedback_id'"));
			//echo "<pre>";print_r($DBFilter->SelectRecords('feedback'));
			 //exit;
		//$ids = $frmdata['chkbox'];
		$action = $frmdata['actions'];
		$cond = "device_id=";
		//$row = $DBFilter->SelectRecords('feedback',"device_id ='$id'");
		//print_r($ids);
		//exit;
			$flag = 0;
			for($i = 0; $i < $total_chk; $i++) //Create Array of multiple checkbox id's
			{	
				$chk_id = $frmdata['chkbox'][$i];

				$rows = $DBFilter->SelectRecord('feedback',"device_id ='$chk_id'");
				//print_r($rows);echo '<br/>';
				if($rows)
				{	
					$flag = $flag +1;
				}
			}//exit;
			
				if($flag == 0)
				{
				// echo "hello";
				// 	exit;
					multipleAction($ids,'device',$action,$cond);
					$_SESSION['success'] = 'Devices has been updated successfully.';
					Redirect(CreateURL('index.php', 'mod=device'));
					die();
				}
				else
				{
					$_SESSION['error'] = 'Device is in use. Please delete device related feedback first.';
					//Redirect(CreateURL('index.php', "mod=feedback&do=list&id='$rows->device_id'"));
					Redirect(CreateURL('index.php', 'mod=feedback'));
					die();
				}
			//exit;
			*/

			/*foreach ($ids as $id)
			{
				# code...
				$row = $DBFilter->SelectRecord('feedback',"device_id ='$id'");
				// print_r($row);
				// exit;

				if($row)
				{	//echo "else";
					$_SESSION['error'] = 'Device is in use. Please delete device related feedback first.';
					Redirect(CreateURL('index.php', "mod=feedback&do=list&id='$id'"));
					die();
					//exit;
				}
				else
				{// echo "hello";
				// 	exit;
					multipleAction($id,'device',$action,$cond);
					$_SESSION['success'] = 'Devices has been updated successfully.';
					Redirect(CreateURL('index.php', 'mod=device'));
					die();
				}

			}*/
		
		}
	}

    //============================================================================
        
    //Delete the Device added by : Neha Pareek
        
    //============================================================================
    elseif ($do == 'del')
    {	//echo $id;
    	//$feedbck = $DBFilter->SelectRecords('feedback',"device_id='$id' and is_deleted='N' or is_active='Y'");
		$feedbck = $DBFilter->SelectRecords('feedback',"device_id='".$id."' and is_deleted='N' and company_id =".$_SESSION['company_id']);

        $data = array('is_deleted'=>'Y','is_active'=>'N');
			if($feedbck)
			{
				//echo $feedbck->device_id; exit;
				 $dev = $DBFilter->SelectRecord('device',"device_id='$id'");
				 $_SESSION['error'] = 'Device '. $dev->device_name .' is in use. please delete related feedbacks first.';
				 Redirect(CreateURL('index.php', 'mod=feedback'));
				 die();
			}
			else
			{
				//exit;
				$DBFilter->UpdateRecord('device', $data, "device_id ='$id'");
        
        		$_SESSION['success'] = 'Device has been deleted successfully.';
        
        		Redirect(CreateURL('index.php', 'mod=device'));
        		die();
			}
        
    }

?>