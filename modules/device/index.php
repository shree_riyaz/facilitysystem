<?php 
if($_REQUEST['call']!='API')
{	
	$permission = $_SESSION['permission'];
	$do = (isset($_REQUEST['do']) ? $_REQUEST['do'] : 'list'); 
	$id = (isset($_REQUEST['id']) ? $_REQUEST['id'] : '');
	if($do=='del' && $_SESSION['usertype']=='company_admin')
	 $permission = 'del';
	$userAlowedActions = array($permission);
	//print_r($userAlowedActions);
    if(!in_array($do,$userAlowedActions))	
	{
?> 
	<script type="text/javascript">
	alert('Your are not allowed to perform this action');
		document.location.href="<?php
		echo 'index.php?mod=company&do=view&id='.$_SESSION['company_id']; ?>";
	</script>
   <?php
	   exit;
	}
}
	$company= $DBFilter->SelectRecords('company_detail');
	$admin_company = $DBFilter->SelectRecord('company_detail',"is_deleted='N' and is_active='Y' and company_id=".$_SESSION['company_id']);
	$loc = $DBFilter->SelectRecords('device_locations',"is_deleted='N' and is_active='Y' and  company_id=".$_SESSION['company_id']);
	$users = $DBFilter->SelectRecords('users',"is_deleted='N' and is_active='Y' and role_id = '4' and company_id=".$_SESSION['company_id']);
	$users = $DBFilter->SelectRecords('users',"is_deleted='N' and is_active='Y' and role_id = '4' and company_id=".$_SESSION['company_id']);

	if(empty($users)){
		$users = $DBFilter->SelectRecords('users',"is_deleted='N' and is_active='Y' and role_id = '2' and company_id=".$_SESSION['company_id']);
	}
//	echo "<pre>"; print_r($users); exit;
	include_once('function.php');
    switch ($do)
    {
		case 'logout':
			unset($_SESSION['usertype']);
			$CFG->template = "device/logout.php";			
			break;		
		case 'add':
			$CFG->template = "device/add.php";
            break;
		case 'edit':
			$Row= $DBFilter->SelectRecord('device', "device_id='$id' and is_deleted='N'");
			// print_r($admin_company);exit;
			$CFG->template = "device/edit.php";
            break;
        case 'list':
           if($_REQUEST['call']=='API')
			{
				$result = getAllDevices($frmdata);
			}
		   
		    if ($frmdata['search'])
            {
                $frmdata['pageNumber'] = 1;
            }
            PaginationWork();
            $totalCount= 0;
            $Row  = getdevice($totalCount);
            $CFG->template = "device/list.php";
            break;
            
    }
    
	/* function is used to get device for CMS*/
    function getdevice(&$totalCount)
    {
        global $DB, $frmdata,$DBFilter;


        $query = " SELECT d.*,u.first_name,u.last_name,dl.location_name from device as d left join users as u on d.user_id = u.user_id left join device_locations as dl on d.location_id = dl.location_id";
		$where = " where d.is_deleted = 'N' and (u.role_id = '4' OR u.role_id = '2') and u.is_deleted='N'";
		//$where = " where d.is_deleted = 'N' and d.is_active = 'Y' and u.role_id = '4' and u.is_deleted='N'";
		if (trim($frmdata['keyword']) != '')
        {
			$frmdata['keyword'] = trim($frmdata['keyword']);
		
            $where .= " and (d.device_name like  '%".$frmdata['keyword']."%' or d.device_status like  '%".$frmdata['keyword']."%' or dl.location_name like '%".$frmdata['keyword']."%' or u.first_name like '%".$frmdata['keyword']."%' or concat(u.first_name,' ',u.last_name) like '%".$frmdata['keyword']."%')";
			$_SESSION['keywords'] == 'Y';
        }
		else
		{
			$where.= '';
		}
		
		if($_SESSION['company_id']!='' && $_SESSION['usertype']!='super_admin')
		{
			$company_id = $_SESSION['company_id'];
			if($_SESSION['role_id']=='4')
			{
				$where .= " and d.user_id = '".$_SESSION['user_id']."'";	
				
			}
			$where .=" and d.company_id='$company_id'";
		
		}
		
        if (isset($frmdata['orderby']) && $frmdata['orderby'] != '')
        {
            $order .= " order by " . $frmdata['orderby'];
        }
        else
        {
            $order .= " order by d.device_id desc";
        }
		
         $query = $query.$where.$order; 
//	echo $query; exit;
        $result = $DBFilter->RunSelectQueryWithPagination($query,$totalCount);
        return $result;
    }
    
	function getAllDevices($data)
	{
	   global $DB, $DBFilter;
	   $access_key = $DBFilter->SelectRecord('login_detail',"access_key='".$data['access_key']."'");
	   $user = $DBFilter->SelectRecord('users',"user_id=".$access_key->user_id);
       if($access_key)	  
	   {
		   $Rows= $DBFilter->SelectRecords('device',"is_deleted='N' and is_active='Y' and company_id=".$user->company_id);
		   if($Rows != '')
		   {	
				$devices = array();
				for($i=0;$i<count($Rows[0]);$i++)
				{
					$devices[$i]['device_id']= $Rows[0][$i]->device_id;
					$devices[$i]['device_name']= $Rows[0][$i]->device_name;
				}
				$final_data = array('result'=>$devices,'status'=>1);
				echo json_encode($final_data);
				exit;	
			}
			else
			{
				$final_data = array('result'=>'No Device Found','status'=>0);
				echo json_encode($final_data);
				exit; 
			}
	   }
	   else
		{
			$final_data = array('result'=>'Access key is expired.Please login again.','status'=>2);
			echo json_encode($final_data);
			exit; 
		}
	}
	
    include_once(CURRENTTEMP . "/index.php");
?>