<?php
//$rows = $DBFilter->SelectRecord('feedback',"user_id='7'");
//print_r($rows);
//exit;
//print_r($_SESSION);
//include('class.encryptdecrypt');
if (isset($frmdata['clearsearch'])) {
    echo $frmdata['clearsearch'];
    unset($frmdata);
    unset($_SESSION['pageNumber']);
    $CFG->template = "rigs/list.php";
}

if (isset($frmdata['add_question'])) {

//    echo '<pre>'; print_r($frmdata['question_option_type']); exit;

    $total_question_detail = $DBFilter->RunSelectQuery("SELECT field_type,total_option from feedback_form_settings where company_id=".$_SESSION['company_id']);
    $total_option_in_setting = current(current($total_question_detail))->total_option;
    $err = '';
    if (trim($frmdata['title']) == '') {
        $err .= "Please enter question title.<br>";
        $frmdata['title'] = '';
    }
    if (trim($frmdata['feedback_type']) == '') {
            $err .= "Please enter in feedback type.<br>";
            $frmdata['feedback_type'] = '';
        }

    if (trim($frmdata['capture_issue_detail']) == '') {
        $err .= "Please select option for capture issue detail.<br>";
        $frmdata['capture_issue_detail'] = '';
    }
    if (trim($frmdata['extra_comment']) == '') {
        $err .= "Please select comment title.<br>";
        $frmdata['extra_comment'] = '';
    }

    if (trim($frmdata['capture_fault_detail']) == '') {
        $err .= "Please select option for capture fault detail.<br>";
        $frmdata['capture_fault_detail'] = '';
    }
    sort($frmdata['question_option_type']);
    array_unique($frmdata['question_option_type']);
//    echo '<pre>'; print_r(array_unique($frmdata['question_option_type'])); exit;

    if ($total_option_in_setting == count(array_unique($frmdata['question_option_type']))){
        for($p=0;$p< count(array_unique($frmdata['question_option_type']));$p++) {
            if ($frmdata['question_option_type'][$p] == 0){
                $err .= "Question option field can not be blank.<br>";
                $frmdata['question_option_type'][$p] = '';
            }
        }
    }else{
        $err .= "You have to choose all $total_option_in_setting rating options and should not be repeat any single option .<br>";
    }


//    echo '<pre>'; print_r($frmdata['question_option_type']); exit;

    if ($err == '') {
        $company_id = $_SESSION['company_id'];
        $data = array('title' => $frmdata['title'], 'feedback_type' => $frmdata['feedback_type'],'capture_issue_detail' => $frmdata['capture_issue_detail'], 'extra_comment' => $frmdata['extra_comment'],'capture_fault_detail' => $frmdata['capture_fault_detail'],'company_id' => $company_id);

        if ($q_id = $DBFilter->InsertRecord('feedback_questions', $data)) {
                for($k=0;$k<count($frmdata['question_option_type']);$k++){
                    $data_q_option[$k] = array('question_id' => $q_id,'option_id' => $frmdata['question_option_type'][$k]);
                    $DBFilter->InsertRecord('feedback_question_options', $data_q_option[$k]);
                }
                $k++;

            $_SESSION['success'] = "Question added successfully.";
            Redirect(CreateURL('index.php', 'mod=question'));
            die();

        } else {
            $_SESSION['error'] = "<font color=red>Oops! Due to some reason record could not be saved.</font>";
        }
    } else {
        $_SESSION['error'] = "<font color=red>" . $err . "</font>";
    }

}

elseif (isset($frmdata['update_question'])) {

    $total_question_detail = $DBFilter->RunSelectQuery("SELECT field_type,total_option from feedback_form_settings where company_id=".$_SESSION['company_id']);
    $total_option_in_setting = current(current($total_question_detail))->total_option;
    $err = '';
//    echo '<pre>'; print_r($frmdata); exit;
    $question_id = $_GET['id'];

    if (trim($frmdata['title']) == '') {
        $err .= "Please enter question title.<br>";
        $frmdata['title'] = '';
    }
    if (trim($frmdata['feedback_type']) == '') {
        $err .= "Please enter in feedback type.<br>";
        $frmdata['feedback_type'] = '';
    }

    if (trim($frmdata['capture_issue_detail']) == '') {
        $err .= "Please select option for capture issue detail.<br>";
        $frmdata['capture_issue_detail'] = '';
    }
    if (trim($frmdata['extra_comment']) == '') {
        $err .= "Please select comment title.<br>";
        $frmdata['extra_comment'] = '';
    }

    if (trim($frmdata['capture_fault_detail']) == '') {
        $err .= "Please select option for capture fault detail.<br>";
        $frmdata['capture_fault_detail'] = '';
    }

   /* if ($total_option_in_setting == count($frmdata['question_option_type'])){
        for($p=0;$p<count($frmdata['question_option_type']);$p++) {
            if ($frmdata['question_option_type'][$p] == 0){
                $err .= "Question option field can not be blank.<br>";
                $frmdata['question_option_type'][$p] = '';
            }
        }
    }else{
    $err .= "You have to choose $total_option_in_setting rating options .<br>";
    }*/

    if ($err == '') {
        $company_id = $_SESSION['company_id'];

        $data = array('title' => $frmdata['title'], 'feedback_type' => $frmdata['feedback_type'],'capture_issue_detail' => $frmdata['capture_issue_detail'], 'extra_comment' => $frmdata['extra_comment'],'capture_fault_detail' => $frmdata['capture_fault_detail'],'company_id' => $company_id);

        $q_id =  $DBFilter->UpdateRecord('feedback_questions', $data, "id =" . $question_id);
//            echo '<pre>'; print_r($frmdata['question_option_type']); exit;

        /*for($k=0;$k<count($frmdata['question_option_type']);$k++){
            $data_q_option[$k] = array('question_id' => $question_id,'option_id' => $frmdata['question_option_type'][$k]);
            $DBFilter->UpdateRecord('feedback_question_options', $data_q_option[$k]);
        }
        $k++;*/
                $_SESSION['success'] = "Question updated successfully.";
                Redirect(CreateURL('index.php', 'mod=question'));
                die();


        } else {
            $_SESSION['error'] = "<font color=red>" . $err . "</font>";
        }
}
//============================================================================

//Delete Multiple or Other Actions added by : Neha Pareek

//============================================================================
if ($frmdata['actions']) {

    if ($frmdata['chkbox'] == '' && isset($frmdata['actions'])) {
        $_SESSION['error'] = 'Please select at least one user';
    } else {
        $ids = $frmdata['chkbox'];
        $action = $frmdata['actions'];
        $cond = "user_id=";
        foreach ($ids as $id) {
            # code...
            //echo "<pre>";
            //print_r($DBFilter->SelectRecords('feedback',"user_id='4'"));
            /*$row = $DBFilter->SelectRecords('feedback',"user_id='$id' or assigned_to='$id'");
            if($row)
            {
            // 	print_r($row);
            // 	exit;
                $_SESSION['error'] = 'Please delete user related feedback first.';
                Redirect(CreateURL('index.php', 'mod=feedback'));
                die();
            }
            else
            {*/
            //exit;
            multipleAction($ids, 'users', $action, $cond);
            $_SESSION['success'] = 'Users has been ' . $action . 'd successfully.';
            Redirect(CreateURL('index.php', 'mod=user'));
            die();
            //}
        }

    }
}
//============================================================================

//Delete the User

//============================================================================
elseif ($do == 'del') {
    $data = array('is_deleted' => 'Y', 'is_active' => 'N');
    $q_uid_dlt =  $_GET['id'];
//    $related_detail = DeleteRelatedData($id, $_SESSION['usertype'], 'users');
        echo $q_uid_dlt;
        $result = $DBFilter->UpdateRecord('feedback_questions', $data, "id ='" . $q_uid_dlt . "'");
        //exit;
        $_SESSION['success'] = 'Question has been deleted successfully.';

        Redirect(CreateURL('index.php', 'mod=question'));
        die();

}

/* function is used to allowed number of users according to plan */


?>