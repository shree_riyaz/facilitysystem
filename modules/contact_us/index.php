<?php


if($_REQUEST['call']=='API' && $_REQUEST['do'] == 'contact')
{
    $permission = 'contact';
}else
{
    $permission = $_SESSION['permission'];
}
$do = (isset($_REQUEST['do']) ? $_REQUEST['do'] : 'list');


$id = (isset($_REQUEST['id']) ? $_REQUEST['id'] : '');

//print_r($do); exit;

include_once('function.php');

//echo "<pre>";
//print_r($services_res);
//print_r($faults_res);

// print_r($_SESSION);

switch ($do)
{
    case 'contact12':
        unset($_SESSION['usertype']);
        $CFG->template = "feedback/logout.php";
        break;

    case 'list':
        if ($frmdata['search'])
        {
            $frmdata['pageNumber'] = 1;
        }

        PaginationWork();
        $totalCount= 0;
        $Row = getContactList($totalCount);
//        echo "<pre>"; print_r($Row);
        $CFG->template = "contact_us/list.php";

        break;

    case 'contact':
        if($_REQUEST['call']=='API' && $_REQUEST['do']=='contact'&& $_REQUEST['mod']=='contact_us')
        {

            try{

            $result = addContactInfo($frmdata);
            if($result)
            {
                $final_data = array("message"=>"Your query added successfully","status"=>1);
                echo json_encode($final_data);
                exit;
            }
            else

                $final_data = array("message"=>"Query not added.try again","status"=>0);
            echo json_encode($final_data);
            exit;
            }
            catch (Exception $e){
                CreateLog($e->getMessage());
            }
        }

        break;
}

#### Function is used to show all feedbacks

function addContactInfo($data)
{
    date_default_timezone_set('Asia/Kolkata');
    global $DB, $frmdata, $DBFilter;

    $session_id = $data['session_id'];
    $email = $data['email'];
    $phone_number = $data['contact_number'];

    $get_feedback = $DBFilter->RunSelectQuery("SELECT feedback_id FROM feedback where is_active = 'Y' and is_deleted = 'N' and session ='" . $session_id . "'");
    $get_feedback = current($get_feedback);
//    echo "<pre>"; print_r($get_feedback); exit;

    if (count($get_feedback) > 0) {

        foreach ($get_feedback as $get_feedback_key => $get_feedback_value) {

            $feedback_id = $get_feedback_value->feedback_id;

            $querys = "update feedback set email= '" . $email . "', phone_number= '" . $phone_number . "' where feedback_id=" .$feedback_id;
            $DBFilter->RunSelectQuery($querys);
        }
        $final_data = array("result" => 'Contact submitted successfully.', "status" => 1);
        echo json_encode($final_data);
        exit;
    }else{
        $final_data = array("result" => 'Oops! It seems this session ID is no longer now.', "status" => 0);
        echo json_encode($final_data);
        exit;
    }
}

/* function for updating task status using webservice*/
//include_once(CURRENTTEMP . "/index.php");

function getContactList(&$totalCount)
{
    global $DB, $frmdata,$DBFilter;
        $order = '';


    $query = "SELECT * FROM contact_us as cu ";
//    $query = $DBFilter->RunSelectQuery("SELECT * FROM contact_us cu");
//    $userss = $DBFilter->RunSelectQuery("SELECT * FROM contact_us where user_info !='' ");


    $where = "where company_id = ".$_SESSION['company_id'];
//        echo"<pre>";print_r($where);exit;

    if (trim($frmdata['keyword']) != '')
    {
        $frmdata['keyword'] = trim($frmdata['keyword']);

        $where .= " and (cu.email like  '%".$frmdata['keyword']."%' or cu.contact_number like  '%".$frmdata['keyword']."%' or dl.location_name like '%".$frmdata['keyword']."%')";
        $_SESSION['keywords'] ='Y';
    }
    else
    {
        $where.= '';
    }

    if (isset($frmdata['orderby']) && $frmdata['orderby'] != '')
    {
        $order.= " order by " . $frmdata['orderby'];
    }
    else
    {
        $order.= " order by cu.id desc ";
    }


    $query = $query.$where.$order;
//    $query = $query.$where;
//    echo '<pre>'; print_r($query); exit;

    $result = $DBFilter->RunSelectQueryWithPagination($query,$totalCount);
    return $result;

}
?>