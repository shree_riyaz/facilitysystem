<?php
/*############################################################################################################################
	Name : Richa verma
	Date : 15-06-2011
//##########################################################################################################################*/
try{
require_once ("xajax.inc.php");
if(file_exists("serverinc.php"))
	$xajax = new xajax("serverinc.php");
else
	$xajax = new xajax("../serverinc.php");

//this is function for geting room for a particular category
$xajax->registerFunction("showgrade");
$xajax->registerFunction("showoption");
$xajax->registerFunction("showtotal");
$xajax->registerFunction("getquestion");
$xajax->registerFunction("getquestionedit");
$xajax->registerFunction("showtotalmarks");
$xajax->registerFunction("getcustomquestion");
$xajax->registerFunction("getcustomquestionedit");
$xajax->registerFunction("customMarks");
$xajax->registerFunction("gettotalmarks");
$xajax->registerFunction("deletePermission");
$xajax->registerFunction("getExamTypeIdByExamId");
$xajax->registerFunction("getExamSubject");
$xajax->registerFunction("getSubjectQuestion_intell");
$xajax->registerFunction("getSubjectQuestion");
$xajax->registerFunction("getCandidateExamDetail");
$xajax->registerFunction("getSubjectMarksTable");
$xajax->registerFunction("makeTestListByExamId");
$xajax->registerFunction("isExamConducted");
$xajax->registerFunction("resetTotalMarks");
$xajax->registerFunction("checkMaxMarks");
$xajax->registerFunction("enableTest");
$xajax->registerFunction("archiveTest");
$xajax->registerFunction("loadsubequp");
$xajax->registerFunction("loadsubunit");
$xajax->registerFunction("loadsubunitupgrading");
$xajax->registerFunction("loadsubunitdone");
$xajax->registerFunction("loadsubunitupgradingdone");
$xajax->registerFunction("loadunit");
$xajax->registerFunction("loadbrigade");
$xajax->registerFunction("loaddivision");
$xajax->registerFunction("loadcommand");
$xajax->registerFunction("loadcorps");
$xajax->registerFunction("loadsubcat");
$xajax->registerFunction("uploadTempFile");
$xajax->registerFunction("showYear");
$xajax->registerFunction("unsetSession");
$xajax->registerFunction("showBrigade");
$xajax->registerFunction("showCommand");
$xajax->registerFunction("showDivision");
$xajax->registerFunction("testcaptchaWork");
$xajax->registerFunction("loadExamSubcategory"); 
$xajax->registerFunction("hideNotification");
$xajax->registerFunction("enable_candidate");
$xajax->registerFunction("changeMaxMarksByPaper");
$xajax->registerFunction("getExamPaper");
$xajax->registerFunction("saveQuestionTypeCount");
$xajax->registerFunction("loadCourse");
$xajax->registerFunction("showgradedone");
$xajax->registerFunction("showgradeupgradingdone");
$xajax->registerFunction("TdnUpgrading");
$xajax->registerFunction("getTradeQuestion");
}
catch (Exception $e)
{
	echo "catch ".$e->getMessage();exit;
}
?>