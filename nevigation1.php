<?php 
include_once("lib/language.php");
include_once("lib/class.authorise.php");
include_once("lib/dbfilter.php");
$language = new Language();
$lang = $language->english('eng');
$auth = new Authorise();
$DB = new DBFilter();
$user_id = $_SESSION['user_id'];
$module_name = $auth->getOneAccessibleModule($user_id);
$user_name = $DB->SelectRecord('users',"user_id='$user_id'");

?>
<div class="bs-example">
<br/><br/><br/>
 <ul class="nav nav-tabs">
	<?php 
			for($i=0;$i<count($module_name[0]);$i++)
			{ 
				$module = $module_name[0][$i]->module_name;
					
				if($module=='device' )
				{
			?>
       			<li class="dropdown" <?php if($_REQUEST['mod']==$module){ ?> class ="active" <?php } ?> >
			 	 <a class="dropdown-toggle" data-toggle="dropdown" href="#"><?php echo ucfirst($module)."s";?> <span class="caret"></span></a>
			 	 <ul class="dropdown-menu">
				<li style="color: #3969ab;"><a href="<?php print CreateURL('index.php','mod=device');?>"><?php echo $lang['Devices']?></a></li>
				<li  style="color: #3969ab;"><a href="<?php print CreateURL('index.php','mod=device_locations');?>"><?php echo $lang['Device Locations']?></a></li></ul>
	<?php        }
				else if($module =='faults')
				{
				?>
                <li <?php if($_REQUEST['mod']==$module){ ?> class ="active" <?php } ?>>
				<a href="<?php print CreateURL('index.php','mod=faults&do=view&id='.$service->service_id );?>"><?php echo $lang['Facility & Faults']?></a>			
			    </li>	
                <?php
					
				}
			 else if($module!='device_locations') { //echo $singular = substr($module, -1);
			 	?>
    			
			<li <?php if($_REQUEST['mod']==$module){ ?> class ="active" <?php } ?>>
				<a href="<?php print CreateURL('index.php','mod='.$module);?>">
				<?php echo ucfirst($module)."s"; 
					//print_r($module); 
				
				?></a>			
			</li>
		<?php 
			}	
			
		}
			
		?>
			<li>
				<a href="<?php print CreateURL('index.php','mod=login&do=logout'); ?>"><?php echo $lang['Logout']?></a>			
			</li>
           <li style="float:right;"><?php echo 'Welcome '.$user_name->first_name;?> <span class="glyphicon glyphicon-user"></span></li>
		
		</ul>
        
  <div align="right"></div>
	</div> 